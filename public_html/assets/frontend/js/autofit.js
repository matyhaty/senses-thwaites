/*
* MIT License
*
* Copyright (c) 2011 Francisco M.S. Ferreira
* http://www.codescream.com
* https://github.com/fmsf/jquery-autoHtmlTextFit
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*
*/

(function ( $ ) {
    $.fn.autoFit = function(){
        
        return this.each(function(){
            var $this = $(this),                
                w = $this.width(),
                h = $this.height(),
                font = $this.css("font"),
                fontSize = $this.css("font-size"),
                id1 = "jq__sizeadjuster__",
                id2 = "jq__sizeadjuster__inner",
                $div = $('<div id="'+id1+'"><div id="'+id2+'">'+$this.html()+'</div></div>').css({
                    "position": "absolute",
                    "visibility": "hidden",
                    "width": (w!==undefined ? w : "auto"),
                    "height": (h!==undefined ? h : "auto"),
                    "font" : font
                });                
                $('body').append($div);
                
                fontSize = fontSize.substring(0,fontSize.indexOf("px"));
                
                while(fontSize>0){
                    $div.css("font-size", fontSize+"px");                     
                    if($("#"+id2).height()<=h && $("#"+id2).width()<=w){
                        break; // retain font height
                    }
                    fontSize--;
                }            
                $("#"+id1).remove();
                if(fontSize>0){
                    $this.css("font-size", fontSize+"px");
                }
        });
        
    };

})(jQuery);