var custom =
{
	removeListItem : function (html,ob){
		$('body').append(html);
	} ,
	hideBusy : function(busySel,object){
		this.throb.stop();
		//stop the throbber 
		$('canvas',object).remove();
		//remove the canvas item
    },
	showBusy : function( busySelector , object ){
		
    	// remove old throbber for clickers
    	$('canvas',object).remove();
		var id = $(object).attr('id');
    	
		this.throb = new Throbber({
		    size: 32,
		    fade: 1000, // fade duration, will also be applied to the fallback gif
		    fallback: 'ajax-loader.gif',
		    rotationspeed: 0,
		    lines: 14,
		    strokewidth: 1.8,
		    alpha: 0.4 // this will also be applied to the gif
		}).appendTo( document.getElementById( id ) ).start();
	} ,
    init : function()
    {
		
		$("select").chosen({no_results_text: "No results matched"}); //
		
		$('#header-search-btn').toggle(
				function(){
					$('~ input',this).stop().show(500);
				}, 
				function() {
					$('~ input',this).stop().hide(500);
				}
			);
			
		$('.header_auth').click(function() {
			$('#header_auth_reg').stop(true, true).slideUp();
			$('#header_auth_log').stop(true, true).slideToggle();
			return false;
		});	
		
		$('#header-join-btn').click(function() {
			$('#header_auth_log').stop(true, true).slideUp();
			$('#header_auth_reg').stop(true, true).slideToggle();
			return false;
		});	
		
		if( $('#upload_button').length>0 ){
			this.ajax_multi();
		}
		
    } 
	,
    ajax_multi : function()
    {
		
		var upload_state	= $('#upload_state');
		var interval		= null;
		return new AjaxUpload('upload_button', {
			  action: document.URL ,
			  name: 'file1',
			  data: { 'up' :1
			  },
			  autoSubmit: true,
			  responseType: false,
			  onChange: function(file, extension){},
			  onSubmit: function(file, extension) {
				  upload_state.text('Uploading');
				  this.disable();
				  interval = window.setInterval(function(){
				  var text = upload_state.text();
				  if (text.length < 13){
					  upload_state.text(text + '.');
				  } else {
					  upload_state.text('Uploading');
				  }
				  }, 200);				  
				  
				  
			  },
			  onComplete: function(file, response) {
				  
				  upload_state.text('Done!');
				  window.clearInterval(interval);
				  this.enable();
				  $('<li></li>').appendTo('#upload_state').text(file); 				  
				  
			  }
			});
    } , 
    ajax_wall : function (html,ob)
    {
    	$(html).insertAfter('#wall_con');
	} , 
    ajax_comment: function (html,ob)
    {
    	var commentID	= $(ob).attr('id').replace(/[^0-9]/g,'');
    	var appendSel	= '#wall_comment_box_' + commentID + ' .comment_body:last';
    	$( html ).insertAfter( appendSel );
	} 
    


}
$(document).ready(function(){
	custom.init();
});

