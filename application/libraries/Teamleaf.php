<?php

/*
 * Teamleaf
 *
 * Main conversion lib for Teamleaf 2
 *
 * @package		Teamleaf
 * @author		Matt Hiscock
 * @version		1.0.1
 * @license		Copyright (c) s3d designs
 */

class Teamleaf
{

	function create_db_test($majortable, $has_one, $has_many)
	{
		echo '<hr>';
		echo $majortable . ' <br/>';
		echo print_r($has_one, true) . ' <br/>';
		echo print_r($has_many, true) . ' <br/>';
	}

	function create_table($majortable, $table)
	{
		$CI = &get_instance();
		$CI -> load -> dbforge();

		$newtable = array();
		$newtable[0] = $majortable;
		$newtable[1] = $table . 's';
		sort($newtable);

		//print_r($this->has_one);

		$newTableName = $newtable[0] . '_' . $newtable[1];

		$table_non_plural = substr_replace($majortable, "", -1);

		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 6,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'' . $table_non_plural . '_id' => array(
				'type' => 'INT',
				'constraint' => 6,
			),
			'' . $table . '_id' => array(
				'type' => 'INT',
				'constraint' => 6,
			)
		);

		$CI -> dbforge -> add_field($fields);
		$CI -> dbforge -> add_key('id', TRUE);
		$CI -> dbforge -> create_table($newTableName, TRUE);

		echo '<div style="padding:5px; padding-top:20px; width:600px; border-bottom: 1px solid #ccc">' . $newTableName . ' Table Created: </div>';
		echo '<div style="padding:5px; width:600px; border-bottom: 1px dashed #ccc">';

		foreach ($fields as $field => $vals)
		{
			echo 'Field: ' . $field . ' <br/>';
		}
		echo '</div>';
	}

	function create_db($majortable, $has_one, $has_many)
	{
		$CI = &get_instance();
		$CI -> load -> dbforge();
		foreach ($has_one as $table => $vals)
		{
			$this -> create_table($majortable, $table);
		}

		foreach ($has_many as $table => $vals)
		{
			$this -> create_table($majortable, $table);
		}
	}

	function shortDate($date, $return = true)
	{
		if (!$return)
		{
			echo date("d M, Y", strtotime($date));
		}
		else
		{
			return date("d M, Y", strtotime($date));
		}
	}

	function date($date, $return = true)
	{
		if (!$return)
		{
			echo date("d M, Y", strtotime($date));
		}
		else
		{
			return date("d M, Y", strtotime($date));
		}
	}

	function dateTime($date, $return = true)
	{
		if (!$return)
		{
			echo date("d M, Y - H:i", strtotime($date));
		}
		else
		{
			return date("d M, Y - H:i", strtotime($date));
		}
	}

	function dateTimeRange($dateFrom, $dateTo)
	{
		if (date("d M, Y", strtotime($dateFrom)) == date("d M, Y", strtotime($dateTo)))
		{
			return date("d M, Y - H:i", strtotime($dateFrom)) . ' until ' . date("H:i", strtotime($dateTo));
		}
		else
		{
			return date("d M, Y - H:i", strtotime($dateFrom)) . ' until ' . date("d M, Y - H:i", strtotime($dateTo));
		}
	}

	function timeRange($dateFrom, $dateTo)
	{
		if (date("d M, Y", strtotime($dateFrom)) == date("d M, Y", strtotime($dateTo)))
		{
			return date("H:i", strtotime($dateFrom)) . ' until ' . date("H:i", strtotime($dateTo));
		}
		else
		{
			return date("d M, Y - H:i", strtotime($dateFrom)) . ' until ' . date("d M, Y - H:i", strtotime($dateTo));
		}
	}

	function dateTimeRangeDifference($dateFrom, $dateTo)
	{
		$date_a = new DateTime($dateFrom);
		$date_b = new DateTime($dateTo);

		$interval = $date_a -> diff($date_b);
		$rt = '';

		$days = $interval -> format('%d');
		if ($days)
		{
			$rt .= $interval -> format('%d day');
			if ($days > 1)
			{
				$rt .= 's, ';
			}
			else
			{
				$rt .= ', ';
			}
		}
		$hours = $interval -> format('%h');
		if ($hours)
		{
			$rt .= $interval -> format('%h hour');
			if ($hours > 1)
			{
				$rt .= 's ';
			}
			else
			{
				$rt .= ', ';
			}
		}

		$mins = $interval -> format('%i');
		if ($mins)
		{
			$rt .= 'and ';
			$rt .= $interval -> format('%i minute');
			if ($mins > 1)
			{
				$rt .= 's.';
			}
			else
			{
				$rt .= '. ';
			}
		}

		return $rt;

	}

	function workingCost($dateFrom, $dateTo, $perhourcost)
	{
		$d1 = strtotime($dateFrom);
		$d2 = strtotime($dateTo);
		$hours = floor(($d2 - $d1) / 3600);
		return $hours * $perhourcost;
	}

	function checkPath($path)
	{
		// path is from the UPLOADS define onwards only

		$folders = explode('/', $path);
		//print_r($folders);
		for ($x = 0; $x < count($folders); $x++)
		{
			// Build path
			$path = '';
			$count = 0;
			while ($count <= $x)
			{
				if ($count)
				{
					$path .= '/';
				}
				$path .= $folders[$count];
				$count++;
			}
			if (!is_dir(UPLOAD_PATH . $path))
			{
				mkdir(UPLOAD_PATH . $path, 0777);
				chmod(UPLOAD_PATH . $path, 0777);
				//echo "The directory " . $path . " was successfully created.";
			}
			else
			{
				//echo "The directory " . $path . " existed.";
			}
		}
	}

	function filelink($file)
	{
		$extension = end(explode('.', $file -> file_name));

		$myPath = explode('uploads/', $file -> full_path);

		$markup = anchor_popup(UPLOAD_URL . $myPath[1], '<img class="tl-file" src="' . IMAGE_URL . 'icons/files/' . $extension . '.png" />');
		return $markup;
	}

	function updateDepotAssetRelationships($id = null)
	{
		// we must relate the asset to the depot(s) when they are under ownership
		$today = date("Y-m-d H:i:s");
		$depots = $this -> getObject('depot', null, null, false, array('state' => 'Active'), '', '') -> dm_object;

		if (!$id)
		{
			$assets = $this -> getObject('asset', null, null, false, array('state' => 'Active'), '', '') -> dm_object;
		}
		else
		{
			$assets = $this -> getObject('asset', $id, null, false, array('state' => 'Active'), '', '') -> dm_object;
		}

		foreach ($assets as $asset)
		{
			// clear all relationships for this asset and depot
			$asset -> delete($depots -> all);

			$assetownership = $this -> getObject('assetownership', $asset -> id, null, false, array(
				'start >' => $today,
				'end < ' => $today,
				'state' => 'Active'
			), '', '') -> dm_object;

			if ($assetownership -> result_count())
			{
				foreach ($assetownership as $ao)
				{
					$depot = $this -> getObject('depot', $ao -> depot, null, false, array('state' => 'Active'), '', '') -> dm_object;
					$asset -> save($depot);
				}
			}

			//Tester to create cache
			// get all depots related to this asset. (e.g. current owners.)
			$tdepot = $this -> getObject('assets', null, $asset, false, array('state' => 'Active'), '', '') -> dm_object;

		}
	}

	function money($amount)
	{
		return '&pound;' . $amount;
	}

	function getContrastColor($color)
	{
		return (hexdec($color) > 0xffffff / 2) ? '000000' : 'ffffff';
	}

	function negativeColor($color)
	{
		//get red, green and blue
		$r = substr($color, 0, 2);
		$g = substr($color, 2, 2);
		$b = substr($color, 4, 2);

		//revert them, they are decimal now
		$r = 0xff - hexdec($r);
		$g = 0xff - hexdec($g);
		$b = 0xff - hexdec($b);

		//now convert them to hex and return.
		return dechex($r) . dechex($g) . dechex($b);
	}

	/**
	 * Returns a lowercase string with keywords ordered by occurance in content seperated with comma's
	 * Use KeywordsGenerator::generateKeywords($string);
	 *
	 * @Author Martijn van Nieuwenhoven
	 * @Email info@axyrmedia.nl
	 */

	/**
	 * Extract Keywords
	 * Returns a lowercase string with keywords ordered by occurance in content seperated with comma's
	 * @var $string
	 * @var $min_word_char
	 * @var $keyword_amount
	 * @var $exclude_words
	 */

	function generateKeywords($string = '', $min_word_char = 4, $keyword_amount = 25)
	{
		$stopwords = file(base_url() . 'assets/stopwords.txt');
		$exclude_words = implode(', ', array_slice($stopwords, 0));
		return $this -> calculateKeywords($string, $min_word_char, $keyword_amount, $exclude_words);
	}

	function calculateKeywords($string = '', $min_word_char = 2, $keyword_amount = 1, $exclude_words = 'the,and, there, take, during, combined, interesting, from, known, looking')
	{

		$exclude_words = explode(",", $exclude_words);
		//add space before br tags so words aren't concatenated when tags are stripped
		$string = preg_replace('/\<br(\s*)?\/?\>/i', " ", $string);
		// get rid off the htmltags
		$string = html_entity_decode(strip_tags($string), ENT_NOQUOTES, 'UTF-8');

		// count all words with str_word_count_utf8
		$initial_words_array = $this -> str_word_count_utf8($string, 1);
		$total_words = sizeof($initial_words_array);

		$new_string = $string;

		//convert to lower case
		$new_string = mb_convert_case($new_string, MB_CASE_LOWER, "UTF-8");

		// strip excluded words
		foreach ($exclude_words as $filter_word)
		{
			$new_string = preg_replace("/\b" . $filter_word . "\b/i", "", $new_string);
		}

		// calculate words again without the excluded words using str_word_count_utf8
		$words_array = $this -> str_word_count_utf8($new_string, 1);
		$words_array = array_filter($words_array, create_function('$var', 'return (strlen($var) >= ' . $min_word_char . ');'));

		//print_r($words_array);

		$popularity = array_count_values($words_array);
		$unique_words_array = array_unique($words_array);

		//echo '<hr>Popularity<hr>';
		//print_r($popularity);

		array_multisort($popularity, $unique_words_array);

		//echo '<hr>$unique_words_array<hr>';
		//print_r($unique_words_array);
		// create density array

		//echo '<hr>Popularity Sorted<hr>';
		//print_r($popularity);

		$popularity = array_reverse($popularity);

		$keys = array();
		foreach ($popularity as $key => $value)
		{
			$keys[] = $key;
		}
		// glue keywords to string seperated by comma, maximum 15 words
		$keystring = implode(', ', array_slice($keys, 0, $keyword_amount));

		// return the keywords
		return $keystring;
	}

	/**
	 * Sort array by count value
	 */
	function cmp($a, $b)
	{
		return ($a['count'] > $b['count']) ? +1 : -1;
	}

	/** Word count for UTF8
	 /* Found in: http://www.php.net/%20str_word_count#85592
	 /* The original mask contained the apostrophe, not good for Meta keywords:
	 /* "/\p{L}[\p{L}\p{Mn}\p{Pd}'\x{2019}..."
	 */
	function str_word_count_utf8($string, $format = 0)
	{
		switch ($format)
		{
			case 1 :
				preg_match_all("/\p{L}[\p{L}\p{Mn}\p{Pd}]*/u", $string, $matches);
				return $matches[0];
			case 2 :
				preg_match_all("/\p{L}[\p{L}\p{Mn}\p{Pd}]*/u", $string, $matches, PREG_OFFSET_CAPTURE);
				$result = array();
				foreach ($matches[0] as $match)
				{
					$result[$match[1]] = $match[0];
				}
				return $result;
		}
		return preg_match_all("/\p{L}[\p{L}\p{Mn}\p{Pd}]*/u", $string, $matches);
	}

	public function slugify($text)
	{
		// replace non letter or digits by -
		$text = preg_replace('~[^\\pL\d]+~u', '-', $text);

		// trim
		$text = trim($text, '-');

		// lowercase
		$text = strtolower($text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		if (empty($text))
		{
			return 'n-a';
		}

		return $text;
	}

	function isActive($me, $currentURL = '')
	{
		$CI = &get_instance();
		if ($currentURL == '')
		{
			$currentURL = $CI -> uri -> uri_string();
		}

		if ($currentURL == $me)
		{
			return 'active';
		}
		else
		{
			return '';
		}

	}

	function link_subdestination($dmobject, $linkedItem, $sitemap = 0)
	{
		$md = new Destination($dmobject -> parentdestination);

		if ($sitemap)
		{
			return base_url() . 'destinations/' . $this -> slugify($md -> title) . '/' . $this -> slugify($dmobject -> title);
		}
		else
		{
			return anchor('destinations/' . $this -> slugify($md -> title) . '/' . $this -> slugify($dmobject -> title), $linkedItem);
		}
	}

	function link_majordestination($dmobject, $linkedItem, $sitemap = 0)
	{
		if ($sitemap)
		{
			return base_url() . 'destinations/' . $this -> slugify($dmobject -> title);
		}
		else
		{
			return anchor('destinations/' . $this -> slugify($dmobject -> title), $linkedItem);
		}
	}

	function link_hotel($dmobject, $linkedItem, $sitemap = 0)
	{
		if ($sitemap)
		{
			return base_url() . 'hotels/viewDetail/' . $dmobject -> id . '/' . $this -> slugify($dmobject -> title);
		}
		else
		{
			return anchor('hotels/viewDetail/' . $dmobject -> id . '/' . $this -> slugify($dmobject -> title), $linkedItem);
		}
	}

	function link_person($dmobject, $linkedItem, $sitemap = 0)
	{
		if ($sitemap)
		{
			return base_url() . '' . $dmobject -> username;
		}
		else
		{
			return anchor($dmobject -> username, $linkedItem);
		}
	}

	function getCacheView($cacheFolder, $cacheFile, $path, $file, $data = null)
	{
		$CI = &get_instance();
		if ($cacheRes = $CI -> maintaincache -> getCache($cacheFolder . '/' . $cacheFile . '.txt'))
		{
			return $cacheRes;
		}
		else
		{
			$cacheRes = $CI -> load -> view($path . '/' . $file, $data, true);
			$CI -> maintaincache -> setCache($cacheFolder . '/' . $cacheFile . '.txt', $cacheRes);
			return $cacheRes;
		}
	}

	function personNameFull($p, $link = true)
	{
		if ($p -> id)
		{
			if ($link)
			{
				return anchor('admin/persons/viewDetail/' . $p -> id, $p -> firstname . ' ' . $p -> surname);
			}
			else
			{
				return $p -> firstname . ' ' . $p -> surname;
			}
		}
		else
		{
			// no id was found
			log_message('error', 'No person ID was passed. Library:teamleaf:personNameFull');
			return false;
		}
	}

	function personNameFullFrontend($p, $link = true)
	{
		if ($p -> id)
		{
			if ($link)
			{
				return anchor('travel-expert/'.$this->slugify($p->firstname.' '.$p->surname).'/' . $p -> id, $p -> firstname . ' ' . $p -> surname);
			}
			else
			{
				return $p -> firstname . ' ' . $p -> surname;
			}
		}
		else
		{
			// no id was found
			log_message('error', 'No person ID was passed. Library:teamleaf:personNameFull');
			return false;
		}
	}

	function personNameShort($p, $link = true)
	{
		if ($p -> id)
		{
			if ($link)
			{
				return anchor('admin/persons/viewDetail/' . $p -> id, $p -> firstname . ' ' . substr($p -> surname, 0, 1));
			}
			else
			{
				return $p -> firstname . ' ' . substr($p -> surname, 0, 1);
			}
		}
		else
		{
			// no id was found
			log_message('error', 'No person ID was passed. Library:teamleaf:personNameShort');
			return false;
		}
	}

	function personNameShortFrontend($p, $link = true)
	{
		if ($p -> id)
		{
			if ($link)
			{
				return anchor('persons/viewDetail/' . $p -> id, $p -> firstname . ' ' . substr($p -> surname, 0, 1));
			}
			else
			{
				return $p -> firstname . ' ' . substr($p -> surname, 0, 1);
			}
		}
		else
		{
			// no id was found
			log_message('error', 'No person ID was passed. Library:teamleaf:personNameShort');
			return false;
		}
	}

	function words($str, $length)
	{
		$str = strip_tags($str);
		$str = explode(" ", $str);
		return implode(" ", array_slice($str, 0, $length));
	}

	
	
	function getTel($text = true)
    {
       if($text)
	   {
	   	return 'Call us now on 0845 123 9482';
	   }
	   else
	   	{
	   		return '0845 123 9482';
	   	}
    }
	
	function expertFlashSet($expertid)
    {
        $CI = &get_instance();
           $data['travelexpertID'] = $expertid;
        $CI->teamleaf->setvars($data);

        return;
    }

	function leadFlashSet($leader)
    {
        $CI = &get_instance();
            $data['leader'] = $leader;
        $CI->teamleaf->setvars($data);

        return;
    }

	function leadFlashMemberSet($memberID)
    {
        $CI = &get_instance();
            $data['leader_memberID'] = $memberID;
        $CI->teamleaf->setvars($data);

        return;
    }

	function getvars()
    {
        $CI = &get_instance();
        $a = unserialize((string)$CI->session->userdata('pagedata_array'));
        //$CI->fb->info($a, 'getvars');
        return $a;
    }

    function setvars($array, $overwrite = false)
    {
        $CI = &get_instance();
        $arr = $this->getvars();
        if (!$overwrite)
        {
            $result = array_merge((array)$arr, (array)$array);
            foreach ($result as $key => $val)
            {
                if ($val == '')
                {
                    unset($result[$key]);
                }
            }
            $CI->session->set_userdata('pagedata_array', serialize($result));
            //$CI->fb->info($result, 'setvars');
        }
        else
        {
            $CI->session->set_userdata('pagedata_array', serialize($array));
           // $CI->fb->info($array, 'setvars');
        }

    }
	

}
