<?php

/*
 * Maintinace Cache
 *
 * Main cache update lib for Teamleaf 3
 *
 * @package     Teamleaf : MaintainCache
 * @author      Matt Hiscock
 * @version     1.0.1
 * @license     Copyright (c) s3d designs
 */

class Maintaincache
{
	function initCache()
	{
		define('CACHE_PATH', BASE_PATH.'application/cache/');
		mkdir(CACHE_PATH.'database', 0777);
		mkdir(CACHE_PATH.'views', 0777);
		mkdir(CACHE_PATH.'views/communications', 0777);
	}
	function getCache($cache_id, $type = 'file')
	{
		$CI = &get_instance();
		if ($type == 'file')
		{
			return $CI -> cache -> file -> get($cache_id);
		}
		else
		{
			$CI -> firephp -> error("getCache -> No cache type of " . $type . " exists");
			return false;
		}
	}

	function setCache($cache_id, $data, $type = 'file')
	{
		$CI = &get_instance();
		if ($type == 'file')
		{
			return $CI -> cache -> file -> save($cache_id, $data, 99999999);
		}
		else
		{
			$CI -> firephp -> error("setCache -> No cache type of " . $type . " exists");
			return false;
		}

	}

	function appendCache($cache_id, $data, $where = 'top', $type = 'file')
	{
		$CI = &get_instance();
		if ($type == 'file')
		{
			$currentCache = $this -> getCache($cache_id, $type);
			if ($where == 'top')
			{
				$newCache = $data . $currentCache;
			}
			else
			{
				$newCache = $currentCache . $data;
			}
			return $CI -> cache -> file -> save($cache_id, $newCache, 99999999);
		}
		else
		{
			$CI -> firephp -> error("appendCache -> No cache type of " . $type . " exists");
		}
	}

	function cleanCacheID($id)
	{
		return str_replace('/', '::', $id);
	}

	function deleteCache($cache_id)
	{
		return $this -> cache -> file -> delete($cache_id);
	}

	function all()
	{
		$this -> clears('all', 'database/');
		$this -> clears('all', 'views/common/');
		$this -> clears('all', 'views/communications/');
		$this -> clears('all', 'views/destinations/');
		$this -> clears('all', 'views/itinerarys/');
		$this -> clears('all', 'views/users/');
	}

	function destination($id)
	{
		$name = 'destination';
		$this -> clears('['.$name.'-]', 'database/');
		$this -> clears('['.$name.'-' . $id . ']', 'database/');
		$this -> clears(''.$name.'-with-ID-of-' . $id, 'database/');
		$this -> clears(''.$name.'-' . $id, 'views/destinations/');
	}
	
	function communication($id)
	{
		$name = 'communication';
		$this -> clears('['.$name.'-]', 'database/');
		$this -> clears('['.$name.'-' . $id . ']', 'database/');
		$this -> clears(''.$name.'-with-ID-of-' . $id, 'database/');
		$this -> clears(''.$name.'-' . $id, 'views/communications/');
		$this -> clears('-' . $id, 'views/communications/');
	}
	
	function communication_status($taskID)
	{

	}
	
	function communication_note($id)
	{

	}

	
	function hotel($id)
	{
		$name = 'hotel';
		$this -> clears('['.$name.'-]', 'database/');
		$this -> clears('['.$name.'-' . $id . ']', 'database/');
		$this -> clears(''.$name.'-with-ID-of-' . $id, 'database/');
		$this -> clears(''.$name.'-' . $id, 'views/hotels/');
	}
	
	function itinerary($id)
	{
		$name = 'itinerary';
		$this -> clears('['.$name.'-]', 'database/');
		$this -> clears('['.$name.'-' . $id . ']', 'database/');
		$this -> clears(''.$name.'-with-ID-of-' . $id, 'database/');
		$this -> clears(''.$name.'-' . $id, 'views/itinerarys/');
	}
	

	function user($id)
	{
		$this -> clears('[user-]', 'database/');
		$this -> clears('[user-' . $id . ']', 'database/');
		$this -> clears('user-with-ID-of-' . $id, 'database/');
		
		$this -> clears('txt', 'views/common/');
	}
	
	function user_depot($userID)
	{
		// Clear the quick links for the tasks create dropdown
		$this -> clears('user-' . $id, 'views/tasktypes/');
	}

	function status($id)
	{
		$this -> clears('[statu-]', 'database/');
		$this -> clears('[statu-' . $id . ']', 'database/');
		$this -> clears('statu-with-ID-of-' . $id, 'database/');
	}

	function clears($string, $folders = '', $type = 'file')
	{
		if ($type == 'file')
		{
			$cache = new Cache_Controller();
			//$this -> cache = $cache -> getCache();
			$CI = &get_instance();

			$path = $CI -> config -> item('cache_path') . $folders;
			$CI -> firephp -> info("Cache maintance clear for " . $folders . ' :: ' . $string);
			//$CI->firephp->info("Cache maintance clear for path: ".$path);

			$files = scandir($path);
			if ($files)
			{
				foreach ($files as $file)
				{
					// we add 'File-' to the file name to ensure, if found $pos would be a positive number
					$pos = strpos('File-' . $file, $string);
					if (($pos || $string == 'all') && strlen($file) > 4)
					{
						//echo $folders . $file;
						if ($CI -> cache -> file -> delete($folders . $file))
						{
							$CI -> firephp -> info("Cache deleted successfully: " . $file);
						}
						else
						{
							$CI -> firephp -> error("Cache failed to delete: " . $file);
						}
					}

				}
			}
		}
	}

	function clear($string, $folders = '', $type = 'file')
	{
		$CI = &get_instance();
		if ($type == 'file')
		{

			if ($CI -> cache -> file -> delete($folders . $string))
			{
				$CI -> firephp -> info("Cache deleted successfully: " . $folders . $string);
			}
			else
			{
				$CI -> firephp -> error("Cache failed to delete: " . $folders . $string);
			}
		}

	}

}
