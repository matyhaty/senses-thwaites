<?php
define('XML_PATH', '');
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * XML_Forms
 *
 * XML Form library for Code Igniter.
 *
 * @package		Xml_forms
 * @author		Matt Hiscock
 * @version		1.2.5
 * @license		Copyright (c) 2011 Senses Web Solutions / s3d designs
 */
class Xml_forms
{

	private $error = array();
	var $requiredMarkup = '<span style="color:#ccc">*</span>';

	function __construct()
	{
		$this -> ci = &get_instance();

		//$this->ci->load->config('Xml_forms', TRUE);

		$this -> ci -> load -> library('session');
		$this -> ci -> load -> database();
		//$this->ci->load->model('tank_auth/users');
	}

	function loadXML($path, $item, $additionalrules = '')
	{
		$CI = &get_instance();

		if (0)
		{

			$file1 = XML_PATH . $path . '/' . $tlc -> systemxml . '_' . $item . '_admin.xml';
			$file2 = XML_PATH . $path . '/' . $tlc -> systemxml . '_' . $item . '.xml';
			$file3 = XML_PATH . $path . '/' . $item . '_admin.xml';
			$file4 = XML_PATH . $path . '/' . $item . '.xml';
			$loadfile = '';
			if (file_exists($file1))
			{
				$loadfile = $file1;
			}
			else
			if (file_exists($file2))
			{
				$loadfile = $file2;
			}
			else
			if (file_exists($file3))
			{
				$loadfile = $file3;
			}
			else
			if (file_exists($file4))
			{
				$loadfile = $file4;
			}
			else
			{
				$CI -> fb -> error('XML File not found.  Path: ' . $path . ' Item: ' . $item);
				$CI -> teamleaf -> growl_add('XML File not found');
			}

			$CI -> fb -> info('Discovering Admin XML files.... Loading: ' . $loadfile);
			$xml = simplexml_load_file($loadfile);
		}
		else
		{
			//$file2 = XML_PATH . $path . '/' . $tlc->systemxml . '_' . $item . '.xml';
			$file4 = XML_PATH . $path . '/' . $item . '.xml';

			//			if (file_exists($file2))
			//			{
			//				$CI->fb->info('Discovering user XML files.... Loading: '.$file2);
			//				$xml = simplexml_load_file($file2);
			//			}
			//			else
			if (file_exists($file4))
			{
				//$CI->fb->info('Discovering user XML files.... Loading: '.$file4);
				$xml = simplexml_load_file($file4);
			}
			else
			{
				$CI -> firephp -> error('XML File not found.  Path file4: ' . $file4 . ' Item: ' . $item);
				//$CI->teamleaf->growl_add('XML File not found');
			}
		}

		if (!isset($xml))
		{
			$CI -> firephp -> error('XML File not loaded (was found).  Path: ' . $path . ' Item: ' . $item);
			//$CI->teamleaf->growl_add('XML File not loaded');
			return false;
		}
		else
		{
			$data['xml'] = $xml;
			foreach ($xml as $element)
			{

				if ($element -> required != '' && $element -> required != 'no')
				{
					$CI -> form_validation -> set_rules((string)$element -> dbname, (string)$element -> name, (string)$element -> required);
				}
				$data[(string)$element -> dbname] = $CI -> input -> post((string)$element -> dbname);
			}
			// Set Additional rules here.
			// this can be done in the controller
			return $data;
		}
	}

	function uploadfiles($files, $path, $types = '*', $sizelimit = 100000)
	{
		$CI = &get_instance();
		$errors = '';
		$silent_errors = false;
		$rt = array();

		if (is_array($files))
		{
			// options here to deal with non standard names of file uploads (e.g. not file1, file2 etc)
			$number_of_files = count($files);
			// KLUDGE - incomplete
			//$errors = 'FAIL, code not written yet!';
			//die('FAIL, code not written yet!');
			
			$config['upload_path'] = UPLOAD_PATH . $path . '/';
			$config['allowed_types'] = $types;
			$config['max_size'] = $sizelimit;
			$CI -> load -> library('upload', $config);
			$tlf = new file();

			$f=0;
			//-------   Upload the potential 1 file
			foreach($files as $filefield)
			{
				$f++;
				if ($_FILES[$filefield]['name'] != '' || $_FILES[$filefield]['error'] == 0)
				{
					if ($CI -> upload -> do_upload($filefield))
					{
						$filedata = $CI -> upload -> data();
						$file = $filedata['file_name'];
						$rt[$filefield]['file_name'] = $filedata['file_name'];
						$rt[$filefield]['data'] = $CI -> upload -> data();

						foreach ($filedata as $key => $value)
						{
							$tlf -> $key = $value;
						}
						$tlf -> save();
						$rt[$filefield]['id'] = $tlf -> id;
					}
					else
					{
						//echo 'fail<br/>';
						//echo $CI->upload->display_errors();
						$CI -> firephp -> error('Do_Upload failed! Field name: file' . $filefield . '  Upload files failed:' . $CI -> upload -> display_errors() . ' :: ' . ' Upload-Path: ' . UPLOAD_PATH . $path . '/');
					}
				}
				else
				{
					// no file was entered in the form...
					//$silent_errors = true;
				}
			
			}
			
		}
		else
		{
			$number_of_files = $files;
			$config['upload_path'] = UPLOAD_PATH . $path . '/';
			$config['allowed_types'] = $types;
			$config['max_size'] = $sizelimit;
			$CI -> load -> library('upload', $config);
			$tlf = new file();

			//-------   Upload the potential 1 file
			for ($f = 1; $f <= $number_of_files; $f++)
			{
				if ($_FILES['file' . $f]['name'] != '' || $_FILES['file' . $f]['error'] == 0)
				{
					if ($CI -> upload -> do_upload('file' . $f))
					{

						$filedata = $CI -> upload -> data();
						$file = $filedata['file_name'];
						$rt[$f]['file_name'] = $filedata['file_name'];
						$rt[$f]['data'] = $CI -> upload -> data();

						foreach ($filedata as $key => $value)
						{
							$tlf -> $key = $value;
						}
						$tlf -> save();
						$rt[$f]['id'] = $tlf -> id;
					}
					else
					{
						//echo 'fail<br/>';
						//echo $CI->upload->display_errors();
						$CI -> firephp -> error('Do_Upload failed! Field name: file' . $f . '  Upload files failed:' . $CI -> upload -> display_errors() . ' :: ' . ' Upload-Path: ' . UPLOAD_PATH . $path . '/');
					}
				}
				else
				{
					// no file was entered in the form...
					$silent_errors = true;
				}

			}
		}

		//$CI->teamleaf->checkpath($path);

		if ($errors != '')
		{
			$CI -> fb -> error('Upload errors: ' . $errors . ' Upload-Path: ' . $config['upload_path']);
			$rt['errors'] = $errors;
			$rt['result'] = false;
		}
		else
		if ($silent_errors)
		{
			$rt['errors'] = $errors;
			$rt['result'] = false;
		}
		else
		{
			$rt['result'] = true;
		}

		return $rt;
	}

	function createform($xml, $objData, $formdropdowns, $extras, $timestamp = '')
	{

		$CI = $this -> ci;
		$CI -> load -> library('encrypt');
		$ajaxEnabled = false;
		$formClass = '';
		$attributes = array(
			'method' => 'post',
			'enctype' => 'multipart/form-data'
		);

		if (!$xml)
		{
			$CI -> firephp -> error('No XML data   library:teamleaf:createform');
			return false;
		}

		$grid = 'grid_24';
		if (isset($extras['grid']))
		{
			$grid = $extras['grid'];
		}

		if (isset($extras['nosheet']))
		{
			if ($extras['nosheet'] == 'greyarea')
			{
				$markup = "    <div class='" . $grid . " greyarea'>
                                <div class='greyarea-paddy'>
                                    <div class=''>";
			}
			else
			{
				$markup = "    <div class='" . $grid . " page-form'>
                                    <div class='page-form-outer'>
                                       <div class='page-form-whitearea'>";
			}
		}

		else
		{

			$markup = "<div class='sheet'>
                        <div class='innersheet'>
                            <div class='" . $grid . " page-form'>
                                <div class='page-form-outer'>
                                    <div class='page-form-whitearea'>";
		}

		foreach ($xml as $element)
		{
			if ($element['type'])
			{

				if ($element['type'] == 'submit_to')
				{
					$formID = '';
					$selfURL = false;
					if ($element -> name == 'self')
					{
						$selfURL = true;
						$element -> name = current_url();
					}

					//echo 'ajax on';
					//$CI->fb->info('Submit To Type detected - Ajax is on');
					$ajaxEnabled = true;

					if ($element -> class)
					{
						$formClass = $element -> class;
					}
					else
					{
						$formClass = '';
					}

					if ($element -> id)
					{
						$formID = $element -> id;
					}
					else
					{
						$formID = time();
					}
					
					if ($element -> rules)
					{
						$formRules = str_replace('[BASEURL]', base_url(), $element -> rules);
						//$formRules = $element -> rules;
					}
					else
					{
						$formRules = '';
					}

					$attributes = array(
						'id' => $formID,
						'class' => $formClass
					);

					// Load jquery for the validation client side
					$CI -> javascript -> ajaxform($formID, $element -> name, $formRules);
					//open the form
					// when we use self, we will end back on the calling controller function, whether new or edit.
					$markup .= form_open($element -> name, $attributes);
					
					$js = "
							$(document).ready(function() {
							    $('form:first *:input[type!=hidden]:first').focus();
							});  
			                ";
					$CI -> template -> add_js($js, 'embed');

				}
				else
				if ($element['type'] == 'submit_to_upload')
				{

					if ($element -> name == 'self')
					{
						$selfURL = true;
						$element -> name = current_url();
					}

					if ($element -> class)
					{
						$formClass = $element -> class;
					}
					else
					{
						$formClass = '';
					}

					if ($element -> id)
					{
						$formID = $element -> id;
					}
					else
					{
						$formID = time();
					}

					$attributes = array(
						'id' => $formID,
						'class' => $formClass
					);

					// Load jquery for the validation client side
					$CI -> javascript -> ajaxform($formID, $element -> name);

					if (isset($extras['id']))
					{
						$markup .= form_open_multipart($element -> name . '/' . $extras['id'], $attributes);
					}
					else
					{
						$markup .= form_open_multipart($element -> name, $attributes);
					}
				}
				else
				if ($element['type'] == 'fieldset_start')
				{
					$markup .= form_fieldset($element -> name);
				}
				else
				if ($element['type'] == 'fieldset_end')
				{
					$markup .= form_fieldset_close();
				}
				else
				if ($element['type'] == 'form_close')
				{
					$markup .= form_close();
				}
				else
				if ($element['type'] == 'submit')
				{
					if ($ajaxEnabled)
					{
						$markup .= form_submit((String)$element -> name, (string)$element -> value . '');
					}
					else
					{
						$markup .= form_submit((String)$element -> name, (string)$element -> value . '');
					}
				}
				else
				if ($element['type'] == 'js')
				{
					$markup .= '<script type="text/javascript">' . (string) str_replace('SITEURL', site_url(), $element -> script) . '</script>';
				}
				else
				if ($element['type'] == 'note')
				{
					$markup .= '<div class="' . $element -> class . '">' . $element -> text . '</div>';
				}
			}
			else
			{
				$dbname = (string)$element -> dbname;
				if ($CI -> input -> post($dbname) != '')
				{
					// check to see if they have just failed validation and new stuff is in the post
					$mydata[$dbname] = $CI -> input -> post($dbname);
				}
				else
				if (isset($objData -> $dbname))
				{
					// if not get any records (for an edit)
					$mydata[$dbname] = $objData -> $dbname;
				}
				else
				if (isset($extras[$dbname]))
				{
					// if not, see if we have passed over something special to populate this
					$mydata[$dbname] = $extras[$dbname];
				}
				else
				{
					// else it can be nothing but blank
					$mydata[$dbname] = '';
				}

				if ($element -> formtype == 'text')
				{
					$markup .= $this -> formtype_text($element, $mydata);
				}
				else
				if ($element -> formtype == 'googleAddress')
				{
					$markup .= $this -> formtype_googleAddress($element, $mydata, $timestamp);
				}
				else
				if ($element -> formtype == 'upload')
				{
					$markup .= $this -> formtype_upload($element, $mydata);
				}
				else
				if ($element -> formtype == 'upload-multi')
				{
					$markup .= $this -> formtype_upload_multi($element, $mydata);
				}
				else
				if ($element -> formtype == 'colourpicker')
				{
					$markup .= $this -> formtype_colourpicker($element, $mydata);
				}
				else
				if ($element -> formtype == 'password')
				{
					$markup .= $this -> formtype_password($element, $mydata);
				}
				else
				if ($element -> formtype == 'date')
				{
					$markup .= $this -> formtype_date($element, $mydata, $timestamp);
				}
				else
				if ($element -> formtype == 'daterange')
				{
					$markup .= $this -> formtype_daterange($element, $mydata, $timestamp);
				}
				else
				if ($element -> formtype == 'time')
				{
					$markup .= $this -> formtype_time($element, $mydata, $timestamp);
				}
				else
				if ($element -> formtype == 'textarea')
				{
					$markup .= $this -> formtype_textarea($element, $mydata);
				}
				else
				if ($element -> formtype == 'select')
				{
					$markup .= $this -> formtype_select($element, $mydata, $formdropdowns, $timestamp);
				}
				else
				if ($element -> formtype == 'multiselect')
				{
					$markup .= $this -> formtype_multiselect($element, $mydata, $formdropdowns, $timestamp);
				}
				else
				if ($element -> formtype == 'emptyselect')
				{
					$markup .= $this -> formtype_emptyselect($element, $mydata, $formdropdowns);
				}
				else
				if ($element -> formtype == 'hidden')
				{
					$markup .= $this -> formtype_hidden($element, $mydata);
				}
				else
				if ($element -> formtype == 'checkbox')
				{
					$markup .= $this -> formtype_checkbox($element, $mydata);
				}
				else
				if ($element -> formtype == 'multicheckbox')
				{
					$markup .= $this -> formtype_multicheckbox($element, $mydata);
				}
				else
				{
					//echo 'Unknown form type: ' . $element->formtype . ' :: ' . $element->name . '  Cant use it. libs:Xml forms';
					$CI -> firephp -> error('Unknown form type: "' . $element -> formtype . '"  Cant use it. libs:Xml forms on line 458');
				}
			}
		}

		if (isset($extras['nosheet']))
		{
			$markup .= '<div class="clear"></div></div></div></div>';
		}
		else
		{
			$markup .= '</div></div><div class="clear"></div></div></div>';
		}
		return $markup;
	}

	function form_text($data)
	{
		$form = form_input($data);
		return $form;
	}

	function form_hidden($name, $val)
	{
		$form = form_hidden((string)$name, $val);
		return $form;
	}

	function form_textarea($data)
	{
		$form = form_textarea($data);
		return $form;
	}

	function required($string)
	{
		if ($string == 'required|xss_clean')
		{
			return 'required';
		}
		
else
		if ($string == 'required|email')
		{
			return 'required email';
		}
		
else
		if ($string == 'required|xss_clean|callback_username_check')
		{
			return 'required';
		}
		
else
		if ($string == 'no')
		{
			return '';
		}
		else
		{
			return 'required ' . $string;
		}
	}

	function formtype_text($element, $mydata)
	{

		// decrpyt contents?
		$value = $this -> check_encryption($element, $mydata);

		// Required?
		$requiredclass = $this -> required($element -> required);

		// Check the label for the form element
		$textdataLabel = $this -> check_label($element, $mydata, $requiredclass);

		// Form Array
		$textdata = array(
			'name' => (string)$element -> dbname,
			'id' => (string)$element -> dbname,
			'value' => $value,
			'maxlength' => $element -> length,
			'class' => $requiredclass
		);

		$markup = "<div class='form-item'>";
		$markup .= "<div class='form-label'>";
		$markup .= $textdataLabel;
		$markup .= "</div>";
		$markup .= "<div class='form-textfield'>";
		$markup .= $this -> form_text($textdata);
		$markup .= "</div>";
		$markup .= "</div>";

		return $markup;

	}

	function formtype_googleAddress($element, $mydata, $timestamp)
	{
		$CI = $this -> ci;
		// decrpyt contents?
		$value = $this -> check_encryption($element, $mydata);

		// Required?
		$requiredclass = $this -> required($element -> required);

		// Check the label for the form element
		$textdataLabel = $this -> check_label($element, $mydata, $requiredclass);

		// Form Array
		$textdata = array(
			'name' => (string)$element -> dbname,
			'id' => 'googleaddress',
			'value' => $value,
			'maxlength' => $element -> length,
			'class' => $requiredclass
		);

		$markup = "<div class='form-item' style='position: relative'>";
		$markup .= "<div id='test' class='gmap3'></div>";
		$markup .= "<div class='form-label'>";
		$markup .= $textdataLabel;
		$markup .= "</div>";
		$markup .= "<div class='form-textfield'>";
		$markup .= $this -> form_text($textdata);
		$markup .= "</div>";
		$markup .= "</div>";

		$js = "
				$(function(){
					$('#test').gmap3();
				
					$('#googleaddress').autocomplete(
					{
						source: function() 
					  	{
					    	$('#test').gmap3(
					    	{
					      		action:'getAddress',
					      		address: $(this).val(),
					      		callback:function(results)
					      		{
					        		if (!results) return;
					        		$('#googleaddress').autocomplete(
					          			'display', 
							          results,
							          false
							        );
					      		}
					    	});
					  	},
					  	cb:
					  	{
					  		//$('#postcode').val('something');
					    	cast: function(item)
					    	{ 
					      		return item.formatted_address;
					    	},
					    	select: function(item) 
					    	{
					    		$('#postcode').val(item.formatted_address.match(/([A-Z0-9]{2,4}\s[A-Z0-9]{2,4}),/)[1]);
								
					      		$('#test').gmap3(
					        	{action:'clear', name:'marker'},
					        	{
					        		action:'addMarker',
					          		latLng:item.geometry.location,
					          		map:{center:true, zoom: 15}
			                  	});
			              	}
			            }
			          })
			          
			          
			      });
          
                ";
		$CI -> template -> add_js($js, 'embed');

		return $markup;

	}

	function formtype_hidden($element, $mydata)
	{
		$CI = $this -> ci;
		// decrpyt contents?
		$value = $this -> check_encryption($element, $mydata);

		// Required?
		$requiredclass = $this -> required($element -> required);

		// Check the label for the form element
		$textdataLabel = $this -> check_label($element, $mydata, $requiredclass);

		if ($value == '')
		{
			//$CI->fb->error('Form hidden fields shouldnt be empty (no value got)');
		}

		$markup = $this -> form_hidden($element -> dbname, $value);
		return $markup;
	}

	function formtype_password($element, $mydata)
	{
		// decrpyt contents?
		$value = $this -> check_encryption($element, $mydata);

		// Required?
		$requiredclass = $this -> required($element -> required);

		// Check the label for the form element
		$textdataLabel = $this -> check_label($element, $mydata, $requiredclass);

		// Form Array
		$textdata = array(
			'name' => (string)$element -> dbname,
			'id' => (string)$element -> dbname,
			'value' => $value,
			'maxlength' => $element -> length,
			'class' => $requiredclass
		);

		$markup = "<div class='form-item'>";
		$markup .= "<div class='form-label'>";
		$markup .= $textdataLabel;
		$markup .= "</div>";
		$markup .= "<div class='form-textfield'>";
		$markup .= form_password($textdata);
		$markup .= "</div>";
		$markup .= "</div>";

		//$markup = "<div class='".$element->dbname."'>" .
		//        "<label>" . $textdataLabel . "</label>" .
		//form_password($textdata) .
		//       "</div>";

		return $markup;

	}

	function formtype_upload($element, $mydata)
	{
		// decrpyt contents?
		$value = $this -> check_encryption($element, $mydata);

		// Required?
		$requiredclass = $this -> required($element -> required);

		// Check the label for the form element
		$textdataLabel = $this -> check_label($element, $mydata, $requiredclass);

		$textdata = array(
			'name' => (string)$element -> dbname,
			'id' => (string)$element -> dbname,
			'value' => $value,
			'maxlength' => $element -> length,
			'class' => $requiredclass,
		);

		$markup = '<div class="clear"></div>';
		$markup .= "<div class='form-item'>";
		$markup .= "<div class='form-label'>";
		$markup .= $textdataLabel;
		$markup .= "</div>";
		$markup .= "<div class='form-textfield'>";
		$markup .= form_upload($textdata);
		$markup .= "</div>";

		if ($value && file_exists($_SERVER['DOCUMENT_ROOT'] . $element -> path . $value))
		{
			$markup .= "		<div>
								<img src='" . $element -> path . $value . "' alt='' />
								<label for='delete_" . $element -> dbname . "'>Remove " . $value . "</label>
								<input id='delete_" . $element -> dbname . "' type='checkbox' name='delete_" . $element -> dbname . "' value='1' />
							</div>";
		}
		$markup .= "</div>";

		return $markup;
	}

	function formtype_upload_multi($element, $mydata)
	{
		// decrpyt contents?
		$value = $this -> check_encryption($element, $mydata);

		// Required?
		$requiredclass = $this -> required($element -> required);

		// Check the label for the form element
		$textdataLabel = $this -> check_label($element, $mydata, $requiredclass);

		$textdata = array(
			'name' => (string)$element -> dbname . '[]',
			'id' => (string)$element -> dbname,
			'class' => $requiredclass,
		);

		$markup = '<div class="clear"></div>';
		$markup .= "<div class='form-item'>";
		$markup .= "<div class='form-label'>";
		$markup .= $textdataLabel;
		$markup .= "</div>";
		$markup .= "<div class='form-textfield'>";
		$markup .= '<input type="file" name="' . $textdata['name'] . '" multiple>';
		$markup .= "</div>";
		$markup .= "</div>";
		$markup .= '<div class="progress">
                        <div class="bar"></div>
                        <div class="percent"></div>
                    </div>';
		$markup .= '<div id="status"></div>';

		return $markup;
	}

	function formtype_colourpicker($element, $mydata)
	{
		// decrpyt contents?
		$value = $this -> check_encryption($element, $mydata);

		// Required?
		$requiredclass = $this -> required($element -> required);

		// Check the label for the form element
		$textdataLabel = $this -> check_label($element, $mydata, $requiredclass);

		$textdata = array(
			'name' => (string)$element -> dbname,
			'id' => (string)$element -> dbname,
			'value' => $value,
			'maxlength' => $element -> length,
			'class' => $requiredclass,
		);

		$markup = "<div class='" . $element -> dbname . "'>" . "<label>" . $textdataLabel . "</label>" . $this -> form_text($textdata) . "</div>

                <script>
                    $('#" . (string)$element -> dbname . "').ColorPicker({
                            color: '#0000ff',
                            onShow: function (colpkr) {
                                    $(colpkr).fadeIn(500);
                                    return false;
                            },
                            onHide: function (colpkr) {
                                    $(colpkr).fadeOut(500);
                                    return false;
                            },
                            onChange: function (hsb, hex, rgb) {
                                    $('#" . (string)$element -> dbname . " div').css('backgroundColor', '#' + hex);

                            },
                            onSubmit: function(hsb, hex, rgb, el) {
                                    $(el).val(hex);
                                    $(el).ColorPickerHide();
                            }
                    });

                </script>  
                
                ";

		return $markup;
	}

	function formtype_date($element, $mydata, $timestamp)
	{
		$CI = $this -> ci;

		// decrpyt contents?
		$value = $this -> check_encryption($element, $mydata);

		if ($value)
		{
			$initialValue = $value;
		}
		else
		{
			if ($element -> value == 'now' || $element -> value == 'today')
			{
				$initialValue = date('Y-m-d');
			}
			
else
			if ($element -> value == 'tomorrow')
			{
				$tomorrow = mktime(0, 0, 0, date("m"), date("d") + 1, date("Y"));
				$initialValue = date('Y-m-d', $tomorrow);
			}
			else
			{
				$initialValue = '';
			}
		}
		// Required?
		$requiredclass = $this -> required($element -> required);

		// Check the label for the form element
		$textdataLabel = $this -> check_label($element, $mydata, $requiredclass);

		$textdata = array(
			'name' => (string)$element -> dbname,
			'id' => (string)$element -> dbname . $timestamp,
			'value' => $initialValue,
			'class' => '' . $requiredclass . ' text-input datepicker',
		);

		$yearRange = '';
		if ($element -> yearRange)
		{
			$yearRange = 'yearRange: "' . $element -> yearRange . '",';
		}
		//$CI->template->add_js('/assets/js/jquery-ui.min.js');
		//$CI->template->add_js('/assets/js/time-picker.js');
		//$CI->template->add_css('/assets/css/jquery-ui-1.8.18.custom.css');

		$markup = "<div class='form-item'>";
		$markup .= "<div class='form-label'>";
		$markup .= $textdataLabel;
		$markup .= "</div>";
		$markup .= "<div class='form-textfield'>";
		$markup .= $this -> form_text($textdata);
		$markup .= "</div>";
		$markup .= "</div>";

		$js = "
                    $(function() {
                        $( '#" . (string)$element -> dbname . $timestamp . "' ).datepicker({
                            
                            numberOfMonths: 1,
                            changeMonth: true,
                            changeYear: true,
                            showButtonPanel: true,
                            " . $yearRange . "
                            dateFormat: 'yy-mm-dd' 	    
                        });
                    });
                ";
		$CI -> template -> add_js($js, 'embed');

		return $markup;
	}

	function formtype_daterange($element, $mydata, $timestamp)
	{
		$CI = $this -> ci;

		// decrpyt contents?
		$value = $this -> check_encryption($element, $mydata);

		if ($element -> value == 'now' || $element -> value == 'today')
		{
			$element -> value = date('Y-m-d');
		}

		if ($element -> value == 'tomorrow')
		{
			$tomorrow = mktime(0, 0, 0, date("m"), date("d") + 1, date("Y"));
			$element -> value = date('Y-m-d', $tomorrow);
		}
		// Required?
		$requiredclass = $this -> required($element -> required);

		// Check the label for the form element
		//$textdataLabel = $this->check_label($element, $mydata, $requiredclass);

		$textdataTo = array(
			'name' => (string)$element -> dbname . '_to',
			'id' => (string)$element -> dbname . '_to' . $timestamp,
			'value' => $element -> value,
			'class' => 'datepicker ' . $requiredclass,
		);

		$textdataFrom = array(
			'name' => (string)$element -> dbname . '_from',
			'id' => (string)$element -> dbname . '_from' . $timestamp,
			'value' => $element -> value,
			'class' => 'datepicker ' . $requiredclass,
		);

		//$CI->template->add_js('/assets/js/jquery-ui.min.js');
		//$CI->template->add_js('/assets/js/time-picker.js');
		//$CI->template->add_css('/assets/css/jquery-ui-1.8.18.custom.css');
		//echo $element->halfform;
		$markup = '';
		if ($element -> halfform == 'yes')
		{
			$markup .= "<div class='half-form-item'>";
		}
		$markup .= "<div class='form-item'>";
		$markup .= "<div class='form-label'>";
		$markup .= '<span style="color:#ccc">*</span> From';
		$markup .= "</div>";
		$markup .= "<div class='form-textfield'>";
		$markup .= $this -> form_text($textdataFrom);
		$markup .= "</div>";

		$markup .= "</div>";
		if ($element -> halfform == 'yes')
		{
			$markup .= "</div>";
		}

		if ($element -> halfform == 'yes')
		{
			$markup .= "<div class='half-form-item'>";
		}
		$markup .= "<div class='form-item'>";
		$markup .= "<div class='form-label'>";
		$markup .= '<span style="color:#ccc">*</span> To';
		$markup .= "</div>";
		$markup .= "<div class='form-textfield'>";
		$markup .= $this -> form_text($textdataTo);
		$markup .= "</div>";
		$markup .= "</div>";
		if ($element -> halfform == 'yes')
		{
			$markup .= "</div>";
		}

		$markup .= "<div class='clear'></div>";

		$js = "
                    $(function() {
                    					
                    				
                    	$( '#" . (string)$element -> dbname . "_from" . $timestamp . "' ).datepicker({
							defaultDate: '+0',
							changeMonth: true,
							numberOfMonths: 3,
							dateFormat: 'yy-mm-dd', 	
							onSelect: function( selectedDate ) {
								$( '#" . (string)$element -> dbname . "_to" . $timestamp . "' ).datepicker( 'option', 'minDate', selectedDate );
							}
						});
						$( '#" . (string)$element -> dbname . "_to" . $timestamp . "' ).datepicker({
							defaultDate: '+0',
							changeMonth: true,
							numberOfMonths: 3,
							dateFormat: 'yy-mm-dd', 	
							onSelect: function( selectedDate ) {
								$( '#f" . (string)$element -> dbname . "_from" . $timestamp . "' ).datepicker( 'option', 'maxDate', selectedDate );
							}
						});		
                    		
                    	
                       
                    });
          
                ";
		$CI -> template -> add_js($js, 'embed');

		return $markup;
	}

	function formtype_time($element, $mydata, $timestamp)
	{
		$CI = $this -> ci;

		// decrpyt contents?
		$value = $this -> check_encryption($element, $mydata);

		// Required?
		$requiredclass = $this -> required($element -> required);

		// Check the label for the form element
		$textdataLabel = $this -> check_label($element, $mydata, $requiredclass);

		if($value)
		{
			$element->value = $value;
		}
		if (!$element -> value)
		{
			$now = mktime(date('G'), date('i'), 0, date("m"), date("d"), date("Y"));
			$element -> value = date('G:i', $now);
		}
		
		if($element->value == 'no')
		{
			$element->value = '';
		}

		$textdata = array(
			'name' => (string)$element -> dbname,
			'id' => (string)$element -> dbname . $timestamp,
			'value' => $element -> value,
			'class' => 'timepicker ' . $requiredclass,
		);

		$markup = "<div class='form-item'>";
		$markup .= "<div class='form-label'>";
		$markup .= $textdataLabel;
		$markup .= "</div>";
		$markup .= "<div class='form-textfield'>";
		$markup .= $this -> form_text($textdata);
		$markup .= "</div>";
		$markup .= "</div>";

		$js = "
                    $(function() {
                        $( '#" . (string)$element -> dbname . $timestamp . "' ).timePicker({step: 15});
                    });
                
                ";
		$CI -> template -> add_js($js, 'embed');

		return $markup;
	}

	function formtype_textarea($element, $mydata)
	{
		// decrpyt contents?
		$value = $this -> check_encryption($element, $mydata);

		// Required?
		$requiredclass = $this -> required($element -> required);

		// Check the label for the form element
		$textdataLabel = $this -> check_label($element, $mydata, $requiredclass);

		// Form Array
		$textdata = array(
			'name' => (String)$element -> dbname,
			'id' => (String)$element -> dbname,
			'value' => $value,
			'maxlength' => $element -> length,
			'class' => $requiredclass,
		);

		$markup = "<div class='form-item'>";
		$markup .= "<div class='form-label'>";
		$markup .= $textdataLabel;
		$markup .= "</div>";
		$markup .= "<div class='form-textarea'>";
		$markup .= $this -> form_textarea($textdata);
		$markup .= "</div>";
		$markup .= "</div>";

		//$markup = "<div class='".$element->dbname."'>" .
		//        "<label>" . $textdataLabel . "</label>" .
		//$this->form_textarea($textdata) .
		//        "</div>";
		return $markup;

	}

	function formtype_select($element, $mydata, $formdropdowns, $timestamp)
	{
		$CI = $this -> ci;

		// Required?
		$requiredclass = $this -> required($element -> required);

		// Check the label for the form element
		$textdataLabel = $this -> check_label($element, $mydata, $requiredclass);

		$ddname = $element -> dbname;
		if (isset($element -> js))
		{
			$js = $element -> js;
		}
		else
		{
			$js = '';
		}

		$name = $ddname = (string)$ddname;
		if (isset($formdropdowns[$ddname]))
		{
			$multi = '';
			if (isset($element -> multi))
			{
				if ($element -> multi == 'true')
				{
					$multi = 'multiple="multiple"';
					$name = $ddname . '[]';
				}

			}

			$val = $formdropdowns[$ddname];
			if (isset($val['selected']))
			
			{
				//echo 'Selected is being passed';	
				$selected = $val['selected'];
				unset($val['selected']);
			}
			else
			{
				$selected = null;
			}

			$markup = "<div class='form-item'>";
			$markup .= "<div class='form-label'>";
			$markup .= $textdataLabel;
			$markup .= "</div>";
			$markup .= "<div class='form-select'>";
			$markup .= form_dropdown($name, $val, @$selected, $js . ' id="' . $element -> dbname . $timestamp . '"' . $multi);
			$markup .= "</div>";
			$markup .= "</div>";
			$markup .= "<div class='clear'></div>";

			if (isset($element -> multi))
			{
				$markup .= "<script>
                    $(function() {
                        $( '#" . (string)$element -> dbname . $timestamp . "' ).multiselect({
						   selectedList: 4 // 0-based index
						});
                    });
                </script>
                ";
			}
			//$markup = "<div class='".$element->dbname."'>";
			//$markup .= "<label for='".$element->dbname."'>" . $textdataLabel . "</label>" .
			//	form_dropdown($name, $val, @$selected, $js . ' id="'.$element->dbname.'"' . $multi  );
			//$markup .= "</div>";
		}
		else
		{
			$val = '';
			//$CI->fb->error('Form drop down options not present. libraries:xmlforms:createform:select');
			$markup = "<div class='form-item'>" . "<div class='form-label'>" . $textdataLabel . "</div>" . 'ELEMENT ERROR' . "</div>";
		}
		return $markup;
	}

	function formtype_multiselect($element, $mydata, $formdropdowns, $timestamp)
	{
		$CI = $this -> ci;

		// Required?
		$requiredclass = $this -> required($element -> required);

		// Check the label for the form element
		$textdataLabel = $this -> check_label($element, $mydata, $requiredclass);

		$ddname = $element -> dbname;
		if (isset($element -> js))
		{

			$js = $element -> js;
		}
		else
		{
			$js = '';
		}

		$name = $ddname = (string)$ddname;
		if (isset($formdropdowns[$ddname]))
		{

			$val = $formdropdowns[$ddname];
			if (isset($val['selected']))
			{
				$selected = $val['selected'];
				unset($val['selected']);
			}
			else
			{
				$selected = null;
			}

			$markup = "<div class='form-item'>";
			$markup .= "<div class='form-label'>";
			$markup .= $textdataLabel;
			$markup .= "</div>";
			$markup .= "<div class='form-select'>";
			//'id' => (string)$element->dbname . $timestamp,
			$markup .= form_dropdown($name, $val, @$selected, $js . ' id="' . $element -> dbname . $timestamp . '"');
			$markup .= "</div>";
			$markup .= "</div>";
			$markup .= "<div class='clear'></div>";

			$markup .= "<script>
                    $(function() {
                        $( '#" . (string)$element -> dbname . $timestamp . "' ).multiselect();
                    });
                </script>
                ";

		}
		else
		{
			$val = '';
			//$CI->fb->error('Form drop down options not present. libraries:xmlforms:createform:select');
			$markup = "<div class='form-item'>" . "<div class='form-label'>" . $textdataLabel . "</div>" . 'ELEMENT ERROR' . "</div>";
		}
		return $markup;
	}

	function formtype_emptyselect($element, $mydata, $formdropdowns)
	{
		$ddname = $dbname;

		if (isset($element -> js))
		{
			$js = $element -> js;
		}
		else
		{
			$js = '';
			$CI -> fb -> error('We much have an id (at least) to populate this field. libraries:xmlforms:createform:select');
		}

		if (isset($formdropdowns[$ddname]))
		{

			$val = $formdropdowns[$ddname];
			$markup = "<div class='" . $element -> dbname . "'>" . "<label>" . $element -> name . $req . "</label><div id='" . $element -> name . "_working'></div><div id='" . $element -> dbname . "_working'></div>" . form_dropdown($element -> dbname, $val, @$val['selected'], $js) . "</d>";
		}
		else
		{
			$markup = "<div class='" . $element -> dbname . "'>" . "<label>" . $element -> name . $req . "</label><div id='" . $element -> dbname . "_working'>" . $element -> default . "</div>" . form_dropdown($element -> dbname, $val, @$val['selected'], $js) . "</div>";
		}
		return $markup;
	}

	function formtype_checkbox($element, $mydata)
	{
		// decrpyt contents?
		$value = $this -> check_encryption($element, $mydata);

		// Required?
		$requiredclass = $this -> required($element -> required);

		// Check the label for the form element
		$textdataLabel = $this -> check_label($element, $mydata, $requiredclass);

		// Create Data array
		$textdata = array(
			'name' => (string)$element -> dbname,
			'id' => (string)$element -> dbname,
			'value' => 1,
			'checked' => $value,
		);

		$markup = "<div class='form-item'>";
		$markup .= "<div class='form-label'>";
		$markup .= $textdataLabel;
		$markup .= "</div>";
		$markup .= "<div class='form-checkbox'>";
		$markup .= form_checkbox($textdata);
		$markup .= "</div>";
		$markup .= "</div>";

		//$markup = "<div class='" . $element->dbname . "'>" . "<label>" . $textdataLabel . "</label>" . form_checkbox($textdata) . "</div>";

		return $markup;
	}

	function formtype_multicheckbox($element, $mydata)
	{
		// decrpyt contents?
		$value = $this -> check_encryption($element, $mydata);

		// Required?
		$requiredclass = $this -> required($element -> required);

		// Check the label for the form element
		$textdataLabel = $this -> check_label($element, $mydata, $requiredclass);

		$markup = '<div>' . $textdataLabel;

		foreach ($element->options->option as $option)
		{
			//$markup .= $option->name;
			// Create Data array
			$textdata = array(
				'name' => (string)$option -> dbname,
				'id' => (string)$option -> dbname,
				'value' => 1,
				'checked' => $value,
			);

			$markup .= "<div class='" . $element -> dbname . "'>" . form_checkbox($textdata) . $option -> name;
			if ($option -> item)
			{
				$markup .= $this -> formtype_text($option -> item, $mydata);
			}
			$markup .= "</div>";
		}

		$markup .= '</div>';

		return $markup;
	}

	function check_encryption($element, $mydata)
	{
		$CI = &get_instance();
		if (array_key_exists('' . $element -> dbname . '', $mydata))
		{
			if ($mydata['' . $element -> dbname . ''] == null || $mydata['' . $element -> dbname . ''] == '')
			{
				return false;
			}
			else
			{
				if ($element -> encrypt == 1)
				{
					$value = $CI -> encrypt -> decode($mydata['' . $element -> dbname . '']);
					$value = $CI -> teamleaf -> decodeText($value);
					return $value;
				}
				else
				{
					//$value = $CI->teamleaf->decodeText($mydata[''.$element->dbname.'']);
					$value = $mydata['' . $element -> dbname . ''];
					return $value;
				}
			}
		}
		else
		{
			return '';
		}
	}

	function check_label($element, $mydata, $requiredclass)
	{
		$textdataLabel = $element -> name;
		if ($requiredclass)
		{
			$textdataLabel = $this -> requiredMarkup . ' ' . $textdataLabel;
		}

		return $textdataLabel;
	}

}

/* End of file Xml_forms.php */
/* Location: ./application/libraries/Xml-forms.php */
