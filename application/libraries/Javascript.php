<?php

/*
 * Javascript - Teamleaf
 *
 * Main javascript lib for Teamleaf 2
 *
 * @package		Teamleaf
 * @author		Matt Hiscock
 * @version		1.0.1
 * @license		Copyright (c) s3d designs
 */

class Javascript
{

    function ajax_form($formSelector, $url, $target, $close='', $busySelector = 'busy' )
    {
        $script = "
           
            $(document).ready(function() 
            {
                // $.AJAX Example Request
                $('" . $formSelector . "').live('submit', function(eve) {
 
                    eve.preventDefault();
                    

                    $.ajax({
                        url: '" . site_url() . '/' . $url . "',
                        type: 'POST',
                        dataType: 'html',
                        data: $('" . $formSelector . "').serialize(),
                        beforeSend: function(){
                            $('" . $busySelector . "').show();
                        },	
                        success: function(html) {
                            $('" . $busySelector . "').hide();
                            $('" . $target . "').html(html);
                            $('" . $target . "').show('slow');  
                            $('" . $close . "').hide('slow');                          
                        }
                    });

                });	
            });
        ";

        $CI = & get_instance();
        $CI->template->add_js($script, 'embed');
    }
    
    function ajax_form_plugin($formSelector, $url, $target, $close='', $targetError = '', $busySelector = '.busy' )
    {
        $script = "
           
            $(document).ready(function() 
            {
                        var options = 
                        { 
                            target:        '".$target."',   // target element(s) to be updated with server response 
                        
                            beforeSubmit:  function(html) {
                                //alert('Pre Submit');
                                $('" . $busySelector . "').show();
                            },
                            
                            success:       function(html) {
									
                            	if(html.charAt(0) == '0' && html.charAt(1) == '@')
								{
									//alert('Error');
									$('" . $busySelector . "').hide();
		                            $('" . $targetError . "').html(html);
		                            $('" . $targetError . "').show('slow');  
		                            $('" . $close . "').hide('slow');   
								}
								else
								{
		                            //alert('Success');
		                            $('" . $busySelector . "').hide();
		                            $('" . $target . "').html(html);
		                            $('" . $target . "').show('slow');  
		                            $('" . $close . "').hide('slow');       
								}                   
                            },
                            
                            url:       '".site_url().'/'.$url."',           // override for form's 'action' attribute 
                            type:      'post',                              // 'get' or 'post', override for form's 'method' attribute 
                            clearForm: false,                                // clear all form fields after successful submit 
                            resetForm: false,                                // reset the form after successful submit 
                 
                        }; 
                    
                        $('".$formSelector."').ajaxForm(options);

               
            });
        ";

        $CI = & get_instance();
        $CI->template->add_js($script, 'embed');
    }
    
    
    function ajax_form_plugin_v2($sectionName='', $url='', $formSelector='', $target='', $close='', $targetError = '', $busySelector = '.busy' )
    {
    	$CI = &get_instance();
    	if($sectionName == '')
    	{
    		$CI->firephp->error("Base is required for all forms in v2");
    	}
		
		if($formSelector == '')
		{
			$formSelector = '#'.$sectionName.'-add';
		}
		if(is_int($url))
		{
			$url = ''.$sectionName.'/'.$sectionName.'s/create/'.$url;
		}
		if($target == '')
		{
			$target = '#'.$sectionName.'s_current';
		}
		if($close == '')
		{
			$close = '#'.$sectionName.'s_add_form';
		}
		if($targetError == '')
		{
			$targetError = '#'.$sectionName.'s_form_error';
		}
		
    		
    	
        $script = "
           
            $(document).ready(function() 
            {
                        var options = 
                        { 
                            
                        
                            beforeSubmit:  function(html) {
                                //alert('Pre Submit');
                                $('" . $busySelector . "').show();
                            },
                            
                            success:       function(html) {
									
								var mySplitResult = html.split('@');	
                            	if(mySplitResult[0] == '0')
								{
									//alert('Error: ".$targetError." --- '+html);
									$('" . $busySelector . "').hide();
		                            $('" . $targetError . "').html(mySplitResult[1]);
		                            $('" . $targetError . "').show('slow');  
		                            //$('" . $close . "').hide('slow');   
		                          
								}
								else
								{
		                            //alert('Success');
		                            $('" . $busySelector . "').hide();
		                            $('" . $target . "').html(html);
		                            $('" . $target . "').show('slow');  
		                            $('" . $close . "').hide('slow');  
								    
								}                   
                            },
                            
                            
                            url:       '".site_url().'/'.$url."',           // override for form's 'action' attribute 
                            type:      'post',                              // 'get' or 'post', override for form's 'method' attribute 
                            clearForm: false,                                // clear all form fields after successful submit 
                            resetForm: false,                                // reset the form after successful submit 
                 
                        }; 
                    
                        $('".$formSelector."').ajaxForm(options);

               
            });
        ";

        $CI = & get_instance();
        $CI->template->add_js($script, 'embed');
    }
    
    
    function ajax_link($selector, $url, $target='', $busySelector = '.busy')
    {
        $script = "
           
            $(document).ready(function() 
            {
                // $.AJAX Example Request
                $('" . $selector . "').live('click', function(eve) {
                    eve.preventDefault();

                    $.ajax({
                        url: '" . site_url() . '' . $url . "',
                        type: 'POST',
                        dataType: 'html',
                        data: $('" . $selector . "').serialize(),
                        beforeSend: function(){
                             $('" . $busySelector . "').slideDown();
                             $('" . $target . "').hide();
                        },  
                        success: function(html) {
                            $('" . $busySelector . "').slideUp();
                            $('" . $target . "').html(html);
                            $('" . $target . "').slideDown('slow');
                        }
                    });

                }); 
            });
        ";

        $CI = & get_instance();
        $CI->template->add_js($script, 'embed');
    }
    
    function ajax_link_and_form($selector, $url, $form_target, $update_target, $busySelector = '.busy')
    {
        $this->ajax_link($selector, $url, $form_target, $busySelector);
        $this->ajax_form($form_target .' form', $url, $update_target);
    }
    
    function groupDomReady($formSelector, $url, $processFunction, $busySelector = 'busy' , $selector_is_class=false)
    {
    	
    	if($url=='self'){
    		$url = current_url();
    	}else{
    		$url = site_url() . '/' . $url;
    	}
    	
        $script = "
           

		// $.AJAX Example Request
		$('" . $formSelector . "').live(
			'submit',
			function( eve ){
		    eve.preventDefault();
			var object = this;
			$.ajax({
				url: '" . $url . "',
				type: 'POST',
				dataType: 'html',
				data: $('" . $formSelector . "',$(object).parent() ).serialize(),
				// only in the context of the submitted form, all ajax forms should have parent containers in this case
				beforeSend: function(){
				    custom.showBusy('" . $busySelector . "' ,object);
				},complete : function(){
					custom.hideBusy('" . $busySelector . "' ,object);
				},
				success: function(html) {
				    " . $processFunction . "(html,object);
				}
			});
		
		});	
        ";

        $CI = & get_instance();
        $CI->template->write( 'dom_ready' , $script);    	
    	
    }
    
    function ajaxself($formSelector, $url, $processFunction, $busySelector = 'busy')
    {
        $script = "
           
            $(document).ready(function() 
            {
                // $.AJAX Example Request
                $('" . $formSelector . "').submit(function(eve){
                    eve.preventDefault();
                   
                    $.ajax({
                        url: '" . site_url() .  $url . "',
                        type: 'POST',
                        dataType: 'html',
                        data: $('#'+this.id).serialize(),
                        beforeSend: function(){
                           custom.showBusy('" . $busySelector . "');
                        },	
                        success: function(html) {
                            " . $processFunction . "(html);
                        }
                    });

                });	
            });
        ";

        $CI = & get_instance();
        $CI->template->add_js($script, 'embed');
    }

    function toggle($clickSelector, $targetArea)
	{
		$js = "
		
		$(document).ready(function()
		{
			//Hide the tooglebox when page load
			$('".$targetArea."').hide();
			
		
            $(document).on('click', '".$clickSelector."', function (event) 
			{
				//alert('Clicked: ".$targetArea."');
				event.preventDefault();
				$('".$targetArea."').slideToggle('slow');
				return true;
			});
		});

		";
		
		$CI = & get_instance();
        $CI->template->add_js($js, 'embed');
	}

    
    function link($selector, $url, $processFunction, $busySelector = 'busy')
    {
        $script = "
           
            $(document).ready(function() 
            {
                // $.AJAX Example Request
                $('" . $selector . "').live('click', function(eve) {
                    eve.preventDefault();

                    $.ajax({
                        url: '" . site_url() . $url . "',
                        type: 'POST',
                        dataType: 'html',
                        data: 'class='+this.className+'&id='+this.id+'',

                        beforeSend: function(){
                            custom.showBusy('" . $busySelector . "');
                        },	
                        success: function(html) {
                            " . $processFunction . "(html,this);
                        }
                    });

                });	
            });
        ";

        $CI = & get_instance();
        $CI->template->add_js($script, 'embed');
    }
    
    
    
    function hrefLink($selector, $processFunction, $busySelector = 'busy' )
    {
   	
    	
    	$selector	= "'" . $selector . "'  ";
    	
        $script = "
           
            $(document).ready(function() 
            {
                // $.AJAX Example Request
                
                
                $(".$selector.").live('click', function(eve) {
                    eve.preventDefault();
					object = this;
                    $.ajax({
                        url: $(object).attr('href') ,
                        type: 'POST',
                        dataType: 'html',
                        data: 'class='+this.className+'&id='+this.id+'',

                        beforeSend: function(){
                            custom.showBusy('" . $busySelector . "');
                        },	
                        success: function(html) {
                            " . $processFunction . "(html,object);
                        }
                    });

                });	
            });
        ";

        $CI = & get_instance();
        $CI->template->add_js($script, 'embed');
    }    
    
    
    
    function moreComments($selector, $processFunction, $busySelector = 'busy' )
    {
   	
    	
    	$selector	= "'" . $selector . "'  ";
    	
        $script = "
           
            $(document).ready(function() 
            {
                // $.AJAX Example Request
                
               
                
                $(".$selector.").live('click', function(eve) {
                    eve.preventDefault();
					object = this;
					
					id = this.id.replace(/[^0-9]/g ,'');
					
					var last_comment	= $('#wall_comment_box_' + id + ' .comment_body:last'  )
					var last_comment_id	= $(last_comment).attr('id').replace(/[^0-9]/g ,'');
					
                    $.ajax({
                        url: $(object).attr('href') ,
                        type: 'POST',
                        dataType: 'html',
                        data: 'comment_id='+ last_comment_id +'&class='+this.className+'&id='+this.id+'',

                        beforeSend: function(){
                            custom.showBusy('" . $busySelector . "' , object );
                        },complete	: function(){
                            custom.hideBusy('" . $busySelector . "', object );
                        },
                        success: function(html) {
                            " . $processFunction . "(html,object);
                        }
                    });

                });	
            });
        ";

        $CI = & get_instance();
        $CI->template->add_js($script, 'embed');
    }  
    
  
    
    
    function ajaxform($formSelector, $url, $formRules = '') {
        $script = "

            jQuery(document).ready(function()
            {
                jQuery('#".$formSelector."').validate(".$formRules.");

            });
        ";

        $CI =& get_instance();
        $CI->template->add_js($script, 'embed');

    }
    
    function script($js)
    {
        $CI = & get_instance();
        $CI->template->add_js('$(document).ready(function() {'.$js.'});', 'embed');
    }
	/**
	 * 
	 */
    function removeListItem($id)
    {
        return 
        '
			<script>
				$("#list-item-'.$id.'").fadeOut( "slow" , function(){
					$(this).remove();
				});
			</script>
        ';
    }    
    
    
    function file($url)
    {
        $CI = & get_instance();
        $CI->template->add_js('js/' . $url, 'import');
    }

}

?>