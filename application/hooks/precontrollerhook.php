<?php
/**
 * checkAccess
 *
 */

define('ADMIN_USER_TYPE', 9);

class preControllerHook extends MX_Controller
{

	/**
	 * Enter description here...
	 *
	 */
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * should be checked against routes,
	 *
	 */
	function checkAccess()
	{
		if ($this -> uri -> segment(1) == 'admin')
		{
			log_message('info', '--------------- Check Access ------------------');
			log_message('info', $_SERVER['PHP_SELF']);
			log_message('info', '-----------------------------------------------');

			$this -> load -> library('session');
			$this -> load -> helper(array(
				'password_helper',
				'url'
			));

			$perms = array();
			$loggedIn = false;
			$myFriends = array();
			$this -> user = $user = new User();

			if ($session_user = isLoggedIn($this -> session))
			{
				// user is logged in, lets check their state
				$user = $this -> tank_auth -> is_logged_in($session_user, 'Active', $this -> cache);

				

				// will return user dm object is they are active
				if ($this -> router -> method == 'activate')
					return;
				elseif (!$user && $this -> router -> method != 'send_again' && $this -> router -> method != 'logout') 
				{
					//echo $this -> router -> method;
					//echo '<hr>';
					//echo $this -> router -> class;
				    return redirect('admin/user/auth/send_again');
				}
				// check the user state against active
				elseif ($this -> router -> method == 'send_again' && $user)
				{
					return redirect('admin/user/profile/index');
				}
				else
					$loggedIn = true;

				if ($user)
				{
					//$subscription		= new Subscription();
					//$myFriends 			= $subscription->getFriends( $user );
					// friends should be available with in the apps, nice as they will be cahced!
				}
			}

			if (!$user instanceof DataMapper)
				$user = $this -> user;
			// set the user back to the DM

			$CI = &get_instance();
			//$CI -> firephp -> error("PRECONTROLLER");
			$hasAccess = check_permissions($user);
			//$CI -> firephp -> error("PRECONTROLLER END");

			// if page was not redirected by fail or success hooks
			if ($hasAccess === false)
			{
				//echo 'N ACCESS';
				return redirect('home/noaccess');
			}
			elseif ($hasAccess === 'must_be_logged_in')
			{
				$this -> session -> set_flashdata('message', 'You need to be logged in!');
				return redirect('admin/login');
			}
			elseif ($hasAccess === 'must_be_logged_out')
			{
				$this -> session -> set_flashdata('message', 'You are already logged in');
				return redirect('admin/home');
			}
			else
			{
				$this -> CI = get_instance();
				$this -> CI -> perms = $perms;
				//$this->CI->myFriends = $myFriends;
				$this -> CI -> loggedIn = $loggedIn;
				$this -> CI -> session_user = $session_user;
				$this -> CI -> user = $user;
			}

			if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == "XMLHttpRequest")
			{
				$this -> CI -> is_ajax = true;
			}
			else
			{
				$this -> CI -> is_ajax = false;
			}
		}
		
		else
		{
			// On Frontend of the site
			
			$this -> load -> library('session');
			$this -> load -> helper(array(
				'password_helper',
				'url'
			));

			$perms = array();
			$loggedIn = false;
			
			$this -> user = $user = new User();

			if ($session_user = isLoggedIn($this -> session))
			{
				// user is logged in, lets check their state
				$user = $this -> tank_auth -> is_logged_in($session_user, 'Active', $this -> cache);

				

				// will return user dm object is they are active
				if ($this -> router -> method == 'activate')
					return;
				elseif (!$user && $this -> router -> method != 'send_again' && $this -> router -> method != 'logout') 
				{
					//echo $this -> router -> method;
					//echo '<hr>';
					//echo $this -> router -> class;
				    return redirect('admin/user/auth/send_again');
				}
				// check the user state against active
				elseif ($this -> router -> method == 'send_again' && $user)
				{
					return redirect('admin/user/profile/index');
				}
				else
					$loggedIn = true;

				if ($user)
				{
					//$subscription		= new Subscription();
					//$myFriends 			= $subscription->getFriends( $user );
					// friends should be available with in the apps, nice as they will be cahced!
				}
			}

			if (!$user instanceof DataMapper)
				$user = $this -> user;
			// set the user back to the DM

			$CI = &get_instance();
			//$CI -> firephp -> error("PRECONTROLLER");
			$hasAccess = check_permissions($user);
			//$CI -> firephp -> error("PRECONTROLLER END");
			
			

			// if page was not redirected by fail or success hooks
			if ($hasAccess === false)
			{
				//echo 'N ACCESS';
				return redirect('home/noaccess');
			}
			elseif ($hasAccess === 'must_be_logged_in')
			{
				$this -> session -> set_flashdata('message', 'You need to be logged in!');
				return redirect('login');
			}
			elseif ($hasAccess === 'must_be_logged_out')
			{
				$this -> session -> set_flashdata('message', 'You are already logged in');
				return redirect('');
			}
			else
			{
				$this -> CI = get_instance();
				$CI -> firephp -> info("Already logged in as User " . $user->id . " (" . $user->username.")");
				$this -> CI -> perms = $perms;
				//$this->CI->myFriends = $myFriends;
				$this -> CI -> loggedIn = $loggedIn;
				$this -> CI -> session_user = $session_user;
				$this -> CI -> user = $user;
			}


				
			
		}		

	}

}
?>