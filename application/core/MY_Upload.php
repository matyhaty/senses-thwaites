<?php

class MY_Upload extends CI_Upload {
   
    var $my_config;
    var $my_error_msg = array();
    var $my_data = array();
    var $error_count = 0;
    
    function __construct($props = array())
    {
        parent::__construct();
        $this->my_config = $props;
    }
    
    function do_upload($field = 'userfile')
    {
        log_message('error','hello my_upload');
        $success = FALSE;
        if (isset($_FILES[$field]) and is_array($_FILES[$field]['error']))
        {
            // Create a pseudo file field for each file in our array
            for ($i = 0; $i < count($_FILES[$field]['error']); $i++)
            {
                // Give it a name not likely to already exist!
                $pseudo_field_name = '_psuedo_'. $field .'_'. $i;
                // Mimick the file
                $_FILES[$pseudo_field_name] = array(
                    'name'     => $_FILES[$field]['name'][$i],
                    'size'     => $_FILES[$field]['size'][$i],
                    'type'     => $_FILES[$field]['type'][$i],
                    'tmp_name' => $_FILES[$field]['tmp_name'][$i],
                    'error' => $_FILES[$field]['error'][$i]
                    );

                if ($_FILES[$field]['error'][$i] != 4)
                {
                    // Let do_upload work it's magic on our pseudo file field
                    $success[$i]         = parent::do_upload($pseudo_field_name);
                    $this->my_data[]    = parent::data();
                    $this->my_error_msg    = array_merge($this->my_error_msg, $this->error_msg);
                } else {
                    // Let do_upload work it's magic on our pseudo file field
                    parent::do_upload($pseudo_field_name);
                    // Since this is an empty field, set $succes to NULL
                    $success[$i]         = 'NULL';
                    $this->my_data[]    = parent::data();
                    $this->my_error_msg    = array_merge($this->my_error_msg, $this->error_msg);
                }
                
                // Count the errors
                if ($success[$i] === FALSE)
                {
                    $this->error_count++;
                }
                
                // Re-initialize after every file
                parent::initialize($this->my_config);
            }
        }
        else if (isset($_FILES[$field]))
        // Works just like do_upload since it's not an array of files
        {
            $success = parent::do_upload($field);
        }
        return $success;
    }
    
    /**
     * Display the error message
     *
     * @access    public
     * @param    string
     * @param    string
     * @return    string | array
     */    
    function display_errors($open = '<p>', $close = '</p>')
    {
        if (count($this->my_error_msg))
        {
            $arr = array();
            
            foreach ($this->my_error_msg as $val)
            {
                $arr[] = $open.$val.$close;
            }
            return $arr;
        } else {
            $str = '';
            
            foreach ($this->error_msg as $val)
            {
                $str .= $open.$val.$close;
            }
            return $str;
        }
    }
    
    /**
     * @acces    public
     * @return    integer
     */
    function error_count()
    {
        return $this->error_count;
    }
    
    /**
     * Finalized Data Array
     *    
     * Returns an associative array containing all of the information
     * related to the upload, allowing the developer easy access in one array.
     *
     * @access    public
     * @return    array
     */    
    function data()
    {
        return $this->my_data;
    }
   
} 

