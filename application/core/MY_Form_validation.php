<?php

/**
 * Extends the Form_Validation class by assing custom validation.
 * default: valid_postcode
 * @author Luke Steadman
 *
 */
class MY_Form_Validation extends CI_Form_Validation
{
	/**
	 * 
	 * Enter description here ...
	 */
    function __construct($rules = array())
    {
        parent::__construct($rules);
    }
    
    /**
     * Validates UK postcodes
     * @param $pc
     */
	function valid_postcode($pc) {
		// Postcode validation rules
		// Original regex found at http://www.five-twelve.co.uk/development/full-uk-postcode-regular-expression-pattern-incl-london
		// Dale: added /i flag
		$regex = 
    	"~^(GIR 0AA)|(TDCU 1ZZ)|(ASCN 1ZZ)|(BIQQ 1ZZ)|(BBND 1ZZ)"
  		. "|(FIQQ 1ZZ)|(PCRN 1ZZ)|(STHL 1ZZ)|(SIQQ 1ZZ)|(TKCA 1ZZ)"
  		. "|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]"
  		. "|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))"
  		. "|[0-9][A-HJKS-UW])\s?[0-9][ABD-HJLNP-UW-Z]{2}$~i";
		$result = preg_match($regex, $pc);

		// Validate
		if($result > 0) {
			return TRUE;
		} 
		
		else {
			$this->set_message('valid_postcode', 'Please enter a valid postcode');
			return FALSE;
		}
	}    
}