<?php

require APPPATH . 'controllers/cache_controller.php';

class MY_Model extends DataMapper
{

	public $cache_id = '';
	public $dm_object = null;

	function __construct($id = null)
	{
		$cache = new Cache_Controller();
		$this -> cache = $cache -> getCache();
		parent::__construct($id);
		$this -> dm_object = '';
	}

	protected function create_db()
	{
		$CI = &get_instance();
		$CI -> teamleaf -> create_db($this -> table, $this -> has_one, $this -> has_many);
	}

	/*
	 public function getTimelineTransaction($transactionID, $cacheType = 'file', $recreate = true)
	 {
	 $CI = &get_instance();
	 $this -> cache_id = 'view/tasktimelines/[View Cache - Timeline-' . $transactionID . ']';
	 $limit = 5;
	 switch($cacheType)
	 {
	 // START - FILE CACHE
	 // ------------------------------------------------------------------------------------

	 case 'file' :
	 $cacheLength = 3600;
	 $cache = $this -> cache -> file -> get($this -> cache_id);
	 if ($cache && !$recreate)
	 {
	 $CI -> firephp -> info("Timeline Cache Loaded for: " . $this -> cache_id);
	 return $cache;
	 }
	 else
	 {
	 $CI -> firephp -> info("Creating Timeline Cache for: " . $this -> cache_id);
	 $timeline = new Timeline();
	 $view = $timeline -> createTimelineTransactionMarkup($transactionID);
	 $this -> cache -> file -> save($this -> cache_id, $view, $cacheLength);
	 return $view;
	 }
	 }
	 }
	 */
	public function getObjectv2($array, $model, $id = null, $parentObject = null, $cacheType = 'file')
	{

		/*****
		 *
		 * Formatting notes
		 * ----------------
		 *
		 * WHERE
		 * Pass an Array of
		 * $arr['field'] = field name to check against
		 * $arr['value] = value
		 *
		 * WHERE IN
		 * Pass an Array of
		 * $arr['field'] = field name to check against
		 * $arr['values] Aarray of values (key not required)
		 *
		 *
		 */

		$CI = &get_instance();

		// BUILD CACHE ID
		// ------------------------------------------------------------------------------------
		$this -> cache_id = 'database/[' . $model . '-' . $id . ']';
		if ($parentObject)
		{
			$parent = $parentObject -> model;
			$this -> cache_id = 'database/[' . $model . '-' . $id . ']';
			$this -> cache_id .= '[ON_PARENT-' . $parent . '-with-ID-of-' . $parentObject -> id . ']';
		}

		foreach ($array as $key => $value)
		{
			//$CI -> firephp -> info("ARRAY VALUES: " . $key .'----'.$value);
			switch($key)
			{
				case 'where' :
					if (is_array($value))
					{
						$this -> cache_id .= '___Where--';
						foreach ($value as $sk => $sv)
						{
							$this -> cache_id .= $sk . '-' . $sv . '-';
						}
					}
					break;

				case 'or_where' :
					if (is_array($value))
					{
						//print_r($value);
						$this -> cache_id .= '___OrWhere--';
						foreach ($value['values'] as $svk => $svV)
						{
							$this -> cache_id .= '-' . $svk . '-' . $svV . '-';
						}
					}
					break;

				case 'or_like' :
					if (is_array($value))
					{
						//print_r($value);
						$this -> cache_id .= '___OrLike--' . $value['field'] . '-';
						foreach ($value['values'] as $svk => $svV)
						{
							$this -> cache_id .= '-' . $svk . '-' . $svV . '-';
						}
					}
					break;

				case 'wherein' :
					if (is_array($value))
					{
						$this -> cache_id .= '___WhereIn--' . $value['field'] . '-hasTheValues-';
						foreach ($value['values'] as $whereinvals)
						{
							$this -> cache_id .= $whereinvals . '-or-';
						}
					}
					break;

				case 'wherein2' :
					if (is_array($value))
					{
						$this -> cache_id .= '___WhereIn2--' . $value['field'] . '-hasTheValues-';
						foreach ($value['values'] as $whereinvals)
						{
							$this -> cache_id .= $whereinvals . '-or-';
						}
					}
					break;

				case 'sort' :
					if (is_array($value))
					{
						$this -> cache_id .= '___Orderby--';
						foreach ($value as $sk => $sv)
						{
							$this -> cache_id .= $sk . '-' . $sv . '-';
						}
					}
					break;

				case 'limit' :
					$this -> cache_id .= '___Limit--' . $value;
					break;

				case 'groupby' :
					$this -> cache_id .= '___Groupby--' . $value;
					break;

				case 'orderby' :
					$this -> cache_id .= '___Orderby--';
					foreach ($value as $orderKey => $orderValue)
					{
						$this -> cache_id .= $orderKey . '=' . $orderValue . '-';
					}
					break;
			}
		}

		// END -- BUILD CACHE ID
		// ------------------------------------------------------------------------------------

		switch($cacheType)
		{
			case 'db' :
				$CI -> firephp -> warn("Forced DB: " . $this -> cache_id);
				if ($parentObject)
				{
					$parentObject -> $model -> get();
					$this -> dm_object = $parentObject -> $model;
				}
				else
				{
					$this -> dm_object -> get();
				}
				break;

			// START - FILE CACHE
			// ------------------------------------------------------------------------------------

			case 'file' :
				$cacheLength = 3600;
				$cache = $this -> cache -> file -> get($this -> cache_id);
				if ($cache)
				{
					$CI -> firephp -> info("File Cache Loaded for: " . $this -> cache_id);
					$this -> dm_object = new $model();
					$this -> dm_object = $this -> dm_object -> all_from_array($cache);
				}
				else
				{

					if ($parentObject)
					{
						foreach ($array as $key => $value)
						{
							switch($key)
							{
								case 'where' :
									$this -> addWhereClause($parentObject -> $model, $value);
									break;
								case 'or_where' :
									$this -> addOrWhereClause($parentObject -> $model, $value);
									break;

								case 'or_like' :
									$this -> addOrLikeClause($parentObject -> $model, $value);
									break;

								case 'wherein' :
									$this -> addWhereInClause($parentObject -> $model, $value);
									break;

								case 'wherein2' :
									$this -> addWhereInClause($parentObject -> $model, $value);
									break;

								case 'sort' :
									$this -> addSortClause($parentObject -> $model, $value);
									break;

								case 'limit' :
									$this -> addLimitClause($parentObject -> $model, $value);
									break;
								case 'groupby' :
									$this -> addGroupByClause($parentObject -> $model, $value);
									break;
								case 'orderby' :
									$CI -> firephp -> info("ORDER BY CALLED");
									$this -> addOrderByClause($parentObject -> $model, $value);
									break;
							}
						}

						if ($id)
						{
							$CI -> firephp -> info("ID PASSED");
							$this -> addWhereClause($parentObject -> model, array('id' => $id));
						}

						$parentObject -> $model -> get();
						$this -> dm_object = $parentObject -> $model;
						$arr = $this -> dm_object -> all_to_array();
						$this -> cache -> file -> save($this -> cache_id, $arr, $cacheLength);
						$this -> dm_object = $parentObject -> $model;
						$CI -> firephp -> info("No Cache for: " . $this -> cache_id . ' A cache has been created');
					}

					// NO PARENT
					else
					{
						$this -> dm_object = new $model();

						if ($id)
						{
							$CI -> firephp -> info("ID PASSED");
							$this -> addWhereClause($this -> dm_object, array('id' => $id));
						}
						foreach ($array as $key => $value)
						{
							switch($key)
							{
								case 'where' :
									$this -> addWhereClause($this -> dm_object, $value);
									break;

								case 'or_where' :
									$this -> addOrWhereClause($this -> dm_object, $value);
									break;

								case 'or_like' :
									$this -> addOrLikeClause($this -> dm_object, $value);
									break;

								case 'wherein' :
									$this -> addWhereInClause($this -> dm_object, $value);
									break;

								case 'wherein2' :
									$this -> addWhereInClause($this -> dm_object, $value);
									break;

								case 'sort' :
									$this -> addSortClause($this -> dm_object, $value);
									break;

								case 'limit' :
									$this -> addLimitClause($this -> dm_object, $value);
									break;
								case 'groupby' :
									$this -> addGroupByClause($this -> dm_object, $value);
									break;
								case 'orderby' :
									$this -> addOrderByClause($this -> dm_object, $value);
									break;
							}
						}
						$this -> dm_object -> get();
						$arr = $this -> dm_object -> all_to_array();
						$this -> cache -> file -> save($this -> cache_id, $arr, $cacheLength);
						$CI -> firephp -> info("No Cache for: " . $this -> cache_id . ' A cache has been created');
						//$CI -> firephp -> info('LAST:'. $this -> dm_object -> check_last_query());
					}

				}
				break;
		}

		// return this , which includes the dm_object
		$this -> dm_object -> cache_id = $this -> cache_id;
		return $this;
	}

	/**
	 * Get a data mapper, if it is cached return the cached version!
	 *
	 * @param String $model
	 * @param Int $id
	 * @param DataMapper $parentbject
	 * @param Boolean $from_db
	 * @param Array $where
	 */
	public function getObject($model, $id, DataMapper $parentObject = null, $from_db = false, $where = '', $sort = '', $whereInObject = null, $limit = null, $whereIn = null)
	{
		$CI = &get_instance();
		if (!$parentObject)
		{
			$this -> cache_id = 'database/[' . $model . '-' . $id . ']';
			if (is_array($where))
			{
				$this -> cache_id .= '___WHERE_' . implode('_', $where) . '_' . implode('_', array_flip($where));
			}

			if (is_array($whereIn))
			{
				$str = '___WHERE-IN__field-' . $whereIn['field'];

				foreach ($whereIn['values'] as $v)
				{
					$str .= '_id-' . $v;
				}
				$this -> cache_id .= $str;
			}

			if (is_array($whereInObject))
			{
				if (isset($whereInObject['object']) && isset($whereInObject['column']) && isset($whereInObject['value']))
				{
					// do stuff
					$whereinmodel = $whereInObject['object'];
					@$this -> cache_id .= '___WHEREIN_' . $whereinmodel . '__column-' . $whereInObject['column'] . '_value-' . $whereInObject['value'];
				}
				else
				{
					$CI -> firephp -> error("Error - included related used, or bad where in called - this is not supported. Cache id so far: " . $this -> cache_id);
					//$from_db = true;
				}
			}
			if ($sort)
			{
				$this -> cache_id .= '___SORT_' . '_' . implode('_', array_flip($sort));
			}
			if ($limit)
			{
				$this -> cache_id .= '___LIMIT_' . $limit;
			}

			//check the cache
			if ($from_db)
			{
				$CI -> firephp -> warn("No Cache. Forced DB [P]: " . $this -> cache_id);
				$this -> dm_object = new $model($id);

				if (is_array($sort))
				{
					$this -> addSortClause($this -> dm_object, $sort);
				}

				if (is_array($where))
				{
					$this -> addWhereClause($this -> dm_object, $where);
				}

				if (is_array($whereIn))
				{
					$this -> addWhereInClause($this -> dm_object, $whereIn);
				}

				if ($limit)
				{
					$this -> addLimitClause($this -> dm_object, $limit);
				}

				if (is_array($whereInObject))
				{
					if (isset($whereInObject['object']) && isset($whereInObject['column']) && isset($whereInObject['value']))
					{
						$this -> addWhereInRelatedClause($this -> dm_object, $whereInObject);
					}
					else
					{
						$CI -> firephp -> error("Error - included related used, or bad where in called - this is not supported. Cache id so far: " . $this -> cache_id);
					}
				}
				//echo('<div class="cachefeedback"> Calling from the DB for the '. $model .' <br>ID '.$id.'  Cache ID: '.$this->cache_id.'</div>');
				$this -> dm_object -> get();

			}
			else
			{
				$cache = $this -> cache -> file -> get($this -> cache_id);
				if ($cache)
				{
					//$cache = unserialize($cache);
					$CI -> firephp -> info("Cache Loaded [P]: " . $this -> cache_id);
					//$CI->firephp->info("Cache Type: " . $cache);
					$this -> dm_object = new $model();
					//$CI->firephp->info("Model: " . $model);
					//$CI->firephp->info("Cache RAW for ".$this->cache_id." : " . print_r($cache, true));
					$this -> dm_object = $this -> dm_object -> all_from_array($cache);
					//$CI->firephp->info("Cache DM: " . print_r($this->dm_object, true));

					//$this->dm_object = $tempModel;
					//$CI->firephp->info("CACHE: ".$this->dm_object->all);

				}

				else
				{
					$CI -> firephp -> info("No Cache. Getting and creating [P]: " . $this -> cache_id);
					//echo( '<hr> Creating a cache for model '. $model .' <br>ID '.$id    );
					// we DO NOT have a cache...
					$this -> dm_object = new $model();
					if ($id)
					{
						$this -> addWhereClause($this -> dm_object, array('id' => $id));
					}

					if (is_array($sort))
					{
						$this -> addSortClause($this -> dm_object, $sort);
					}

					if (is_array($where))
					{
						$this -> addWhereClause($this -> dm_object, $where);
					}

					if (is_array($whereIn))
					{
						$this -> addWhereInClause($this -> dm_object, $whereIn);
					}

					if ($limit)
					{
						$this -> addLimitClause($this -> dm_object, $limit);
					}

					if (is_array($whereInObject))
					{
						if (isset($whereInObject['object']) && isset($whereInObject['column']) && isset($whereInObject['value']))
						{
							$this -> addWhereInRelatedClause($this -> dm_object, $whereInObject);
						}
						else
						{
							$CI -> firephp -> error("Error - included related used, or bad where in called - this is not supported. Cache id so far: " . $this -> cache_id);
						}
					}
					$this -> dm_object -> get();
					//
					$arr = $this -> dm_object -> all_to_array();
					//print_r($arr);
					//$CI->firephp->info("Cache save: ".$this->cache_id." : " . print_r($arr, true));
					$this -> cache -> file -> save($this -> cache_id, $arr, 3600);
					//echo('<div class="cachefeedback"> Creating a cache for the '. $model .' <br>ID '.$id.' Cache ID: '.$this->cache_id.'</div>');

					//$CI->firephp->info("NO CACHE: ".$this->dm_object->all);
				}

			}
			// datamapper is inside;
			//$CI->firephp->info("Returning -this- Marker 1: " );
			return $this;
		}

		else
		{

			$parent = $parentObject -> model;
			$this -> cache_id = 'database/[' . $model . '-' . $id . ']';
			$this -> cache_id .= '___ON_PARENT_' . $parent . '-with-ID-of-' . $parentObject -> id;

			if (is_array($where))
			{
				@$this -> cache_id .= '___WHERE_' . implode('_', $where) . '_' . implode('_', array_flip($where));
			}

			if (is_array($whereIn))
			{
				$str = '___WHERE-IN__field-' . $whereIn['field'];

				foreach ($whereIn['values'] as $v)
				{
					$str .= '_id-' . $v;
				}
				$this -> cache_id .= $str;
			}

			if (is_array($whereInObject))
			{
				if (isset($whereInObject['object']) && isset($whereInObject['column']) && isset($whereInObject['value']))
				{
					$CI -> firephp -> error("Error - WHERE IN cannot be called on a parent");
				}
				else
				{
					$CI -> firephp -> error("Error - included related used, or bad where in called - this is not supported. Cache id so far: " . $this -> cache_id);
				}
			}

			if ($sort)
			{
				$this -> cache_id .= '___SORT_' . '_' . implode('_', array_flip($sort));
			}

			if ($limit)
			{
				$this -> cache_id .= '___LIMIT_' . implode('_', $limit);
			}

			//echo $this->cache_id.'<br/>';
			//echo $this->cache->file->get( $this->cache_id );
			// cache id based on model parent combo
			if ($from_db)
			{
				$CI -> firephp -> info("No Cache. Forced DB: " . $this -> cache_id);
				//$parentObject->$model->get();
				if (is_array($where))
				{
					$this -> addWhereClause($parentObject -> $model, $where);
				}

				if (is_array($whereIn))
				{
					$this -> addWhereInClause($this -> dm_object, $whereIn);
				}

				if (is_array($sort))
				{
					$this -> addSortClause($this -> dm_object, $sort);
				}

				if ($limit)
				{
					$this -> addLimitClause($this -> dm_object, $limit);
				}

				$parentObject -> $model -> get();
				$this -> dm_object = $parentObject -> $model;
				//echo('<div class="cachefeedback"> Calling from DB for the '. $model .' <br>ID '.$id.' Parent model ' . $parent.'</div>');
			}

			if ($cache = $this -> cache -> file -> get($this -> cache_id))
			{
				$CI -> firephp -> info("Cache Loaded: " . $this -> cache_id);
				$this -> dm_object = new $model();
				//$CI->firephp->info("Model: " . $model);
				//$CI->firephp->info("Cache RAW for ".$this->cache_id." : " . print_r($cache, true));
				$this -> dm_object -> all_from_array($cache, '');
				//$CI->firephp->info("ID: " . $this->dm_object->id);
			}

			else
			{
				$CI -> firephp -> info("No Cache. Getting and creating: " . $this -> cache_id);
				//echo 'failled to load cache';
				//$parentObject->$model->get();
				if (is_array($where))
				{
					$this -> addWhereClause($parentObject -> $model, $where);
				}

				if (is_array($whereIn))
				{
					$this -> addWhereInClause($parentObject -> $model, $whereIn);
				}

				if (is_array($sort))
				{
					$this -> addSortClause($parentObject -> $model, $sort);
				}

				if ($limit)
				{
					$this -> addLimitClause($parentObject -> $model, $limit);
				}

				$parentObject -> $model -> get();
				$this -> dm_object = $parentObject -> $model;
				// Save the cache file
				$arr = $this -> dm_object -> all_to_array();
				$this -> cache -> file -> save($this -> cache_id, $arr, 3600);
			}

			return $this;
		}

	}

	private function addWhereClause(DataMapper &$dm_object, array $where)
	{
		foreach ($where as $field => $condition)
		{
			$dm_object -> where($field, $condition);
		}
		//print_R( get_class_methods( $dm_object) );
		//print_R(  $dm_object->get_sql() );

		return true;
	}

	private function addOrWhereClause(DataMapper &$dm_object, array $where)
	{
		$CI = &get_instance();
		$count = 1;
		$CI -> firephp -> error("Error - OR WHERE");
		//print_r($where);
		
		$column = $where['column'];
		$dm_object -> group_start();
		foreach ($where['values'] as $svK => $svV)
		{
			if ($count)
			{
				log_message("error", '<br/>WHERE ' . $svK . ' is ' . $svV);
				$dm_object -> where($column, $svV);
				$count = 0;
			}
			else
			{
				log_message("error", '<br/>OR WHERE ' . $svK . ' is ' . $svV);
				$dm_object -> or_where($column, $svV);
			}

		}
		$dm_object -> group_end();

		return true;
	}

	private function addOrLikeClause(DataMapper &$dm_object, array $where)
	{
		$CI = &get_instance();
		$count = 1;
		$CI -> firephp -> error("Error - OR LIKE");
		$dm_object -> group_start();
		foreach ($where['values'] as $svK => $svV)
		{
			if ($count)
			{
				log_message("error", '<br/>LIKE ' . $svK . ' is ' . $svV);
				$dm_object -> like($svK, $svV);
				$count = 0;
			}
			else
			{
				log_message("error", '<br/>OR LIKE ' . $svK . ' is ' . $svV);
				$dm_object -> or_like($svK, $svV);
			}

		}
		$dm_object -> group_end();

		return true;
	}

	private function addWhereInClause(DataMapper &$dm_object, array $whereIn)
	{
		$field = $whereIn['field'];
		$values = $whereIn['values'];
		$dm_object -> where_in($field, $values);
		return true;
	}

	private function addWhereInRelatedClause(DataMapper &$dm_object, array $wherein)
	{
		$dm_object -> where_in_related($wherein['object'], $wherein['column'], $wherein['value']);
		//print_R( get_class_methods( $dm_object) );
		//print_R(  $dm_object->get_sql() );

		return true;
	}

	private function addSortClause(DataMapper &$dm_object, array $sort)
	{
		foreach ($sort as $field => $condition)
		{
			$dm_object -> order_by($field, $condition);
		}
		//print_R( get_class_methods( $dm_object) );
		//print_R(  $dm_object->get_sql() );

		return true;
	}

	private function addLimitClause(DataMapper &$dm_object, $limit)
	{

		$dm_object -> limit($limit);

		//print_R( get_class_methods( $dm_object) );
		//print_R(  $dm_object->get_sql() );

		return true;
	}

	private function addOrderByClause(DataMapper &$dm_object, $arr)
	{
		$CI = &get_instance();
		foreach ($arr as $key => $value)
		{
			$CI -> firephp -> info("OREDERBY INSIDE: ARRAY VALUES: " . $key . '----' . $value);
			$dm_object -> order_by($key, $value);
		}

		//print_R( get_class_methods( $dm_object) );
		//print_R(  $dm_object->get_sql() );

		return true;
	}

	private function addGroupByClause(DataMapper &$dm_object, $group)
	{

		$dm_object -> group_by($group);

		//print_R( get_class_methods( $dm_object) );
		//print_R(  $dm_object->get_sql() );

		return true;
	}

	private function addIncludeRelatedClause(DataMapper &$dm_object, array $include_related)
	{
		foreach ($include_related as $condition)
		{
			$dm_object -> include_related($condition, '*', true, true);
		}
		//echo $field;
		//print_R( get_class_methods( $dm_object) );
		//print_R(  $dm_object->get_sql() );

		return true;
	}

	final function removeCachedObject($model, $id, DataMapper $parentObject = null, $where = '', $sort = '', $include_related = '', $limit = '')
	{
		$CI = &get_instance();
		if (!$parentObject)
		{
			$this -> cache_id = 'database/[' . $model . '-' . $id . ']';
			if (is_array($where))
			{
				@$this -> cache_id .= '___WHERE_' . implode('_', $where) . '_' . implode('_', array_flip($where));
			}

			if (is_array($include_related))
			{
				@$this -> cache_id .= '___INCLUDERELATED_' . implode('_', $include_related) . '_' . implode('_', array_flip($include_related));
			}
			if ($sort)
			{
				$this -> cache_id .= '___SORT_' . '_' . implode('_', array_flip($sort));
			}

			if ($limit)
			{
				$this -> cache_id .= '___LIMIT_' . $limit;
			}

		}
		else
		{
			$parent = $parentObject -> model;
			$this -> cache_id = 'database/[' . $model . '-' . $id . ']';
			$this -> cache_id .= '___ON_PARENT_' . $parent . '-with-ID-of-' . $parentObject -> id;

			if (is_array($where))
			{
				@$this -> cache_id .= '___WHERE_' . implode('_', $where) . '_' . implode('_', array_flip($where));
			}

			if (is_array($include_related))
			{
				@$this -> cache_id .= '___INCLUDERELATED_' . implode('_', $include_related) . '_' . implode('_', array_flip($include_related));
			}
			if ($sort)
			{
				$this -> cache_id .= '___SORT_' . '_' . implode('_', array_flip($sort));
			}

			if ($limit)
			{
				$this -> cache_id .= '___LIMIT_' . $limit;
			}
		}

		//$CI->firephp->error("Cache about to be deleted: ".$this->cache_id);
		//log_message('error', "Cache about to be deleted: ".$this->cache_id);
		if (@$this -> cache -> file -> delete($this -> cache_id))
		{
			$CI -> firephp -> info("Cache deleted successfully: " . $this -> cache_id);
			return true;
		}
		else
		{
			$CI -> firephp -> error("Cache failed to delete: " . $this -> cache_id);
			return false;
		}

	}

	function dm_timestamp()
	{
		$timestamp = (DataMapper::$config['local_time']) ? date(DataMapper::$config['timestamp_format']) : gmdate(DataMapper::$config['timestamp_format']);
		return (DataMapper::$config['unix_timestamp']) ? strtotime($timestamp) : $timestamp;
	}

	function timeline($object, $userID, $timestamp, $data, $newCreation = false)
	{
		//log_message('error', 'Timeline Call with Object of:'.get_class($object).'    $data of: '.get_class($data));
		//log_message('error', '$data of: '.$data->exists());

		if ($data && strtolower(get_class($data)) != 'my_model')
		{
			log_message('error', 'we have data and $data is not my_model');
			log_message('error', '$data is: ' . get_class($data) . ' with id of: ' . $data -> id);
			log_message('error', '$object is: ' . get_class($object) . ' with id of: ' . $object -> id);
			$dataModelName = get_class($data);

			if (0)//$object -> is_related_to($dataModelName, $data->id))
			{
				// already related, no change
				log_message('error', 'already related, no change');
			}
			else
			{
				log_message('error', 'not related, in else');
				$modelName = get_class($object);
				log_message('error', 'we have model name: ' . $modelName);
				$transactionID = uniqid(rand(), true);
				//log_message('error', 'Data is??? ' . get_class($data));
				$tr = new Timeline();
				$tr -> related_model = get_class($data);
				$tr -> related_model_id = $data -> id;
				$tr -> model = $modelName;
				$tr -> model_id = $object -> id;
				$tr -> updated_by = $userID;
				$tr -> updated = $timestamp;
				$tr -> created = $timestamp;
				$tr -> created_by = $userID;
				$tr -> transactionID = $transactionID;
				$tr -> type = 'relationship';

				log_message('error', 'timeline - about to save');
				$tr -> save();
				log_message('error', 'change - timeline changed');
				$newTimelineMarkup = $tr -> createTimelineTransactionMarkup($tr);
			}
		}
		else
		{
			//log_message('error', 'invlaid data, no relation');
			$modelName = get_class($object);
			$oldObject = new $modelName($object -> id);
			$transactionID = uniqid(rand(), true);

			// AUDIT TABKEL
			$fields = $this -> db -> list_fields($object -> table);
			//log_message('error', 'OldObject Exists?: ' . $oldObject -> exists());
			//log_message('error', 'OldObject ID?: ' . $oldObject -> id);
			//log_message('error', 'OldObject Updated?: ' . $oldObject -> updated);

			// If it has never been updated before, it must be a new one (for some reason the object already exists, when using DMZ-fromarray)
			if (!$newCreation)
			{
				foreach ($fields as $field)
				{
					//log_message('error', $object -> $field . '<Object oldObject->' . $oldObject -> $field . ' Field->' . $field);

					if ($object -> $field != $oldObject -> $field)
					{
						// We have a change!
						$t = new Timeline();
						$t -> oldValue = (string)$oldObject -> $field;
						$t -> newValue = $object -> $field;
						$t -> field = $field;
						$t -> type = 'update';
						$t -> transactionID = $transactionID;

						$t -> model = $modelName;
						$t -> model_id = $object -> id;

						if ($data)
						{
							$t -> related_model = get_class($data);
							$t -> related_model_id = $data -> id;
						}

						$t -> updated_by = $userID;
						$t -> updated = $timestamp;
						$t -> created = $timestamp;
						$t -> created_by = $userID;

						$t -> save();

						$ti = new Timeline();
						$newTimelineMarkup = $ti -> createTimelineTransactionMarkup($t);

					}

				}
			}
			else
			{
				// This is new creation
				$t = new Timeline();

				$t -> type = 'creation';
				$t -> transactionID = $transactionID;
				$t -> model = $modelName;
				$t -> model_id = $object -> id;
				$t -> updated_by = $userID;
				$t -> updated = $timestamp;
				$t -> created = $timestamp;
				$t -> created_by = $userID;
				$t -> save();
				$ti = new Timeline();
				$newTimelineMarkup = $ti -> createTimelineTransactionMarkup($t);
			}
		}

	}

	/**
	 * Sorta overload, so we can add some custom save fields
	 *
	 * @param unknown_type $data
	 */
	function save($data = null)
	{
		//print_r($data);
		$this -> CI = get_instance();
		$this -> user = $this -> CI -> user;

		$timestamp = $this -> dm_timestamp();
		//(DataMapper::$config['local_time']) ? date(DataMapper::$config['timestamp_format']) : gmdate(DataMapper::$config['timestamp_format']);

		if ($this -> id)
		{
			$this -> updated_by = $this -> user -> id;
			$this -> updated = $timestamp;
		}
		else
		{
			$this -> created = $timestamp;
			$this -> created_by = $this -> user -> id;
		}

		// Check table fields are all there.......
		if (ENVIRONMENT == 'development')
		{
			$fields = array();
			$ok = true;
			if (!$this -> db -> field_exists('updated', $this -> table))
			{
				$ok = false;
				log_message('error', 'updated field did not exist.');

				$fields['updated'] = array('type' => 'datetime', );

			}
			if (!$this -> db -> field_exists('updated_by', $this -> table))
			{
				$ok = false;
				log_message('error', 'updated by field did not exist.');

				$fields['updated_by'] = array(
					'type' => 'INT',
					'constraint' => 6,
				);
			}
			if (!$this -> db -> field_exists('created', $this -> table))
			{
				$ok = false;
				log_message('error', 'created field did not exist.');

				$fields['created'] = array('type' => 'datetime', );

			}
			if (!$this -> db -> field_exists('created_by', $this -> table))
			{
				$ok = false;
				log_message('error', 'created by field did not exist.');

				$fields['created_by'] = array(
					'type' => 'INT',
					'constraint' => 6,
				);
			}

			if (!$ok)
			{
				log_message('error', 'Updating Table: ' . $this -> table . '  Adding fields: ' . print_r($fields, 1));
				$this -> CI -> load -> dbforge();
				$this -> CI -> dbforge -> add_column($this -> table, $fields);
			}
		}
		// End developement check

		// Get Changes
		if ($this -> table != 'timelines')
		{
			if ($this -> created && $this -> updated)
			{
				$this -> timeline($this, $this -> user -> id, $timestamp, $data);
				$res = parent::save($data);
			}
			else
			{
				// This is a new object, and thus we need to save it first, so we can get its ID
				$res = parent::save($data);
				$this -> timeline($this, $this -> user -> id, $timestamp, $data, true);
			}
		}
		else
		{
			// do not timeline a timelines save! Recursive!
			$res = parent::save($data);
		}

		return $res;

	}

	public function search(array $data, DataMapper $dmObject, $where = '')
	{

		$search = $data['search'];
		$dmObject -> or_like('title', $search);
		$dmObject -> or_like('description', $search);
		if ($where)
		{
			if (is_array($where))
			{

				foreach ($where as $k => $w)
				{
					$sql[] = $k . ' = "' . $w . '"';
				}
				$where = '( ' . implode(' AND ', $sql) . ')';
			}

			$dmObject -> having($where);
		}
		$dmObject -> get();

		return $dmObject;
	}

	public function getItems($type, $model, $sort, $where = null, $include_related = null, $returnArray = false)
	{

		// if its a post, we should search the DB
		if (isset($_GET['search']))
		{
			// check if user is sorting
			if ($sort)
			{
				//add sort clause
				$model -> order_by("created", $sort);
			}
			$data['search'] = $_GET['search'];
			if ($returnArray)
			{
				return $model -> search($data, $model, $where, $include_related) -> all_to_array();
			}
			return $model -> search($data, $model, $where, $include_related);
			// search using parent function, generic across the board
		}
		else// just return either cached or sorted results
		{

			if ($returnArray)
			{
				return $model -> getObject($type, null, null, true, $where, $include_related, $sort) -> dm_object -> all_to_array();
			}
			return $model -> getObject($type, null, null, true, $where, $include_related, $sort) -> dm_object;

			// get the default cached list of item, return as array
		}
	}

	public function more($id, $model)
	{

		return $model -> where('id >' . $id) -> order_by("created", 'asc') -> limit(3) -> get();

	}

	public function getCachedActivity()
	{

		$cacheID = 'activity_cache';
		// set the cache id

		return $this -> cache -> file -> get($cacheID);

	}

	public function commentCacheActivity($type, $arr, array $comment)
	{

		$cacheID = 'activity_cache';
		// set the cache id

		if (!$arr['id'])
		{
			throw new Exception('No id found in the datamapper object, there must be an ID');
			// if there was no id throw an exception as, it should never passs here with out having an id
		}

		if (!$arr['created_by'])
		{
			throw new Exception('No created_by int found in the datamapper object, there must be an created by value');
			// if there was no id throw an exception as, it should never passs here with out having an id
		}
		// check the datamapper object

		$activityCache = $this -> cache -> file -> get($cacheID);
		// get the activity cache

		$activityCache[$type . '_' . $arr['id']][$arr['created_by']][$type][$arr['id']]['comments'][] = $comment;
		// instead of just having a numerical index combine both type and id so it will always be unque, iner lvels keys also contain this info

		// time used as a way of making it possible to sort

		$activityCache[$type . '_' . $arr['id']][$arr['created_by']][$type][$arr['id']][time()] = $arr;
		//    ^			^		^		  ^
		//createdby// //type// //item_id// //order//
		// set up updated cached object

		$this -> cache -> file -> save($cacheID, $activityCache, 1000000);
		// save the cache

		return true;

	}

	public function updateCacheActivity($type, array $arr)
	{

		$cacheID = 'activity_cache';
		// set the cache id

		if (!$arr['id'])
		{
			throw new Exception('No id found in the datamapper object, there must be an ID');
			// if there was no id throw an exception as, it should never passs here with out having an id
		}

		if (!$arr['created_by'])
		{
			throw new Exception('No created_by int found in the datamapper object, there must be an created by value');
			// if there was no id throw an exception as, it should never passs here with out having an id
		}
		// check the datamapper object

		$activityCache = $this -> cache -> file -> get($cacheID);
		// get the activity cache

		$arr['likes'] = 0;
		$arr['comments'] = array();
		// instead of just having a numerical index combine both type and id so it will always be unque, iner lvels keys also contain this info

		// time used as a way of making it possible to sort

		unset($activityCache[$type . '_' . $arr['id']][$arr['created_by']][$type][$arr['id']]);

		$activityCache[$type . '_' . $arr['id']][$arr['created_by']][$type][$arr['id']][time()] = $arr;
		//    ^			^		^		  ^
		//createdby// //type// //item_id// //order//
		// set up updated cached object

		$this -> cache -> file -> save($cacheID, $activityCache, 1000000);
		// save the cache

		return true;
	}

	public function removeCacheActivity($type, $id)
	{

		$activityCache = $this -> cache -> file -> get($cacheID);
		// get the activity cache

		unset($activityCache[$type . '_' . $id]);

		$this -> cache -> file -> save($cacheID, $activityCache);

	}

	public function createCacheActivity($type, array $arr)
	{

		$cacheID = 'activity_cache';
		// set the cache id

		if (!$arr['id'])
		{
			throw new Exception('No id found in the datamapper object, there must be an ID');
			// if there was no id throw an exception as, it should never passs here with out having an id
		}

		if (!$arr['created_by'])
		{
			throw new Exception('No created_by int found in the datamapper object, there must be an created by value');
			// if there was no id throw an exception as, it should never passs here with out having an id
		}
		// check the datamapper object

		$activityCache = $this -> cache -> file -> get($cacheID);
		// get the activity cache

		$arr['likes'] = 0;
		$arr['comments'] = array();
		// instead of just having a numerical index combine both type and id so it will always be unque, iner lvels keys also contain this info
		$activityCache[$type . '_' . $arr['id']][$arr['created_by']][$type][$arr['id']][time()] = $arr;
		//    ^			^		^		  ^
		//createdby// //type// //item_id// //order//
		// set up updated cached object

		$this -> cache -> file -> save($cacheID, $activityCache, 1000000);
		// save the cache

		return true;

	}

}

/**
 * So we dont have to rename all the classes
 *
 */
class Base_model extends MY_Model
{
	function __construct($id = null)
	{
		parent::__construct($id);
	}

	function __destruct()
	{

	}

}
