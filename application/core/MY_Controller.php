<?php
/**
 * We should probably change the name of class
 *
 */
class My_Controller extends MX_Controller
{

	public $region = null;
	public $buffer = false;
	public $parse = false;
	public $autoRender = true;
	public $user = array();
	public $myFriends = array();
	public $loggedIn = false;
	public $model = null;
	public $is_ajax = null;
	public $data = array();

	function setTemplate($type)
	{
		// Load libs
		$CI = get_instance();
		$CI->config->load('teamleaf');

		parent::__construct();

		$IE6 = (@ereg('MSIE 6', $_SERVER['HTTP_USER_AGENT'])) ? true : false;
		$IE7 = (@ereg('MSIE 7', $_SERVER['HTTP_USER_AGENT'])) ? true : false;
		$IE8 = (@ereg('MSIE 8', $_SERVER['HTTP_USER_AGENT'])) ? true : false;

		$this -> template -> set_template($type);

		if (($IE6 == 1) || ($IE7 == 1) || ($IE8 == 1))
		{
			//$this -> template -> add_css('assets/frontend/css/wendywu_oldbrowsers.css');
		}
		else
		{
			//$this -> template -> add_css('assets/frontend/css/bootstrap-responsive.css');
		}

		// this is a front end page!!
        if (!$this->config->item('minifyfiles'))
        {
            foreach ($this->config->item('css_files') as $css)
            {
                $this -> template -> add_css('assets/frontend/css/' . $css); 
            }
            foreach ($CI->config->item('js_files') as $js)
            {
                $this -> template -> add_js('assets/frontend/js/' . $js);
            }
        }
        else
        {
            //echo '<link rel="stylesheet" type="text/css" href="' . base_url() . 'css/frontend/all.css" media="screen" />';
            //echo '<script type="text/javascript" src="' . base_url() . 'js/frontend/all.js"></script>';
        }

		/// common template stuff
		$this -> data['footer'] = $this->teamleaf->getCacheView('views/common', 'footer', 'frontend', 'footer');
		$this -> data['header'] = $this->teamleaf->getCacheView('views/common', 'header', 'frontend', 'header');
		$this -> data['megamenu'] = $this->teamleaf->getCacheView('views/common', 'megamenu', 'frontend', 'megamenu');
		$this -> template -> write('footer', $this -> data['footer']);
		$this -> template -> write('header', $this -> data['header']);
		$this -> template -> write('navigation', $this -> data['megamenu']);
	}

	/**
	 * Constructor
	 *
	 * @param Object $model
	 *
	 */
	function __construct($model = null)
	{
		if ($this -> uri -> segment(1) == 'admin')
		{
			//echo 'URI';
			//echo $this->uri->segment(1);
			//$this -> load -> helper('url');
			//$this -> load -> model('facebook_model');

			// Load libs
			$this -> CI = get_instance();

			$this -> session_user = (isset($this -> CI -> session_user) ? $this -> CI -> session_user : null);
			$this -> user = $this -> CI -> user;
			$this -> loggedIn = (isset($this -> CI -> loggedIn) ? $this -> CI -> loggedIn : null);
			$this -> is_ajax = @$this -> CI -> is_ajax;

			parent::__construct();
			$this -> lang -> load('tank_auth', 'english');
			$this -> lang -> load('main', 'english');

			// example use - echo l('alpha_dash_dot');
			$this -> load -> library('form_validation');

			// All styles here
			$this -> template -> add_css('assets/admin/css/reset.css');
			$this -> template -> add_css('assets/admin/css/text.css');
			$this -> template -> add_css('assets/admin/css/960.css');
			$this -> template -> add_css('assets/admin/css/layout.css');
			$this -> template -> add_css('assets/admin/css/header.css');
			$this -> template -> add_css('assets/admin/css/smoothness/jquery-ui-1.8.20.custom.css');
			$this -> template -> add_css('assets/admin/css/jHtmlArea.css');
			$this -> template -> add_css('assets/admin/css/tasks.css');
			$this -> template -> add_css('assets/admin/css/tables.css');
			$this -> template -> add_css('assets/admin/css/timelines.css');
			$this -> template -> add_css('assets/css/validationEngine.jquery.css');
			$this -> template -> add_css('assets/admin/css/timePicker.css');
			$this -> template -> add_css('assets/admin/css/jquery.multiselect.css');
			$this -> template -> add_css('assets/admin/css/jquery-autocomplete.css');
			$this -> template -> add_css('assets/admin/css/TableTools.css');
			//$this->template->add_css('assets/css/fineuploader.css');

			// Google Fonts
			$this -> template -> add_css('http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700', 'externallink');

			// All scripts here
			$this -> template -> add_js('assets/admin/js/jquery-1.7.2.min.js');
			$this -> template -> add_js('assets/admin/js/jquery.form.js');
			$this -> template -> add_js('assets/admin/js/jquery-ui-1.8.20.custom.min.js');
			$this -> template -> add_js('assets/admin/js/jquery.dataTables.min.js');
			$this -> template -> add_js('assets/admin/js/TableTools.min.js');
			$this -> template -> add_js('assets/admin/js/jquery.tools.min.js');
			//$this->template->add_js('assets/js/chosen/chosen.jquery.js');
			$this -> template -> add_js('assets/admin/js/jquery.validate.min.js');
			$this -> template -> add_js('assets/js/jquery.validationEngine-en.js');
			$this -> template -> add_js('assets/admin/js/jHtmlArea-0.7.0.js');
			$this -> template -> add_js('assets/admin/js/jquery.timePicker.min.js');
			$this -> template -> add_js('assets/admin/js/jquery.multiselect.min.js');
			$this -> template -> add_js('assets/admin/js/jquery.easy-confirm-dialog.js');
			//$this->template->add_js('assets/js/jquery.fineuploader-3.0.min.js');

			$this -> template -> add_js('http://maps.google.com/maps/api/js?sensor=false', 'external');
			$this -> template -> add_js('assets/admin/js/gmap3.min.js');
			$this -> template -> add_js('assets/admin/js/jquery-autocomplete.min.js');

			//meta stuff add here
			$this -> template -> write('meta', '<meta charset="utf-8" />');
			$this -> template -> write('meta', '<meta name="viewport" content="initial-scale=1, maximum-scale=1" />');

			/// common template stuff
			//ECHO 'LOAD HEADER';
			$this -> template -> write_view('header', 'admin/common/header', array());
			//$this->template->write_view('navigation', 'common/navigation', array());
			//$this -> template -> write_view('footer', 'admin/common/footer', array());

			//$message = $this -> session -> flashdata('message');
			//$this -> template -> write_view('flash_message', 'common/flash_message', array('message' => $message));

			// Active navigation highlighting
			if ($this -> uri -> segment(1) == 'tasks')
			{
				$targetclass = 'menu_tasks';
			}
			if ($this -> uri -> segment(1) == 'activity')
			{
				$targetclass = 'menu_activity';
			}
			if ($this -> uri -> segment(1) == 'reports')
			{
				$targetclass = 'menu_reports';
			}
			if ($this -> uri -> segment(1) == 'registers')
			{
				$targetclass = 'menu_registers';
			}
			if (isset($targetclass))
			{
				$this -> template -> write('dom_ready', '$(".' . $targetclass . '").addClass("active");');
			}

			// lets auto create model and cache if the model name is sent!
			if ($model)
			{
				// parent construct sets  up cache, it loads the driver since the model cant!
				$this -> model = new $model();
				// a new DM
			}
		}
	}

	/**
	 * Send email message of given type (activate, forgot_password, etc.)
	 *
	 * @param	string
	 * @param	string
	 * @param	array
	 * @return	void
	 */
	public function _send_email($type, $email, &$data)
	{
		$this -> load -> library('email');
		$this -> email -> from($this -> config -> item('webmaster_email', 'tank_auth'), $this -> config -> item('website_name', 'tank_auth'));
		$this -> email -> reply_to($this -> config -> item('webmaster_email', 'tank_auth'), $this -> config -> item('website_name', 'tank_auth'));
		$this -> email -> to($email);
		$this -> email -> subject(sprintf($this -> lang -> line('auth_subject_' . $type), $this -> config -> item('website_name', 'tank_auth')));
		$this -> email -> message($this -> load -> view('email/' . $type . '-html', $data, TRUE));
		$this -> email -> set_alt_message($this -> load -> view('email/' . $type . '-txt', $data, TRUE));
		$this -> email -> send();
	}

	/**
	 * Show info message
	 *
	 * @param	string
	 * @return	void
	 */
	public function _show_message($message, $redirect = 'user/auth/index')
	{
		$this -> session -> set_flashdata('message', $message);
		redirect($redirect);
	}

	public function addFlashMessage($message)
	{
		$this -> session -> set_flashdata('message', $message);
	}

	function __destruct()
	{

	}

}

class Base_Controller extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * @param Object $model
	 *
	 */
	function __construct($model = null)
	{

		parent::__construct($model);
	}

}
