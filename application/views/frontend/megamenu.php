<script type="text/javascript">
    $(document).ready(
    function(){
                
        jQuery(function() {
                
            jQuery("#menu").megaMenu('hover_fade');
        });
					
					
    });
</script>



<div class="outer-menu">
    <ul id="menu">
        <li><a href="<?php echo site_url(); ?>" class="drop">Home</a></li>
        <li><a href="<?php echo site_url('destinations'); ?>" class="drop">Destinations</a>
            <div class="drop12columns dropcontent dropfirst"><!-- Begin Drop Down Container -->
                <div class="drop-outer">
                    <div class="inner-wrapper">
                        <div class="drop-header">
                            <h1>Discover Our Exclusive Holiday Destinations</h1><br/>


                            <?php
                            $destination = new Destination();
                            $majordestinations = $destination->where('parentdestination', '');
                            $majordestinations = $destination->where('state', 'Active');
                            $majordestinations = $destination->order_by('title', 'asc');
                            $majordestinations = $destination->get();

                            $majorcount = 0;
                            foreach ($majordestinations as $md)
                            {   
                                //$subdestinations = '';
                                //$subdestinations = new Destination();
                                //$subdestinations->where('parentdestination', $md->id);
                                //$subdestinations->where('state', 'Active');
                                //$subdestinations->order_by('title', 'asc');
                                //$subdestinations->limit(3);
                                //$subdestinations->get();
                                //$count = 0;
                                ?>

                                <div class="grid_2">
                                    <div class="destinations-box">
                                        <div class="">
                                            <div class="">
                                                <div class="">
                                                    <div class="menu-header">
                                                        <h3>
                                                            <?php echo $this->teamleaf->link_majordestination($md, htmlspecialchars($md->title)); ?>
                                                              
                                                        </h3>
                                                    </div>

                                                    <div class="menu-dots">
                                                        <div class="menu-thumb">
                                                            <?php 
                                                            $pic = slir("/destinations/" . $md->id . "/profileimage/", $md->profileimage,  $width = 155, $height = 50,$crop = array(
																'width' => '155',
																'height' => '50'
															), 'title="Profile image" class="profile-image"'); 
															
                                                            echo $this->teamleaf->link_majordestination($md, $pic); ?>
                                                            <?php //echo $this->teamleaf->slir("destinations/" . $md->id . "/profileimage/" . $md->profileimage, 'w=65&h=50&c=65:50'); ?>
                                                        </div>
                                                       
                                                        <div class="clear"></div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div> 
                                </div>
    <?php
}
?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </li>
       
        <li><?php echo anchor('find-a-travel-expert', 'Find a Travel Expert'); ?></li>
        <li><?php echo anchor('inspireme', 'Inspire Me'); ?></li>
        <li><?php echo anchor('enquiry', 'Enquiry'); ?></li>
        <li><?php echo anchor('luxurycruises', 'Luxury Cruises'); ?></li>
        <li><?php echo anchor('aboutus', 'About Us'); ?></li>
   
    </ul>
</div>