<div class="latestreviews homepage">
	<h2> Latest reviews... </h2>
	<?php 
		$p = new Blog();
		$p->where('type', 'hotelreview');
		$p->where('state', 'Active');
		$p->limit(4);
		$p->order_by('blogdate', 'desc');
		$p->get();
		
		foreach ($p as $b)
		{


	?>

	<div>
		<div class="first article">
			<div class='greytitle'>
				<?php echo anchor('blogs/viewDetail/' . $b->id, $b->title);?>
			</div>
			<p class="meta">
				<?php $p = new Person($b->creator);?>
				by <?php echo $this->teamleaf->personNameFullFrontend($p);?>,
				<span class='cutetime'><?php echo $b->blogdate;?></span>
			</p>
			<?php echo anchor('blogs/viewDetail/' . $b->id, $this->teamleaf->slir('blogs/' . $b->id . '/' . $b->file1, 'w=65&h=65&c=1:1'), 'class="image"');?>

			<?php
				$excerptwords = str_word_count($b->excerpt);
				
				if ($excerptwords < $this->config->item('summary_latestreviews_minlimit'))
				{
					$output = $b->excerpt . '...<br/>';
					$remainingwords = $this->config->item('summary_latestreviews_maxlimit') - $excerptwords - 1;
					$output .= $this->teamleaf->words(strip_tags($b->content), $remainingwords);
				}
				else
				{
					$output = $this->teamleaf->words(strip_tags($b->excerpt), $this->config->item('summary_latestreviews_maxlimit'));
				}
			?>

			<p>
				<?php echo $output; ?>  ~  <? echo anchor('blogs/viewDetail/' . $b->id, "<span class='readmore'>View Blog.</span>"); ?>
			</p>
		</div>
	</div>
	<div class="clear"></div>
	<?php
		}
	?> 
</div>
