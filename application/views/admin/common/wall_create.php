<script>
	$(function() {
		$( "#tabs" ).tabs();
	});
	</script>
<div id="wall_con" style="min-height: 71px; overflow: hidden;">
	<div id="tabs">
		<ul>
			<li><a href="#tabs-1">Update status</a></li>
			<li><a href="#tabs-2">Post useful link</a></li>
			<li><a href="#tabs-3">Add video</a></li>
			<li><a href="#tabs-4">Add File</a></li>
			<li><a href="#tabs-5">Add image</a></li>
		</ul>
	
	
		<div id="tabs-1">
			
			
			<?php echo form_open( l('_D_WALL_URI_USERS_CREATE') ,'enctype="multipart/form-data" id="form_update_status"'  ); ?>
				<input type="text" value="" name="title" />
				<textarea name="description"></textarea>
				<input type="submit" value="Go!" />
				<input type="hidden" value="status" name="type" />
				<input type="hidden" name="userid" value="<?php echo $wall_userid ;?>" />
				<input type="hidden" name="return" value="<?php echo  base64_encode(current_url());?>" />
			</form>
		</div>
		
		<div id="tabs-2">
			
			
			<?php echo form_open( l('_D_WALL_URI_USERS_CREATE') ,'enctype="multipart/form-data"  id="form_link"' ); ?>
				<input type="text" value="" name="title" />
				<textarea name="description"></textarea>
				<input type="text" name="link" value="" />
				<input type="submit" value="Lets do this!" />
				<input type="hidden" value="link" name="type" />
				<input type="hidden" name="userid" value="<?php echo $wall_userid ;?>" />
				<input type="hidden" name="return" value="<?php echo  base64_encode(current_url());?>" />
			</form>
		</div>
		
		
		
		<div id="tabs-3">
			
			
			<?php echo form_open( l('_D_WALL_URI_USERS_CREATE') ,'enctype="multipart/form-data"  id="form_video"' ); ?>
				<input type="file"   name="file1" />
				<input type="text" value="" name="title" />
				<textarea name="description"></textarea>
				<input type="submit" value="Upload video" />
				<input type="hidden" name="type" value="video" />
				<input type="hidden" name="userid" value="<?php echo $wall_userid ;?>" />
				<input type="hidden" name="return" value="<?php echo  base64_encode(current_url());?>" />
			</form>
		</div>
		
		
		<div id="tabs-4">
			
			<?php echo form_open( l('_D_WALL_URI_USERS_CREATE') ,'enctype="multipart/form-data"  id="form_file"' ); ?>
				<input type="file"   name="file1" />
				<input type="text" value="" name="title" />
				<textarea name="description"></textarea>
				<input type="submit" value="Upload" />
				<input type="hidden" name="type" value="file" />
				<input type="hidden" name="userid" value="<?php echo $wall_userid ;?>" />
				<input type="hidden" name="return" value="<?php echo  base64_encode(current_url());?>" />
			</form>
		</div>
		
		
		<div id="tabs-5">
			
			<?php echo form_open( l('_D_WALL_URI_USERS_CREATE') ,'enctype="multipart/form-data"  id="form_image"' ); ?>
				<input type="file"   name="file1" />
				<input type="text" value="" name="title" />
				<textarea name="description"></textarea>
				<input type="submit" value="Upload" />
				<input type="hidden" name="type" value="image" />
				<input type="hidden" name="userid" value="<?php echo $wall_userid ;?>" />
				<input type="hidden" name="return" value="<?php echo base64_encode(current_url());?>" />
			</form>
		</div>
	
	</div>
	<div id="wall_busy"></div>
</div>