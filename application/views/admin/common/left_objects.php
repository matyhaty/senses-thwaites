<!--
<p><?php echo permAnchor( $this->user , l('_D_URI_PROFILE_DISPLAY') , '<div class="icons profile">Profile</div>');?></p>	
<p><?php echo permAnchor( $this->user , '#' , '<div class="icons inbox">Inbox</div>');?></p>	
<p><?php echo permAnchor( $this->user , l('_D_URI_CLASSES_DISPLAY') , '<div class="icons classes"> ' . get_user_class_count($this->user->id) . ' ' . l('_D_CLASSES') . '</div>');?></p>
<p><?php echo permAnchor( $this->user , l('_D_URI_EVENTS_DISPLAY') , '<div class="icons events"> ' . get_user_event_count($this->user->id) . ' ' . l('_D_EVENTS') . '</div>');?></p>
<p><?php echo permAnchor( $this->user , l('_D_URI_VIDEOS_DISPLAY') , '<div class="icons videos"> 0 ' . l('_D_VIDEOS') . '</div>');?></p>
<p><?php echo permAnchor( $this->user , l('_D_URI_IMAGES_DISPLAY') , '<div class="icons images"> ' . get_user_image_count($this->user->id) . ' ' . l('_D_IMAGES') . '</div>');?></p>
<p><?php echo permAnchor( $this->user , l('_D_URI_JOBS_DISPLAY') , '<div class="icons jobs"> ' . get_user_job_count( $this->user->id ) . ' ' . l('_D_JOBS') . '</div>');?></p>
<p><?php echo permAnchor( $this->user , l('_D_URI_FRIENDS_DISPLAY') , '<div class="icons friends"> 0 ' . l('_D_FRIENDS') . '</div>');?></p>
<p><?php echo permAnchor( $this->user , l('_D_URI_LIKES_DISPLAY') , '<div class="icons likes"> 0 ' . l('_D_LIKES') . '</div>');?></p>
<p><?php echo permAnchor( $this->user , l('_D_URI_RECOMMENDATIONS_DISPLAY') , '<div class="icons recommendations"> 0 ' . l('_D_RECOMMENDATIONS') . '</div>');?></p>
<p><?php echo permAnchor( $this->user , l('_D_URI_GROUPS_DISPLAY') , '<div class="icons groups"> 0 ' . l('_D_GROUPS') . '</div>');?></p>
<p><?php echo permAnchor( $this->user , l('_D_URI_QUESTIONS_DISPLAY') , '<div class="icons questions"> 0 ' . l('_D_QUESTIONS') . '</div>'); ?></p>
-->


<div id="grindbox" class="grey-box">
<h3>Grind box</h3>

	<div class="icons profile"><?php echo permAnchor( $this->user , l('_D_URI_PROFILE_DISPLAY') , 'Profile');?></div>

	<div class="icons inbox"><?php echo permAnchor( $this->user , '#' , 'Inbox');?></div>

	<div class="icons classes"><?php echo permAnchor( $this->user , l('_D_URI_CLASSES_DISPLAY'), get_user_class_count($userid) . ' ' . l('_D_CLASSES'));?></div>

	<div class="icons events"><?php echo permAnchor( $this->user , l('_D_URI_EVENTS_DISPLAY'), get_user_event_count($userid) . ' ' . l('_D_EVENTS'));?></div>

	<div class="icons videos"><?php echo permAnchor( $this->user , l('_D_URI_VIDEOS_DISPLAY'), "0" . ' ' . l('_D_VIDEOS'));?></div>

	<div class="icons images"><?php echo permAnchor( $this->user , l('_D_URI_IMAGES_DISPLAY'), get_user_image_count($userid) . ' ' . l('_D_IMAGES'));?></div>

	<div class="icons jobs"><?php echo permAnchor( $this->user , l('_D_URI_JOBS_DISPLAY'), get_user_job_count($userid) . ' ' . l('_D_JOBS'));?></div>

	<div class="icons friends"><?php echo permAnchor( $this->user , l('_D_URI_FRIENDS_DISPLAY'), "0" . ' ' . l('_D_FRIENDS'));?></div>

	<div class="icons likes"><?php echo permAnchor( $this->user , l('_D_URI_LIKES_DISPLAY'), "0" . ' ' . l('_D_LIKES'));?></div>
	
	<!---
	<p><a href="/recommendations"></a></p><div class="icons recommendations">
	<div class="icons recommendations"><?php echo permAnchor( $this->user , l('_D_URI_RECOMMENDATIONS_DISPLAY'), "0" . ' ' . l('_D_RECOMMENDATIONS'));?></div>
	<p></p>
	
	<p><a href="/groups"></a></p><div class="icons groups">
	<div class="icons groups"><?php echo permAnchor( $this->user , l('_D_URI_GROUPS_DISPLAY'), "0" . ' ' . l('_D_GROUPS'));?></div>
	<p></p>

	<p><a href="/questions"></a></p>
	<div class="icons questions"><?php echo permAnchor( $this->user , l('_D_URI_QUESTIONS_DISPLAY'), "0" . ' ' . l('_D_QUESTIONS'));?></div>
	<p></p>
	-->

</div>

<div class="expand-your-profile">
	<h3>Expand your profile</h3>
	<p>Another way to dream; no ads, <br />advanced search and analytics</p>
</div>
