<!-- Start of Header Section -->
<?php $this->CI = get_instance(); ?>
<div class="grid_18 header">
    <div class='logo-image'>
        <a id="logo" href="/admin"><img src="<?php echo base_url();?>assets/admin/images/layout/teamleaf-logo.png" alt="Teamleaf" /></a>
    </div>
    <div class='busy' style='display:none;position: absolute; top: 0px; left: 50%; margin-left: 125px'>
        <img src='<?php echo base_url(); ?>assets/admin/images/dots.gif'/>
    </div>
    <div class='logo-text'>
        <a href='<?php echo base_url();?>admin'><?php echo SITE_NAME; ?></a>
    </div>
    <div class='logo-end'>
        |
    </div>
    <div class='navigation-text'>
    	
    	<?php if(isAdmin($this->CI->user->id))
		{
			?>
        <a class='menu_orders' href='<?php echo base_url();?>admin/orders/list'>Orders</a> 
        &middot; 
        <a class='menu_blogs' href='<?php echo base_url();?>admin/menus/view'>Menus</a> 
        &middot; 
        <a class='menu_users' href='<?php echo base_url();?>admin/user/view/'>Users</a> 
        &middot; 
        <a class='menu_cms' href='<?php echo base_url();?>admin/cms/view/'>CMS</a> 
        &middot; 
        <?php  } ?>
        </div>
</div>

<?php
    $major = $this->uri->segment(2);
    if ($major == 'destinations')
        $navid = "menu_destinations";
    else if ($major == 'hotels')
        $navid = "menu_hotels";
	 else if ($major == 'itinerary')
        $navid = "menu_itinerarys";
    else if ($major == 'user')
        $navid = "menu_users";
	else if ($major == 'communications')
        $navid = "menu_communications";
    else
        $navid = "";

    if($navid != '')
    {
    ?>
        <script>$(".<?php echo $navid; ?>").addClass('selected');</script>
    <?php } ?>

<div class="grid_6 account-search">
    <div class='account-text'>
        
        <?php 
        
        
        if($this->CI->loggedIn)
        {
            echo "<a href='".base_url()."admin/user/profile/display'>".$this->CI->user->firstname .' '.$this->CI->user->lastname."</a>";
            ?>
            &middot; 
            <a href='<?php echo base_url();?>admin/user/account/'>Account</a>    
            &middot; 
            <?php
            if($this->CI->user->user_admin)
            {
                echo "<a href='".base_url()."admin/home/admin'>Admin</a> &middot; ";
            }
            echo "<a href='".base_url()."admin/user/auth/logout'>Log out</a>";
        }
        
        
        else
        {
            echo "<a href='".base_url()."admin/login'>Log In</a>";
        }
        ?>            
    </div>
    <div class="searchbox">
    	<?php echo form_open('task/tasks/viewSearch'); ?>
        <input type="text" data-behavior="placeholder" placeholder="Search..." name='searcher' data-hotkey="f">
        <?php echo form_close(); ?>
        <dl style="display: none; "></dl>
    </div>
</div>
<div class="clear">
    &nbsp;
</div>
<!-- End of Header Section -->
