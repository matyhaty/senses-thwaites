<hgroup class="title">
	<h2><?php echo $data['title'];?> <a href="#" class="orange-btn-sml rounded">Follow</a></h2>
	<h4>by <span><?php echo $user[0]['firstname'] . " " . $user[0]['lastname']; ?></span></h4>
</hgroup>

<section class="section">
	
	<div class="row">
	
		<div class="col col4-3">
			<div class="media-item">
			<img src="/assets/images/media-image.jpg" alt="" />
			</div>
			
			<div class="media-info">
			<p><?php echo $data['description'];?></p>
			</div>
			
			<!--
			<div class="media-comments">
			<h4>Comments</h4>
			<p>Nunc ipsum dui, lacinia ac sodales ac, porttitor et nibh. Sed malesuada tempor tempor. Donec sit amet imperdiet nunc. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus leo enim, pulvinar non mollis molestie, condimentum eget orci. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed id ornare nisi. </p>
			</div>
			-->
			
		</div>
		
		<?php $this->load->view("common/appearin", array("id" => $id, "type" => $type)); ?>
		
	</div>
	
</section>