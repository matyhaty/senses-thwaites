<div>
<div id="wall_<?php echo $post['id']; ?>" class="wall" style="clear:left">

	<div class="sttext">
	
		<div class="wall_img">
			<?php echo slir(  '/assets/images/profile/' , $post['user']['photo'] , $width=50, $height=50, $crop = array('width'=>'300','height'=>'300' ) , 'title="Profile image"') ;?>
		</div>
	
		<div class="wall_text">
		<a title="Delete update" id="2" href="#" class="stdelete">X</a> 
		<a style="font-weight: bold;" href="my-wall/<?php echo  $post['user']['username']; ?>"><?php echo $post['user']['username']; ?></a></b> 
		<?php switch( $post['type'] ):	
		
			  case 'image' :?>
				<span>added a new photo.</span>	
				<div style="text-align: center;margin:10px 0 ">
					<?php echo slir(  str_replace( $_SERVER['DOCUMENT_ROOT'], '', $post['file_path'] ) , $post['client_name'] , $width=500, $height=0, $crop = array(  ) , 'title="Profile image"') ;?>
				</div>
				<div style="background:#BFFFF7;border-radius:5px;padding:10px;margin:10px;text-align: center">
				<h4><?php echo $post['title']?$post['title']:''; ?></h4>
				<p><?php echo $post['description']?$post['description']:'';  ?></p>
				</div>
		<?php break; ?>
		
		<?php case 'link' :?>
			<span>added a new link.</span>	
			<h4><?php echo $post['title']?$post['title']:''; ?></h4>
			<p><?php echo $post['description'] ?></p>				
				
		<?php break; ?>
		
		<?php case 'video' :?>
			<span>added a new video.</span>	
			<h4><?php echo $post['title']?$post['title']:''; ?></h4>
			<p><?php echo $post['description']; ?></p>	
				
			<iframe src="http://player.vimeo.com/video/<?php echo $post['video_id'] ;?>?title=0&amp;byline=0&amp;portrait=0" width="400" height="300" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
								
				
				
				
		<?php break; ?>
		
		<?php case 'file' : ?>	
			<span>added a new file.</span>	
			<h4><?php echo $post['title']?$post['title']:''; ?></h4>
			<p><?php echo $post['description'] ?></p>
			<p>File attachment</p>
		
		<?php break; ?>
		
		<?php case 'status' :?>
		<?php default:?>
		<p><?php echo $post['description'] ?></p>
		
		<?php endswitch; ?>
		
		
		
		<div class="sttime">
			<?php echo time_since($post['created']); ?> | <a title="Comment" id="2" class="commentopen" href="#">Comment </a>
		</div>
		<div id="stexpandbox">
			<div id="stexpand2"></div>
		</div>
		<?php $this->load->view('common/wall_comment'  , array('post'=>$post )  ); ?>
		</div>
	</div>
	<div class="wall_busy" id="wall_busy_<?php echo $post['id']; ?>"></div>
</div>
<div class="clear"></div>
<br>
<hr />
</div>