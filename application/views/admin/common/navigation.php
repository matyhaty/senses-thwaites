    <!-- Start of Navigation Section -->
    <nav id="navigation" class="inner">
        <ul>
            <li><?php //echo permAnchor( $this->user , l('_D_URI_QUESTIONS_DISPLAY') , 'Q&amp;A'  );?></li>
            <li><?php //echo permAnchor( $this->user , l('_D_URI_JOBS_DISPLAY') , 'Jobs'  );?></li>
            <li><?php //echo permAnchor( $this->user , l('_D_URI_GROUPS_DISPLAY') , 'Groups'  );?></li>
            <li><?php //echo permAnchor( $this->user , l('_D_URI_CLASSES_DISPLAY') , 'Classes'  );?></li>
            <li><?php //echo permAnchor( $this->user , l('_D_URI_EVENTS_DISPLAY') , 'Events'  );?></li>
        </ul>
        
        <span class="share-grow">
        <a class="rounded" href="#">Share</a> &amp; <a class="rounded" href="#">Grow</a>
        </span>
        
    </nav>
    <!-- End of Navigation Section -->