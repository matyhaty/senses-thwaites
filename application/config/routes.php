<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/


$route['default_controller'] = "home/home";
$route['404_override'] = '';


// FROTNEND



// User
// --------------------------------------------------------==========================
// --------------------------------------------------------==========================
$route['admin/user/view'] =     'user/profile/view/';

$route['admin/user/profile/display/(:num)'] =          "user/profile/display/$1";

$route['admin/cms/view'] =     'cm/profile/view/';
$route['admin/cm/profile/display/(:num)'] =          "cm/profile/display/$1";
$route['admin/cm/profile/create/(:num)'] = "cm/profile/create/$1";
$route['admin/cm/profile/create'] = "cm/profile/create";
$route['admin/cm/profile/createmedia/(:num)'] = "cm/profile/createmedia/$1";


$route['admin/menus/view'] =     'designmenu/profile/view/';
$route['admin/menus/profile/display/(:num)'] =          "designmenu/profile/display/$1";
$route['admin/menus/profile/create/(:num)'] = "designmenu/profile/create/$1";
$route['admin/menus/profile/create'] = "designmenu/profile/create";



// General route information
$route['admin/register']     = 'user/register/index';
$route['admin/login']   = 'user/auth/index';
$route['login']   = 'user/auth/frontend_login';


$route['admin/user/auth/logout']   = 'user/auth/logout';
$route['admin/auth/login']  = 'user/auth/index';
$route['admin/user/profile/display']  = 'user/profile/display';
$route['admin/user/auth/change_user_password'] = 'user/auth/change_user_password';
$route['admin/user/auth/change_user_password/(:any)'] = "user/auth/change_user_password/$1";
$route['admin/user/auth/send_again'] = 'user/auth/send_again';
// Account/profile
$route['admin/account']   = "user/profile/index";
$route['admin/profile']   = "user/profile/display";
$route['admin/profile/(:any)'] = "user/profile/display";
$route['admin/user/profile/create/(:num)'] = "user/profile/create/$1";


// --------------------------------------------------------==========================
// --------------------------------------------------------==========================

$route['admin/home/admin']    = 'home/admin';
$route['admin/home/admin/(:any)']  = 'home/admin/$1';
$route['admin/home']     = 'home/adminhome';
$route['admin/init']     = 'home/home/init';
$route['admin']       = 'home/admin';

$route['admin/blog/list']   = 'blog/lister/view';



$route['designmenu']       = 'designmenu/designmenus/index';
$route['designmenu/review']       = 'designmenu/designmenus/review';

$route['designposter']       = 'designposter/designposters/index';
$route['designposter_step2']     = 'designposter/designposters/step2';
$route['designposter_step3']     = 'designposter/designposters/addToCart';

$route['taproom']        = 'home/home/taproom';
$route['resources']        = 'home/home/resources';
$route['news']        = 'home/home/news';
$route['offers']        = 'home/home/offers';

$route['article/(:num)']        = 'home/home/article/$1';

$route['cart/view']        = 'cart/carts/view';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
