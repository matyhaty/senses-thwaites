<?php
/**
 * Class *
 */
class Referrer extends Base_Model
{

	/**
	 * table name
	 *
	 * @var String
	 */
	public $table = 'referrers';
	public $has_one = array();
	public $has_many = array();

	/**
	 * Constructor
	 *
	 * @param unknown_type $id
	 */
	function __construct($id = NULL)
	{
		parent::__construct($id);
	}

	public function getOptions($currentFlag = NULL)
	{
		$optionsArr = $this -> getObject('referrer', null, null, false, array('state !=' => 'Disabled')) -> dm_object -> all_to_array();
		if ($currentFlag)
		{
			$options['selected'] = $currentFlag;
		}
		foreach ($optionsArr as $op)
		{
			$options[$op['id']] = $op['title'];
		}
		return $options;
	}
	
	function getByID($id)
	{
		return $this -> getObject('referrer', $id, null, false, array('state !=' => 'Disabled')) -> dm_object;	
	}


}
