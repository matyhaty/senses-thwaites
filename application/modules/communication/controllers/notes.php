<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
 * Questions class
 *
 */
class Notes extends Base_Controller {

	/**
	 * Constructor
	 *
	 */
	function __construct() {
		parent::__construct();
		$this -> model = new Communication('communication');
		$this -> model -> cache = $this -> cache;
	}

	/**
	 * Main function. This should display either a list of grid view of questions
	 *
	 */
	public function add($communicationID) {
		$this -> template -> write('page_header', 'Edit/Create Communication');
		$formdropdowns = '';
		$createDropDowns = false;
		
		$object = new Note();
		$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/note/forms', 'communication_note');

		if ($this -> input -> post()) {
			// If posted and form validation passes
			if ($this -> form_validation -> run() == TRUE) {
				$data = $this -> input -> post();

				$data['userid'] = $this -> CI -> user -> id;
				$saved = DMZ_Array::from_array($object, $data, null, true);
				// this saves this automatically to the Task Object (e.g. same as $user->save());

				// Save the task
				if ($saved) {
					$s = new Communication($communicationID);
					$s -> save($object);

					$this -> maintaincache -> communication($communicationID);
					$this->maintaincache->communication_note($object -> id);

					redirect('admin/communications/display/' . $communicationID);
				} else {
					$this -> template -> write('validation_errors', 'Failed to save Destination');
				}
			} else {
				$createDropDowns = true;
				$this -> template -> write('validation_errors', validation_errors());
				// with some errors!
			}
		} else {
			$createDropDowns = true;
		}

		if ($createDropDowns) {

		}

		$form = $this -> xml_forms -> createform($formdata['xml'], $object, $formdropdowns, array());
		$this -> template -> write('sheet', $form);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

}

/* End of file questions.php */
/* Location: ./application/controllers/questions.php */
