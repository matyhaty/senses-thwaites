<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
 * Questions class
 *
 */
class Status extends Base_Controller
{

	/**
	 * Constructor
	 *
	 */
	function __construct()
	{
		parent::__construct();
		$this -> model = new Communication('communication');
		$this -> model -> cache = $this -> cache;
	}

	/**
	 * Main function. This should display either a list of grid view of questions
	 *
	 */
	public function update($communicationID, $statusID, $returnTo = 'communication')
	{
		$t = $this -> model -> getObject('communication', $communicationID, null, false, array('state' => 'Active'), '', '') -> dm_object;
		$t -> statusID = $statusID;
		$s = $this -> model -> getObject('statu', $statusID, null, false, array('state' => 'Active'), '', '') -> dm_object;
		
		if($s->id == 7 || $s->id == 6)
		{
			$t->state = 'Complete';
			$returnTo = 'communications';
		}
		
		$t -> save($s);
		
		
		$this -> model -> removeCachedObject('statu', null, $t, array('state' => 'Active'));

		$this -> maintaincache -> communication($communicationID);

		if ($returnTo == 'communication')
		{
			redirect('admin/communications/display/' . $communicationID);
		}
		if ($returnTo == 'communications')
		{
			redirect('admin/communications/list');
		}
	}

	public function update_to_booked($communicationID)
	{
		$object = new Note();
		$data = $this -> input -> post();
		$data['note'] = 'This communication as been marked as Boooked.   <p>Note:' . $data['note'].'</p>';
		$data['userid'] = $this -> CI -> user -> id;
		$saved = DMZ_Array::from_array($object, $data, null, true);

		$s = new Communication($communicationID);
		$s -> save($object);

		$this -> maintaincache -> communication($communicationID);
		$this -> maintaincache -> communication_note($object -> id);

		$this -> update($communicationID, 7);
	}

	public function update_to_closed($communicationID)
	{
		$object = new Note();
		$data = $this -> input -> post();
		$data['note'] = 'This communication as been marked as Closed.   <p>Reason:' . $data['note'].'</p>';
		$data['userid'] = $this -> CI -> user -> id;
		$saved = DMZ_Array::from_array($object, $data, null, true);

		$s = new Communication($communicationID);
		$s -> save($object);

		$this -> maintaincache -> communication($communicationID);
		$this -> maintaincache -> communication_note($object -> id);

		$this -> update($communicationID, 6);
	}

}
