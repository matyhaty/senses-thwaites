<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Profile extends Base_Controller
{

	/**
	 * Constructor for profile page
	 *
	 */
	function __construct()
	{
		parent::__construct('Communication');
	}

	public function display($id)
	{
		$object = $this -> model -> myGet($id);
		$object_statu = $object->getStatus($object);
		$this -> template -> write('page_header', $object -> title);
		
		if(isAdmin($this->CI->user->id))
		{
			$viewFile = 'communication/profile_admin';
			$cacheFile = 'communication-profile_admin';
		}
		else
		{
			$viewFile = 'communication/profile_admin';
			$cacheFile = 'communication-profile_admin';
		}	
		
		// get status
		
		if($this->CI->user->id == $object->info_travelexpert && $object_statu->id < 3)
		{
			// We have them looking at this for the first time... Move status on...
			// setting status to '3' which is 'In Progress'
			$s = $this -> model ->getObject('statu', 3, null, false, array('state' => 'Active'), '', '') -> dm_object;
			$s->save($object);

			$n = new Note();
			$data['note'] = 'Viewed this commication for the first time.';
			$saveNote = DMZ_Array::from_array($n, $data, null, true);
			$n -> save($object);
			
			$this->maintaincache->communication($id);
			$this->maintaincache->communication_status($id);
			$this->maintaincache->communication_note($id);
		}
		
		if ($cacheRes = $this -> maintaincache -> getCache('views/communications/'.$cacheFile.'-' . $id . '.txt'))
		{
			$data['markup'] = $cacheRes;
			$this -> template -> write('sheet', $data['markup']);
		}
		else
		{
			$c = new Communication();
			$data['object'] = $object;
			$data['object_statu'] = $c->getStatus($object);
			$data['object_status'] = $object->status($object);
			$data['object_notes'] = $c->getNotes($object);
			$rt = $this -> load -> view($viewFile, $data, true);
			$this -> template -> write('sheet', $rt);
			$this -> maintaincache -> setCache('views/communications/'.$cacheFile.'-' . $id . '.txt', $rt);
		}
		// Render template
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}



	public function index()
	{
		$this -> display($username = '');
	}

	public function view()
	{
		$this -> template -> write('page_header', l('_D_MY') . ' ' . l('_D_QUESTIONS'));
		$this -> load -> library('session');
		$this -> load -> helper('url');
		$userid = $this -> session -> userdata('userid');
		$data['assets'] = $this -> model -> getAssets();
		// super chain!
		$this -> template -> write_view('sheet', 'asset/view', $data);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

	/**
	 * Creates a User
	 *
	 */

	public function edit($id)
	{
		$this -> create($id, true);
	}

	public function create($id = null, $edit = false)
	{
		$this -> template -> write('page_header', 'Edit/Create Communication');
		$formdropdowns = '';
		$createDropDowns = false;

		if ($id)
		{
			$object = new Communication($id);
			$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/communication/forms', 'communication');
		}
		else
		{
			$object = new Communication();
			$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/communication/forms', 'communication');
		}


		if ($this -> input -> post())
		{
			// If posted and form validation passes
			if ($this -> form_validation -> run() == TRUE)
			{
				$data = $this -> input -> post();

				//$userid = $this -> session -> userdata('userid');
				$saved = DMZ_Array::from_array($object, $data, null, true);
				// this saves this automatically to the Task Object (e.g. same as $user->save());

				// Save the task
				if ($saved)
				{
					$s = new Statu($data['status']);
					$s->save($object);
					
					$n = new Note();
					$data['note'] = 'Note Created / editted';
					$saveNote = DMZ_Array::from_array($n, $data, null, true);
					$n -> save($object);
					
					$this -> maintaincache -> communication($object -> id);
					$this->maintaincache->communication_status($object -> id);
					
					
					
					
					redirect('admin/communications/display/'.$object->id);
				}
				else
				{
					$this -> template -> write('validation_errors', 'Failed to save Destination');
				}
			}
			else
			{
				$createDropDowns = true;
				$this -> template -> write('validation_errors', validation_errors());
				// with some errors!
			}
		}
		else
		{
			$createDropDowns = true;
		}

		if ($createDropDowns)
		{
			// we need any drop downs to be created here
			$u = new User();
			$formdropdowns['info_travelexpert'] = $u -> getUserOptions($object -> info_travelexpert);
			$s = new Statu();
			$refer = new Referrer();
			$formdropdowns['info_lead_source'] = $refer -> getOptions($object -> info_lead_source);
			$formdropdowns['status'] = $s -> getOptions($object -> status);
			$formdropdowns['state'] = $object -> getStateOptions($object -> state);
		}

		$form = $this -> xml_forms -> createform($formdata['xml'], $object, $formdropdowns, array());
		$this -> template -> write('sheet', $form);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}



	public function booking_edit($id = null, $edit = false)
	{
		$this -> template -> write('page_header', 'Edit/Create Booking Information');
		$formdropdowns = '';
		$createDropDowns = false;

		if ($id)
		{
			$object = new Communication($id);
			$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/communication/forms', 'booking');
		}
		else
		{
			$object = new Communication();
			$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/communication/forms', 'booking');
		}


		if ($this -> input -> post())
		{
	
			// If posted and form validation passes
			if ($this -> form_validation -> run() == TRUE)
			{
				
				$data = $this -> input -> post();

				//$userid = $this -> session -> userdata('userid');
				$saved = DMZ_Array::from_array($object, $data, null, true);
				// this saves this automatically to the Task Object (e.g. same as $user->save());

				// Save the task
				if ($saved)
				{
					$this -> maintaincache -> communication($object -> id);
					redirect('admin/communications/display/'.$object->id);
				}
				else
				{
					echo 'failled';
					$this -> template -> write('validation_errors', 'Failed to save Communication');
				}
			}
			else
			{
				$createDropDowns = true;
				$this -> template -> write('validation_errors', validation_errors());
				// with some errors!
			}
		}
		else
		{
			$createDropDowns = true;
		}

		if ($createDropDowns)
		{
			// we need any drop downs to be created here
			//$u = new User();
			//$formdropdowns['info_travelexpert'] = $u -> getUserOptions($object -> expertID);
			//$s = new Statu();
			//$refer = new Referrer();
			//$formdropdowns['info_lead_source'] = $refer -> getOptions($object -> expertID);
			//$formdropdowns['status'] = $s -> getOptions($object -> expertID);
		}

		$form = $this -> xml_forms -> createform($formdata['xml'], $object, $formdropdowns, array());
		$this -> template -> write('sheet', $form);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}


	public function finance_edit($id = null, $edit = false)
	{
		$this -> template -> write('page_header', 'Edit/Create Booking Information');
		$formdropdowns = '';
		$createDropDowns = false;

		if ($id)
		{
			$object = new Communication($id);
			$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/communication/forms', 'finance');
		}
		else
		{
			$object = new Communication();
			$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/communication/forms', 'finance');
		}


		if ($this -> input -> post())
		{
	
			// If posted and form validation passes
			if ($this -> form_validation -> run() == TRUE)
			{
				
				$data = $this -> input -> post();

				//$userid = $this -> session -> userdata('userid');
				$saved = DMZ_Array::from_array($object, $data, null, true);
				// this saves this automatically to the Task Object (e.g. same as $user->save());

				// Save the task
				if ($saved)
				{
					$this -> maintaincache -> communication($object -> id);
					redirect('admin/communications/display/'.$object->id);
				}
				else
				{
					echo 'failled';
					$this -> template -> write('validation_errors', 'Failed to save Communication');
				}
			}
			else
			{
				$createDropDowns = true;
				$this -> template -> write('validation_errors', validation_errors());
				// with some errors!
			}
		}
		else
		{
			$createDropDowns = true;
		}

		if ($createDropDowns)
		{
			// we need any drop downs to be created here
			//$u = new User();
			//$formdropdowns['info_travelexpert'] = $u -> getUserOptions($object -> expertID);
			//$s = new Statu();
			//$refer = new Referrer();
			//$formdropdowns['info_lead_source'] = $refer -> getOptions($object -> expertID);
			//$formdropdowns['status'] = $s -> getOptions($object -> expertID);
		}

		$form = $this -> xml_forms -> createform($formdata['xml'], $object, $formdropdowns, array());
		$this -> template -> write('sheet', $form);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}
	
	
	public function status_edit($id = null, $edit = false)
	{
		$this -> template -> write('page_header', 'Edit/Create Booking Information');
		$formdropdowns = '';
		$createDropDowns = false;

		if ($id)
		{
			$object = new Communication($id);
			$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/communication/forms', 'status');
		}
		else
		{
			$object = new Communication();
			$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/communication/forms', 'status');
		}


		if ($this -> input -> post())
		{
	
			// If posted and form validation passes
			if ($this -> form_validation -> run() == TRUE)
			{
				
				$data = $this -> input -> post();

				//$userid = $this -> session -> userdata('userid');
				$saved = DMZ_Array::from_array($object, $data, null, true);
				// this saves this automatically to the Task Object (e.g. same as $user->save());

				// Save the task
				if ($saved)
				{
					$this -> maintaincache -> communication($object -> id);
					redirect('admin/communications/display/'.$object->id);
				}
				else
				{
					echo 'failled';
					$this -> template -> write('validation_errors', 'Failed to save Communication');
				}
			}
			else
			{
				$createDropDowns = true;
				$this -> template -> write('validation_errors', validation_errors());
				// with some errors!
			}
		}
		else
		{
			$createDropDowns = true;
		}

		if ($createDropDowns)
		{
			// we need any drop downs to be created here
			//$u = new User();
			//$formdropdowns['info_travelexpert'] = $u -> getUserOptions($object -> expertID);
			//$s = new Statu();
			//$refer = new Referrer();
			//$formdropdowns['info_lead_source'] = $refer -> getOptions($object -> expertID);
			//$formdropdowns['status'] = $s -> getOptions($object -> expertID);
		}

		$form = $this -> xml_forms -> createform($formdata['xml'], $object, $formdropdowns, array());
		$this -> template -> write('sheet', $form);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}
	

}

//end class
