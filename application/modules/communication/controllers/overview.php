<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Overview extends Base_Controller
{

	/**
	 * Constructor for profile page
	 *
	 */
	function __construct()
	{
		parent::__construct('Destination');
	}

	public function index()
	{
		$this -> setTemplate('page');

		//$this -> meta_tags -> set_meta_tag('author', 'Designer Travel');
		$this -> meta_tags -> add_keyword('Luxury resorts, luxury hotels, 5 star accommodation, romantic destinations, the high life, perfect getway, Boutique hotel, stylish hotel, Tailor Made Destinations, most exotic locations');
		$this -> meta_tags -> set_title('Find your Perfect Luxury Hotel Destination with Designer Travel');
		$this -> meta_tags -> set_meta_tag('description', 'Luxury, Tailor Made Destinations offering stunning, exclusive hotels and elegant resorts and villas in some of the most exotic locations in the world.');
		$this -> template -> write('page_meta', $this -> meta_tags -> generate_meta_tags());
		
		$this->data = '';
		$this -> data['wearetheexperts'] = $this->teamleaf->getCacheView('views/common', 'wearetheexperts', 'home', 'wearetheexperts');
		$this -> data['newsletter'] = $this->teamleaf->getCacheView('views/common', 'newsletter', 'home', 'newsletter');
		$this -> template -> write_view('content', 'destination/frontend_overview', $this -> data);

		$this -> data = '';

		// Render template
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

}

//end class
