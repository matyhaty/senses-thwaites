<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lister extends Base_Controller
{
    function __construct()
    {
        $this->load->library('form_validation');
        $this->load->helper(array(
            'form',
            'url'
        ));
        parent::__construct('Communication');

    }

    public function view()
    {
    	
		
		if(isAdmin($this->CI->user->id))
		{
			$data['communications'] = $this->model->myGetsAdmin();
		}
		else
		{
			$data['communications'] = $this->model->myGets($this->CI->user->id);
		}
	
        $this->template->write('page_header', 'Communications');
        $userid = $this->session->userdata('userid');
        
        // super chain!
        $this->template->write_view('sheet', 'communication/listview', $data);
        $this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);
    }
}

//end class
