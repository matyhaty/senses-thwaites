<?php
$referrer = new Referrer();
$thisReferrer = $referrer -> getByID($object -> info_lead_source);

$s = new Statu($object -> status);
?>

<div class='sheet'>
	<div class='innersheet'>
	
		<div class="grid_15 page-header">
	        <div class = 'taskColours'>
        		<div class='number'>
            		<span>
            			Enquirer Name: <?php echo $object -> enquiry_name; ?>
            		</span>
            	</div>
            	<div class='status'>
            		<span>
            			Contact Via: <?php echo $object -> enquiry_email; ?> or <?php echo $object -> enquiry_telephone; ?>
            		</span>
            	</div>
            	<div class='clear'></div>
            	<div class='reference'>
            		<span>
            			Contact them: <?php echo $object -> enquiry_contacttime; ?>
            		</span>
        		</div>
        			
        		<div class='subreference'>
        			<span>
        				(based on enquiry date of: <?php echo $this -> teamleaf -> date($object -> enquiry_date); ?>)
        			</span>
        		</div>
        		<div class='clear'></div>
        		<div class='client'>
        			<span>
        				 Assigned to: <?php echo get_user_fullname($object -> info_travelexpert); ?>
        			 </span>
        		</div>
        		
        		
        	</div>
	    </div>
	    
	    <div class="grid_9 page-options">
	        <div class="option" style="width:1px; min-width: 10px">
                
            </div>
	        <div class='option'>
	            <a class="tooltiptrigger" id='taskstatus'>
	                <div class='title'>
	                     Enquiry Status
	                </div>
	                <div class='subtitle'>
	                    Update Status
	                </div> 
	            </a>
	        </div>
	    
		    <div class="tooltip" id='taskstatus_target'>
                <div class="arrowtopright"></div>
                <div class="titleframe">
                    <div class='title'>
                        Enquiry Status
                    </div>
                </div>
                <div class='content'>
                    <div class='title'>
                        Current Status
                    </div>
                    <div class='info'>
                    	<div style='height:11px; padding:3px; margin-right:10px; background-color: #<?php echo $object_statu -> colour; ?>; color: #<?php echo $this -> teamleaf -> getContrastColor($object_statu -> colour); ?>'>
                        	<?php echo $object_statu -> title; ?>
                       </div>
                       
                    </div>
                    <div class='title'>
                        Update To:
                    </div>
                    <div class='info'>
                        <?php

						foreach ($object_status as $s) {
							
							if ($s -> id == 7)
							{
							
									echo '<div class="dropdown_status" style="border-left: solid 4px #' . $s -> colour . '">' . anchor('admin/communication/status/update/' . $object -> id . '/' . $s -> id, $s -> title, array(
										'class' => 'confirmform',
										'formid' => 'booked-form'
									)) . '</div>';
							}
							else if ($s -> id == 6)
							{
									echo '<div class="dropdown_status" style="border-left: solid 4px #' . $s -> colour . '">' . anchor('admin/communication/status/update/' . $object -> id . '/' . $s -> id, $s -> title, array(
										'class' => 'confirmform',
										'formid' => 'closed-form'
									)) . '</div>';
							}
							
							else
							{
								echo '<div class="dropdown_status" style="border-left: solid 4px #' . $s -> colour . '">' . anchor('admin/communication/status/update/' . $object -> id . '/' . $s -> id, $s -> title) . '</div>';
							}
						}
                        ?>
                        <br/>
                    </div>
                </div>
			</div>
			
			<!-- ------------------------- -->
			<!-- ------------------------- -->
			<!-- ------------------------- -->
			
			<div id="booked-form" title="Marked as Booked">
			  <p class="validateTips">Complete a note and confirm.</p>
			 
			  <?php echo form_open('admin/communications/status/update_to_booked/'.$object->id); ?>
			  <fieldset>
			 	<div class='form-item'>
				<div class='form-textarea'>

			    <?php 
			    	$data = array(
		              'name'        => 'note',
		              'id'          => 'note',
		              'style'       => 'width:300px; min-height:100px; height:100px;',
		            );
            		echo form_textarea($data);
				?>
			</div>
			</div>
			  </fieldset>
			  <?php echo form_submit('mysubmit', 'Submit Booking'); ?>
			  <?php echo form_close(); ?>
			</div>
					
					
			<script>
				$( "#booked-form" ).dialog({
			      autoOpen: false,
			      height: 300,
			      width: 350,
			      modal: true,
				});
			</script>	
			
			<!-- ------------------------- -->
			<!-- ------------------------- -->
			<!-- ------------------------- -->
			
			
			<div id="closed-form" title="Marked as Closed">
			  <p class="validateTips">Complete a note and confirm.</p>
			 
			  <?php echo form_open('admin/communications/status/update_to_closed/'.$object->id); ?>
			  <fieldset>
			 	<div class='form-item'>
				<div class='form-textarea'>

			    <?php 
			    	$data = array(
		              'name'        => 'note',
		              'id'          => 'note',
		              'style'       => 'width:300px; min-height:100px; height:100px;',
		            );
            		echo form_textarea($data);
				?>
			</div>
			</div>
			  </fieldset>
			  <?php echo form_submit('mysubmit', 'Submit Booking'); ?>
			  <?php echo form_close(); ?>
			</div>
					
					
			<script>
				$( "#closed-form" ).dialog({
			      autoOpen: false,
			      height: 300,
			      width: 350,
			      modal: true,
				});
			</script>	
			
			<!-- ------------------------- -->
			<!-- ------------------------- -->
			<!-- ------------------------- -->
			

			<div class='option'>
	            <a class="tooltiptrigger" id='taskstatus'>
	                <div class='title'>
	                     Emails
	                </div>
	                <div class='subtitle'>
	                    Update &amp; Prompt the assignee
	                </div> 
	            </a>
	       
			</div>
			
			<div class="tooltip" id='taskstatus_target'>
                <div class="arrowtopright"></div>
                <div class="titleframe">
                    <div class='title'>
                        Communication Log
                    </div>
                </div>
                <div class='content'>
                    <div class='title'>
                        Previous Emails
                    </div>
                    <div class='info'>
                    	<div style='height:11px; padding:3px; margin-right:10px; background-color: #<?php echo $object_statu -> colour; ?>; color: #<?php echo $this -> teamleaf -> getContrastColor($object_statu -> colour); ?>'>
                        	<?php //echo $object_statu -> title; ?>
                       </div>
                       
                    </div>
                    <div class='title'>
                        Generate Email
                    </div>
                    <div class='info'>
                    	Emails are currently disabled.
                        <?php

						foreach ($object_status as $s) {

							//	echo '<div class="dropdown_status" style="border-left: solid 4px #' . $s -> colour . '">' . anchor('task/status/update/' . $object -> id . '/' . $s -> id, $s -> title) . '</div>';

						}
                        ?>
                        <br/>
                    </div>
                </div>
			</div>
			
			
			
			<div class='clear'></div>
		</div>
		<div class='clear'></div>
	

		
	
		<div class="grid_12">
	        <div class="profile-segment">
	            <div class='segment-header'>
	                <div class='title'>Communication Information</div>
	                <div class='option'><a href='<?php echo base_url(); ?>admin/communications/edit/<?php echo $object -> id; ?>'>Edit</a></div>
	            </div>
	            
	            <div class='segment-content'>
	                <table class='basic_information tt'>
	                	<tr>
	                        <td>Assigned To:</td>
	                        <td><?php echo get_user_fullname($object -> info_travelexpert); ?></td>
	                    </tr>
	                    <tr>
	                        <td>Customer Name</td>
	                        <td><?php echo $object -> enquiry_name; ?></td>
	                    </tr>
	                    <tr>
	                        <td>Source</td>
	                        <td><?php echo $thisReferrer -> title; ?></td>
	                    </tr>
	                    <tr>
	                        <td>Equiry Date</td>
	                        <td>
	                        	<?php echo $this -> teamleaf -> date($object -> enquiry_date); ?>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td>File Number</td>
	                        <td>
	                        	<?php echo $object -> info_file_number; ?>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td>Status</td>
	                        <td>
	                        	<?php echo $object_statu -> title; ?>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td>State</td>
	                        <td>
	                        	<?php echo $object -> state; ?>
	                        </td>
	                    </tr>
	                    
	                    <tr>
	                        <td>Last Updated</td>
	                        <td>
	                        	<?php echo $object -> updated; ?>
	                        </td>
	                    </tr>
	                    
	                    <tr>
	                        <td>Updated By:</td>
	                        <td>
	                        	<?php echo get_user_fullname($object -> updated_by); ?>
	                        </td>
	                    </tr>
	                    
	                    
	                </table>
	            </div>
	            
	            <div class="seo-segment">
		            <div class='segment-header'>
		                <div class='title'>Enquiry Info</div>
		                <div class='option'><a href='<?php echo base_url(); ?>admin/communications/edit/<?php echo $object -> id; ?>'>Edit</a></div>
		            </div>
	            
		            <div class='segment-content'>
		                <table class='basic_information tt'>
		                	<tr>
		                        <td>State</td>
		                        <td>
		                        	<?php echo $object -> enquiry_date; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Name</td>
		                        <td>
		                        	<?php echo $object -> enquiry_name; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Telephone</td>
		                        <td>
		                        	<?php echo $object -> enquiry_telephone; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Email</td>
		                        <td>
		                        	<?php echo $object -> enquiry_email; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>When to contact:</td>
		                        <td>
		                        	<?php echo $object -> enquiry_contacttime; ?>
		                        </td>
		                    </tr>
		                </table>
		            </div>
				</div>
				
				<div class="seo-segment">
		            <div class='segment-header'>
		                <div class='title'>Requested Holiday</div>
		                <div class='option'><a href='<?php echo base_url(); ?>admin/communications/edit/<?php echo $object -> id; ?>'>Edit</a></div>
	           
		            </div>
	            
		            <div class='segment-content'>
		                <table class='basic_information tt'>
		                	<tr>
		                        <td>Departure Date</td>
		                        <td>
		                        	<?php echo $object -> holiday_departure_date; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Flex?</td>
		                        <td>
		                        	<?php echo $object -> holiday_flexible; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Departure Airport:</td>
		                        <td>
		                        	<?php echo $object -> holiday_departure_airport; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Duration:</td>
		                        <td>
		                        	<?php echo $object -> holiday_duration; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Destination:</td>
		                        <td>
		                        	<?php echo $object -> holiday_destination; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Hotel:</td>
		                        <td>
		                        	<?php echo $object -> holiday_hotel; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Room Type:</td>
		                        <td>
		                        	<?php echo $object -> holiday_room_type; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Board:</td>
		                        <td>
		                        	<?php echo $object -> holiday_board; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>No Adults:</td>
		                        <td>
		                        	<?php echo $object -> holiday_adults; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>No Children:</td>
		                        <td>
		                        	<?php echo $object -> holiday_chidren; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Child Ages:</td>
		                        <td>
		                        	<?php echo $object -> holiday_child_ages; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Budget:</td>
		                        <td>
		                        	<?php echo $object -> holiday_budget; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Note from enquirer:</td>
		                        <td>
		                        	<?php echo $object -> holiday_notes; ?>
		                        </td>
		                    </tr>
		                    
		                </table>
		            </div>
				</div>
				
				<div class="seo-segment">
		            <div class='segment-header'>
		                <div class='title'>Finance</div>
		             	<div class='option'><a href='<?php echo base_url(); ?>admin/communications/finance_edit/<?php echo $object -> id; ?>'>Edit</a></div>
		            </div>
	            
		            <div class='segment-content'>
		               <table class='basic_information tt'>
							<tr>
		                        <td>Travel Cost:</td>
		                        <td>
		                        	<?php echo $object -> finances_travelcost; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Discounts:</td>
		                        <td>
		                        	<?php echo $object -> finances_discounts; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Net Commission:</td>
		                        <td>
		                        	<?php echo $object -> finances_netcommission; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Vendor Commission:</td>
		                        <td>
		                        	<?php echo $object -> finances_vendorcommission; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Homeworker Commission:</td>
		                        <td>
		                        	<?php echo $object -> finances_homeworkercommission; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Profit:</td>
		                        <td>
		                        	<?php echo $object -> finances_profit; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Incentives:</td>
		                        <td>
		                        	<?php echo $object -> finances_incentives; ?>
		                        </td>
		                    </tr>
		                    
		                     <tr>
		                        <td>Incentive Claim Code(s):</td>
		                        <td>
		                        	<?php echo $object -> finances_incentivesclaimcode; ?>
		                        </td>
		                    </tr>
					</table>
		            </div>
				</div>
				<div class='clear'></div>
				<div class='segment-header'>
	                <div class='title'>Booking:</div>
	                <div class='option'><a href='<?php echo base_url(); ?>admin/communications/booking_edit/<?php echo $object -> id; ?>'>Edit</a></div>
	           
  				</div>
				<div class='segment-content'>
		                <table class='basic_information tt'>
		                	<tr>
		                        <td>Booking Date</td>
		                        <td>
		                        	<?php echo $object -> booking_date; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Booking Reference</td>
		                        <td>
		                        	<?php echo $object -> booking_reference; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Booking Supplier</td>
		                        <td>
		                        	<?php echo $object -> booking_supplier; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Booking Traveldate</td>
		                        <td>
		                        	<?php echo $object -> booking_traveldate; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Booking Destination:</td>
		                        <td>
		                        	<?php echo $object -> booking_destination; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Booking Hotel:</td>
		                        <td>
		                        	<?php echo $object -> booking_hotel; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Booking Airport:</td>
		                        <td>
		                        	<?php echo $object -> booking_airport; ?>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td>Booking Room Type:</td>
		                        <td>
		                        	<?php echo $object -> booking_room_type; ?>
		                        </td>
		                    </tr>             
		                </table>
		            </div>
	            <div class='clear'></div>	
				
				
            </div>
       	</div>

		<div class="grid_12">
	        <div class="profile-segment">
	            <div class='segment-header'>
	                <div class='title'>Notes &amp; Comments:</div>
	                <div class='option'><a href='<?php echo base_url(); ?>admin/communications/note_add/<?php echo $object -> id; ?>'>Add note</a></div>
	           
  				</div>
	            
	            <div class='segment-content'>
		        	<?php
		        	foreach($object_notes as $note)
					{
						
						$user = new User($note->created_by);
						?>
						<div style='padding-bottom:10px'>
							<div class='image' style='float: left'>
					            <?php echo slir('/users/profileimages/', $user -> profileimage, $width = 60, $height = 60, $crop = array(
									'width' => '55',
									'height' => '55'
								), 'title="Profile image" class="profile-image"');
					            ?>
					        </div>
					        <div class='info' style='float: left; padding-left:10px; max-width:450px'>
					        	<?php echo $user->firstname; ?> <?php echo $user->surname; ?> added:<br/>
					        	<span style='color: grey;'>on the <?php echo $this->teamleaf->date($note->created); ?></span>
					        	<div style='padding-top:5px'>
					        		<?php echo $note->note; ?>
					        	</div>
					        </div>
					        <div class='clear'></div>
				        </div>
				        
						<?php
						
					}
		        	
		        	?>        
		        </div>
	            <div class='clear'></div>	
	            
	            
				
				
				
            </div>
        </div>

        <div class='clear'></div>
    </div>
</div>