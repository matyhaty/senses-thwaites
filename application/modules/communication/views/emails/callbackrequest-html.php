<?php

$e = new Communication($enquiryID);

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Call back request for Designer Travel</title>
	</head>
	<body>
		<div style="max-width: 800px; margin: 0; padding: 30px 0;">
			<table width="80%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="5%"></td>
					<td align="left" width="95%" style="font: 13px/18px Arial, Helvetica, sans-serif;"><h2 style="font: normal 20px/23px Arial, Helvetica, sans-serif; margin: 0; padding: 0 0 18px; color: black;">Call back Request</h2>Somebody wants you.....
					<br />
					Call Back:
					<br />
					<br />
					<big style="font: 16px/18px Arial, Helvetica, sans-serif;" style="color: #3366cc;"><?php echo $e->enquiry_telephone; ?></b></big>
					<br />
					<br />
					Full information:
					<br />
					Email: <?php echo $e->email_to; ?>
					<br />
					Name: <?php echo $e->enquiry_name; ?>
					<br />
				
					Comments: <?php echo $e->holiday_notes; ?>
					<br />
				
					Leader/Referrer: <?php echo $e->info_lead_source; ?>
					
					<br />
					<br />
					Please remember to mark your enquiry dealt with in the administraion area.
					<br/><br/>
					'Good Times',
					<br />
					The Designer Travel Team
					<br/>
					<?php echo site_url();?></td>
				</tr>
			</table>
		</div>
	</body>
</html>