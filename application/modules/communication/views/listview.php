<div class='sheet'>
    <div class='innersheet'>
        <!-- Start of Content of Sheet -->
        <div class="grid_8 page-header">
            <div class='title'>
                Communication Register
            </div>
            <div class='subtitle'>
                Current live communications on the system.
            </div>
        </div>
        <div class="grid_16 page-options">
        	 
            
            
            <div class='option'>
                <a class="" id='createatask' href='<?php echo site_url('admin/communications/create'); ?>'>
                <div class='title'>
                    Create a New Communication
                </div>
                <div class='subtitle'>
                    Quick add a new enquiry.
                </div> </a>
            </div>
            <div class='option'>
                <a class="" id='createatask' href='<?php echo site_url('admin/report/show'); ?>'>
                <div class='title'>
                    Get Report
                </div>
                <div class='subtitle'>
                    Create your report
                </div> </a>
            </div>
            
    
        </div>
        <div class="clear"></div>
        <div class="grid_24 datatable">
            
            <div class='somethingelse'></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display bordered-table" id="user_table" width="100%">
                <thead>
                    <tr>
                        <th class='idColumn'>No:</th>
                        <th>Source</th>
                        <th>Travel Expert</th>
                        <th>Enquiry Date</th>
                        <th>Enquiry Name</th>
            			<th>File Number</th>
                        <th class='stateCodlumn'>Status</th>
                        <th class='stateColumn'>State</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($communications as $item)
                    {
                    	$referrer = new Referrer();
						$thisReferrer = $referrer->getByID($item -> info_lead_source);
						
						$cs = new Communication();
						$status = $cs->getStatus($item);
						//$md = new Destination($item -> parentdestination);
                    ?>

                    <tr id='<?php echo $item -> id; ?>'> 
                        <td><?php echo $item -> id; ?></td>
                        

                        <td>
                        	 <span style='padding: 1px 3px; background-color: #<?php echo $thisReferrer -> colour; ?>; color: #<?php echo $this->teamleaf->getContrastColor($thisReferrer -> colour);?>'>
                        	 	<?php echo $thisReferrer -> title; ?>
                        	 </span>
                        </td>
                        <td><?php echo get_user_fullname($item -> info_travelexpert); ?></td>
                        <td><?php echo $this->teamleaf->date($item -> enquiry_date); ?></td>
                        <td><?php echo $item -> enquiry_name; ?></td>
                        <td><?php echo $item -> info_file_number; ?></td>
                        <td>
							<span style='padding: 1px 3px; background-color: #<?php echo $status -> colour; ?>; color: #<?php echo $this->teamleaf->getContrastColor($status -> colour);?>'>
                        	 	<?php echo $status -> title; ?>
                        	 </span>
						</td>
                        <td><?php echo $item->state; ?></td>
                    </tr>
                    <?php
					}
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th class='idColumn'>No:</th>
                        <th>Source</th>
                        <th>Travel Expert</th>
                        <th>Enquiry Date</th>
                        <th>Enquiry Name</th>
            			<th>File Number</th>
                        <th class='stateCodlumn'>Status</th>
                        <th class='stateColumn'>State</th>
                    </tr>
                </tfoot>
              
            </table>
            
            
            <script type="text/javascript" charset="utf-8">
               $(document).ready(function() {
                    $('#user_table').dataTable({
                        'bJQueryUI' : true,
                        "sDom" : '<"top"  <"info"i>   <"show"l>  <"pagination"p> T <"search"f> >rt<"bottom"><"clear">',
                       "oTableTools": {
							"sSwfPath": "<?php echo base_url();?>assets/admin/swf/copy_csv_xls_pdf.swf"
						},
                        "oLanguage" : {
                            "sInfo" : "_TOTAL_ entries (_START_ to _END_)",
                            "sSearch" : "",
                        },
                        'iDisplayLength' : 50,
                        "aaSorting": [[ 0, "desc" ]],
                    });

                    $('#user_table tr').live('click', function() {
                        if(this.id) {
                            //alert(this.id);
                            window.location = '<?php echo base_url(); ?>admin/communications/display/'+this.id;
                        }
                    });
                });

            </script>
            
            
        </div>
        <div class="clear"></div>
        <!-- End of Content of Sheet -->
        <!-- Start of Content of Form area -->
        
    </div>
</div>