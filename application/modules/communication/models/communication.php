<?php
/**
 * User Class
 *
 */
class Communication extends Base_Model
{

	public $table = 'communications';
	public $has_one = array('statu');
	public $has_many = array(
		'user',
		'note'
	);

	/**
	 * Cons
	 *
	 * @param Int $id
	 */
	function __construct($id = null)
	{
		parent::__construct($id);
	}

	public function myGets($userID, $state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array(
				'info_travelexpert' => $userID,
				'state' => $state
			);
		}
		$sqlArray['orderby'] = array('id' => 'desc');
		return $this -> getObjectv2($sqlArray, strtolower(get_class($this))) -> dm_object;
	}

	public function myGetsAdmin($state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			//$sqlArray['where'] = array('state' => $state);
		}
		$sqlArray['orderby'] = array('id' => 'desc');
		return $this -> getObjectv2($sqlArray, strtolower(get_class($this))) -> dm_object;
	}
	
	public function myGetsAdminMonth($state = 'Active', $date)
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array('state' => $state);
		}
		$sqlArray['orderby'] = array('id' => 'desc');
		return $this -> getObjectv2($sqlArray, strtolower(get_class($this))) -> dm_object;
	}

	public function myGet($id, $state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			//$sqlArray['where'] = array('state' => $state);
		}
		return $this -> getObjectv2($sqlArray, strtolower(get_class($this)), $id) -> dm_object;
	}

	public function myGetBySlug($slugify, $state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array(
				'state' => $state,
				'slugify' => $slugify
			);
		}
		return $this -> getObjectv2($sqlArray, strtolower(get_class($this))) -> dm_object;
	}

	public function status($object, $state = 'Active')
	{
		// GETs all status, there is no relationships in DT for comms to status!!
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array('state' => $state);
		}
		return $this -> getObjectv2($sqlArray, 'statu', null) -> dm_object;
	}

	public function getStatus($communication)
	{
		return $this -> getObject('statu', null, $communication, null, array('state' => 'Active'), '', '') -> dm_object;
	}

	public function getNotes($communication)
	{
		return $this -> getObject('note', null, $communication, null, array('state' => 'Active'), '', '') -> dm_object;
	}

	function enquiryalert($enquiryID)
	{
		log_message('error', 'Enquiry Alert Function Call');
		$c = new Communication($enquiryID);
		//$e = new Enquiry($enquiryID);
		$p = new User($c -> info_travelexpert);
		$amanda = new User(259);
		$karen = new User(266);
		$matt = new User(1);

		$data = array();
		$data['subject'] = 'Call me back Request';
		$type = 'callbackrequest';
		$email_to = $c -> enquiry_email;
		$email_from = $p -> email;
		$data['cc'] = $amanda -> email;
		$data['cc'] .= ', ' . $matt -> email;
		$data['cc'] .= ', ' . $karen -> email;
		$data['enquiryID'] = $enquiryID;

		$this -> send_email_frontend($type, $email_to, $email_from, $data);

	}

	function send_email_frontend($type, $email_to, $email_from, $data)
	{
		log_message('error', 'Send Email Frontend Function Call');
		$CI = &get_instance();

		$CI -> load -> library('email');
		$CI -> email -> from($email_from, 'Designer Travel');
		//$CI->email->reply_to($email_from, $email_from);
		$CI -> email -> to($email_to);

		if ($data['cc'] != '')
		{
			$CI -> email -> cc($data['cc']);
		}
		$CI -> email -> subject($data['subject']);
		$message = $this -> load -> view('communication/emails/' . $type . '-html', $data, TRUE);
		$CI -> email -> message($message);
		//$CI->email->set_alt_message($this->load->view('admin/email/' . $type . '-txt', $data, TRUE));

		log_message('error', 'About to send message - check for success');
		if ($CI -> email -> send())
		{
			log_message('error', 'Email sent to: '.$email_to.' and CC-ed:'.$data['cc']);
			//log_message('error', $CI->email->print_debugger());
			return TRUE;
			
		}
		else
		{
			log_message('error', 'Email sending failled. models:communicate:send_email_frontend - > communciation ID ');
			return FALSE;
		}
	}

 	function getStateOptions($currentFlag = NULL)
    {

        //$types = $this->getAll();
        $options = array(
            'Active' => 'Active',
            'Deleted' => 'Delete',
        );
        if ($currentFlag)
        {
            $options['selected'] = $currentFlag;
        }

        return $options;

    }
	


}

/* End of file user.php */
/* Location: ./application/models/user.php */
