<div class='sheet'>
    <div class='innersheet'>
        <!-- Start of Content of Sheet -->
        <div class="grid_8 page-header">
            <div class='title'>
                Status Management
            </div>
            <div class='subtitle'>
                Administration Area
            </div>
        </div>
        <div class="grid_16 page-options">
            <div class='option'>
                <a href="<?php echo base_url();?>statu/admin/create/" id=''>
	                <div class='title'>
	                    Create New
	                </div>
	                <div class='subtitle'>
	                    Create a new Status
	                </div> 
                </a>
            </div>
            
        </div>
        
        <div class="clear"></div>
        <div class="grid_24 datatable">
            <div class='somethingelse'></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display bordered-table" id="example" width="100%">
                <thead>
                    <tr >
                        <th>No:</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Colour</th>
                        <th>TaskType</th>
                        <th>Department</th>
                        <th>Base State</th>
                        <th>State</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($status as $object)
                    {
                    	$s_department = $this -> task -> getObject('department', $object->departmentID, null, false, array(  'state'=> 'Active')) -> dm_object;
						if($object->tasktypeID)
						{
							$s_tasktype = $this -> task -> getObject('tasktype', $object->tasktypeID, null, false, array(  'state'=> 'Active')) -> dm_object;
						}
                    ?>

                    <tr class="" id='<?php echo $object -> id;?>'>
                        <td><?php echo $object -> id; ?></td>
                        <td><?php echo $object -> title; ?></td>
                        <td><?php echo $object -> description;?></td>
                        <td><?php echo $object -> colour;?></td>
                        <td><?php if($object->tasktypeID) { echo $s_tasktype -> title; } else { echo 'Generic'; }?></td>
                        <td><?php echo $s_department->title;?></td>
                        <td><?php echo $object -> baseState;?></td>
                        <td><?php echo $object -> state;?></td>
                    </tr>
                    <?php
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>No:</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Colour</th>
                        <th>TaskType</th>
                        <th>Department</th>
                        <th>Base State</th>
                        <th>State</th>
                    </tr>
                </tfoot>
            </table>
            
            
            <script type="text/javascript" charset="utf-8">
                $(document).ready(function() {
                    $('#example').dataTable({
                        'bJQueryUI' : true,
                        "sDom" : '<"top"  <"info"i>   <"show"l>  <"pagination"p> <"search"f> >rt<"bottom"><"clear">',
                        "oLanguage" : {
                            "sInfo" : "_TOTAL_ entries (_START_ to _END_)",
                            "sSearch" : ""
                        },
                        'iDisplayLength' : 50,
                    });

                    $('#example tr').live('click', function() {
                        if(this.id) {
                            //alert(this.id);
                            window.location = '<?php echo base_url(); ?>statu/admin/create/'+this.id;
                        }
                    });
                });

            </script>
            
            
        </div>
        <div class="clear"></div>
        <!-- End of Content of Sheet -->
        <!-- Start of Content of Form area -->
        
    </div>
</div>