<?php
/**
 * Class *
 */
class Statu extends Base_Model
{

	/**
	 * table name
	 *
	 * @var String
	 */
	public $table = 'status';
	public $has_one = array();
	public $has_many = array('communication');

	/**
	 * Constructor
	 *
	 * @param unknown_type $id
	 */
	function __construct($id = NULL)
	{
		parent::__construct($id);
	}

	public function getOptions($currentFlag = NULL)
	{
		$optionsArr = $this -> getObject('statu', null, null, false, array('state !=' => 'Disabled')) -> dm_object -> all_to_array();
		if ($currentFlag)
		{
			$options['selected'] = $currentFlag;
		}
		foreach ($optionsArr as $op)
		{
			$options[$op['id']] = $op['title'];
		}
		return $options;
	}

	function adminGetStatus($clearCache = false)
	{
		if ($clearCache)
		{
			$this -> removeCachedObject('status', null, null, array('state' => 'Active'));
		}

		$stat = $this -> getObject('statu', null, null, false, array('state' => 'Active'), '', '') -> dm_object;
		return $stat;

	}

	function getStatu($id)
	{
		$obj = $this -> getObject('statu', $id, null, false, array('state' => 'Active'), '', '') -> dm_object;
		return $obj;
	}

	function getStatu_Tasktype($parent)
	{
		$obj = $this -> getObject('tasktype', null, $parent, false, array('state' => 'Active'), '', '') -> dm_object;
		return $obj;
	}

	function getStatu_Department($parent)
	{
		$obj = $this -> getObject('department', null, $parent, false, array('state' => 'Active'), '', '') -> dm_object;
		return $obj;
	}

	function deleteDepot($id, $userID)
	{
		$user = $this -> getObject('user', $userID, null, false, array('state' => 'Active'), '', '') -> dm_object;
		$depot = $this -> getObject('depot', $id, null, false, array('state' => 'Active'), '', '') -> dm_object;

		$user -> delete($depot);
		// we are only removing a relationship!

		$this -> removeCachedObject('depot', null, $user, false, null, '', '');

		return true;
	}

}
