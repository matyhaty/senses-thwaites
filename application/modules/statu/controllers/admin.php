<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Admin extends Base_Controller
{

	/**
	 * Constructor for statu page
	 *
	 */
	function __construct()
	{
		$this -> load -> library('form_validation');
		$this -> load -> helper(array(
			'form',
			'url'
		));
		parent::__construct('Statu');
	}

	public function manage()
	{
		$userid = $this -> session -> userdata('userid');
		$data['status'] = $this -> model -> adminGetStatus();

		$this -> template -> write_view('sheet', 'statu/admin_manage', $data);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

	public function autoCreate()
	{
		$object = $this -> model;
		$tasktypeM = new Tasktype();
		$departmentM = new Department();

		$tt = new Tasktype();
		$tt -> where('state', 'Active') -> get();

		$de = new Department();
		$de -> where('state', 'Active') -> get();

		$types = array();
		$types[] = 'On Site';
		$types[] = 'Off Site';
		$types[] = 'On Hold';
		$types[] = 'Client Placed on Hold';
		$types[] = 'Planning';
		$types[] = 'Scheduled';
		$types[] = 'Processing';
		$types[] = 'Awaiting Resource';
		$types[] = 'Currently Processing';
		$types[] = 'Back Log';
		$types[] = 'Pending';
		$types[] = 'Client on Hold';
		$types[] = 'Inactive';
		$types[] = 'Dead / Deleted';
		$types[] = 'Complete';
		$types[] = 'Awaiting Payment Check';


		$colours = array(
			'F44747',
			'D88934',
			'EEAB41',
			'C39F77',
			'BEC0C0',
			'BFCBAB',
			'DBE5C3',
			'E0E9DD',
			'D4E5F2',
			'85A9A6',
			'24213E',
			'BA2A46',
			'737349',
			'723660',
			'9B746F',
			'5B7E7F',
			'5693B4',
			'C7457B',
			'E0E9DD',
			'D4E5F2',
			'85A9A6',
			'24213E',
			'BA2A46',
			'737349',
			'723660',
			'9B746F',
			'5B7E7F',
			'5693B4'
		);
		foreach ($tt as $t)
		{
			foreach ($de as $d)
			{
				$c = 0;
				foreach ($types as $type)
				{
					// create array
					$statu = new Statu();
					$data = '';
					$data['title'] = $type;
					$data['description'] = '.';
					$data['colour'] = $colours[$c];
					$data['tasktypeID'] = $t -> id;
					$data['departmentID'] = $d -> id;

					$test = new Statu();
					$test -> where('tasktypeID', $t -> id) -> where('departmentID', $d -> id) -> where('title', $type) -> get();
					if ($test -> result_count())
					{
						// status already exists, do not duplicate.
						echo '<hr>status already exists, do not duplicate. - update colours tho! ';
						$test->colour = $colours[$c];
						$test->save();
					}
					else
					{
						echo '<hr>Created: ';
						print_r($data);
						$saved = DMZ_Array::from_array($statu, $data, NULL, TRUE);
					}
					$c++;
				}
			}
		}
	}

	public function create($id = null)
	{
		$object = $this -> model;
		$tasktypeM = new Tasktype();
		$departmentM = new Department();

		if ($id != 'null' && $id)
		{
			$object = $this -> model -> getStatu($id);
			//$object_tasktype = $this -> model -> getStatu_Tasktype($object);
			//$object_department = $this -> model -> getStatu_Department($object);

			$formdropdowns['tasktype'] = $tasktypeM -> getOptions($object -> tasktypeID);
			$formdropdowns['department'] = $departmentM -> getOptions($object -> departmentID);
		}
		else
		{
			$formdropdowns['tasktype'] = $tasktypeM -> getOptions();
			$formdropdowns['department'] = $departmentM -> getOptions();
		}

		$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/statu/forms', 'admin_edit');

		if ($this -> input -> post())
		{
			if ($this -> form_validation -> run() == TRUE)
			{
				$data = $this -> input -> post();
				$data['tasktypeID'] = $data['tasktype'];
				$data['departmentID'] = $data['department'];
				$saved = DMZ_Array::from_array($object, $data, NULL, TRUE);

				// Save the job
				if ($saved)
				{
					// relate
					$this -> maintaincache -> status($object -> id);
					redirect('statu/admin/manage');

				}
				else
				{
					$this -> template -> write('validation_errors', validation_errors());
				}
			}
			else
			{
				$this -> template -> write('validation_errors', validation_errors());
			}

		}

		$form = $this -> xml_forms -> createform($formdata['xml'], $object, $formdropdowns, array());
		$this -> template -> write('sheet', $form);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);

	}

}

//end class
