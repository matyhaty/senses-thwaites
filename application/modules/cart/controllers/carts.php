<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Carts extends Base_Controller
{

	function __construct()
	{
		parent::__construct();
		//$theme				= 'home';
		//$this->template->set_master_template( $theme .'/template' );

	}

	public function view()
	{
	    $CI = &get_instance();
		$this -> template -> set_template('cart');

		$this -> data = '';
		$myCart = new Cart();
        $myCart->where('userID', $CI->user->id);
        $myCart->where('orderID', null);
		$myCart->get();
		$this->data['carts'] = $myCart;
			
		$this -> template -> write('slider', 'Shopping Cart');
		$this -> template -> write_view('content', 'cart/view', $this -> data);

		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
		
		
	}
    
    public function remove($id)
    {
        $CI = &get_instance();
        
        $myCart = new Cart($id);
        $myCart->delete();
        
        redirect('cart/carts/view');
        
    }
	 
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
