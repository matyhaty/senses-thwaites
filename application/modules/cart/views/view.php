<!-- FORMS
        ================================================== -->


<div class="row">
        <div class="span24">
            <div class="form-chain"></div>
        </div>
    </div>


    <div class="row">
        <div class="span24">
            <div class="form-container">
            <?php echo form_open('designposter_step2'); ?>
                <div class="row">
                    <div class="span24">
                        <div class="left-content">
                            <h1>Your cart</h1>
                               <table class='cart'>
                                   <?php foreach($carts as $cart) { ?>
                                       <tr>
                                           <td>
                                                <?php echo $cart->title; ?>       
                                           </td> 
                                           <td>
                                               <?php echo $cart->summary; ?>   
                                           </td>
                                           <td class='price'>
                                               <?php echo $cart->totalPrice; ?>   
                                           </td>
                                           <td class='options'>
                                               <?php echo anchor('cart/carts/remove/'.$cart->id, 'Remove'); ?> 
                                           </td>
                                       </tr>
                             
                             
                                    <?php } ?>
                                   
                                   
                               </table>
                            
                        </div>
                    </div>

                </form>
                
            </div>
        </div>
    </div>
