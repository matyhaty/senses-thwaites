<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cache extends Base_Controller {

	
	function __construct()
	{
		parent::__construct();
	}
	
	
	/**
	 * Main function. This should display either a list of grid view of events
	 *
	*/
	public function index()
	{
		$this->load->library('session');
		$this->load->helper('url');			
		$this->load->driver('cache');
		
		
		
		if($person =  $this->cache->file->get('messages_person_1') ){
			echo 'Person Now cached<br> ';
		}else{
			$person = new person(1);
			$person->messages_person->get();
			$this->cache->file->save('messages_person_1', $person);
		}
		
		foreach($person->messages_person as $message)
		{
			if($myMessages =  $this->cache->file->get( 'messages_user_1_message_' . $message->id ) ){
				echo 'Message now cached<br>  ';
			}else{
				$message->message->get();
				$myMessages  = $message;
				$this->cache->file->save('messages_user_1_message_' . $message->id , $myMessages);
			}		     
		     
		     foreach($myMessages->message as $m){
		     	echo $m->message;
		     }
		}
		
		
		
		
		
		
		//print_r($person);
		die;
		
		$this->template->write_view('content',  'cache/index' , array(  ) );
		$this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}
	
	
	
}

/* End of file cache.php */
/* Location: ./application/controllers/cache.php */
