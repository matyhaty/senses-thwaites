<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Profile extends Base_Controller
{

	/**
	 * Constructor for profile page
	 *
	 */
	function __construct()
	{

		$this -> load -> library('form_validation');
		$this -> load -> helper(array(
			'form',
			'url'
		));
		parent::__construct();
		$this -> model = new Cm('cm');
		$this -> model -> cache = $this -> cache;

	}

	public function frontend_view($slug, $id, $posted = false)
	{
		$this -> setTemplate('travelexpert');
		$this -> teamleaf -> expertFlashSet($id);
		$u = new User($id);
		
		
		
		if ($posted && $this -> input -> post('name') && $this -> input -> post('email') && $this -> input -> post('telephone'))
		{
			$e = new Communication();
			$e -> enquiry_name = $this -> input -> post('name');
			$e -> enquiry_email = $this -> input -> post('email');
			$e -> enquiry_telephone = $this -> input -> post('telephone');
			$e -> holiday_notes = $this -> input -> post('comments');

			//$e -> subscribed = $this -> input -> post('subscribed');
			$e -> enquiry_date = date('Y-m-d H:i:s');
			$e -> info_lead_source = '1';
			$e -> enquirytype = 'Enquiry';
			$e -> enquiry_address = $this -> input -> post('address');
			
			$e->status = 1;
			//$e -> enquiry = $this -> input -> get('enquiry');

			$e -> info_travelexpert = 259;
			if ($this -> input -> post('travelexpertID'))
			{
				$e -> info_travelexpert = $this -> input -> post('travelexpertID');
			}

			if (isset($data['travelexpertID']))
			{
				$e -> info_travelexpert = $data['travelexpertID'];
			}

			if (isset($data['leader']))
			{
				$r = new Referrer();
				$r->where('title', $data['leader']);
				$r->get();

				$e -> info_lead_source = $r->id;
				
				if (isset($data['leader_memberID']))
				{
					$e -> holiday_notes .= '   QUIDCO MEMBER ID: '.$data['leader_memberID'];
				}
			}

			$e -> save();
			$e -> enquiryalert($e -> id);
			
			$this -> maintaincache -> communication($e -> id);
					$this->maintaincache->communication_status($e -> id);
			
			
			$this->data['posted'] = true;
		}

		

		//$data = $this -> teamleaf -> getvars();
		//$this -> teamleaf -> travelExpertFlashSet($id);

		$person = new User($id);
		if ($person -> state != 'Active')
		{
			$person = new User(259);
			$id = 259;
			// force Amanda for deleted users.
		}

		$te = new Blog();
		$te -> where('creator', $id) -> order_by('blogdate', 'desc') -> limit(15) -> get();

		$this -> data['person'] = $person;
		$this -> data['blogs'] = $te;
		$this -> data['myRecommendedHotels'] ='';
		$count = 0;
		
			$h = new Hotel();
			$h -> where('state', 'Active');
			$h -> limit(6);
			$h -> get();
			foreach ($h as $recommendedhotel)
			{
				$count++;
				$data['h'] = new Hotel($recommendedhotel -> id);
				$this -> data['myRecommendedHotels'] .= $this -> load -> view('hotel/hotel_mini_3', $data, true);
			}
		
		
		if ($count < 6)
		{
			$this -> fb -> info('else');
			foreach ($recommendedHotels as $recommendedhotel)
			{
				$ht = new Hotel($recommendedhotel -> hotelID);
				$d = new Destination($ht -> subdestination);
				$d -> hotel -> limit(12) -> get();
				foreach ($d->hotel as $h)
				{
					$count++;

					$data['h'] = new Hotel($h -> id);
					$this -> data['myRecommendedHotels'] .= $this -> load -> view('hotel/hotel_mini_3', $data, true);
					if ($count == 6)
					{
						break;
					}
				}

				if ($count == 6)
				{
					break;
				}

			}
		}
		//$this -> fb -> info('render');
		$this -> data['latestreviews'] = $this -> load -> view('blog/latestreviews', '', true);
		$this -> data['latestblogs'] = $this -> load -> view('blog/latestarticles', '', true);

		$this -> data['travelexpertID'] = $person -> id;
		$this -> data['callmeback'] = $this -> load -> view('user/callmeback_viewdetail', $this -> data, true);

		$this -> template -> write_view('content', 'user/frontend_viewDetail', $this -> data);

		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);

	}

	public function display($cmsID)
	{
		$this -> load -> helper('url');
		// Get the user profile
		// if its a post, we should search the DB
		$this -> template -> write('page_header', '');
        
        $cm = $this -> model -> getCm($cmsID);

		
		//$user_company = $this->model->getUser_Company($user);

		$this -> template -> write_view('sheet', 'cm/profile', array(
			"cm" => $cm,
		
		));

		// Render template
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

	public function index()
	{
		$this -> display($username = '');
	}

	public function view()
	{
		$this -> template -> write('page_header', l('_D_MY') . ' ' . l('_D_QUESTIONS'));
		$this -> load -> library('session');
		$this -> load -> helper('url');
		$data['cms'] = $this -> model -> getCms();
		// super chain!
		$this -> template -> write_view('sheet', 'cm/view', $data);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

	/**
	 * Creates a User
	 *
	 */
	public function create($id = null)
	{
		$this -> template -> write('page_header', l('_D_CREATE') . ' ' . l('_D_QUESTIONS'));
		$user = $this -> model;
		$formdropdowns = '';

		if ($id)
		{
			$cm = new Cm($id);
		}
		else
		{
            $cm = new Cm();
		}
		// Load XML
		$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/cm/forms', 'cms_basic');

		if ($this -> input -> post())
		{
			// If posted and form validation passes
			if ($this -> form_validation -> run() == TRUE)
			{
				$data = $this -> input -> post();

                $filesToUpload = array();
				$filesToUpload[] = 'file1';
				$filesToUpload[] = 'file2';
				$filesToUpload[] = 'file3';
				$filesToUpload[] = 'file4';
				$filesToUpload[] = 'file5';
				$filesToUpload[] = 'image1';
				$filesToUpload[] = 'image2';
				$filesToUpload[] = 'image3';
				$filesToUpload[] = 'image4';
				$filesToUpload[] = 'image5';
				
				if(!$cm->id)
				{
					$saved = DMZ_Array::from_array($cm, $data, null, true);
				}
				
                $path = 'cms/'.$cm->id.'';
                $this -> teamleaf -> checkpath($path);
                $fileresult = $this -> xml_forms -> uploadfiles($filesToUpload, $path);
                if ($fileresult['result'])
				{
					foreach($filesToUpload as $filee)
					{
						if($filee != '' && isset($fileresult[$filee]['file_name'])) 
						{
							$data[$filee] = @$fileresult[$filee]['file_name'];
						}
					}
				}
				
				if($data['excerpt'] == '')
				{
					$words = explode(" ",strip_tags($data['content']));
    				$data['excerpt'] = implode(" ",array_splice($words,0,rand(90,110)));
					$data['excerpt'] .= '...';
				}
				$saved = DMZ_Array::from_array($cm, $data, null, true);
				// this saves this automatically to the Task Object (e.g. same as $user->save());

				// Save the task
				if ($saved)
				{
					//$company = new Company($data['company']);
					//$user->save($company);
					//$this -> maintaincache -> user($user -> id);
					redirect(base_url() . 'admin/cm/profile/display/' . $cm -> id);
				}
				else
				{
					$this -> template -> write('validation_errors', l('_D_ERROR'));
					// with some errors!
				}
			}
			else
			{
				// render errors
				$this -> template -> write('validation_errors', 'Error:' . validation_errors());
				// with some errors!
			}
		}

		if ($id)
		{
			$cm = $this -> model -> getCm($id);
			//$user_company = $this->model->getUser_Company($u);
			//$companyID = $user_company->id;
		}
		else
		{
			$cm = new Cm();
		}

		//$companyM = new Company();
		//$formdropdowns['company'] = $companyM->getOptions($companyID);
		
		$cmM = new CM();
		$formdropdowns['type'] = $cmM -> getCmTypes($cm -> type);
        $formdropdowns['template'] = $cmM -> getCmTemplates($cm -> template);
        $formdropdowns['state'] = $cmM -> getCmStates($cm -> state);
        $formdropdowns['featured'] = $cmM -> getCmFeatured($cm -> featured);
		
        $form = $this -> xml_forms -> createform($formdata['xml'], $cm, $formdropdowns, array());
		$this -> template -> write('sheet', $form);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}


    /**
     * Creates a User
     *
     */
    public function createmedia($id = null)
    {
        $this -> template -> write('page_header', l('_D_CREATE') . ' ' . l('_D_QUESTIONS'));
        $user = $this -> model;
        $formdropdowns = '';

        if ($id)
        {
            $cm = new Cm($id);
        }
        else
        {
            $cm = new Cm();
        }
        // Load XML
        $formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/cm/forms', 'cms_media');

        if ($this -> input -> post())
        {
            // If posted and form validation passes
            if ($this -> form_validation -> run() == TRUE)
            {
                $data = $this -> input -> post();

				$filesToUpload = array();
				$filesToUpload[] = 'file1';
				$filesToUpload[] = 'file2';
				$filesToUpload[] = 'file3';
				$filesToUpload[] = 'file4';
				$filesToUpload[] = 'file5';
				$filesToUpload[] = 'image1';
				$filesToUpload[] = 'image2';
				$filesToUpload[] = 'image3';
				$filesToUpload[] = 'image4';
				$filesToUpload[] = 'image5';
				
                $path = 'cms/'.$cm->id.'/';
                $this -> teamleaf -> checkpath($path);
                $fileresult = $this -> xml_forms -> uploadfiles($filesToUpload, $path);
                if ($fileresult['result'])
				{
					foreach($fileFields as $filee)
					{
						$data[$filee] = @$fileresult[$filee]['file_name'];
					}
					
				}

                //$userid = $this -> session -> userdata('userid');*/
                $saved = DMZ_Array::from_array($cm, $data, null, true);
                // this saves this automatically to the Task Object (e.g. same as $user->save());

                // Save the task
                if ($saved)
                {
                    redirect(base_url() . 'admin/cm/profile/display/' . $cm -> id);
                }
                else
                {
                    $this -> template -> write('validation_errors', l('_D_ERROR'));
					echo 'error on save';
                    // with some errors!
                }
            }
            else
            {
                // render errors
                $this -> template -> write('validation_errors', 'Error:' . validation_errors());
				echo 'error';
                // with some errors!
            }
        }

        if ($id)
        {
            $cm = $this -> model -> getCm($id);
            //$user_company = $this->model->getUser_Company($u);
            //$companyID = $user_company->id;
        }
        else
        {
            $cm = '';
        }

        //$companyM = new Company();
        //$formdropdowns['company'] = $companyM->getOptions($companyID);
        
        $cmM = new Cm();
		$formdropdowns = '';
        //$formdropdowns['type'] = $cmM -> getCmTypes($cm -> type);
        //$formdropdowns['template'] = $cmM -> getCmTemplates($cm -> template);
        //$formdropdowns['state'] = $cmM -> getCmStates($cm -> state);
        
        $form = $this -> xml_forms -> createform($formdata['xml'], $cm, $formdropdowns, array());
        $this -> template -> write('sheet', $form);
        $this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
    }

}

//end class
