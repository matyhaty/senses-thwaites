<?php
/**
 * User Class
 *
 */
class Cm extends Base_Model
{

	public $table = 'cms';
	public $has_one = array( 'user');
	public $has_many = array(

			'permission',
			'message',
			'comment',
			'wall',
			'file',
			'communication',
			'task',
			'blog'
	);
	/**
	 * Cons
	 *
	 * @param Int $id
	 */
	function __construct($id = null)
	{
		parent::__construct($id);
	}

	/**
	 * Save the user
	 *
	 * @param unknown_type $data
	 * @param unknown_type $user
	 * @return unknown
	 */

	/**
	 * Get all users, must be an asso array
	 *
	 * @param unknown_type $data
	 */
	
	public function getCms()
	{
		return $this -> getObject('cm', null, null, false, array('state' => 'Active'), '', '') -> dm_object;
	}
    
    public function getCm($id)
    {
        return $this -> getObject('cm', $id, null, false, array('state' => 'Active'), '', '') -> dm_object;
    }
	
	public function getCmsByType($type)
    {
        return $this -> getObject('cm', null, null, false, array('state' => 'Active', 'type' => $type ), array('id' => 'desc'), '') -> dm_object;
    }
	
	public function getCmsFeatured()
    {
        return $this -> getObject('cm', null, null, false, array('state' => 'Active', 'featured' => '1' ), '', '') -> dm_object;
    }
	
	public function getCmsNewsPage()
    {
        $sqlArray = array();
		$sqlArray['where'] = array('state' => 'Active');
		
		$arr['column'] = 'type';
		$arr['values'] = array('news', 'offers', 'incentives');
		
		$sqlArray['or_where'] = $arr;
		$rt =  $this -> getObjectv2($sqlArray, strtolower(get_class($this))) -> dm_object;
		//$rt->check_last_query();
		return $rt;
    }
    
    function getCmTypes($currentFlag = NULL)
    {

        
        $types = array(
        
            'news' => 'News',
            'taproom' => 'Tap Room',
            'offers' => 'Offers',
            'homepageintro' => 'Homepage Intro',
            'reasource-products-and-prices' => 'Reasources - Products and Prices',
            'reasource-compliance' => 'Reasources - Compliance',
            'reasource-partners-and-support' => 'Reasources - Partners and Support',
            
        
        );
        $options = array();
        foreach ($types as $key => $val)
        {
            $options[$key] = $val;
        }
        if ($currentFlag)
        {
            $options['selected'] = $currentFlag;
        }

        return $options;
    }
    
    function getCmFeatured($currentFlag = NULL)
    {

        
        $types = array(
        
            '0' => 'No',
            '1' => 'Yes'
        
        );
        $options = array();
        foreach ($types as $key => $val)
        {
            $options[$key] = $val;
        }
        if ($currentFlag)
        {
            $options['selected'] = $currentFlag;
        }

        return $options;
    }

    function getCmTemplates($currentFlag = NULL)
    {

        
        $types = array(
        
            'news_basic' => 'News Basic',
            'taproom_basic' => 'Tap Room Basic',
            'offers_basic' => 'Offers Basic',
            'reasource_basic' => 'Reasources '
        
        );
        $options = array();
        foreach ($types as $key => $val)
        {
            $options[$key] = $val;
        }
        if ($currentFlag)
        {
            $options['selected'] = $currentFlag;
        }

        return $options;
    }
    
    function getCmStates($currentFlag = NULL)
    {

        
        $types = array(
        
            'Active' => 'Active',
            'Deleted' => 'Deleted',

        
        );
        $options = array();
        foreach ($types as $key => $val)
        {
            $options[$key] = $val;
        }
        if ($currentFlag)
        {
            $options['selected'] = $currentFlag;
        }

        return $options;
    }

}

/* End of file user.php */
/* Location: ./application/models/user.php */
