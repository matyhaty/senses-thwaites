<!-- Profile Head -->
<section class="section">
	
	<div class="row">
		
		<div class="col col2-1">
			<div class="profile-image-container">
			<img class="overlay" src="/assets/images/profile-image-overlay.png" alt="" />
			<img class="profile-image" src="/assets/images/test-profile.png" alt="" />
			</div>
		</div>
	
		<div class="col col2-1">
			<hgroup>
				<h2><?php echo $profile['fullname']; ?></h2>
				<h4>is currently out enjoying life walking and photographing <span>Edit</span></h4>
			</hgroup>
			
			<div class="profile-desc">
			  <p><?php echo $profile['bio']; ?></p>
			<a href="#" class="orange-btn-sml rounded">Edit profile</a>
			<a href="#" class="orange-btn-sml rounded">Edit/add resume</a>
			
			</div>
			
		</div>
	
	</div>


</section>

	<section class="section" style="margin-top: -102px;">

	<div class="row">

		<?php echo $this->load->view("user/pms/left"); ?>
	
		<div class="col col4-3" style="margin-top:18px;">
		
			<div class="row">
				<div class="col col1">
				<h3>Create a message</h3>
				</div>
			</div>
			
			<?php echo form_open('user/pms/send'); ?>
				<?php if($msg == null) : ?>
				<div class="row">
					<div class="col col4-1"><h3>Recipient (s):</h3></div>
					<div class="col col4-3"><input class="pm-field rounded" type="text" name="to_user" value="<?php echo $message['to_user']; ?>" /><input type="hidden" value="" name="msg_id" /></div>
				</div>
				<?php else: ?> 
				<input type="hidden" value="<?php echo $msg; ?>" name="msg_id" />
				<input type="hidden" value="<?php echo $message['to_user']; ?>" name="to_user" />
				<?php endif; ?>
				
				<div class="row">
					<div class="col col4-1"><h3>Subject:</h3></div>
					<div class="col col4-3"><input class="pm-field rounded" type="text" name="subject" value="<?php echo $message['subject']; ?>" /></div>
				</div>
				
				<div class="row">
					<div class="col col4-1"><h3>Message:</h3></div>
					<div class="col col4-3"><textarea class="pm-field rounded" name="msg"><?php echo $message['message']; ?></textarea></div>
				</div>
				
				<div class="row">
					<div class="col col4-1">&nbsp;</div>
					<div class="col col4-3">
					<input type="submit" class="orange-btn-med rounded" name="send" value="Send" />
					<input type="submit" class="orange-btn-med rounded" name="save" value="Save" />
					<input type="submit" class="orange-btn-med rounded" name="cancel" value="Cancel" />
					</div>
				</div>
			<?php form_close(); ?>
		</div>
		
	</div>
</section>