<div class="col col4-1">
	<div class="grey-box">
		<h3>Grind box</h3>
		
		<div class="icons inbox"><a href="/inbox">Inbox (<?php echo get_user_pms_count($profile['id'], "active", 0, $profile['id']); ?>)</a></div>
		<div class="icons inbox"><a href="/compose">New</a></div>
		<div class="icons inbox"><a href="/sent">Sent (<?php echo get_user_pms_count($profile['id'], "sent", $profile['id'], null); ?>)</a></div>
		<div class="icons inbox"><a href="/draft">Drafts (<?php echo get_user_pms_count($profile['id'], "draft", $profile['id'], 0); ?>)</a></div>
	</div>
	
	<div class="expand-your-profile">
		<h3>Expand your profile</h3>
		<p>Another way to dream; no ads, <br />advanced search and analytics</p>
	</div>
	
</div>