<div class='sheet'>
    <div class="page-header-grey">
        
        <div class='titles'>
            <div class='title'>
                <?php echo $cm -> title; ?>'s Information Page
            </div>
            <div class='subtitle'>
                <?php echo anchor('somewhere', 'Preview Post on Site'); ?>
            </div>
        </div>
        <div class='clear'></div>
    </div>
    <div class='innersheet'>
        <!--
        	<div class="grid_24 page-tab-bar">
            <ul>
                <li>Latest Activity</li>
                <li>Basic Information</li>
                <li>Qualifications</li>
                <li>Attendance</li>
                <li>Depots</li>
            </ul>
        </div>
        <div class='clear'></div>  
        -->  
            
        <div class="grid_24 profile-segment">
            <div class='segment-header'>
                <div class='title'>Basic Information</div>
                <div class='option'><a href='<?php echo base_url(); ?>admin/cm/profile/create/<?php echo $cm -> id; ?>'>Edit</a></div>
                       </div>
            
            <div class='segment-content'>
            
                <table class='basic_information tt'>
                	<tr>
                        <td>Title</td>
                        <td><?php echo $cm -> title; ?> </td>
                    </tr>
                    <tr>
                        <td>Excerpt</td>
                        <td><?php echo $cm -> excerpt; ?> </td>
                    </tr>
                    <tr>
                        <td>Content</td>
                        <td><?php echo $cm -> content; ?></td>
                    </tr>
                    <tr>
                        <td>Type</td>
                        <td><?php echo $cm -> type; ?></td>
                    </tr>
                    <tr>
                        <td>Template</td>
                        <td><?php echo $cm -> template; ?></td>
                    </tr>
                    <tr>
                        <td>Created</td>
                        <td><?php echo $cm -> created; ?></td>
                    </tr>
                    <tr>
                        <td>Created By</td>
                        <td><?php echo $cm -> created_by; ?></td>
                    </tr>
                    <tr>
                        <td>State</td>
                        <td><?php echo $cm -> state; ?></td>
                    </tr>
                    <tr>
                        <td>Published Date</td>
                        <td><?php echo $cm -> published_date; ?></td>
                    </tr>
                    <tr>
                        <td>Featured</td>
                        <td><?php if($cm -> featured) { echo 'Yes'; } else { echo 'No'; } ?></td>
                    </tr>
                   
                </table>
            
            </div>
            
            
        </div>
        
        <div class="grid_24 profile-segment">
            <div class='segment-header'>
                <div class='title'>Images And Files</div>
                <div class='option'><a href='<?php echo base_url(); ?>admin/cm/profile/create/<?php echo $cm -> id; ?>'>Edit</a></div>
            </div>
            
            <div class='segment-content'>
            
            <?php 

			$imageRT = '';
			
            for($i=1; $i<6; $i++) 
            {
            	$nameImage = 'image'.$i;
            	if($cm->$nameImage)
				{
					echo '<div style="padding:10px; float:left;">'. slir('/cms/'.$cm->id.'/', $cm->$nameImage, $width = 160, $height = 160, $crop = array(
						'width' => '160',
						'height' => '160'
					), 'title="Image '. $i .': '.$cm->$nameImage.'" class="profile-image"').'</div>';
				}
				
				
            	
            } 
            
            ?>
            
            <div class='clear'></div>
            
                <table class='basic_information tt'>
                    <tr>
                        <td>Image 1</td>
                        <td><?php echo $cm -> image1; ?> </td>
                    </tr>
                    <tr>
                        <td>Image 2</td>
                        <td><?php echo $cm -> image2; ?> </td>
                    </tr>
                    <tr>
                        <td>Image 3</td>
                        <td><?php echo $cm -> image3; ?></td>
                    </tr>
                    <tr>
                        <td>Image 4</td>
                        <td><?php echo $cm -> image4; ?></td>
                    </tr>
                    <tr>
                        <td>Image 5</td>
                        <td><?php echo $cm -> image5; ?></td>
                    </tr>
                    
                    <tr>
                        <td>File 1</td>
                        <td><?php echo $cm -> file1; ?> </td>
                    </tr>
                    <tr>
                        <td>File 2</td>
                        <td><?php echo $cm -> file2; ?> </td>
                    </tr>
                    <tr>
                        <td>File 3</td>
                        <td><?php echo $cm -> file3; ?></td>
                    </tr>
                    <tr>
                        <td>File 4</td>
                        <td><?php echo $cm -> file4; ?></td>
                    </tr>
                    <tr>
                        <td>File 5</td>
                        <td><?php echo $cm -> file5; ?></td>
                    </tr>
                  
                   
                </table>
            
            </div>
            
            
        </div>
        
        
        <div class='clear'></div>        
    </div>
</div>