<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Pubs Net - Thwaites</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content=""><!-- Le styles -->
    <link href="<?php echo base_url(); ?>assets/frontend/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/frontend/css/docs.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/frontend/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/frontend/css/layout.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/frontend/css/ui-lightness/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css"><!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
    <!-- Le fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/frontend/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/frontend/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/frontend/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/frontend/ico/apple-touch-frontend/icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/fronend/ico/favicon.png">
    <meta http-equiv="content-type" content="text/html; charset=utf-8"><?php //echo $page_meta; ?><?php //echo $_styles; ?><!-- Le javascript
    ================================================== --><!-- Placed at the end of the document so the pages load faster -->

    <script type="text/javascript" src="http://platform.twitter.com/widgets.js">
</script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/jQuery.js">
</script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/holder/holder.js">
</script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/google-code-prettify/prettify.js">
</script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/bootstrap-transition.js">
</script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/bootstrap-carousel.js">
</script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/bootstrap-dropdown.js">
</script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/jquery-1.7.2.min.js">
</script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/jquery-ui-1.8.20.custom.min.js">
</script><?php //echo $_scripts; ?>
</head>

<body>
    


    <div class="" id="loginModal">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

            <h3>Time to get personal:</h3>
        </div>

        <div class="modal-body">
            <div class="well">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#login" data-toggle="tab">Login</a></li>
                </ul>

                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane active in" id="login">
                        <form class="form-horizontal" action="http://www.thwaitesdev.com/login" method="post" accept-charset="utf-8" id="1366911348" class="">
                            <fieldset>
                                <div id="legend">
                                    <legend class="">Login</legend>
                                </div>

                                <div class="control-group">
                                    <!-- Username -->
                                    <label class="control-label" for="username">Username</label>

                                    <div class="controls">
                                        <input type="text" id="username" name="username" placeholder="" class="input-xlarge">
                                    </div>
                                </div>

                                <div class="control-group">
                                    <!-- Password-->
                                    <label class="control-label" for="password">Password</label>

                                    <div class="controls">
                                        <input type="password" id="password" name="password" placeholder="" class="input-xlarge">
                                    </div>
                                </div>

                                <div class="control-group">
                                    <!-- Button -->

                                    <div class="controls">
                                        <button class="btn btn-success">Login</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
