<div class="grid_3" style='padding-bottom: 30px;'>
	<div class="travelexpertbox">
		<div class='image'>
			<?php
				echo slir('/users/profileimages/' , $person -> profileimage, 164, 164, array('width' => 1, 'height' => 1));
			?>
		</div>
		<div class='contact'>
			<div class="call">
				<h1>Call for advice</h1>
			</div>
			<div class="tel">
				Telephone (Work): <?php echo $person -> work;?>
			</div>
			<div class="mobile">
				Telephone (Mobile): <?php echo $person -> mobile;?>
			</div>
			<div class="email">
				Email: <?php echo $person -> email;?>
			</div>
			<div class="call">
				<h1>Contact Hours</h1>
			</div>
			<div class="tel">
				<?php echo $person -> contacthours;?>
			</div>
			<div class="social-info" style='margin-top:10px'>
				<h1 style='margin-bottom:3px'>Facebook</h1>
				<div class="fb-like" data-href="<?php echo "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];?>" data-send="true" data-layout="button_count" data-width="175" data-show-faces="true"></div>
			</div>
			<div  style='margin-top:10px'>
				<?php
if($person->twitter != '')
{
$this->javascript->script("
$('#tweet-activity-recent').tweet(
{

username : '".$person->twitter."',
avatar_size : 48,
count : 1,
loading_text : 'loading tweet...'
});
");
				?>
				<h1 style='margin-bottom:3px'>Latest Tweet</h1>
				<div id="tweet-activity-recent" style='width: 225px'></div>
				<?php
					}
				?>

				<br/>
				<br/>
				<a href="http://www.facebook.com/<?php echo $person -> facebook;?>" title="Designer Travel Facebook" target="_blank" id="facebook"><span>Facebook</span></a>
				<a href="http://www.twitter.com/<?php echo $person -> twitter;?>" title="Designer Travel Twitter" target="_blank" id="twitter"><span>Twitter</span></a>
			</div>
		</div>
	</div>
</div>
<div class="grid_9">
	<div class="travelexpert-title">
		<?php echo $this -> teamleaf -> personNameFull($person);?>
		Travel Expert
	</div>
	<div class="travelexpert-subtitle">
		<?php echo $person -> aboutmeshort;?>
	</div>
	<div class="clear"></div>
	<div class="travelexpertbio">
		<p>
			<?php echo $person -> aboutmelong;?>
		</p>
	</div>
	
	<div class="travelexpertcallback">
		<h2>Call <?php echo $person -> firstname;?></h2>
	<?php echo $callmeback; ?>
	</div>
	
	<div class="travelexpertarticles">
		<h2><?php echo $person -> firstname;?>'s Travel Articles</h2>
	</div>
	<div class="travelexpertarticleswrapper">
		<?php
		$blogcount = 0;
		foreach ($blogs as $b)
		{
		$blogcount++;
		if($blogcount < 4)
		{
		?>
		<div class="travelexpertarticles">
			<div class="block">
				<div class="first article">
					<h3> <?php echo anchor('blogs/'.$this->teamleaf->slugify($b->title).'/' . $b -> id, $b -> title);?> </h3>
					<p class="meta cutetime">
						<?php echo $b -> blogdate;?>
					</p>
					<a href="#" class="image"> <?php echo slir('/blogs/' . $b -> id . '/' , $b -> file1, 50,50, array('width' => 1, 'height' => 1)); ?> </a>
					<?php
						$excerptwords = str_word_count($b -> excerpt);

						if ($excerptwords < $this -> config -> item('summary_latestreviews_minlimit'))
						{
							$output = $b -> excerpt . '...
					<br/>
					';
							$remainingwords = $this -> config -> item('summary_latestreviews_maxlimit') - $excerptwords - 1;
							$output .= $this -> teamleaf -> words(strip_tags($b -> content), $remainingwords);
						}
						else
						{
							$output = $this -> teamleaf -> words(strip_tags($b -> excerpt), $this -> config -> item('summary_latestreviews_maxlimit'));
						}
					?>
					<p>
						<?php echo $output;?>  ~  <? echo anchor('blogs/'.$this->teamleaf->slugify($b->title).'/' . $b -> id, "<span class='readmore'>View Blog.</span>");?>
					</p>
				</div>
			</div>
		</div>
		<?php
			}
			else
			{
			if($blogcount == 4)
			{
			echo '
			<div class="travelexpertarticlesmini">
			<div class="moreblogs">
			More blogs...
			</div>
			</div>
			';
			}
		?>
		<div class="travelexpertarticlesmini">
			<div class="block">
				<div class='minititle'>
					<?php echo anchor('blogs/'.$this->teamleaf->slugify($b->title).'/' . $b -> id, $b -> title);?> &middot;  (<span class='cutetime'><?php echo $b -> blogdate;?></span>)
				</div>
			</div>
		</div>
		<?php
			}

			}
		?>
	</div>
</div>
<div class="clear"></div>
<div class="grid_6" style='padding-top:20px;'>
	<?php
		if ($person -> twitter != '')
		{
			$tweetid = $person -> twitter;
		}
		else
		{
			$tweetid = 'designertravel';
		}

		$this -> javascript -> script("
	$('#tweet-activity').tweet(
	{
	join_text : 'auto',
	username : '" . $tweetid . "',
	avatar_size : 48,
	count : 9,
	loading_text : 'loading tweet...'
	});
	");
	?>
	<div class="titlebox">
		My Tweets
	</div>
	<div id="tweet-activity"></div>
</div>
<div class="grid_6" style='padding-top:20px;'>
	<div class="titlebox">
		My Recommended Hotels
	</div>
	<?php
		echo $myRecommendedHotels;
	?>
</div>
