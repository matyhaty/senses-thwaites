<div class='sheet'>
    <div class='innersheet'>
        <!-- Start of Content of Sheet -->
        <div class="grid_8 page-header">
            <div class='title'>
                CMS Register
            </div>
            <div class='subtitle'>
                Current CMS Items.
            </div>
        </div>
        <div class="grid_16 page-options">
            <div class='option'>
                <a class="" id='createatask' href='<?php echo base_url(); ?>admin/cm/profile/create/'>
                <div class='title'>
                    Create a New CMS Items
                </div>
                <div class='subtitle'>
                    For all content additions
                </div> </a>
            </div>
           
    
        </div>
        <div class="clear"></div>
        <div class="grid_24 datatable">
            
            <div class='somethingelse'></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display bordered-table" id="cms_table" width="100%">
                <thead>
                    <tr >
                        <th>No:</th>
                        <th>Type</th>
                        <th>Template</th>
                        <th>Title</th>
                        <th>State</th>
                        <th>Created</th>
                        <th>Published</th>
                        <th>Featured</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $rowStyle = 1;
                    foreach ($cms as $c)
                    {
                   
                    ?>

                    <tr id='<?php echo $c -> id;?>'>
                        
                        <td><?php echo $c->id; ?></td>
                        <td><?php echo $c->type;?></td>
                        <td><?php echo $c->template;?></td>
                        <td><?php echo $c->title;?></td>
                        <td><?php echo $c -> state;?></td>
                        <td><?php echo $this->teamleaf->date($c -> created);?></td>
                        <td><?php echo $this->teamleaf->date($c -> published_date);?></td>
                        <td><?php if($c -> featured) { echo 'Yes'; } else { echo 'No'; } ?></td>
                        
                    </tr>
                    <?php
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>No:</th>
                        <th>Type</th>
                        <th>Template</th>
                        <th>Title</th>
                        <th>State</th>
                        <th>Created</th>
                        <th>Published</th>
                        <th>Featured</th>
                    </tr>
                </tfoot>
            </table>
            
            
            <script type="text/javascript" charset="utf-8">
                $(document).ready(function() {
                    $('#cms_table').dataTable({
                        'bJQueryUI' : true,
                        "sDom" : '<"top"  <"info"i>   <"show"l>  <"pagination"p> T <"search"f> >rt<"bottom"><"clear">',
                        "oTableTools": {
							"sSwfPath": "<?php echo base_url();?>assets/admin/swf/copy_csv_xls_pdf.swf"
						},
                        "oLanguage" : {
                            "sInfo" : "_TOTAL_ entries (_START_ to _END_)",
                            "sSearch" : "",
                            
                        },
                        "aaSorting": [[ 0, "desc" ]],
                        'iDisplayLength' : 50,
                    });

                    $('#cms_table tr').live('click', function() {
                        if(this.id) {
                            //alert(this.id);
                            window.location = '<?php echo base_url(); ?>admin/cm/profile/display/'+this.id;
                        }
                    });
                });

            </script>
            
            
        </div>
        <div class="clear"></div>
        <!-- End of Content of Sheet -->
        <!-- Start of Content of Form area -->
        
    </div>
</div>