	<h1>Dreamitize - Please login ></h1>

	<div id="body">
	<h2>Sign up</h2>
		<p>Create an account using form below.</p>
		<?php echo validation_errors(); ?>
		<hr/>
		<?php echo form_open(  current_url()  ); ?>

			<label class="label" for="username">Username: </label>
			<input type="text" name="username" id="username" value="<?php echo set_value('username'); ?>" />
			<label class="label" for="firstname">First name: </label>
			<input type="text" name="firstname" id="firstname" value="<?php echo set_value('firstname'); ?>" />
			<label class="label" for="lastname">Last name: </label>
			<input type="text" name="lastname" id="lastname" value="<?php echo set_value('lastname'); ?>" />
			<label class="label" for="email">Email: </label>
			<input type="text" name="email" id="email" value="<?php echo set_value('email'); ?>" />
			<label class="label" for="password">Password: </label>
			<input type="password" name="password" id="password" value="<?php echo set_value('password'); ?>" />
			<div><input type="submit" value="Login" /></div>
		</form>
	</div>
