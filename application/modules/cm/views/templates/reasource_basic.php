<div class="row">

    <div class="span1">
        <a href="#"><img src="<?php echo base_url(); ?>assets/frontend/img/menu.gif" alt="Menu"></a>
    </div>

    <div class="span11 resourceSegment">
    
            <div class='downloadTitle'><?php echo $post->title; ?></div>
            <div class='downloadContent'>
                <?php echo $post->content; ?>
            </div>
            
            <?php if($post->file1 != '')
            {
                echo '<div class="downloadLink">'.anchor_popup(base_url().'uploads/cms/'.$post->id.'/'.$post->file1, 'Download "'.$post->file1.'"').'</div>';
            }
            ?>
            <?php if($post->file2 != '')
            {
                echo '<div class="downloadLink">'.anchor_popup(base_url().'uploads/cms/'.$post->id.'/'.$post->file2, 'Download "'.$post->file2.'"').'</div>';
            }
            ?>
            <?php if($post->file3 != '')
            {
                echo '<div class="downloadLink">'.anchor_popup(base_url().'uploads/cms/'.$post->id.'/'.$post->file3, 'Download "'.$post->file3.'"').'</div>';
            }
            ?>
            <?php if($post->file4 != '')
            {
                echo '<div class="downloadLink">'.anchor_popup(base_url().'uploads/cms/'.$post->id.'/'.$post->file4, 'Download "'.$post->file4.'"').'</div>';
            }
            ?>
            <?php if($post->file5 != '')
            {
                echo '<div class="downloadLink">'.anchor_popup(base_url().'uploads/cms/'.$post->id.'/'.$post->file5, 'Download "'.$post->file5.'"').'</div>';
            }
            ?>
    </div>

</div>

<div class='clear'></div>
