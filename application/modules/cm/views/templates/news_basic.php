

<div class="row">
    <div class="span17 resourceSegment newsArticle">
        <div style='float:left; padding-right:20px; padding-bottom:10px'>
            <?php
            echo '' . slir('/cms/' . $post -> id . '/', $post -> image1, $width = 150, $height = 100, $crop = array(
                'width' => '150',
                'height' => '100'
            ), 'title=""' . $post -> image1 . '" class="" ') . '';
            ?>
        </div>
    
        <h1><?php echo $post -> title; ?></h1>
        <p class='tags'>In <?php echo $post -> type; ?> on <?php echo $this -> teamleaf -> date($post -> published_date); ?></p>
        <p>
            <?php echo $post -> excerpt; ?>
            <br/>
            <a href="<?php echo base_url(); ?>article/<?php echo $post -> id; ?>">Click to read the full article</a>
        </p>
    </div>
</div>
<div class='clear'></div>
