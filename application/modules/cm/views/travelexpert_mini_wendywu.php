<?php if($travelexpert->id == '') { log_message('error', 'There is a travel expert in the league tabells with no ID!'); } ?>

<div class="travelexperthello">
	<div class='title'>Our Wendy Wu travel expert</div>
    
    <div class="block">
        <div class="thumb">
            <div class="thumb">
                <?php echo anchor('travel-expert/'.$this->teamleaf->slugify($travelexpert->firstname.' '.$travelexpert->surname).'/' . $travelexpert->id, slir('/users/profileimages/' , $travelexpert->profileimage, 75,75,array('width' => 75, 'height' => 75))); ?>
            </div>
        </div>
        
        <h2><?php echo $this->teamleaf->personNameFullFrontend($travelexpert); ?></h2>
 
        <div class="details">
            Tel: <?php echo $travelexpert->mobile; ?><br/>
            <?php echo $travelexpert->email; ?>
                        
        </div>
        <div class="expert-copy">
            <?php 
            	if($travelexpert->aboutmeshort !='')
            	{
            		echo $this->teamleaf->words($travelexpert->aboutmeshort, 65); ?> <?php echo anchor('travel-expert/'.$this->teamleaf->slugify($travelexpert->firstname.' '.$travelexpert->surname).'/' . $travelexpert->id, 'Read More...'); 
            	}
            	else if($travelexpert->aboutmemedium !='')
            	{
            		echo $this->teamleaf->words($travelexpert->aboutmemedium, 65); ?>... <?php echo anchor('travel-expert/'.$this->teamleaf->slugify($travelexpert->firstname.' '.$travelexpert->surname).'/' . $travelexpert->id, 'Read More...'); 
            	}
            	else if($travelexpert->aboutmelong !='')
            	{
            		echo $this->teamleaf->words($travelexpert->aboutmelong, 65); ?>... <?php echo anchor('travel-expert/'.$this->teamleaf->slugify($travelexpert->firstname.' '.$travelexpert->surname).'/' . $travelexpert->id, 'Read More...'); 
            	}
            ?>
        </div>
        
       
    </div>
</div>