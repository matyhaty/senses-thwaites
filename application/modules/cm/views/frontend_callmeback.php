<?php
//$this->javascript->ajaxForm('request-callback', 'persons/callmebackform', 'ajaxValidationCallback');
?>

<script>

	// Called once the server replies to the ajax form validation request
	function ajaxValidationCallback(status, form, json, options)
	{
		if (window.console)
		{
			console.log(status);
		}
		
		if (status === true) 
		{
			$('#contactform').hide('slow');
			$('#contactformresponse').show('slow');
		}
	} 
</script>

		<div class='titlebox'>
	        Call Me Back
	    </div>
        <div class="contactform" id="forms">
        	<div class="block" id="contactformresponse" style='display:none;'>Thanks!<br/>We will get right back to you.</div>
            <div class="block" id="contactform">Please complete the form below to request a call back from one of our Travel specialists.
            	<?php echo form_open(base_url().'enquiry/posted', 'id="request-callback"'); ?>
                <form id='request-callback' action="">
                    <table class='tlc' style='margin-top:10px;'>
                        <tr>
                            <td width='40px'>Name: </td>
                            <td><input class='onehundered validate[required]' type="text" name="name" id="name" value="" /></td>
                        </tr>
                        
                        <tr>
                            <td>Email: </td>
                            <td><input  class='onehundered validate[required,custom[email]]' type="email" name="email"  id="email"/></td>
                        </tr>
                        <tr>
                            <td>Telephone: </td>
                            <td><input  class='onehundered validate[required]'  type="text"  name="telephone"  id="telephone"/></td>
                        </tr>
                        
                        <tr>
                        	<td><?php if(isset($travelexpertID)) { ?><input type="hidden" name="travelexpertID"  id="travelexpertID" value='<?php if(isset($travelexpertID)) { echo $travelexpertID; } ?>'/><?php } ?></td>
	                        <td>
	                        	<input class="confirm button" type="submit" value="Request Call Back" />
	                        </td>
                    	</tr>
                    	
                    </table>
                </form>
            </div>
        </div>
