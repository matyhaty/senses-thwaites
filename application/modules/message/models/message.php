<?php
/**
 * Class *
 */
class Message extends Base_Model {

	/**
	 * table name
	 *
	 * @var String
	 */
	public $table = 'messages';
	public $has_one = array('user');
	public $has_many = array('file');
	
	/**
	 * Constructor
	 *
	 * @param unknown_type $id
	 */
	function __construct($id = NULL)
	{
	parent::__construct($id);
	}	
	
}
