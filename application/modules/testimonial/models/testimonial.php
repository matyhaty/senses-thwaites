<?php
/**
 * Class *
 */
class Testimonial extends Base_Model {

	/**
	 * table name
	 *
	 * @var String
	 */
	public $table = 'testimonials';
	public $has_one = array('user');
	public $has_many = array();
	
	/**
	 * Constructor
	 *
	 * @param unknown_type $id
	 */
	function __construct($id = NULL)
	{
	parent::__construct($id);
	}	
	
}
