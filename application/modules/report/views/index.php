<div class='sheet'>
    <div class="page-header-grey">
        <div class='image'>
            <?php echo slir('/', 'teamleaf.png', $width = 80, $height = 60, $crop = array(
                'width' => '75',
                'height' => '55'
            ), 'title="Profile image" class="profile-image"');
            ?>
        </div>
        <div class='titles'>
            <div class='title'>
               Reports
            </div>
            <div class='subtitle'>
                Get the information you need.
            </div>
        </div>
        <div class='clear'></div>
    </div>
    <div class='innersheet'>
    	
    	<div class="grid_24 profile-segment">
            <div class='segment-header'>
                <div class='title'>Simple Communication Export</div>
            </div>
            
            <div class='segment-content'>
            	
            	<?php echo $communicationForm; ?>
            
                
            
            </div>
       </div>
        <div class='clear'></div>    
    	
    	
    	
	</div>
</div>