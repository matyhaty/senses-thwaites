<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Communicationreport extends Base_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	public function generate($date=null, $depotID=null)
	{
		$this -> load -> library('excel');
		
		if($this->input->post('date') && $this->input->post('depot'))
		{
			$date = $this->input->post('date');
			$depotID = $this->input->post('depotID');
		}
		
		
		$styleArrayAllBorders = array(
			'borders' => array(
				'allborders' => array(
				'style' => 'thin',
					'color' => array('argb' => '000000'),
				),
			),
		);
		
		$styleArrayDoubleRightBorder = array(
			'borders' => array(
				'right' => array(
				'style' => 'double',
					'color' => array('argb' => '000000'),
				),
			),
		);
		
		$styleArrayDoubleBottomBorder = array(
			'borders' => array(
				'bottom' => array(
				'style' => 'double',
					'color' => array('argb' => '000000'),
				),
			),
		);
		
		$styleArrayDottedBottomBorder = array(
			'borders' => array(
				'bottom' => array(
				'style' => 'dotted',
					'color' => array('argb' => '000000'),
				),
			),
		);
		
		//load our new PHPExcel library
		$this -> load -> library('excel');
		//activate worksheet number 1
		$this -> excel -> setActiveSheetIndex(0);
		
		//$assetsArrayWithTasks = array();
		//$start = new DateTime($date);
		//$end = new DateTime($date);
		//$end -> modify('+1 day');

		$communicationModel = new Communication();
		$userM = new User();
		$refM = new Referrer();
		$statusM = new Statu();
		
		$allCommunications = $communicationModel->myGetsAdmin();

		//name the worksheet
		$this -> excel -> getActiveSheet() -> setTitle('Communication Log');
		//set cell A1 content with some text
		$this -> excel -> getActiveSheet() -> setCellValue('A1', 'Date of Enqury');
		$this -> excel -> getActiveSheet() -> setCellValue('B1', 'Referrer Code');
		$this -> excel -> getActiveSheet() -> setCellValue('C1', 'Associate');
		$this -> excel -> getActiveSheet() -> setCellValue('D1', 'Enquiry Telephone number');
		$this -> excel -> getActiveSheet() -> setCellValue('E1', 'Enquiry Email');
		$this -> excel -> getActiveSheet() -> setCellValue('F1', 'Referrer');
		$this -> excel -> getActiveSheet() -> setCellValue('G1', 'Status');
		$this -> excel -> getActiveSheet() -> setCellValue('H1', 'State');
		$this -> excel -> getActiveSheet() -> setCellValue('I1', 'Booking Day');
		$this -> excel -> getActiveSheet() -> setCellValue('J1', 'Customer Name');
		$this -> excel -> getActiveSheet() -> setCellValue('K1', 'Departure Date');
		$this -> excel -> getActiveSheet() -> setCellValue('L1', 'Destination');
		$this -> excel -> getActiveSheet() -> setCellValue('M1', 'Duration');
		$this -> excel -> getActiveSheet() -> setCellValue('N1', 'No. Adults');
		$this -> excel -> getActiveSheet() -> setCellValue('O1', 'No. Chilren');
		$this -> excel -> getActiveSheet() -> setCellValue('P1', 'Board Basis');
		$this -> excel -> getActiveSheet() -> setCellValue('Q1', 'Hotel');
		$this -> excel -> getActiveSheet() -> setCellValue('R1', 'Airport');
		$this -> excel -> getActiveSheet() -> setCellValue('S1', 'Budget');
		$this -> excel -> getActiveSheet() -> setCellValue('T1', 'Travel Holiday Cost');
		$this -> excel -> getActiveSheet() -> setCellValue('U1', 'Discount');
		$this -> excel -> getActiveSheet() -> getStyle('A1:V1')->applyFromArray($styleArrayDoubleBottomBorder);
		$this -> excel -> getActiveSheet()->getStyle('A1:V1')->getFill()->setFillType('solid');
		$this -> excel -> getActiveSheet()->getStyle('A1:V1')->getFill()->getStartColor()->setARGB('cccccc');
		
		$rowNumber = 2;

		// CREATE EXCEL ROWS FOR EACH ASSET
		foreach ($allCommunications as $communication)
		{
			
				$travelExpert = new User($communication -> info_travelexpert);
				$ref = new Referrer($communication -> info_lead_source);
				$status = new Statu($communication -> status);
			
				$this -> excel -> getActiveSheet() -> setCellValue('A' . ($rowNumber), str_replace('<br>', ', ', $communication -> enquiry_date));
				//$this -> excel -> getActiveSheet() -> setCellValue('B' . ($rowNumber), str_replace('<br>', ', ', $communication -> postcode));
				$this -> excel -> getActiveSheet() -> setCellValue('C' . ($rowNumber), str_replace('<br>', ', ', $travelExpert -> firstname.' '.$travelExpert->surname));			// update
				$this -> excel -> getActiveSheet() -> setCellValue('D' . ($rowNumber), str_replace('<br>', ', ', $communication -> enquiry_telephone));
				$this -> excel -> getActiveSheet() -> setCellValue('E' . ($rowNumber), str_replace('<br>', ', ', $communication -> enquiry_email));
				$this -> excel -> getActiveSheet() -> setCellValue('F' . ($rowNumber), str_replace('<br>', ', ', $ref->title));			// update
				$this -> excel -> getActiveSheet() -> setCellValue('G' . ($rowNumber), str_replace('<br>', ', ', $status->title));					//updayte
				$this -> excel -> getActiveSheet() -> setCellValue('H' . ($rowNumber), str_replace('<br>', ', ', $communication -> state));
				$this -> excel -> getActiveSheet() -> setCellValue('I' . ($rowNumber), str_replace('<br>', ', ', $communication -> booking_date));
				$this -> excel -> getActiveSheet() -> setCellValue('J' . ($rowNumber), str_replace('<br>', ', ', $communication -> enquiry_name));
				$this -> excel -> getActiveSheet() -> setCellValue('K' . ($rowNumber), str_replace('<br>', ', ', $communication -> holiday_departure_date));
				$this -> excel -> getActiveSheet() -> setCellValue('L' . ($rowNumber), str_replace('<br>', ', ', $communication -> holiday_destination));
				$this -> excel -> getActiveSheet() -> setCellValue('M' . ($rowNumber), str_replace('<br>', ', ', $communication -> holiday_duration));
				$this -> excel -> getActiveSheet() -> setCellValue('N' . ($rowNumber), str_replace('<br>', ', ', $communication -> holiday_adults));
				$this -> excel -> getActiveSheet() -> setCellValue('O' . ($rowNumber), str_replace('<br>', ', ', $communication -> holiday_children));
				$this -> excel -> getActiveSheet() -> setCellValue('P' . ($rowNumber), str_replace('<br>', ', ', $communication -> holiday_board));
				$this -> excel -> getActiveSheet() -> setCellValue('Q' . ($rowNumber), str_replace('<br>', ', ', $communication -> holiday_hotel));
				$this -> excel -> getActiveSheet() -> setCellValue('R' . ($rowNumber), str_replace('<br>', ', ', $communication -> holiday_departure_airport));
				$this -> excel -> getActiveSheet() -> setCellValue('S' . ($rowNumber), str_replace('<br>', ', ', $communication -> holiday_budget));
				$this -> excel -> getActiveSheet() -> setCellValue('T' . ($rowNumber), str_replace('<br>', ', ', $communication -> finances_travelcost));
				$this -> excel -> getActiveSheet() -> setCellValue('U' . ($rowNumber), str_replace('<br>', ', ', $communication -> finances_discounts));
				
				$rowNumber++;
		}

		

		$filename = 'communication_log.xls';
		//save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel');
		//mime type
		header('Content-Disposition: attachment;filename="' . $filename . '"');
		//tell browser what's the file name
		header('Cache-Control: max-age=0');
		//no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this -> excel, 'Excel5');
		//force user to download the Excel file without writing it to server's HD
		$objWriter -> save('php://output');

	}

}

//change the font size
//$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
//make the font become bold
//$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
//merge cell A1 until D1
//$this->excel->getActiveSheet()->mergeCells('A1:D1');
//set aligment to center for that merged cell (A1 to D1)
//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
