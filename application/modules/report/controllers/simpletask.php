<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Simpletask extends Base_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	public function generate()
	{
		if ($this -> input -> post('date') && $this -> input -> post('depot'))
		{
			$date = $this -> input -> post('date');
			$depotID = $this -> input -> post('depot');
		}
		else
		{
			echo 'No date or depot gven. Bye';
			die();
		}

		$styleArrayAllBorders = array('borders' => array('allborders' => array(
					'style' => 'thin',
					'color' => array('argb' => '000000'),
				), ), );

		$styleArrayDoubleRightBorder = array('borders' => array('right' => array(
					'style' => 'double',
					'color' => array('argb' => '000000'),
				), ), );

		$styleArrayDoubleBottomBorder = array('borders' => array('bottom' => array(
					'style' => 'double',
					'color' => array('argb' => '000000'),
				), ), );

		$styleArrayDottedBottomBorder = array('borders' => array('bottom' => array(
					'style' => 'dotted',
					'color' => array('argb' => '000000'),
				), ), );

		//load our new PHPExcel library
		$this -> load -> library('excel');
		//activate worksheet number 1
		$this -> excel -> setActiveSheetIndex(0);

		$this -> excel -> getActiveSheet() -> getStyle('B2:V600') -> applyFromArray($styleArrayAllBorders);
		$this -> excel -> getActiveSheet() -> getStyle('M2:M600') -> applyFromArray($styleArrayDoubleRightBorder);
		$this -> excel -> getActiveSheet() -> getStyle('N2:N600') -> applyFromArray($styleArrayDoubleRightBorder);
		$this -> excel -> getActiveSheet() -> getStyle('O2:O600') -> applyFromArray($styleArrayDoubleRightBorder);
		$this -> excel -> getActiveSheet() -> getStyle('P2:P600') -> applyFromArray($styleArrayDoubleRightBorder);
		$this -> excel -> getActiveSheet() -> getStyle('Q2:Q600') -> applyFromArray($styleArrayDoubleRightBorder);
		$this -> excel -> getActiveSheet() -> getStyle('R2:R600') -> applyFromArray($styleArrayDoubleRightBorder);
		$this -> excel -> getActiveSheet() -> getStyle('S2:S600') -> applyFromArray($styleArrayDoubleRightBorder);
		$this -> excel -> getActiveSheet() -> getStyle('T2:T600') -> applyFromArray($styleArrayDoubleRightBorder);
		$this -> excel -> getActiveSheet() -> getStyle('U2:U600') -> applyFromArray($styleArrayDoubleRightBorder);
		$this -> excel -> getActiveSheet() -> getStyle('V2:V600') -> applyFromArray($styleArrayDoubleRightBorder);

		$this -> excel -> getActiveSheet() -> getStyle('U1:U600') -> getFill() -> setFillType('solid');
		$this -> excel -> getActiveSheet() -> getStyle('U1:U600') -> getFill() -> getStartColor() -> setARGB('FFF0F8FF');
		$this -> excel -> getActiveSheet() -> getStyle('V1:V600') -> getFill() -> setFillType('solid');
		$this -> excel -> getActiveSheet() -> getStyle('V1:V600') -> getFill() -> getStartColor() -> setARGB('FFF0F8FF');

		$rowNumber = 1;

		$alphas = range('A', 'Z');
		foreach ($alphas as $a)
		{
			$this -> excel -> getActiveSheet() -> getColumnDimension($a) -> setAutoSize(true);
			$this -> excel -> getActiveSheet() -> getStyle($a . '1') -> getFont() -> setSize(14);
			$this -> excel -> getActiveSheet() -> getStyle($a . '1') -> getFont() -> setBold(true);
		}

		//name the worksheet
		$this -> excel -> getActiveSheet() -> setTitle('Simple Task for ' . $date);
		//set cell A1 content with some text
		$this -> excel -> getActiveSheet() -> setCellValue('A1', 'Task No');
		$this -> excel -> getActiveSheet() -> setCellValue('b1', 'Client');
		$this -> excel -> getActiveSheet() -> setCellValue('C1', 'Client Ref');
		$this -> excel -> getActiveSheet() -> setCellValue('D1', 'Sub Client Ref');
		$this -> excel -> getActiveSheet() -> setCellValue('E1', 'Title');
		$this -> excel -> getActiveSheet() -> setCellValue('F1', 'Department');
		$this -> excel -> getActiveSheet() -> setCellValue('G1', 'Status');
		$this -> excel -> getActiveSheet() -> setCellValue('H1', 'Task Type');
		$this -> excel -> getActiveSheet() -> setCellValue('I1', 'Work Details');
		$this -> excel -> getActiveSheet() -> setCellValue('J1', 'Address');
		$this -> excel -> getActiveSheet() -> setCellValue('K1', 'Assignments');
		$this -> excel -> getActiveSheet() -> setCellValue('L1', 'Logins');
		$this -> excel -> getActiveSheet() -> setCellValue('M1', 'Earnings');
		$this -> excel -> getActiveSheet() -> getStyle('A1:L1') -> applyFromArray($styleArrayDoubleBottomBorder);
		$this -> excel -> getActiveSheet() -> getStyle('A1:L1') -> getFill() -> setFillType('solid');
		$this -> excel -> getActiveSheet() -> getStyle('A1:L1') -> getFill() -> getStartColor() -> setARGB('cccccc');

		$taskM = new Task();
		$taskModel = new Task();
		$tasks = $taskM -> getTasksByDate($date, $depotID);
		//$tasks->check_last_query();
		
		$start = new DateTime($date);
		$end = new DateTime($date);
		$end -> modify('+1 day');
		
	
		foreach ($tasks as $task)
		{
			//echo 'more';
			$rowNumber++;
			$department = $taskModel -> getTask_Department($task);
			$status = $taskModel -> getTask_Statu($task);
			$tasktype = $taskModel -> getTask_Tasktype($task);
			$client = $taskModel -> getTask_Company($task);
			
			$workDetailsClean  = str_replace('<br/>', ',   ', $task -> workdetails);
			$workDetailsClean  = str_replace('<div>', ',   ', $workDetailsClean);
			$workDetailsClean  = str_replace('</div>', ',   ', $workDetailsClean);
			$workDetailsClean  = str_replace('<br>', ',   ', $workDetailsClean);

			// these are all the tasks carried out by this asset on date(s)
			//$thisTask = $taskModel -> getTask($taskID);
			$this -> excel -> getActiveSheet() -> setCellValue('A' . $rowNumber, sprintf('%06d', $task -> id));
			$this -> excel -> getActiveSheet() -> setCellValue('B' . $rowNumber, $client -> title);
			$this -> excel -> getActiveSheet() -> setCellValue('C' . $rowNumber, $task -> clientref);
			$this -> excel -> getActiveSheet() -> setCellValue('D' . $rowNumber, $task -> subclientref);
			$this -> excel -> getActiveSheet() -> setCellValue('E' . $rowNumber, $task -> title);
			$this -> excel -> getActiveSheet() -> setCellValue('F' . $rowNumber, $department -> title);
			$this -> excel -> getActiveSheet() -> setCellValue('G' . $rowNumber, $status -> title);
			$this -> excel -> getActiveSheet() -> setCellValue('H' . $rowNumber, $tasktype -> title);
			$this -> excel -> getActiveSheet() -> setCellValue('I' . $rowNumber, $workDetailsClean);
			$this -> excel -> getActiveSheet() -> setCellValue('J' . $rowNumber, str_replace('<br/>', ',   ', $task -> address) . ', '.$task->postcode);

			// get operators for this task for this date
			$thisTask_taskassign = $taskModel -> getObject('taskassign', null, $task, null, array(
				'state' => 'Active',
				'start >=' => $start -> format('Y-m-d') . ' 00:00:00',
				'end <' => $end -> format('Y-m-d') . ' 00:00:00'
			), '', '') -> dm_object;

			$assigns = '';
			foreach ($thisTask_taskassign as $assign)
			{
				$logon = '';
				$logoff = '';
				
				if($assign->asset_id)
				{
					$assetM = new Asset();
					$asset = $assetM->getAsset($assign->asset_id);
					$assigns.= $asset->title.', ';
				}
				if($assign->person_id)
				{
				$person = $userModel -> getUserByID($operator -> person_id);
				$assigns .= $person -> firstname . ' ' . $person -> lastname . ', ';
				}
			}
			$this -> excel -> getActiveSheet() -> setCellValue('K' . $rowNumber, $assigns);


			$logged = '';
			$thisTask_tasklogin = $taskModel -> getObject('tasklogin', null, $task, null, array(
				'type' => 'Login',
				'state' => 'Active',
				'logged >=' => $start -> format('Y-m-d') . ' 00:00:00',
				'logged <' => $end -> format('Y-m-d') . ' 00:00:00'
			), '', '') -> dm_object;

			if ($thisTask_tasklogin -> result_count())
			{
				if($thisTask_tasklogin->person_id)
				{
					$person = $userModel -> getUserByID($operator -> person_id);
					$initials = $userModel->getUserInitials($person);
				
				$logon = $thisTask_tasklogin -> logged;
				$logged .= $initials.': '.$this -> teamleaf -> dateTime($logon).', ';
				}

			}
			
			$this -> excel -> getActiveSheet() -> setCellValue('L' . $rowNumber, $logged);
/*
			$thisTask_taskearnings = $taskModel -> getObject('taskearning', null, $task, null, array(
				'assetID' => $a -> id,
				'state' => 'Active',
				'date >=' => $start -> format('Y-m-d') . ' 00:00:00',
				'date <' => $end -> format('Y-m-d') . ' 00:00:00'
			), '', '') -> dm_object;

			if ($thisTask_taskearnings -> result_count())
			{
				$this -> excel -> getActiveSheet() -> setCellValue('M' . ($rowNumber), $thisTask_taskearnings -> amount);
			}
*/

		}
		// Operators
		//$this -> excel -> getActiveSheet() -> getStyle('A' . $bottomBorderNumber . ':V' . $bottomBorderNumber) -> applyFromArray($styleArrayDoubleBottomBorder);

		$filename = 'simpleTask_for_' . $date . '.xls';
		//save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel');
		//mime type
		header('Content-Disposition: attachment;filename="' . $filename . '"');
		//tell browser what's the file name
		header('Cache-Control: max-age=0');
		//no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this -> excel, 'Excel5');
		//force user to download the Excel file without writing it to server's HD
		$objWriter -> save('php://output');

	}

}

//change the font size
//$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
//make the font become bold
//$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
//merge cell A1 until D1
//$this->excel->getActiveSheet()->mergeCells('A1:D1');
//set aligment to center for that merged cell (A1 to D1)
//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
