<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Show extends Base_Controller {


	function __construct()
	{
		parent::__construct();
		//$this -> model = new Task('task');
		//$this -> model -> cache = $this -> cache;	
	}
	
	
	public function index()
	{
		$formdropdowns='';
		$formdata = $this->xml_forms->loadXML(APPPATH . 'modules/report/forms', 'communication');
		//$depotM = new Depot();
		//$formdropdowns['depot'] = $depotM->getOptions();
		$data['communicationForm'] = $this -> xml_forms -> createform($formdata['xml'], '', $formdropdowns, array(
			'nosheet' => '',
			'grid' => 'grid_24'
		));
		
		
		
		        
		$this->template->write_view('sheet', 'report/index' , $data );
		$this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
