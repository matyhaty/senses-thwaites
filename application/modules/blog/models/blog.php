<?php
/**
 * User Class
 *
 */
class Blog extends Base_Model
{

	public $table = 'blogs';
	public $has_one = array();
	public $has_many = array();

	/**
	 * Cons
	 *
	 * @param Int $id
	 */
	function __construct($id = null)
	{
		parent::__construct($id);
	}

	public function myGets($state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array('state' => $state);
		}
		//$sqlArray['orderby'] = array('sortorder' => 'asc');
		return $this -> getObjectv2($sqlArray, strtolower(get_class($this))) -> dm_object;
	}

	public function myGet($id, $state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array('state' => $state);
		}
		return $this -> getObjectv2($sqlArray, strtolower(get_class($this)), $id) -> dm_object;
	}
	
	public function myGetBySlug($slugify, $state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array('state' => $state, 'slugify' => $slugify);
		}
		return $this -> getObjectv2($sqlArray, strtolower(get_class($this))) -> dm_object;
	}
	
	public function allMajorGets($state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array('state' => $state, 'parentdestination' => '0');
		}
		$sqlArray['orderby'] = array('sortorder' => 'asc');
		return $this -> getObjectv2($sqlArray, strtolower(get_class($this))) -> dm_object;
	}
	
	public function allSubGets($state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array('state' => $state, 'parentdestination >' => '0');
		}
		$sqlArray['orderby'] = array('sortorder' => 'asc');
		return $this -> getObjectv2($sqlArray, strtolower(get_class($this))) -> dm_object;
	}
	
	public function mySubGets($parentdestinationID, $state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array('state' => $state, 'parentdestination' => $parentdestinationID);
		}
		$sqlArray['orderby'] = array('sortorder' => 'asc');
		return $this -> getObjectv2($sqlArray, strtolower(get_class($this))) -> dm_object;
	}
	
	public function gallerys($object, $state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array('state' => $state);
		}
		return $this -> getObjectv2($sqlArray, 'destinationgallery', null, $object) -> dm_object;
	}
	
	public function gallerysMenu($object, $state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array('state' => $state);
			$sqlArray['limit'] = 4;
		}
		return $this -> getObjectv2($sqlArray, 'destinationgallery', null, $object) -> dm_object;
	}
	
	public function sliders($object, $state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array('state' => $state);
		}
		return $this -> getObjectv2($sqlArray, 'destinationslider', null, $object) -> dm_object;
	}
	
	public function highlights($object, $state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array('state' => $state);
			$sqlArray['orderby'] = array('sortorder' => 'asc');
		}

		return $this -> getObjectv2($sqlArray, 'destinationhighlight', null, $object) -> dm_object;
	}
	
	public function hotels($object, $state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array('state' => $state);
		}
		//$sqlArray['orderby'] = array('sortorder' => 'asc');
		return $this -> getObjectv2($sqlArray, 'hotel', null, $object) -> dm_object;
	}
	
	public function otherhotels($object, $notMeHotelID, $state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array('state' => $state, 'id !=', $notMeHotelID);
		}
		//$sqlArray['orderby'] = array('sortorder' => 'asc');
		return $this -> getObjectv2($sqlArray, 'hotel', null, $object) -> dm_object;
	}
	
	public function expert($object, $state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array('state' => $state);
		}
		//$sqlArray['orderby'] = array('sortorder' => 'asc');
		return $this -> getObjectv2($sqlArray, 'user', $object->expertID) -> dm_object;
	}
	
	public function itinerarys($object, $state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array('state' => $state);
		}
		//$sqlArray['orderby'] = array('sortorder' =>'asc');
		return $this -> getObjectv2($sqlArray, 'itinerary', null, $object) -> dm_object;
	}

	public function itinerarys_noChildren($object, $state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array('state' => $state, 'subdestination' => '0');
		}
		$sqlArray['orderby'] = array('sortorder' =>'asc');
		return $this -> getObjectv2($sqlArray, 'itinerary', null, $object) -> dm_object;
	}
	
	public function itinerarys_children($object, $state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array('state' => $state, 'subdestination >' => '0');
		}
		//$sqlArray['orderby'] = array('sortorder' =>'asc');
		return $this -> getObjectv2($sqlArray, 'itinerary', null, $object) -> dm_object;
	}
	
	
	public function displayLink($object)
	{
		return anchor('desintaion/display/' . $object -> id, $object -> title, array('class' => 'displayLink'));
	}
	
	public function myChildren($object, $state = 'Active')
	{
		$sqlArray = array();
		if ($state)
		{
			$sqlArray['where'] = array('state' => $state, 'parentdestination', $object->parentdestination);
		}
		return $this -> getObjectv2($sqlArray, 'destination') -> dm_object;
	}
	
	function getParentOptions($currentFlag = NULL)
    {

        $types = $this->myGets();
        $options = array();
        $options[0] = 'No Parent';
        foreach ($types as $obj)
        {
            $options[$obj->id] = $obj->title;
        }
        if ($currentFlag)
        {
            $options['selected'] = $currentFlag;
        }

        return $options;
    }
	
	function getMajorOptions($currentFlag = NULL)
    {

        $types = $this->allMajorGets();
        $options = array();
        foreach ($types as $obj)
        {
            $options[$obj->id] = $obj->title;
        }
        if ($currentFlag)
        {
            $options['selected'] = $currentFlag;
        }

        return $options;
    }
	
	function getSubOptions($currentFlag = NULL)
    {

        $types = $this->allSubGets();
        $options = array();
        foreach ($types as $obj)
        {
            $options[$obj->id] = $obj->title;
        }
        if ($currentFlag)
        {
            $options['selected'] = $currentFlag;
        }

        return $options;
    }
	
	function getSubOptionsWithNull($currentFlag = NULL)
    {

        $types = $this->allSubGets();
        $options = array();
		$options[0] = 'No Sub Destination';
        foreach ($types as $obj)
        {
            $options[$obj->id] = $obj->title;
        }
        if ($currentFlag)
        {
            $options['selected'] = $currentFlag;
        }

        return $options;
    }

}

/* End of file user.php */
/* Location: ./application/models/user.php */
