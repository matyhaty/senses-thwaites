<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Highlight extends Base_Controller
{

	/**
	 * Constructor for profile page
	 *
	 */
	function __construct()
	{
		parent::__construct('Destination');
	}


	public function edit($destinationID, $highlightID)
	{
		$this -> create($destinationID, $highlightID, true);
	}

	public function create($destinationID, $highlightID = null, $edit = false)
	{
		$this -> template -> write('page_header', 'Edit/Create Gallery Image');
		$formdropdowns = '';
		$createDropDowns = false;
		
		if($highlightID)
		{
			$destinationhighlight = new Destinationhighlight($highlightID);
		}
		else
		{
			$destinationhighlight = new Destinationhighlight();
		}
		$destination = new Destination($destinationID);

		$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/destination/forms', 'highlight');

		if ($this -> input -> post())
		{
			// If posted and form validation passes
			if ($this -> form_validation -> run() == TRUE)
			{
				$data = $this -> input -> post();
				
				$path = 'destinations/'.$destination->id.'/highlights/';
				$this -> teamleaf -> checkpath($path);
				$fileresult = $this -> xml_forms -> uploadfiles(1, $path);
				if ($fileresult['result'])
				{
					$data['file1'] = @$fileresult[1]['file_name'];
				}
				
				//echo $data['destinationslist'] .' -- new';
				//$userid = $this -> session -> userdata('userid');
				$saved = DMZ_Array::from_array($destinationhighlight, $data, null, true);
				// this saves this automatically to the Task Object (e.g. same as $user->save());

				// Save the task
				if ($saved)
				{
					$destinationhighlight->save($destination);
					$this -> maintaincache -> destination($destination -> id);
					redirect(base_url() . 'admin/destination/display/' . $destination -> id);
				}
				else
				{
					$this -> template -> write('validation_errors', 'Failed to save Destination');
				}
			}
			else
			{
				$createDropDowns = true;
				$this -> template -> write('validation_errors', validation_errors());
				// with some errors!
			}
		}
		else
		{
			$createDropDowns = true;
			
		}

		if($createDropDowns)
		{
			// we need any drop downs to be created here
			//$formdropdowns['parentdestination'] = $object -> getParentOptions($object->parentdestination);
		}
		
		$form = $this -> xml_forms -> createform($formdata['xml'], $destinationhighlight, $formdropdowns, array());
		$this -> template -> write('sheet', $form);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

	function remove($destinationID, $highlightID)
	{
		$destination = new Destination($destinationID);
		$dg = new Destinationhighlight($highlightID);
		$dg->delete($destination);
		
		$this -> maintaincache -> destination($destinationID);
		redirect(base_url() . 'admin/destination/display/' . $destinationID);
	}

}

//end class
