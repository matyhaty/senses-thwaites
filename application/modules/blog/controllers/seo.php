<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Seo extends Base_Controller
{

	/**
	 * Constructor for profile page
	 *
	 */
	function __construct()
	{
		parent::__construct('Destination');
	}

	/**
	 * Creates a User
	 *
	 */

	public function edit($id)
	{
		$this -> create($id, true);
	}

	public function create($id = null, $edit = false)
	{
		$this -> template -> write('page_header', 'Edit/Create Destination');
		$formdropdowns = '';
		$createDropDowns = false;

		if ($id)
		{
			$object = new Destination($id);
			$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/destination/forms', 'seo');
		}
		else
		{
			$object = new Destination();
			$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/destination/forms', 'seo');
		}

		if ($this -> input -> post())
		{
			// If posted and form validation passes
			if ($this -> form_validation -> run() == TRUE)
			{
				$data = $this -> input -> post();
				$saved = DMZ_Array::from_array($object, $data, null, true);
				// this saves this automatically to the Object (e.g. same as $user->save());

				// Save
				if ($saved)
				{
					$this -> maintaincache -> destination($object -> id);
					redirect(base_url() . 'admin/destination/display/' . $object -> id);
				}
				else
				{
					$this -> template -> write('validation_errors', 'Failed to save Destination');
				}
			}
			else
			{
				$createDropDowns = true;
				$this -> template -> write('validation_errors', validation_errors());

				// with some errors!
			}
		}
		else
		{
			$createDropDowns = true;

		}

		if ($createDropDowns)
		{
			// we need any drop downs to be created here
			//$formdropdowns['parentdestination'] = $object -> getParentOptions($object->parentdestination);
		}

		$form = $this -> xml_forms -> createform($formdata['xml'], $object, $formdropdowns, array());
		$this -> template -> write('sheet', $form);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

	public function autocreate($id)
	{

		$d = new Destination($id);
		$myText = $d -> minisummary . ' ' . $d -> title . ' ' . $d -> title . ' ' . $d -> title . ' ' . $d -> title . ' ' . $d -> introtitle . ' ' . $d -> minisummary . ' ' . $d -> title . ' ' . $d -> title . ' ' . $d -> introtitle . ' ' . $d -> details . ' ';
		$keywords = $this -> teamleaf -> generateKeywords($myText);
		$description = $d -> introtitle;

		if ($d -> parentdestination)
		{
			// this is a sub
			$md = new Destination($d -> parentdestination);
			$title = $d -> title . ' in ' . $md -> title . '. ' . $d -> minisummary;
			$description .= '. ' . $d -> title . ' is part of the ' . $md -> title . '. ' . $md -> introtitle;
		}
		else
		{
			$title = $d -> title . '. ' . $d -> minisummary;
		}
		
		$d->meta_description = $description;
		$d->meta_keywords = $keywords;
		$d->meta_title = $title;
		$d->save();
		

		$this -> maintaincache -> destination($d -> id);
		redirect(base_url() . 'admin/destination/display/' . $d -> id);
		
	}

}

//end class
