<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Profile extends Base_Controller
{

	/**
	 * Constructor for profile page
	 *
	 */
	function __construct()
	{
		parent::__construct('Destination');
	}

	public function display($id)
	{
		$object = $this -> model -> myGet($id);
		$this -> template -> write('page_header', $object -> title);

		if (0)//$cacheRes = $this -> maintaincache -> getCache('views/destinations/destination-' . $id . '.txt'))
		{
			$data['markup'] = $cacheRes;
			$this -> template -> write('sheet', $data['markup']);
		}
		else
		{
			$data['object'] = $object;
			$data['object_children'] = $object -> mySubGets($object -> id);
			$data['object_parent'] = $object -> myGet($object -> parentdestination);
			$data['object_gallerys'] = $this -> model -> gallerys($object);
			$data['object_sliders'] = $this -> model -> sliders($object);
			$data['object_highlights'] = $this -> model -> highlights($object);
			$rt = $this -> load -> view('destination/profile', $data, true);
			$this -> template -> write('sheet', $rt);
			$this -> maintaincache -> setCache('views/destinations/destination-' . $id . '.txt', $rt);
		}
		// Render template
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

	public function frontend_major_display($slug, $posted = false)
	{
		$this -> setTemplate('majordestination');

		$destinationModel = new Destination();
		$object = $destinationModel -> myGetBySlug($slug);
		$this -> template -> write('page_header', $object -> title);
		
		// NOTE TURNING ON THE CAHCE WILL MEAN NO TRAVEL EXPERT ID IN SESSION
		// MUST USE A HIDDEN FIELD IN THE FORM INSTEAD!!!! 		

		if (0)//$cacheRes = $this -> maintaincache -> getCache('views/destinations/frontend-major-destination-' . $object->id . '.txt'))
		{
			echo $cacheRes;
		}
		else
		{
			$data['object'] = $object;
			$data['object_children'] = $destinationModel -> mySubGets($object -> id);
			$data['object_parent'] = $destinationModel -> myGet($object -> parentdestination);
			$data['object_majors'] = $destinationModel -> allMajorGets();
			$data['object_gallerys'] = $destinationModel -> gallerys($object);
			$data['object_sliders'] = $destinationModel -> sliders($object);
			$data['object_highlights'] = $destinationModel -> highlights($object);
			$data['object_hotels'] = $destinationModel -> hotels($object);
			$data['object_expert'] = $destinationModel -> expert($object);
			$this -> teamleaf -> expertFlashSet($data['object_expert']->id);
			// GetCacheView($cacheFolder, $cacheFile, $realFolder, $realFile, $data)
			$data['object_sliders_markup'] = $this -> teamleaf -> getCacheView('views/destinations', 'destination_slider-' . $object -> id, 'destination', 'frontend_destinationslider', $data);
			$data['object_hotels_markup'] = $this -> teamleaf -> getCacheView('views/destinations', 'top-hotels-' . $object -> id, 'hotel', 'frontend_tophotels', $data);
			$data['object_children_markup'] = $this -> teamleaf -> getCacheView('views/destinations', 'subdestinations-' . $object -> id, 'destination', 'frontend_subdestinations', $data);
			$data['object_minigallery_markup'] = $this -> teamleaf -> getCacheView('views/destinations', 'minigallery-' . $object -> id, 'destination', 'frontend_minigallery', $data);
			//$data['wearetheexperts'] = $this->teamleaf->getCacheView('views/common', 'wearetheexperts', 'home', 'wearetheexperts');
			$data['callmeback'] = $this->teamleaf->getCacheView('views/common', 'callmeback', 'user', 'frontend_callmeback', $data);
			$data['travelexpert'] = $this->teamleaf->getCacheView('views/users', 'travelexpertbox-'.$object->expertID.'-'.$object->expertID.'-major-destination-'.$object->id, 'user', 'frontend_travelexpert_mini', $data);
			$data['latestreviews'] = $this -> load -> view('blog/latestreviews', '', true);
			$data['latestblogs'] = $this -> load -> view('blog/latestarticles', '', true);
			$object->blog->order_by("blogdate", 'desc')->where('state', 'Active')->limit(2)->get();
			$data['relatedarticles'] = $object->blog;
			$data['articles'] = $this->load->view('blog/relatedarticles', $data, true);
			
			$this -> template -> write_view('content', 'destination/frontend_major_profile', $data);

			// Save Cache
			$toCache = $this -> template -> render($region = NULL, $buffer = TRUE, $parse = FALSE);
			$this -> maintaincache -> setCache('views/destinations/frontend-destination-' . $object -> id . '.txt', $toCache);
		}

		// Render template
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

	
	public function frontend_sub_display($majorSlug, $slug)
	{
		$this -> setTemplate('subdestination');

		$destinationModel = new Destination();
		$object = $destinationModel -> myGetBySlug($slug);
		$this -> template -> write('page_header', $object -> title);

		if (0)//$cacheRes = $this -> maintaincache -> getCache('views/destinations/frontend-major-destination-' . $object->id . '.txt'))
		{
			echo $cacheRes;
		}
		else
		{
			$data['object'] = $object;
			$data['object_children'] = $destinationModel -> mySubGets($object -> id);
			$data['object_parent'] = $destinationModel -> myGet($object -> parentdestination);
			$data['object_majors'] = $destinationModel -> allMajorGets();
			$data['object_gallerys'] = $destinationModel -> gallerys($object);
			$data['object_sliders'] = $destinationModel -> sliders($object);
			$data['object_highlights'] = $destinationModel -> highlights($object);
			$data['object_hotels'] = $destinationModel -> hotels($object);
			$data['object_hotels_major'] = $destinationModel -> hotels($data['object_parent']);
			$data['object_expert'] = $destinationModel -> expert($object);
			$this -> teamleaf -> expertFlashSet($data['object_expert']->id);
			// GetCacheView($cacheFolder, $cacheFile, $realFolder, $realFile, $data)
			$data['object_sliders_markup'] = $this -> teamleaf -> getCacheView('views/destinations', 'destination_slider-' . $object -> id, 'destination', 'frontend_destinationslider', $data);
			$data['object_hotels_markup'] = $this -> teamleaf -> getCacheView('views/destinations', 'recommended-hotels-' . $object -> id, 'hotel', 'frontend_recommendedhotels', $data);
			
			$majorData['object_hotels'] = $data['object_hotels_major'];
			$majorData['object'] = $data['object_parent'];
			$data['object_hotels_major_markup'] = $this -> teamleaf -> getCacheView('views/destinations', 'recommended-major-hotels-' . $object -> id, 'hotel', 'frontend_tophotels', $majorData);
			
			$data['object_children_markup'] = $this -> teamleaf -> getCacheView('views/destinations', 'subdestinations-' . $object -> id, 'destination', 'frontend_subdestinations', $data);
			$data['object_minigallery_markup'] = $this -> teamleaf -> getCacheView('views/destinations', 'minigallery-' . $object -> id, 'destination', 'frontend_minigallery', $data);
			//$data['wearetheexperts'] = $this->teamleaf->getCacheView('views/common', 'wearetheexperts', 'home', 'wearetheexperts');
			$data['callmeback'] = $this->teamleaf->getCacheView('views/common', 'callmeback', 'user', 'frontend_callmeback', $data);
			$data['travelexpert'] = $this->teamleaf->getCacheView('views/users', 'travelexpertbox-'.$object->expertID.'-sub-destination-'.$object->id, 'user', 'frontend_travelexpert_mini', $data);

			$object->blog->order_by("blogdate", 'desc')->where('state', 'Active')->limit(2)->get();
			$data['relatedarticles'] = $object->blog;
			$data['articles'] = $this->load->view('blog/relatedarticles', $data, true);
			
			$this -> template -> write_view('content', 'destination/frontend_sub_profile', $data);

			// Save Cache
			$toCache = $this -> template -> render($region = NULL, $buffer = TRUE, $parse = FALSE);
			$this -> maintaincache -> setCache('views/destinations/frontend-destination-' . $object -> id . '.txt', $toCache);
		}

		// Render template
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

	public function index()
	{
		$this -> display($username = '');
	}

	public function view()
	{
		$this -> template -> write('page_header', l('_D_MY') . ' ' . l('_D_QUESTIONS'));
		$this -> load -> library('session');
		$this -> load -> helper('url');
		$userid = $this -> session -> userdata('userid');
		$data['assets'] = $this -> model -> getAssets();
		// super chain!
		$this -> template -> write_view('sheet', 'asset/view', $data);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

	/**
	 * Creates a User
	 *
	 */

	public function edit($id)
	{
		$this -> create($id, true);
	}

	public function create($id = null, $edit = false)
	{
		$this -> template -> write('page_header', 'Edit/Create Destination');
		$formdropdowns = '';
		$createDropDowns = false;

		if ($id)
		{
			$object = new Destination($id);
			$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/destination/forms', 'destination');
		}
		else
		{
			$object = new Destination();
			$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/destination/forms', 'destination');
		}

		if ($this -> input -> post())
		{
			// If posted and form validation passes
			if ($this -> form_validation -> run() == TRUE)
			{
				$data = $this -> input -> post();

				$path = 'destinations/profileimages';
				$this -> teamleaf -> checkpath($path);
				$fileresult = $this -> xml_forms -> uploadfiles(1, $path);
				if ($fileresult['result'])
				{
					$data['profileimage'] = @$fileresult[1]['file_name'];
				}
				$data['slugify'] = $this -> teamleaf -> slugify($data['title']);
				//$userid = $this -> session -> userdata('userid');
				$saved = DMZ_Array::from_array($object, $data, null, true);
				// this saves this automatically to the Task Object (e.g. same as $user->save());

				// Save the task
				if ($saved)
				{
					$this -> maintaincache -> destination($object -> id);

					if ($object -> meta_keywords == '')
					{
						redirect(base_url() . 'admin/destination/autocreate_seo/' . $object -> id);
					}
					else
					{
						redirect(base_url() . 'admin/destination/display/' . $object -> id);
					}

				}
				else
				{
					$this -> template -> write('validation_errors', 'Failed to save Destination');
				}
			}
			else
			{
				$createDropDowns = true;
				$this -> template -> write('validation_errors', validation_errors());
				// with some errors!
			}
		}
		else
		{
			$createDropDowns = true;

		}

		if ($createDropDowns)
		{
			// we need any drop downs to be created here
			$formdropdowns['parentdestination'] = $object -> getParentOptions($object -> parentdestination);
			$u = new User();
			$formdropdowns['expertID'] = $u -> getUserOptions($object -> expertID);
		}

		$form = $this -> xml_forms -> createform($formdata['xml'], $object, $formdropdowns, array());
		$this -> template -> write('sheet', $form);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

}

//end class
