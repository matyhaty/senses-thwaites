<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lister extends Base_Controller
{
    function __construct()
    {
        $this->load->library('form_validation');
        $this->load->helper(array(
            'form',
            'url'
        ));
        parent::__construct('Blog');

    }

    public function view()
    {
        $this->template->write('page_header', 'News and blogs');
        $userid = $this->session->userdata('userid');
        $data['destinations'] = $this->model->myGets();
        // super chain!
        $this->template->write_view('sheet', 'blog/listview', $data);
        $this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);
    }
}

//end class
