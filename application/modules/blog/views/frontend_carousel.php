<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide carouselHome">
	<div class="carousel-inner">

		<?php 
			$first = true;
			foreach($object_sliders as $slide) { ?>

		<div class="item <?php if($first) { echo 'active'; } ?>">
			<?php
			echo slir('/destinations/' . $object -> id . '/sliders/', $slide -> file1, $width = 1440, $height = 655, $crop = array(), 
			'title="' . $slide -> title . '" class="image"');
			?>
			<div class="container"></div>
		</div>

		<?php 
			$first = false;
			} ?>

	</div>
	<a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a><a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
</div>
<!-- /.carousel -->
