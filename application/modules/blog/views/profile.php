<div class='sheet'>
	
	<div class="page-header-grey">
        <div class='image'>
            <?php echo slir('/destinations/profileimages/', $object -> profileimage, $width = 95, $height = 65, $crop = array(
				'width' => '95',
				'height' => '65'
			), 'title="Profile image" class="profile-image"');
            ?>
        </div>
        <div class='titles'>
            <div class='title'>
                <?php echo $object -> title; ?>'s Profile
            </div>
            <div class='subtitle'>
                State: <?php echo $object -> state; ?>
            </div>
        </div>
        <div class='clear'></div>
    </div>
    
    <div class='innersheet'>
	
		<div class="grid_12">
	        <div class="profile-segment">
	            <div class='segment-header'>
	                <div class='title'>Basic Information</div>
	                <div class='option'><a href='<?php echo base_url(); ?>admin/destination/edit/<?php echo $object -> id; ?>'>Edit</a></div>
	            </div>
	            
	            <div class='segment-content'>
	                <table class='basic_information tt'>
	                	<tr>
	                        <td>Name</td>
	                        <td><?php echo $object -> title; ?></td>
	                    </tr>
	                    <tr>
	                        <td>Slug</td>
	                        <td><?php echo $this->teamleaf->slugify($object -> title); ?></td>
	                    </tr>
	                    <tr>
	                        <td>Parent</td>
	                        <td><?php echo $object_parent -> title; ?></td>
	                    </tr>
	                    <tr>
	                        <td>Children</td>
	                        <td>
	                        	<?php
								foreach ($object_children as $child)
								{
									echo $child -> title . ', ';
								}
	                        	?>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td>Intro</td>
	                        <td><?php echo $object -> introtitle; ?> </td>
	                    </tr>
	                    <tr>
	                        <td>Intro 2nd</td>
	                        <td><?php echo $object -> introtitle2; ?> </td>
	                    </tr>
	                    <tr>
	                        <td>Mini Summary</td>
	                        <td><?php echo $object -> minisummary; ?> </td>
	                    </tr>
	                    <tr>
	                        <td>Details</td>
	                        <td><?php echo $object -> details; ?></td>
	                    </tr>
	                    <tr>
	                        <td>Expert</td>
	                        <td><?php $u = new User(); $expert = $u->getUserByID($object->expertID);  echo $expert->firstname.' '.$expert->surname; ?></td>
	                    </tr>
	                    <tr>
	                        <td>Sort Order</td>
	                        <td><?php echo $object -> sortorder; ?></td>
	                    </tr>
	                </table>
	            </div>
	            
	            <div class="seo-segment">
		            <div class='segment-header'>
		                <div class='title'>SEO Information</div>
		                <div class='option'><a href='<?php echo base_url(); ?>admin/destination/edit_seo/<?php echo $object -> id; ?>'>Edit</a></div>
		                <div class='option'><a href='<?php echo base_url(); ?>admin/destination/autocreate_seo/<?php echo $object -> id; ?>'>Auto Create</a></div>
		            </div>
	            
		            <div class='segment-content'>
		                <table class='basic_information tt'>
		                	<tr>
		                        <td>Keywords</td>
		                        <td><?php echo $object -> meta_keywords; ?></td>
		                    </tr>
		                    <tr>
		                        <td>Description</td>
		                        <td><?php echo $object -> meta_description; ?> </td>
		                    </tr>
		                    <tr>
		                        <td>Title</td>
		                        <td><?php echo $object -> meta_title; ?></td>
		                    </tr>
		                </table>
		            </div>
				</div>
            </div>
       	</div>

		<div class="grid_12">
	        <div class="profile-segment">
	            <div class='segment-header'>
	                <div class='title'>Gallery</div>
  					<div class='option'><a href='<?php echo base_url(); ?>admin/destination/gallery/add/<?php echo $object -> id; ?>'>Add More</a></div>
	            </div>
	            
	            <div class='segment-content'>
	            	 <?php
						foreach ($object_gallerys as $gallery)
						{
						?>
		            	<div class='image-container'>
		              		<?php
								echo slir('/destinations/'.$object->id.'/gallerys/', $gallery -> file1, $width = 95, $height = 65, $crop = array(
									'width' => '95',
									'height' => '65'
								), 'title="'.$gallery -> title.'" class="gallery-image"');
							?>
							<div class='image-options'>
								<?php echo $gallery->sortorder; ?> 
								&middot; 
								<?php echo anchor('admin/destination/gallery/edit/'.$object->id.'/'.$gallery->id, 'Edit'); ?>
								&middot;
								<?php echo anchor('admin/destination/gallery/remove/'.$object->id.'/'.$gallery->id, 'Remove'); ?>
							</div>
						</div>
						<?php } ?>				
	            </div>
	            <div class='clear'></div>	
	            
	            <div class="seo-segment">
		            <div class='segment-header'>
		                <div class='title'>Sliders</div>
		                <div class='option'><a href='<?php echo base_url(); ?>admin/destination/slider/add/<?php echo $object -> id; ?>'>Add More</a></div>
		            </div>
	            
		            <div class='segment-content'>
		                <?php
						foreach ($object_sliders as $gallery)
						{
						?>
		            	<div class='image-container'>
		              		<?php
								echo slir('/destinations/'.$object->id.'/sliders/', $gallery -> file1, $width = 115, $height = 65, $crop = array(
									'width' => '115',
									'height' => '65'
								), 'title="'.$gallery -> title.'" class="gallery-image"');
							?>
							<div class='image-options'>
								<?php echo $gallery->sortorder; ?> 
								&middot; 
								<?php echo anchor('admin/destination/slider/edit/'.$object->id.'/'.$gallery->id, 'Edit'); ?>
								&middot;
								<?php echo anchor('admin/destination/slider/remove/'.$object->id.'/'.$gallery->id, 'Remove'); ?>
							</div>
						</div>
						<?php } ?>		
		            </div>
				</div>
				<div class='clear'></div>
				
				<div class="seo-segment">
		            <div class='segment-header'>
		                <div class='title'>Highlights</div>
		                <div class='option'><a href='<?php echo base_url(); ?>admin/destination/highlight/add/<?php echo $object -> id; ?>'>Add More</a></div>
		            </div>
	            
		            <div class='segment-content'>
		                <?php
						foreach ($object_highlights as $gallery)
						{
						?>
						
		            	<div class='image-container'>
		            		<div class='image-options'>
		              	<?php echo $gallery->title; ?>
		              </div>
		              		<?php
								echo slir('/destinations/'.$object->id.'/highlights/', $gallery -> file1, $width = 115, $height = 65, $crop = array(
									'width' => '115',
									'height' => '65'
								), 'title="'.$gallery -> title.'" class="gallery-image"');
							?>
							<div class='image-options'>
								<?php echo $gallery->sortorder; ?> 
								&middot; 
								<?php echo anchor('admin/destination/highlight/edit/'.$object->id.'/'.$gallery->id, 'Edit'); ?>
								&middot;
								<?php echo anchor('admin/destination/highlight/remove/'.$object->id.'/'.$gallery->id, 'Remove'); ?>
							</div>
						</div>
						<?php } ?>		
		            </div>
				</div>
				<div class='clear'></div>
				
				
				
				<div class="tips-segment">
		            <div class='segment-header'>
		                <div class='title'>Destination Tips</div>
		                <div class='option'><a href='<?php echo base_url(); ?>admin/destination/edit/<?php echo $object -> id; ?>'>Edit</a></div>
		            </div>
	            
		            <div class='segment-content'>
		                <table class='basic_information tt'>
		                	<tr>
		                        <td>Tips</td>
		                        <td><?php echo $object -> destinationtips; ?></td>
		                    </tr>
		                    
		                </table>
		            </div>
				</div>
				
				<div class="tips-segment">
		            <div class='segment-header'>
		                <div class='title'>When to Travel</div>
		                <div class='option'><a href='<?php echo base_url(); ?>admin/destination/edit/<?php echo $object -> id; ?>'>Edit</a></div>
		            </div>
	            
		            <div class='segment-content'>
		                <table class='basic_information tt'>
		                	<tr>
		                        <td>Info</td>
		                        <td><?php echo $object -> whentotravel; ?></td>
		                    </tr>
		                    
		                </table>
		            </div>
				</div>
				
				<!--
				
				<div class="tips-segment">
		            <div class='segment-header'>
		                <div class='title'>Destination Type</div>
		                <div class='option'><a href='<?php echo base_url(); ?>destination/edit/<?php echo $object -> id; ?>'>Manage</a></div>
		            </div>
	            
		            <div class='segment-content'>
		                <table class='basic_information tt'>
		                	<tr>
		                        <td>Types</td>
		                        <td><?php echo $object -> type; ?></td>
		                    </tr>
		                    
		                </table>
		            </div>
				</div>
				
				
				
				<div class="tips-segment">
		            <div class='segment-header'>
		                <div class='title'>Destination Features</div>
		                <div class='option'><a href='<?php echo base_url(); ?>destination/edit/<?php echo $object -> id; ?>'>Manage</a></div>
		            </div>
	            
		            <div class='segment-content'>
		                <table class='basic_information tt'>
		                	<tr>
		                        <td>Features</td>
		                        <td><?php echo $object_tips -> details; ?></td>
		                    </tr>
		                    
		                </table>
		            </div>
				</div>
				-->
				
            </div>
        </div>

        <div class='clear'></div>
    </div>
</div>