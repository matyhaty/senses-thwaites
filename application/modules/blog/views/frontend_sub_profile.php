<div class="grid_4">
    <div class="hotelheadingbox">
        <div class='heading'>
        	<?php echo $object -> title; ?>
       	</div>
        <div class='subheading'>
        	<?php echo $object -> minisummary; ?>
      	</div>
        <div class='call'>
        	Call us now on:<br />
        </div>
        <div class='telephone'><?php echo $this->teamleaf->getTel(false); ?><br />
        </div>
        <div class='enquiry'>
        	Let us create you the perfect holiday</div>
    </div>
</div>
<div class="grid_8">
	
	
    <?php echo $object_sliders_markup; ?>
</div>
<div class="clear"></div>

<div class="grid_4 gridpadtop">
    <?php echo $travelexpert; ?>
        <div class="destination-tips">
       
            <h2> <a href="#" id="toggle-contactform"><?php echo $object -> title; ?> Travel Tips</a> </h2>
            <div>
                <?php echo $object -> destinationtips; ?>					
          
        </div>
    </div>
</div>

<div class="grid_8">
    <div class='hotelintro'>
        <div class='details'><?php echo $object->details; ?></div>
    </div>
    <?php echo $object_hotels_markup; ?>
</div>
<div class="clear"></div>

<div class="grid_12">
    <div class='callus12'><span class='textwhitebg'><?php echo $this->teamleaf->getTel(); ?></span></div>
    <div class="line"></div>
</div>
<div class="clear"></div>


<div class="grid_4">
	<?php echo $object_hotels_major_markup; ?>
</div>

<div class="grid_4">
    <?php echo $callmeback; ?>
    <?php echo $object_minigallery_markup; ?>
</div>

<div class="grid_4">
    <?php echo $articles; ?>
</div>
<div class="clear"></div>

