<div class='sheet'>
    <div class='innersheet'>
        <!-- Start of Content of Sheet -->
        <div class="grid_8 page-header">
            <div class='title'>
                Destination Register
            </div>
            <div class='subtitle'>
                Current live destinations on the system.
            </div>
        </div>
        <div class="grid_16 page-options">
            <div class='option'>
                <a class="" id='createatask' href='<?php echo site_url('admin/destination/create'); ?>'>
                <div class='title'>
                    Create a New Destination
                </div>
                <div class='subtitle'>
                    Quick add a new destination.
                </div> </a>
            </div>
            
            
    
        </div>
        <div class="clear"></div>
        <div class="grid_24 datatable">
            
            <div class='somethingelse'></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display bordered-table" id="user_table" width="100%">
                <thead>
                    <tr>
                        <th class='idColumn'>No:</th>
                        <th>Parent Destination</th>
                        <th>Title</th>
                        <th>Sort Order</th>
                        <th class='stateColumn'>State</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                    foreach ($blogs as $item)
                    {
						//$md = new Destination($item -> parentdestination);
                    ?>

                    <tr id='<?php echo $item -> id; ?>'> 
                        <td><?php echo $item -> id; ?></td>
                        <td><?php echo $md -> title; ?></td>
                        <td><?php echo $item -> title; ?></td>
                       
                        <td><?php echo $item -> sortorder; ?></td>
                        <td><?php echo $item -> state; ?></td>
                    </tr>
                    <?php
					}
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>No:</th>
                        <th>Parent Destination</th>
                        <th>Name</th>
                        <th>Sort Order</th>
                        <th>State</th>
                    </tr>
                </tfoot>
            </table>
            
            
            <script type="text/javascript" charset="utf-8">
                				$(document).ready(function() {
                    $('#user_table').dataTable({
                        'bJQueryUI' : true,
                        "sDom" : '<"top"  <"info"i>   <"show"l>  <"pagination"p> <"search"f> >rt<"bottom"><"clear">',
                        "oLanguage" : {
                            "sInfo" : "_TOTAL_ entries (_START_ to _END_)",
                            "sSearch" : "",
                        },
                        'iDisplayLength' : 50,
                        "aaSorting": [[ 1, "asc" ]],
                    });

                    $('#user_table tr').live('click', function() {
                        if(this.id) {
                            //alert(this.id);
                            window.location = '<?php echo base_url(); ?>admin/destination/display/'+this.id;
                        }
                    });
                });

            </script>
            
            
        </div>
        <div class="clear"></div>
        <!-- End of Content of Sheet -->
        <!-- Start of Content of Form area -->
        
    </div>
</div>