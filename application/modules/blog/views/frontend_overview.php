<?php
    $majordestinations = new Destination();
    $majordestinations->where('parentdestination', '');
    $majordestinations->where('state', 'Active');
    $majordestinations->order_by('title', 'asc');
    $majordestinations->get();

    $majorcount = 0;
    foreach ($majordestinations as $md)
    {   
        $subdestinations = '';
        $subdestinations = new Destination();
        $subdestinations->where('parentdestination', $md->id);
        $subdestinations->where('state', 'Active');
        $subdestinations->order_by('title', 'asc');
        $subdestinations->limit(3);
        $subdestinations->get();
        $count = 0;

?>

<div class="grid_3">
	<div class="destinations-box-page">
		<div class="">
			<div class="">
				<div class="">
					<div class="menu-header-page">
						<h3>
							<?php //echo anchor('destinations/' . $this -> teamleaf -> slugify($md -> title) . '/' . $md -> id, htmlspecialchars($md -> title));?>
							<?php echo $this->teamleaf->link_majordestination($md, htmlspecialchars($md->title)); ?>
						</h3>
					</div>
					<div class="menu-dots-page">
						<div class="menu-thumb">
							<?php $pic = slir('/destinations/'.$md ->id.'/profileimage/', $md -> profileimage, $width = 225, $height = 70, $crop = array('width' => 255, 'height' => 70), 'title="' . $md -> title . '" class="image"'); ?>
							<?php echo $this->teamleaf->link_majordestination($md, $pic);?>

							<?php
								foreach ($subdestinations as $s)
								{
									echo '<div style="float:left">';
									$pic = slir('/destinations/' . $s -> id . '/profileimage/', $s -> profileimage, $width = 75, $height = 35, $crop = array('width' => 75, 'height' => 35), 'title="' . $md -> title . '" class="image"');
									echo $this->teamleaf->link_majordestination($md, $pic);
									echo '</div>';
								}
							?>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?php
	}
?>

<div class="clear"></div>