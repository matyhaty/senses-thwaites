<div class="grid_4">
    <div class="hotelheadingbox">
        <div class='heading'>
        	<?php echo $object -> title; ?>
       	</div>
        <div class='subheading'>
        	<?php echo $object -> minisummary; ?>
      	</div>
        <div class='call'>
        	Call us now on:<br />
        </div>
        <div class='telephone'><?php echo $this->teamleaf->getTel(false); ?><br />
        </div>
        <div class='enquiry'>
        	Let us create you the perfect holiday</div>
    </div>
</div>
<div class="grid_8">
	
	
    <?php echo $object_sliders_markup; ?>
</div>
<div class="clear"></div>

<div class="grid_4 gridpadtop">
    <?php echo $travelexpert; ?>
    
</div>


<div class="grid_8">
    <div class='hotelintro'>
    	<div class='introtext'>"<?php echo $object -> introtitle; ?>"</div>
        <div class='details'><?php echo $object -> details; ?></div>
    </div>
</div>
<div class="clear"></div>


<div class="grid_12">
   <?php echo $object_children_markup; ?>
</div>
<div class="clear"></div>


<div class="grid_4">
	<?php echo $object_hotels_markup; ?>
</div>

<div class="grid_4">
    <?php echo $callmeback; ?>
    <?php echo $object_minigallery_markup; ?>
</div>

<div class="grid_4">
    <?php echo $articles; ?>
</div>
<div class="clear"></div>

