<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * 
 *
 */
class Files extends Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->model = new File('file');	
		$this->model->cache = $this->cache;		
	}
	
	/**
	 *  Send image to deleted
	 *
	 * @return unknown
	 */
	public function delete()
	{
		$fileType			= $this->uri->segment(3);
		$id					= $this->uri->segment(4);
		$this->model->where( array('id'=>$id) );
		switch($fileType)
		{
			case 'event' :
				break;
			default:
		}
		
		$this->model->get();
		$this->model->state			= 'deleted';
		$this->model->deleted_on	= time();
		$this->model->save();	
		die( Javascript::removeListItem($id) );	
		
	}
	
	/**
	 * Allow a user tdd a file 
	 *
	 */
	public function create()
	{
	
		
		$formdata					= $this->xml_forms->loadXML( l('_D_URI_FILE_FORM_CREATE') , 'create_file');
		$formdropdowns['category']	= category::getCategoryOptions($currentFlag = NULL);
		$this->template->write('page_header',  l('_D_CREATE'). ' ' . l('_D_FILE') );

		// Only handle if posted
		if($this->input->post())
		{
			// If posted and form validation passes
			if($this->form_validation->run() == TRUE)
			{
				$data			= $this->input->post();
				$fileDM			= $this->xml_forms->uploadfiles(1, l('_D_FILE_USER_UPLOADS_REL') );
				$category		= $data['category'];
				$categoryDM		= new category($category);
				$categoryDM->save($fileDM);
				// try adding it to the activity cache
				try{
					$this->model->createCacheActivity( 'image' , $fileDM->to_array() );
				}catch(Exception $e){
					die('Handle error!');
					// the exception was thrown, now handle it
				}		
				
				
				$this->_show_message( l('_D_FILE') . ' ' . l('_D_SAVED') ,  l('_D_URI_FILE_CREATE') );
				
				
			}else{
				$this->template->write('validation_errors', validation_errors()); // with some errors!
			}
		}
		// Render output
		$form = $this->xml_forms->createform($formdata['xml'], $this->model , $formdropdowns ,array() );
		$this->template->write( 'content', $form );
		$this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);
		
		
		
	}
	
} //end class