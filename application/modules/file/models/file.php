<?php
/**
 * Class *
 */
class File extends Base_Model
{

    /**
     * table name
     *
     * @var String
     */
    public $table = 'files';
    public $has_one = array(
        'message',
        'job',
        'wall',
        'grow',
        'lecture',
        'category',
        'group',
        'user',
        'qualification'
    );
    public $has_many = array('files');

    /**
     * Constructor
     *
     * @param unknown_type $id
     */
    function __construct($id = NULL)
    {
        parent::__construct($id);
    }

}
