	<?php echo permAnchor( $this->user , l('_D_URI_EVENTS_IMAGES') . $data['id'] , l('_D_ADD') . ' ' . l('_D_EVENT')  ); ?> | 	
	<?php echo permAnchor( $this->user , l('_D_URI_EVENTS_EDIT')   . $data['id'] , l('_D_EDIT') . ' ' . l('_D_EVENT') ); ?> | 
	<?php echo permAnchor( $this->user , l('_D_URI_EVENTS_DELETE') . $data['id'] , l('_D_DELETE') . ' ' . l('_D_EVENT') ); ?> | 
	<?php echo permAnchor( $this->user , l('_D_URI_EVENTS_IMAGES') . $data['id'] ,  l('_D_ADD') . ' ' . l('_D_IMAGES')  ); ?>	
	
	<h2><?php echo $data['title']?></h2>
	<p><?php  echo $data['description']?></p>
	
	<ul>
	<?php foreach($media as $image):?>
		<?php $file			= 'file_' . $image['id']; ?>
		<?php $busySelector = 'busy_file_' . $image['id']; ?>
		<li id="list-item-<?php echo $image['id'];?>">
		
			<?php echo slir(  l('_D_FILE_FORM_EVENT_WALL_IMAGES_REL') , $image['file_name'] , $width=150, $height=150, $crop = array('width'=>'150','height'=>'150'  )  ); ?>			
			<a href="#" id="<?php echo $file?>" >x</a>
			<span id="<?php echo $busySelector;?>"></span>
			<?php Javascript::link( '#' . $file , l('_D_URI_EVENTS_WALL_IMAGES_DEL') .$image['id'] , $processFunction='custom.removeListItem', $busySelector );?>
		</li>
	<?php endforeach; ?>
	</ul>
	
