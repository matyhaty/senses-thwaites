<div class='sheet'>
    <div class="page-header-grey">
        
        <div class='titles'>
            <div class='title'>
                <?php echo $cm -> title; ?>'s Information Page
            </div>
            <div class='subtitle'>
                <?php echo anchor('somewhere', 'Preview Post on Site'); ?>
            </div>
        </div>
        <div class='clear'></div>
    </div>
    <div class='innersheet'>
        <!--
        	<div class="grid_24 page-tab-bar">
            <ul>
                <li>Latest Activity</li>
                <li>Basic Information</li>
                <li>Qualifications</li>
                <li>Attendance</li>
                <li>Depots</li>
            </ul>
        </div>
        <div class='clear'></div>  
        -->  
            
        <div class="grid_24 profile-segment">
            <div class='segment-header'>
                <div class='title'>Basic Information</div>
                <div class='option'><a href='<?php echo base_url(); ?>admin/menus/profile/create/<?php echo $cm -> id; ?>'>Edit</a></div>
                       </div>
            
            <div class='segment-content'>
            
                <table class='basic_information tt'>
                	<tr>
                        <td>Title</td>
                        <td><?php echo $cm -> title; ?> </td>
                    </tr>
                    <tr>
                        <td>Created</td>
                        <td><?php echo $this->teamleaf->date($cm -> created); ?></td>
                    </tr>
                    <tr>
                        <td>Created By</td>
                        <td><?php echo ($cm -> created_by); ?></td>
                    </tr>
                    <tr>
                        <td>State</td>
                        <td><?php echo $cm -> state; ?></td>
                    </tr>
                  
                   
                   
                </table>
            
            </div>
            
            
        </div>
        
        <div class="grid_24 profile-segment">
            <div class='segment-header'>
                <div class='title'>Images And Files</div>
                <div class='option'><a href='<?php echo base_url(); ?>admin/menus/profile/create/<?php echo $cm -> id; ?>'>Edit</a></div>
            </div>
            
            <div class='segment-content'>
            
            <?php 

			$imageRT = '';
			
            for($i=1; $i<6; $i++) 
            {
            	$nameImage = 'image'.$i;
            	if($cm->$nameImage)
				{
					echo '<div style="padding:10px; float:left;">'. slir('/menus/'.$cm->id.'/', $cm->$nameImage, $width = 424, $height = 600, $crop = array(
						'width' => '424',
						'height' => '600'
					), 'title="Image '. $i .': '.$cm->$nameImage.'" class="profile-image"').'</div>';
				}
				
				
            	
            } 
            
            ?>
            
            <div class='clear'></div>
            
                <table class='basic_information tt'>
                    <tr>
                        <td>Image 1</td>
                        <td><?php echo $cm -> image1; ?> </td>
                    </tr>
                    
                  
                   
                </table>
            
            </div>
            
            
        </div>
        
        
        <div class='clear'></div>        
    </div>
</div>