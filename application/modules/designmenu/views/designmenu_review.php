<!-- FORMS
        ================================================== -->

<?php setlocale(LC_MONETARY, 'en_GB'); ?>
<?php echo form_open('designmenu/review'); ?>
<?php $someitems = false; ?>
<?php $somedesign = false; ?>

    <div class="row">
        <div class="span24">
            <div class="form-chain"></div>
        </div>
    </div>

    <div class="row">
        <div class="span24">
            <div class="form-container">
            	<?php  foreach($librarysections as $librarysection) {  ?>
					<?php if($this->input->post($librarysection->code.'_1_title')) { $someitems = true; ?>
		                <div class="row">
		                    <div class="span5">
		                        <div class="left-content">
		                            <h1><?php echo $librarysection->title; ?></h1><?php //echo $librarysection->description; ?>
		                        </div>
		                    </div>
		                    <div class="span19">
		                        <div class="right-content">
		                            <?php 
		                            	$num = 1;
		                            	//echo 'Codes: '. $librarysection->code.' Num: '.$num;
		                            	while($this->input->post($librarysection->code.'_'.$num.'_title'))   
		                            	{
		                            		$title = $librarysection->code.'_'.$num.'_title';
		                            		$price = $librarysection->code.'_'.$num.'_price';
		                            		
		                            		echo '<div class="span13">'.$this->input->post($title).'</div>';
		                            		echo '<div class="span3 offset1"> &pound; '.$this->input->post($price).'</div>';
		                            		
                                            echo form_hidden($librarysection->code.'_'.$num.'_title', $title);
                                            echo form_hidden($librarysection->code.'_'.$num.'_price', $price);
                                            
		                            		//echo '<br/>';
		                            		$num++;                             
		                                }
		                             ?>
		                        </div>
		                    </div>
		                </div>
	                <?php } //end if ?>
                <?php } ?>
                
                <?php if(!$someitems) { ?>
                    
                    <div class="row">
                            <div class="span5">
                                <div class="left-content">
                                    <h1>Menu Items
                                </div>
                            </div>
                            <div class="span19">
                                <div class="right-content">
                                    You did not seem to add any items to your menu. <br/>Please press the back button and add some.
                                </div>
                            </div>
                    </div>
                    
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="span24">
            <div class="form-chain"></div>
        </div>
    </div>


    <div class="row">
        <div class="span24">
            <div class="form-container">
                <div class="row">
                    <div class="span5">
                        <div class="left-content">
                            <h1>Design</h1>Your chosen design
                        </div>
                    </div>

                    <div class="span19">
                        <div class="right-content">
                            <div class="controls controls-row">
                            	<?php if($this->input->post('menu_design')) { 
                            	    $somedesign = true;
                            	    $menu = new DesignMenu($this->input->post('menu_design'));
                            	    
                            	    ?>
                                <div class="span3">
                                	<img class ='selectdesign ' id='<?php echo $this->input->post('menu_design'); ?>' src="<?php echo base_url(); ?>uploads/menus/<?php $menu->id; ?>/<?php echo $menu->image1; ?>" />
                                </div>
                               <?php  echo form_hidden('menu_design', $this->input->post('menu_design')); ?>
                                
                                <?php } else { ?>  
                                    
                                    You did not choose a design.
                                    
                                <?php } ?>
                            </div>
                            <input class='span14 selectedDesignInput' type='hidden' name='menu_design' value='poster1.jpg' />
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="span24">
            <div class="form-chain"></div>
        </div>
    </div>


    <div class="row">
        <div class="span24">
            <div class="form-container">
                <div class="row">
                    

                    <?php if($somedesign && $someitems) { ?>
                        
                        <div class="span5">
                        <div class="left-content">
                            <h1>The Order</h1>
                            How many, sizes, the lot...
                        </div>
                    </div>
                    
                    
                    <div class="span19">
                        <div class="right-content">
                            <div class="controls controls-row">
	                           	<?php echo $this->input->post('quantity'); ?> Copies. <br/>
	                           	<?php echo $this->input->post('lamination'); ?> <br/>
	                           	<?php echo $this->input->post('proof'); ?> <br/>
	                           	Order Notes:<br/>
	                           
	                           	
	                           	
	                           	<?php if($this->input->post('quantity') == '10') { $copies_price = 16.60; } ?>
	                           	<?php if($this->input->post('quantity') == '20') { $copies_price = 35.60; } ?>
	                           	<?php if($this->input->post('quantity') == '30') { $copies_price = 53.50; } ?>
	                           	<?php if($this->input->post('quantity') == '40') { $copies_price = 72.00; } ?>
	                           	<?php if($this->input->post('quantity') == '50') { $copies_price = 89.00; } ?>
	                           	<?php if($this->input->post('quantity') == '100') { $copies_price = 160.00; } ?>
	                           	
	                           	<?php  echo form_hidden('quantity',$this->input->post('quantity').'|'.$copies_price); ?>
	                           	
	                           	<?php if($this->input->post('lamination') == 'Laminate - Gloss') { $lam_price = 25.00; } ?>
	                           	<?php if($this->input->post('lamination') == 'Laminate - Matt') { $lam_price = 25.00; } ?>
	                           	<?php if($this->input->post('lamination') == 'No Lamination') { $lam_price = 0.00; } ?>
	                           	
	                           	   <?php  echo form_hidden('lamination',$this->input->post('lamination').'|'.$lam_price); ?>
	                           	
	                           	<?php if($this->input->post('proof') == 'Proof') { $proof_price = 5.00; } ?>
	                           	<?php if($this->input->post('proof') == 'No Proof') { $proof_price = 0.00; } ?>
	                           	
	                           	<?php  echo form_hidden('proof',$this->input->post('proof').'|'.$proof_price); ?>
	                           	
	                           		<?php echo $this->input->post('notes'); ?> <br/>
	                           		<?php  echo form_hidden('notes',$this->input->post('notes')); ?>
	                           		
	                           	<b>Total: </b><?php echo money_format('%i', $copies_price + $lam_price + $proof_price); ?><br/><br/>

	             
	                        
	                            <div class="controls controls-row">
	                                <button type="submit" class="btn <?php echo $librarysection->code; ?>">Add to Cart</button>
	                            </div>
	                        </div>
                        </div>
                    </div>
                    <?php } else { ?>
                    
                        <div class="row">
                            <div class="span5">
                                <div class="left-content">
                                    <h1>Oops
                                </div>
                            </div>
                            <div class="span19">
                                <div class="right-content">
                                    You did not seem to add items and a design. <br/>Please press the back button and select some items and a design to this menu.
                                </div>
                            </div>
                    </div>
                    
                    
                    <?php } ?>
                    
                </div>
            </div>
        </div>
    </div>
    
</form>
