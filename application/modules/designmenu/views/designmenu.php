<!-- FORMS
        ================================================== -->


<?php echo form_open('designmenu/review'); ?>

<div class="row">
        <div class="span24">
            <div class="form-chain"></div>
        </div>
    </div>
    
    

<?php  foreach
($librarysections as $librarysection)
	{ ?>

    <div class="row">
        <div class="span24">
            <div class="form-container">
                <div class="row">
                    <div class="span7">
                        <div class="left-content">
                            <h1><?php echo $librarysection->title; ?></h1><?php echo $librarysection->description; ?>
                        </div>
                    </div>

                    <div class="span17">
                        <div class="right-content">
                            <div class="controls controls-row">
                                <input class="span12 <?php echo $librarysection->code; ?>_autocomplete" type="text" placeholder="Enter title here..." name='<?php echo $librarysection->code; ?>_1_title'> 
                                <input class="span3" type="text" placeholder="Add Price..." name='<?php echo $librarysection->code; ?>_1_price'>

                                <div class="span1">
                                    <img src="assets/frontend/img/menu.gif" alt="Menu">
                                </div>
                            </div>

                            <div class="controls controls-row">
                                <input class="span12 <?php echo $librarysection->code; ?>_autocomplete" type="text" placeholder="Enter title here..." name='<?php echo $librarysection->code; ?>_2_title'> 
                                <input class="span3" type="text" placeholder="Add Price..." name='<?php echo $librarysection->code; ?>_2_price'>

                                <div class="span1">
                                    <img src="assets/frontend/img/menu.gif" alt="Menu">
                                </div>
                            </div>

                            <div class="controls controls-row">
                                <input class="span12 <?php echo $librarysection->code; ?>_autocomplete" type="text" placeholder="Enter title here..." name='<?php echo $librarysection->code; ?>_3_title'> <input class="span3" type="text" placeholder="Add Price..." name='<?php echo $librarysection->code; ?>_3_price'>

                                <div class="span1">
                                    <img src="assets/frontend/img/menu.gif" alt="Menu">
                                </div>
                            </div>

                            <div class='moreRows<?php echo $librarysection->code; ?>'>
                                <div class='<?php echo $librarysection->code; ?>BlankSpace'></div>
                            </div>

                            <div class="controls controls-row">
                                <button type="button" class="btn addRow<?php echo $librarysection->code; ?>">Add Another</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="span24">
            <div class="form-chain"></div>
        </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function() {

                var num = 3;

                var <?php echo $librarysection->code; ?>HTML = "\
                            <div class='controls controls-row'>\
                                <input class='span12 <?php echo $librarysection->code; ?>_autocomplete' type='text' placeholder='Enter title here...' name='<?php echo $librarysection->code; ?>_"+num+"_title' />\
                                <input class='span3' type='text' placeholder='Add Price...' name='<?php echo $librarysection->code; ?>_"+num+"_price' />\
                                <div class='span1'><img src='assets/frontend/img/menu.gif' alt='Menu'><\/div>\
                            <\/div>";


                $(".addRow<?php echo $librarysection->code; ?>").click(function()
                {
                  $('.<?php echo $librarysection->code; ?>BlankSpace').append(<?php echo $librarysection->code; ?>HTML);
                  num++;
                  return false;
                });
            });



            $(function() {
            //function log( message ) {
            //  $( "<div>" ).text( message ).prependTo( "#log" );
            //  $( "#log" ).scrollTop( 0 );
            //}

                $('.<?php echo $librarysection->code; ?>_autocomplete').live('keyup.autocomplete', function(){
                    $(this).autocomplete({
                        source: "<?php echo base_url(); ?>designmenu/autocomplete/index/<?php echo $librarysection->code; ?>",
                        minLength: 2,
                        select: function( event, ui ) {
                            //log( ui.item ?
                            //"Selected: " + ui.item.value + " aka " + ui.item.id :
                            //"Nothing selected, input was " + this.value );
                        }
                    });
                });
            });

    </script>

    <?php } ?>

    <div class="row">
        <div class="span24">
            <div class="form-container">
                <div class="row">
                    <div class="span5">
                        <div class="left-content">
                            <h1>Design</h1>Pick your design
                        </div>
                    </div>

                    <div class="span19">
                        <div class="right-content">
                            <div class="controls controls-row">
                                
                                <?php foreach($menudesigns as $menu) {
                                  ?>
                                   <div class="span3" style='margin:5px;'>
                                       <div style='height: 205px;'>
                                    <img class ='selectdesign' id='<?php echo $menu->id; ?>' src="<?php echo base_url(); ?>uploads/menus/<?php $menu->id; ?>/<?php echo $menu->image1; ?>" />
                                    </div>
                                    </div>
                                <?php  
                                }
                                ?>
                               
                            </div>
                            <input class='span14 selectedDesignInput' type='hidden' name='menu_design' value='1' />
                            <br/>

                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
	    $(document).ready(function() {
	        $(".selectdesign").click(function()
	        {
	          //alert('slected a design');
	          $(".selectdesign").removeClass("selected");
	          $(this).addClass('selected');
	          $('.selectedDesignInput').val(this.id);
	        });
	    });
    </script>



    <div class="row">
        <div class="span24">
            <div class="form-chain"></div>
        </div>
    </div>


    <div class="row">
        <div class="span24">
            <div class="form-container">
                <div class="row">
                    <div class="span5">
                        <div class="left-content">
                            <h1>All the bits</h1>
                            How many, sizes, the lot...
                        </div>
                    </div>

                    <div class="span19">
                        <div class="right-content">
                            <div class="controls controls-row">

                            	<label class="radio">
								  <input type="radio" name="quantity" id="" value="10" checked>
								  10 Copies - &pound;16.60
								</label>
								<label class="radio">
								  <input type="radio" name="quantity" id="" value="20" >
								  20 Copies - &pound;35.60
								</label>
								<label class="radio">
								  <input type="radio" name="quantity" id="" value="30" >
								  30 Copies - &pound;53.50
								</label>
								<label class="radio">
								  <input type="radio" name="quantity" id="" value="40" >
								  40 Copies - &pound;72.00
								</label>
								<label class="radio">
								  <input type="radio" name="quantity" id="" value="50" >
								  50 Copies - &pound;89.00
								</label>
								<label class="radio">
								  <input type="radio" name="quantity" id="" value="100" >
								  100 Copies - &pound;160.00
								</label>

                            </div>
                            <br/>

                            <div class="controls controls-row">

                            	<label class="radio">
								  <input type="radio" name="lamination" id="" value="Laminate - Gloss" checked>
								  Laminate Menus (Gloss) - &pound;25
								</label>
								<label class="radio">
								  <input type="radio" name="lamination" id="" value="Laminate - Matt" checked>
								  Laminate Menus (Matt) - &pound;25
								</label>
								<label class="radio">
								  <input type="radio" name="lamination" id="" value="No Lamination" >
								  Do not laminate
								</label>
							</div>
								<br/>
							<div class="controls controls-row">

                            	<label class="radio">
								  <input type="radio" name="proof" id="" value="Proof" checked>
								  Proof Menus - &pound;5								</label>
								<label class="radio">
								  <input type="radio" name="proof" id="" value="No Proof" >
								  Do not laminate
								</label>
							</div>
							<br/>
							
							<div class="controls controls-row">
								<textarea class="span16" type="text" placeholder="Add any notes..." name='notes'></textarea>
							</div>
							<br/>
							

                            <div class="controls controls-row">
                                <button type="submit" class="btn <?php echo $librarysection->code; ?>">Review Order</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</form>
