<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Profile extends Base_Controller
{

	/**
	 * Constructor for profile page
	 *
	 */
	function __construct()
	{

		$this -> load -> library('form_validation');
		$this -> load -> helper(array(
			'form',
			'url'
		));
		parent::__construct();
		$this -> model = new Designmenu('designmenu');
		$this -> model -> cache = $this -> cache;

	}


	public function display($cmsID)
	{
		$this -> load -> helper('url');
		// Get the user profile
		// if its a post, we should search the DB
		$this -> template -> write('page_header', '');
        
        $cm = $this -> model -> getMenu($cmsID);

		
		//$user_company = $this->model->getUser_Company($user);

		$this -> template -> write_view('sheet', 'designmenu/profile', array(
			"cm" => $cm,
		
		));

		// Render template
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

	public function view()
	{
		$this -> template -> write('page_header', l('_D_MY') . ' ' . l('_D_QUESTIONS'));
		$this -> load -> library('session');
		$this -> load -> helper('url');
		$data['cms'] = $this -> model -> getMenus();
		// super chain!
		$this -> template -> write_view('sheet', 'designmenu/view', $data);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

	public function create($id = null)
	{
		$this -> template -> write('page_header', l('_D_CREATE') . ' ' . l('_D_QUESTIONS'));
		$user = $this -> model;
		$formdropdowns = '';

		if ($id)
		{
			$cm = new Designmenu($id);
		}
		else
		{
            $cm = new Designmenu();
		}
		// Load XML
		$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/designmenu/forms', 'menu_basic');

		if ($this -> input -> post())
		{
			// If posted and form validation passes
			if ($this -> form_validation -> run() == TRUE)
			{
				$data = $this -> input -> post();

                $filesToUpload = array();
				$filesToUpload[] = 'image1';
				
				if(!$cm->id)
				{
					$saved = DMZ_Array::from_array($cm, $data, null, true);
				}
				
                $path = 'menus/'.$cm->id.'';
                $this -> teamleaf -> checkpath($path);
                $fileresult = $this -> xml_forms -> uploadfiles($filesToUpload, $path);
                if ($fileresult['result'])
				{
					foreach($filesToUpload as $filee)
					{
						if($filee != '' && isset($fileresult[$filee]['file_name'])) 
						{
							$data[$filee] = @$fileresult[$filee]['file_name'];
						}
					}
				}
				
				
				$saved = DMZ_Array::from_array($cm, $data, null, true);
				// this saves this automatically to the Task Object (e.g. same as $user->save());

				// Save the task
				if ($saved)
				{
					//$company = new Company($data['company']);
					//$user->save($company);
					//$this -> maintaincache -> user($user -> id);
					redirect(base_url() . 'admin/menus/profile/display/' . $cm -> id);
				}
				else
				{
					$this -> template -> write('validation_errors', l('_D_ERROR'));
					// with some errors!
				}
			}
			else
			{
				// render errors
				$this -> template -> write('validation_errors', 'Error:' . validation_errors());
				// with some errors!
			}
		}

		if ($id)
		{
			$cm = $this -> model -> getMenu($id);
			//$user_company = $this->model->getUser_Company($u);
			//$companyID = $user_company->id;
		}
		else
		{
			$cm = new Designmenu();
		}

		//$companyM = new Company();
		//$formdropdowns['company'] = $companyM->getOptions($companyID);
		
		$cmM = new Designmenu();
		//$formdropdowns['type'] = $cmM -> getCmTypes($cm -> type);
        //$formdropdowns['template'] = $cmM -> getCmTemplates($cm -> template);
        $formdropdowns['state'] = $cmM -> getCmStates($cm -> state);
        //$formdropdowns['featured'] = $cmM -> getCmFeatured($cm -> featured);
		
        $form = $this -> xml_forms -> createform($formdata['xml'], $cm, $formdropdowns, array());
		$this -> template -> write('sheet', $form);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}


    

}

//end class
