<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Designmenus extends Base_Controller
{

	function __construct()
	{
		parent::__construct();
		//$theme				= 'home';
		//$this->template->set_master_template( $theme .'/template' );

	}

	public function index()
	{
		$this -> template -> set_template('builder');
  

		$this -> data = '';
		
		$ls = new Librarysection();
		$ls->order_by('order', 'asc')->get();
		
		$this->data['librarysections'] = $ls;
        
        
        $md = new Designmenu();
        $menus = $md->getMenus();
        $this->data['menudesigns'] = $menus;
        
		
		$this -> template -> write('slider', 'Menu Creator');
		$this -> template -> write_view('content', 'designmenu/designmenu', $this -> data);

		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
		
		
	}
	
	public function review()
	{
		$this -> template -> set_template('builder');

		$this -> data = '';
		
		$ls = new Librarysection();
		$ls->order_by('order', 'asc')->get();
		
		$this->data['librarysections'] = $ls;
		
		$this -> template -> write('slider', 'Menu Creator');
		$this -> template -> write_view('content', 'designmenu/designmenu_review', $this -> data);

		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
		
		
	}

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
