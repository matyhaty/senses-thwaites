<?php
/**
 * Class *
 */
class Timeline extends Base_Model
{

	/**
	 * table name
	 *
	 * @var String
	 */
	public $table = 'timelines';
	public $has_one = array();
	public $has_many = array();

	/**
	 * Constructor
	 *
	 * @param unknown_type $id
	 */
	function __construct($id = NULL)
	{
		parent::__construct($id);
	}

	public function getOptions($currentFlag = NULL)
	{
		$optionsArr = $this -> model -> getObject('depot', null, null, false, array('state !=' => 'Disabled')) -> dm_object -> all_to_array();
		if ($currentFlag)
		{
			$options['selected'] = $currentFlag;
		}
		foreach ($optionsArr as $op)
		{
			$options[$op['id']] = $op['title'];
		}
		return $options;
	}

	function getDepots($userID, $clearCache = false)
	{
		$user = $this -> getObject('user', $userID, null, false, array('state' => 'Active'), '', '') -> dm_object;

		if ($clearCache)
		{
			$this -> removeCachedObject('depot', null, $user, false, null, '', '');
		}

		$a = $this -> getObject('depot', null, $user, false, null, '', '') -> dm_object;

		$rt = '';
		$rt .= $this -> load -> view('depot/view', array(
			'at' => $a,
			'u' => $user
		), true);

		if ($rt == '')
		{
			$rt .= 'No depots on record';
		}
		return $rt;

	}

	function getDepotByID($id)
	{
		$depot = $this -> getObject('depot', $id, null, false, array('state' => 'Active'), '', '') -> dm_object;
		return $depot;
	}

	function deleteDepot($id, $userID)
	{
		$user = $this -> getObject('user', $userID, null, false, array('state' => 'Active'), '', '') -> dm_object;
		$depot = $this -> getObject('depot', $id, null, false, array('state' => 'Active'), '', '') -> dm_object;

		$user -> delete($depot);
		// we are only removing a relationship!

		$this -> removeCachedObject('depot', null, $user, false, null, '', '');

		return true;
	}

	function createTimelineTransactionMarkup($timeline)
	{
		log_message('error', $timeline -> model .' === '. $timeline -> related_model .' === '. $timeline->field);
		if ($timeline -> model == 'Task' && $timeline -> related_model == 'Taskassign' && $timeline->field == 'relationship')
		{
			$markup = $this -> timeline_taskassign($timeline);
			if ($markup)
			{
				$this->timeline_task_save($timeline, $markup);
			}
		}
		/*
		 case 'Taskftp' :
		 $view .= $this -> timeline_taskftp($timelineModel);
		 break;

		 case 'Statu' :
		 $view .= $this -> timeline_taskstatu($timelineModel);
		 break;

		 case 'Taskcomment' :
		 $view .= $this -> timeline_taskcomment($timelineModel);
		 break;

		 default :
		 $view .= 'ERROR, case for <b>' . $timelineModel -> model . '</b> not written yet for this timeline type: timelone Model.php';
		 break;
		 }
		 */
	}

	function timeline_taskassign($timeline)
	{
		$CI = &get_instance();
		$ta = new Taskassign($timeline -> related_model_id);
		if ($ta -> asset_id)
		{
			$a = new Asset($ta -> asset_id);
			$item = $a -> displayAsset($a);
		}
		else
		if ($ta -> person_id)
		{
			$a = new User($ta -> person_id);
			$item = $a -> displayUser($a);
		}
		else
		{
			$item = 'Assignment Error';
		}

		$user = new User($timeline -> updated_by);
		$rt = '<div class="timelineItem">';
		$rt .= '<div class="title taskAssignTitle">Assignment</div>';
		$rt .= '<div class="timelineContents taskAssign">' . $item . ' was assigned.</div>';
		$rt .= '<div class="when">on' . $CI -> teamleaf -> shortdate($timeline -> updated) . '</div>';
		$rt .= '<div class="byWhom">by ' . $user -> firstname . ' ' . $user -> lastname . '</div>';
		$rt .= '<div class="clear"></div>';
		$rt .= '</div>';

		return $rt;
	}

	function timeline_task_save($timeline, $markup)
	{
		$CI = &get_instance();
		$cache_id = 'views/tasktimelines/Task-Timeline-' . $timeline -> model_id . '.txt';
		$CI->maintaincache->appendCache($cache_id, $markup);
		$CI -> firephp -> info("Timeline Cache updated for: " . $cache_id);
	}




	function timeline_taskftp($timelineModel)
	{
		$CI = &get_instance();
		$timeline = new Timeline();
		$timeline -> where('transactionID', $timelineModel -> transactionID);
		$timeline -> where('model', 'Taskftp');
		$timeline -> get();
		if ($timeline -> updated_by)
		{
			$user = new User($timeline -> updated_by);
		}
		else
		{
			$user = new User($timeline -> created_by);
		}

		$rt = '<div class="timelineItem">';
		$rt .= '<div class="title taskFtpTitle">Server Sync</div>';
		$rt .= '<div class="timelineContents taskFtp">The server files were synced with the system.</div>';
		$rt .= '<div class="when">' . $CI -> teamleaf -> shortdate($timeline -> updated) . '</div>';
		$rt .= '<div class="byWhom">by ' . $user -> firstname . ' ' . $user -> lastname . '</div>';
		$rt .= '<div class="clear"></div>';
		$rt .= '</div>';

		return $rt;
	}

	function timeline_taskstatu($timelineModel)
	{
		$CI = &get_instance();
		$timeline = new Timeline();
		$timeline -> where('transactionID', $timelineModel -> transactionID);
		$timeline -> where('model', 'Task');
		$timeline -> where('related_model', 'Statu');
		$timeline -> where('related_model_id > ', 0);
		$timeline -> get();
		if ($timeline -> updated_by)
		{
			$user = new User($timeline -> updated_by);
		}
		else
		{
			$user = new User($timeline -> created_by);
		}

		$rt = '<div class="timelineItem">';
		$rt .= '<div class="title taskStatuTitle">Status</div>';
		$rt .= '<div class="timelineContents taskFtp">The status was set to ' . $timeline -> related_model_id . '</div>';
		$rt .= '<div class="when">' . $CI -> teamleaf -> shortdate($timeline -> updated) . '</div>';
		$rt .= '<div class="byWhom">by ' . $user -> firstname . ' ' . $user -> lastname . '</div>';
		$rt .= '<div class="clear"></div>';
		$rt .= '</div>';

		return $rt;
	}

	function timeline_taskcomment($timelineModel)
	{
		$CI = &get_instance();
		$timeline = new Timeline();
		$timeline -> where('transactionID', $timelineModel -> transactionID);
		$timeline -> where('model', 'Taskcomment');
		$timeline -> get();
		if ($timeline -> updated_by)
		{
			$user = new User($timeline -> updated_by);
		}
		else
		{
			$user = new User($timeline -> created_by);
		}

		$rt = '<div class="timelineItem">';
		$rt .= '<div class="title taskCommentTitle">Comment</div>';
		$rt .= '<div class="timelineContents taskComment">' . $user -> firstname . ' ' . $user -> lastname . 'made a comment</div>';
		$rt .= '<div class="when">' . $CI -> teamleaf -> shortdate($timeline -> updated) . '</div>';
		$rt .= '<div class="byWhom">by ' . $user -> firstname . ' ' . $user -> lastname . '</div>';
		$rt .= '<div class="clear"></div>';
		$rt .= '</div>';

		return $rt;
	}

}
