	<?php echo permAnchor( $this->user , l('_D_URI_JOBS_CREATE') , l('_D_CREATE_A').' '.l('_D_JOB')  ); ?>
	<ul>
	<?php foreach($data as $job):?>
		<li>
		<?php echo permAnchor( $this->user , l('_D_URI_JOBS_VIEW') . $job['id']	, $job['title']   );?> |
		<?php echo permAnchor( $this->user , l('_D_URI_JOBS_EDIT')  . $job['id'] 	, l('_D_EDIT') . ' ' . l('_D_JOB')  );	?> |		
		<?php echo permAnchor( $this->user , l('_D_URI_JOBS_DELETE') . $job['id'] , l('_D_DELETE') . ' ' . l('_D_JOB')  );?>
		</li>
	<?php endforeach;?>
	</ul>