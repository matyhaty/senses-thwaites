<?php
        $active = '';
        $inactive = '';
        
        foreach($at as $q)
        {
            //$q->depottype->get();
            
            $str = '';
            $str = "
                    <tr>
                        <td>
                            ".$q->code ." 
                        </td>
                        <td>
                            ".$q->title ." 
                        </td>
                        <td>
                            ".$q->address ." 
                        </td>
                        <td>
                            ".$this->teamleaf->shortDate($q->created) ."
                        </td>
                        <td>
                            ".$this->teamleaf->shortDate($q->updated) ."
                        </td>
                        <td>
                            ". $q->state."
                        </td>
                        <td>
                            <div class='options'><a href='".site_url('depot/depots/delete/'.$q->id.'/'.$u->id)."' class='delete_link'>Delete</a></div>
                        </td>
                    </tr>";
            
            
            if($q->state == 'Active')
            {
                $active .= $str;
            }     
            else
            {
                $inactive .= $str;
            }   
             
        }
        ?> 


<table class='qualifications'>
    <tbody>
        <thead>
            <th>
                Code
            </th>
            <th>
                Title
            </th>
            <th>
                Address
            </th>
            <th>
                Created
            </th>
            <th>
                Updated
            </th>
            <th>
                State
            </th>
            <th>
                
            </th>
            
        </thead>
        
        <?php echo $active; ?>

        
    </tbody>
</table>

<div class='inactivetoggle inactivedepots'><a href='#'>[view/hide inactive items]</a></div>

<div class='inactive_depots' style='display:none;'>
<table class='qualifications'>
    <tbody>
        <thead>
            <th>
                Type
            </th>
            <th>
                Start Date
            </th>
            <th>
                End Date
            </th>
            <th>
                Supporting Information
            </th>
            <th>
                Number Of Days
            </th>
            <th>
                State
            </th>
            <th>
                
            </th>
            
        </thead>
        
        <?php echo $inactive; ?>
        
    </tbody>
</table>
</div>
