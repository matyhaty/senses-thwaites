<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Depots extends Base_Controller
{

    /**
     * Constructor for profile page
     *
     */
    function __construct()
    {
        $this->load->library('form_validation');
        $this->load->helper(array(
            'form',
            'url'
        ));
        parent::__construct('Depot');

    }

    public function edit($userID)
    {
        // Load the email helper
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('upload');
        $this->load->library('form_validation');
        $this->load->library('session');


        $depot = $this->model;
        $formdata = $this->xml_forms->loadXML(APPPATH . 'modules/depot/forms', 'depots');
        //$formdropdowns['qualification']  	= qualificationtype::getOptions($currentFlag = NULL);

        if ($this->input->post() && $userID)
        {
            if ($this->form_validation->run() == TRUE)
            {
                $data = $this->input->post();
                $userid = $this->session->userdata('userid');
                //$saved = DMZ_Array::from_array($depot, $data, NULL, TRUE);
                
                // THERE IS NOTHING TO SAVE, ONLY A RELATIONSHIP!!
                $saved = 1;
                
                // Save the job
                if ($saved)
                {
                    // relate to qualifcation type
                    //$qt = new Depottype($data['depot']);
                    //$depot->save($qt);
                    
                    $depot = new Depot($data['depot']);
                    $u = new User($userID);
                    $depot->save($u);
                    $this->maintaincache->depot($depot->id);

                    //$this->model->removeCachedObject('depot', null, $u, false, null, '', '');
                    
                    // Save Files
                    $this->teamleaf->checkpath($userID . '/depots');
                    $errors = '';
                    //-------   Upload the potential 4 files

                    $clearCache = true;
                    echo $depot->getDepots($userID, $clearCache);

                }
                else
                {
                    echo 'Save Error<hr>';
                    echo validation_errors();
                }
            }
            else
            {
                echo validation_errors();
            }

        }
        else
        {
            // Load the view
            $form = $this->xml_forms->createform($formdata['xml'], $this->user, $formdropdowns, array(
                'nosheet' => 1,
                'grid' => ''
            ));
            echo $form;
        }

    }

    public function delete($id, $userID)
    {
           $depot = new Depot();
           $depot->deleteDepot($id, $userID);
           redirect('admin/user/profile/display/'.$userID);
    }

}

//end class
