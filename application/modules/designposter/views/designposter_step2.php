<?php echo form_open('designposter_step3'); ?>
<?php echo form_hidden('menu_design', $poster->image); ?>


<div class="row">
        <div class="span24">
            <div class="form-chain"></div>
        </div>
    </div>
        
    <div class="row">
        <div class="span24">
            <div class="form-container">
                <div class="row">
                    <div class="span7">
                        <div class="left-content">
                            <h1>Add your text</h1>
                            Now you add your own text to the poster.  Enter the text in the red boxes.  
                            <br/>
                            <br/>
                            We will check the sizing and font before printing, so do not worry if it doesn't look perfect.
                            <br/>
                            <br/>
                            Then choose all your options.  How many do you require, laminated, and whether you would like to see a proof of the poster before going to print.
                            <br/><br/>
                            If you have any notes which will help us make your poster even better, please add them here.
                        </div>
                    </div>
                    
                    
                    <?php
                    $largeimage = str_replace('.jpg', '-large.jpg', $poster->image);
                    
                    ?>

                    <div class="span17 posterDesign" 
                    	style='background-image: url("<?php echo base_url(); ?>assets/frontend/images/posterdesigns/<?php echo $largeimage; ?>");
                    	min-height:1200px; height: 1200px; position: relative; z-index: 5;
                    	'>
                    	
                    	
                    	<div class='inputPubName'>
                    		<input class='' type='text' name='inputPubName' id='inputOubName' value='Pub Name'/>
                    	</div>
                    	
                    	<div class='textareaTandC'>
                    		<textarea>Add Terms and Conditions</textarea>
                    	</div>
                    	
                    	<div class='input1'>
                    		<input class='' type='text' id='input1' name='input1' value=''/>	
                    	</div>
                    	
                    	<div class='textareainput1'>
                    		<textarea></textarea>
                    	</div>
                    	
                    	<div class='input2'>
                    		<input class='' type='text' id='input2' name='input2' />
                    	</div>
                    	
                    	<div class='textareainput2'>
                    		<textarea></textarea>
                    	</div>
                    	
                    	<div class='input3'>
                    		<input class='' type='text' id='input3'  name='input3' />
                    	</div>
                    	
                    	<div class='textareainput3'>
                    		<textarea></textarea>
                    	</div>
                    	
                    	<div class='input4'>
                    		<input class='' type='text' id='input4' name='input4' />
                    	</div>
                    	
                    	<div class='textareainput4'>
                    		<textarea></textarea>
                    	</div>
                    	
                    	<div class='input5'>
                    		<input class='' type='text' id='input5' name='input5' />
                    	</div>
                    	
                    	<div class='textareainput5'>
                    		<textarea></textarea>
                    	</div>
                    	
                    	<div class='input6'>
                    		<input class='' type='text' id='input6' name='input6' />
                    	</div>
                    	
                    	<div class='input7'>
                    		<input class='' type='text' id='input7' name='input7' />
                    	</div>
                    	
                    	<div class='input8'>
                    		<input class='' type='text' id='input8' name='input8' />
                    	</div>
                    	
                    	<div class='input9'>
                    		<input class='' type='text' id='input9' name='input9' />
                    	</div>
                    	
                    	<div class='input10'>
                    		<input class='' type='text' id='input10' name='input10' />
                    	</div>
                    	
                    	<div class='input11'>
                    		<input class='' type='text' id='input11' name='input11' />
                    	</div>
                    	
                    	<div class='input12'>
                    		<input class='' type='text' id='input12' name='input12' />
                    	</div>
                    	
                    	<script type="text/javascript">
                    	$(document).ready(function(){
							$(function(){
								$('.input1 input').inputfit();
							})
							
							$(function(){
								$('.input2 input').inputfit();
							})
							
							$(function(){
								$('.input3 input').inputfit();
							})
							
							$(function(){
								$('.input4 input').inputfit();
							})
							
							$(function(){
								$('.input5 input').inputfit();
							})
							
							$(function(){
								$('.input6 input').inputfit();
							})
							
							$(function(){
								$('.input5 input').inputfit();
							})
							
							$(function(){
								$('.input7 input').inputfit();
							})
							
							$(function(){
								$('.input8 input').inputfit();
							})
							
							$(function(){
								$('.input9 input').inputfit();
							})
							
							$(function(){
								$('.input10 input').inputfit();
							})
							
							$(function(){
								$('.input11 input').inputfit();
							})
							
							$(function(){
								$('.input12 input').inputfit();
							})
							
							$(function(){
								$('.inputPubName input').inputfit();
							})
							
							
							
								
							});
	
						</script>

                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="row">
        <div class="span24">
            <div class="form-chain"></div>
        </div>
    </div>
    
    
    
    <div class="row">
        <div class="span24">
            <div class="form-container">
                <div class="row">
                    <div class="span5">
                        <div class="left-content">
                            <h1>All the bits</h1>
                            How many do you require, laminated, and whether you would like to see a proof of the poster before going to print.
<br/><br/>
If you have any notes which will help us make your poster even better, please add them here.
                        </div>
                    </div>

                    <div class="span19">
                        <div class="right-content">
                            <div class="controls controls-row">

                            	<label class="radio">
								  <input type="radio" name="quantity" id="" value="10|16.60" checked>
								  10 Copies - &pound;16.60
								</label>
								<label class="radio">
								  <input type="radio" name="quantity" id="" value="20|35.60" >
								  20 Copies - &pound;35.60
								</label>
								<label class="radio">
								  <input type="radio" name="quantity" id="" value="30|53.50" >
								  30 Copies - &pound;53.50
								</label>
								<label class="radio">
								  <input type="radio" name="quantity" id="" value="40|72.00" >
								  40 Copies - &pound;72.00
								</label>
								<label class="radio">
								  <input type="radio" name="quantity" id="" value="50|89.00" >
								  50 Copies - &pound;89.00
								</label>
								<label class="radio">
								  <input type="radio" name="quantity" id="" value="100|160.00" >
								  100 Copies - &pound;160.00
								</label>

                            </div>
                            <br/>

                            <div class="controls controls-row">

                            	<label class="radio">
								  <input type="radio" name="lamination" id="" value="Laminate - Gloss|25.00" checked>
								  Laminate Menus (Gloss) - &pound;25
								</label>
								<label class="radio">
								  <input type="radio" name="lamination" id="" value="Laminate - Matt|25.00" checked>
								  Laminate Menus (Matt) - &pound;25
								</label>
								<label class="radio">
								  <input type="radio" name="lamination" id="" value="No Lamination|0.00" >
								  Do not laminate
								</label>
							</div>
								<br/>
							<div class="controls controls-row">

                            	<label class="radio">
								  <input type="radio" name="proof" id="" value="Proof|5.00" checked>
								  Proof Menus - &pound;5								</label>
								<label class="radio">
								  <input type="radio" name="proof" id="" value="No Proof|0.00" >
								  Do not laminate
								</label>
							</div>
							<br/>
							
							<div class="controls controls-row">
								<textarea class="span16" type="text" placeholder="Add any notes..." name='notes'></textarea>
							</div>
							<br/>
							

                            <div class="controls controls-row">
                                <button type="submit" class="btn">Add to Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
 <?php echo form_close(); ?>
    
    
    
    <script type="text/javascript">
$(document).ready(function() {
            $(".selectdesign").click(function()
            {
              //alert('slected a design');
              $(".selectdesign").removeClass("selected");
              $(this).addClass('selected');
              $('.selectedDesignInput').val(this.id);
            });
        });
    </script>
