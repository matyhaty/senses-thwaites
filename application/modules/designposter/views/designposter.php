<!-- FORMS
        ================================================== -->


<div class="row">
        <div class="span24">
            <div class="form-chain"></div>
        </div>
    </div>


    <div class="row">
        <div class="span24">
            <div class="form-container">
            <?php echo form_open('designposter_step2'); ?>
                <div class="row">
                    <div class="span7">
                        <div class="left-content">
                            <h1>Choose your poster</h1>
                            Click on the design you would like to use for your poster.  There are loads to choose from.<br/><br/>
                            Once you have choosen a poster design, click the 'Create your poster' button to continue.
                        </div>
                    </div>

                    <div class="span17">
                        <div class="right-content">
                            <div class="controls controls-row">

	                            <?php foreach($posters as $poster)
	                            {
		                        ?>
		                        
		                        <div class="span3 selectdesign">
                                	<img class ='selectposter' id='<?php echo $poster->image; ?>' src="<?php echo base_url(); ?>assets/frontend/images/posterdesigns/<?php echo $poster->image; ?>" />
                                </div>

		                        
		                            
		                        <?php
	                            }
	                            ?>
                            	                                                     
                                <input class='span14 selectedDesignInput' type='hidden' name='menu_design' value='poster1.jpg' />

                            </div>
                            <br/>

                            							

                            <div class="controls controls-row">
                                <button type="submit" class="btn ">Create your poster</button>
                            </div>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>
    </div>

     <script type="text/javascript">
	    $(document).ready(function() {
	        $(".selectdesign").click(function()
	        {
	          //alert('slected a design');
	          $(".selectdesign").removeClass("selected");
	          $(this).addClass('selected');
	          $('.selectedDesignInput').val($(this).children("img").attr("id"));	
	   	    });
	   	});
    </script>
    
    