<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Designposters extends Base_Controller
{

	function __construct()
	{
		parent::__construct();
		//$theme				= 'home';
		//$this->template->set_master_template( $theme .'/template' );

	}

	public function index()
	{
		$this -> template -> set_template('builder');

		$this -> data = '';
		$dp = new Designposter();
		$dp->get();
		$this->data['posters'] = $dp;
			
		$this -> template -> write('slider', 'Poster Creator');
		$this -> template -> write_view('content', 'designposter/designposter', $this -> data);

		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
		
		
	}
	
	public function step2()
	{
		$this -> template -> set_template('builder');
		
		$design = $this->input->post('menu_design');
		
		$dp = new Designposter();
		$dp->where('image', $design)->get();
		
		//$design = str_replace('jpg', 'css', $design);
		$this->template->add_css('assets/frontend/css/posters/'.$dp->stylesheet);

		$this -> data = '';
		$this->data['poster'] = $dp;
			
		$this -> template -> write('slider', 'Poster Creator');
		$this -> template -> write_view('content', 'designposter/designposter_step2', $this -> data);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
		
		
	}
    
    public function addToCart()
    {
        $CI = &get_instance();
        
       $postedData = $_POST;
       
       //print_r($postedData);
       $data['serialized'] = serialize($postedData);
       
       list($data['quantityTitle'], $data['quantityPrice']) = explode('|', $postedData['quantity']);
       list($data['laminationTitle'], $data['laminationPrice']) = explode('|', $postedData['lamination']);
       list($data['proofTitle'], $data['proofPrice']) = explode('|', $postedData['proof']);
       
       
       $data['totalPrice'] = $data['quantityPrice'] + $data['laminationPrice'] + $data['proofPrice'];
       $data['title'] = 'Poster Creation';
       $data['summary'] = 'Printing of posters. 
                                <br/>Quantity: '.$data['quantityTitle'].' (&pound;'.$data['quantityPrice'].')
                                <br/>Lamination: '.$data['laminationTitle'].' (&pound;'.$data['laminationPrice'].')
                                <br/>Proof: '.$data['proofTitle'].' (&pound;'.$data['proofPrice'].')';
       
       if($CI->loggedIn) { 
       $data['userName'] = $CI->user->firstname.' '.$CI->user->surname;
       $data['userID'] = $CI->user->id;
       }
       else {
           echo 'Not logged in!!!';
       }
       $data['type'] = 'poster';
       
       $cart = new Cart();
       $cart->addToCart($data);
       
       
      // echo $data['summary'] ;
      // echo $data['totalPrice'];
        
        
       redirect('/cart/view/'); 
        
    }


	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
