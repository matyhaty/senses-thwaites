<?php
/**
 * Class *
 */
class Designposter extends Base_Model
{

    /**
     * table name
     *
     * @var String
     */
    public $table = 'designposters';
    public $has_one = array(
          );
    public $has_many = array();

    /**
     * Constructor
     *
     * @param unknown_type $id
     */
    function __construct($id = NULL)
    {
        parent::__construct($id);
    }

}
