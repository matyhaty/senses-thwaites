<?php
/**
 * Class *
 */
class Note extends Base_Model
{

	/**
	 * table name
	 *
	 * @var String
	 */
	public $table = 'notes';
	public $has_one = array('communication');
	public $has_many = array();

	/**
	 * Constructor
	 *
	 * @param unknown_type $id
	 */
	function __construct($id = NULL)
	{
		parent::__construct($id);
	}

}
