<div id="myCarousel" class="carousel slide">
    <div class="carousel-inner">
        <div class="item">
            <img src="assets/frontend/img/wine-zone-slide.png" alt="The Wine Zone">

            <div class="container">
                <div class="carousel-caption">
                    <br>
                    <br>

                    <h1>My<br>
                    Wines</h1>

                    <p class="lead"><br>
                    Customise and order your bespoke wine menus and printed ready in no time.<br><br/>
                    Coming soon to Pubs.net
                </div>
            </div>
        </div>

        <div class="item ">
            <img src="assets/frontend/img/sky-slide.png" alt="SKY Deal">

            <div class="container">
                <div class="carousel-caption">
                    <br>
                    <br>

                    <h1>SKY<br>
                    Deal</h1>

                    <p class="lead"><br>
                    30% of Sky Ultimate deal<br>
                    <a href="<?php echo base_url(); ?>article/7" title="Log-In" id="winezone"><span>Info</span></a></p>
                </div>
            </div>
        </div>

        <div class="item active">
            <img src="assets/frontend/img/poster-zone-slide.png" alt="The Wine Zone">

            <div class="container">
                <div class="carousel-caption">
                    <br>
                    <br>

                    <h1>Publicise<br>
                    IT!</h1>

                    <p class="lead"><br>
                    See how easy it is to create professional personalised POS – including posters, menus and wine lists. <br>
                    <a href="<?php echo base_url(); ?>designposter" title="Log-In" id="winezone"><span>Start</span></a></p>
                </div>
            </div>
        </div>
    </div><a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a> <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
</div><!-- /.carousel -->