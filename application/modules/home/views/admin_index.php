<div class='sheet'>
    <div class="page-header-grey">
        <div class='image'>
            <?php echo slir('/', 'teamleaf.png', $width = 80, $height = 60, $crop = array(
                'width' => '75',
                'height' => '55'
            ), 'title="Profile image" class="profile-image"');
            ?>
        </div>
        <div class='titles'>
            <div class='title'>
               Teamleaf
            </div>
            <div class='subtitle'>
                Adminstration Area
            </div>
        </div>
        <div class='clear'></div>
    </div>
    <div class='innersheet'>
        
        <table>

            <tr>
                <td><?php echo anchor('admin/home/admin/clearcache', 'Clear Cache'); ?></td>
            </tr>
            
        </table>
        
        
    </div>
</div>