



  <div class="row">
        <div class="span24">
            <div class="form-chain"></div>
        </div>
    </div>


<div class="row">
    <div class="span24">
        <div class="form-container">
            <div class="row">
                <div class="span5">
                    <div class="left-content">
                        <h1>News</h1>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. In teger elit odio, fermentum quis portti tor quis, rutrum nec erat. Aliq uam ut nulla in libero rhoncus mattis.
                    </div>
                </div>

                <div class="span19">
                    <div class="right-content">
                        <div class="controls controls-row">
                         
                                
                                <?php foreach($cms_news as $post)
                                {
                                    $data['post'] = $post;
                                    echo $this->load->view('cm/templates/'.$post->template, $data, true);
                                }
                                ?>
                                
                               

                              
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="span24">
                <div class="form-chain"></div>
            </div>
        </div>

        <div class="row">
            <div class="span24">
                <div class="form-container">
                    <div class="row">
                        <div class="span5">
                            <div class="left-content">
                                <h1>Offers</h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In teger elit odio, fermentum quis portti tor quis, rutrum nec erat. Aliq uam ut nulla in libero rhoncus mattis.
                            </div>
                        </div>

                        <div class="span19">
                            <div class="right-content">
                                <div class="controls controls-row">
                                    
                                        
                                        <?php foreach($cms_offers as $post)
                                        {
                                            $data['post'] = $post;
                                            echo $this->load->view('cm/templates/'.$post->template, $data, true);
                                        }
                                        ?>
                                
                                
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="span24">
                <div class="form-chain"></div>
            </div>
        </div>

        <div class="row">
            <div class="span24">
                <div class="form-container">
                    <div class="row">
                        <div class="span5">
                            <div class="left-content">
                                <h1>Incentives</h1>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In teger elit odio, fermentum quis portti tor quis, rutrum nec erat. Aliq uam ut nulla in libero rhoncus mattis.
                            </div>
                        </div>

                        <div class="span19">
                            <div class="right-content">
                                <div class="controls controls-row">
                                   
                                        
                                         <?php foreach($cms_incentives  as $post)
                                        {
                                            $data['post'] = $post;
                                            echo $this->load->view('cm/templates/'.$post->template, $data, true);
                                        }
                                        ?>
                                        
                                      
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script>

 $(function(){
        $('#mas').imagesLoaded(function(){
            $('#mas').isotope({
                itemSelector : '.span8',
                
                
            });
        });
    });

</script>
