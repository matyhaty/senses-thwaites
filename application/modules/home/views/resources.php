  <div class="row">
        <div class="span24">
            <div class="form-chain"></div>
        </div>
    </div>


<div class="row">
    <div class="span24">
        <div class="form-container">
            <div class="row">
                <div class="span10">
                    <div class="left-content">
                        <h1>Products and Prices</h1>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. In teger elit odio, fermentum quis portti tor quis, rutrum nec erat. Aliq uam ut nulla in libero rhoncus mattis.
                    </div>
                </div>

                <div class="span14">
                    <div class="right-content">
                        <div class="controls controls-row">
                         
                                
                                <?php foreach($cms_productsandprices as $post)
                                {
                                    $data['post'] = $post;
                                    echo $this->load->view('cm/templates/'.$post->template, $data, true);
                                }
                                ?>
                                
                               

                              
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="span24">
                <div class="form-chain"></div>
            </div>
        </div>

        <div class="row">
            <div class="span24">
                <div class="form-container">
                    <div class="row">
                        <div class="span10">
                            <div class="left-content">
                                <h1>Compliance Documents</h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In teger elit odio, fermentum quis portti tor quis, rutrum nec erat. Aliq uam ut nulla in libero rhoncus mattis.
                            </div>
                        </div>

                        <div class="span14">
                            <div class="right-content">
                                <div class="controls controls-row">
                                    
                                        
                                        <?php foreach($cms_reasource_compliance as $post)
                                        {
                                            $data['post'] = $post;
                                            echo $this->load->view('cm/templates/'.$post->template, $data, true);
                                        }
                                        ?>
                                
                                
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="span24">
                <div class="form-chain"></div>
            </div>
        </div>

        <div class="row">
            <div class="span24">
                <div class="form-container">
                    <div class="row">
                        <div class="span10">
                            <div class="left-content">
                                <h1>Partners And Support</h1>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In teger elit odio, fermentum quis portti tor quis, rutrum nec erat. Aliq uam ut nulla in libero rhoncus mattis.
                            </div>
                        </div>

                        <div class="span14">
                            <div class="right-content">
                                <div class="controls controls-row">
                                   
                                        
                                         <?php foreach($cms_reasource_partners_support as $post)
                                        {
                                            $data['post'] = $post;
                                            echo $this->load->view('cm/templates/'.$post->template, $data, true);
                                        }
                                        ?>
                                        
                                      
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
