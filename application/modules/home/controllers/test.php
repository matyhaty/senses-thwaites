<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends Base_Controller {


	function __construct()
	{
		parent::__construct();	
	}
	
	public function cacheTest()
	{
		$this->maintaincache->setCache('tester/test.txt', 'Hello Cache World');
	}
	
	public function ftpTest($depotCode)
	{
		error_reporting(E_ALL);
		ini_set('display_errors', '1');
	
		$CI = &get_instance();
		$CI -> load -> library('ftp');
		//$fileList = $this -> teamleaf -> ftp_newTask('CONNECTIONTEST', 'DUD', 1);
		echo 'This is the FTP test.';
		echo '<hr>Config found for depot code: '.$depotCode.': <br/>';
		$c = $this -> teamleaf -> ftp_getconfig($depotCode);
		print_r($this -> teamleaf -> ftp_getconfig($depotCode));
		echo '<br/><br/>';
		echo 'The remote path is: <br/>';
		echo $this -> teamleaf -> ftp_remotePath($depotCode);
		
		echo '<h2>Trying to connect</h2>';
		echo $CI -> ftp -> connect($c);
	}
	
	public function ftpTestWrite($depotCode)
	{

		$fileList = $this -> teamleaf -> ftp_newTask('140', 'DUD', 1);

	}
	
	function related()
	{
		$task = new Task(1194);
		$ta = new Taskassign(1460);
		
		echo $task->is_related_to($ta);
		
	}
	
	function hotelupdate()
	{
		$ho = new Hotel();
		$ho->get();
		
		foreach($ho as $h)
		{
			$h->slugify = $this->teamleaf->slugify($h->title);
			$h->save();
		}
		
		
	}
	
	function destinationupdate()
	{
		$ho = new Destination();
		$ho->get();
		
		foreach($ho as $h)
		{
			$h->slugify = $this->teamleaf->slugify($h->title);
			$h->save();
		}
		
		
	}

	public function tester()
	{		
		echo '<hr>Departments Checker<br/>';
		$dd = new Department();
		$dd->where('id', 1);
		$dd->or_where('id', 2);
		$dd->get();
		
		foreach($dd as $r)
		{
			echo $r->id .' - '.$r->title .'<br/>';
			
			
			// DEpartments
			$ddc = new Department();
			$ddc->where('id', $r->id);
			$ddc->get();
			$ddc->task->get();
			echo '<i>Which has related Task:</i>';
			echo '<ul>';
			foreach($ddc->task as $task)
			{
				echo '<li>'.$task->id.' </li>';
			}
			echo '</ul>';		
			
		}
		
		//--------------------------------------------------------------------------------
		echo '<hr>Depot Checker<br/>';
		$dp = new Depot();
		$dp->where('id', 104);
		$dp->get();
		
		foreach($dp as $r)
		{
			echo $r->id .' - '.$r->title .'<br/>';
			
			
			// DEpots
			$ddp = new Depot();
			$ddp->where('id', $r->id);
			$ddp->get();
			$ddp->task->get();
			echo '<i>Which has related Task:</i>';
			echo '<ul>';
			foreach($ddp->task as $task)
			{
				echo '<li>'.$task->id.' </li>';
			}
			echo '</ul>';		
			
		}
		
		//----------------------------------------------------------------------------------
		echo '<hr> Tasks which are related to any of the departmentments and depots:<br/>';
		
		$tt = new Task();
		$tt->where_in_related($dd, 'state', 'Active');
		$tt->where_in_related($dp, 'state', 'Active');
		$tt->get();

		foreach($tt as $r)
		{
			echo $r->id .' - '.$r->title .'<br/>';

		}
	}



}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
