<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Home extends Base_Controller
{

	function __construct()
	{
		parent::__construct();
		//$theme				= 'home';
		//$this->template->set_master_template( $theme .'/template' );

	}

	public function index($excerts = null)
	{
		$this -> template -> set_template('homepage');
		
		/*
		 * This will re-create random length experts for all CMS posts.
		 * 
         * 
         */
         
         if($excerts == 'redo')
         {
    	    $cm = new Cm();
    		$cm->get();
    		foreach($cm as $c)
    		{
    			$limit = rand(99,110);
    			//echo $limit.'<br/>';
    			$words = explode(" ",strip_tags($c->content));
    			$c->excerpt = implode(" ",array_splice($words,0,$limit));
    			$c->excerpt .= '...';
    			$c->save();
    		}
		 }

		$this -> data = '';
		
		$cmsM = new Cm();
        //$this->data['cms'] = $cmsM->getCmsHomepage();
		$this->data['cms_featured'] = $cmsM->getCmsFeatured();
        
        $this->data['homepage_intro'] = $cmsM->getCmsByType('homepageintro');
        
		$this -> template -> write_view('slider', 'home/slider', $this -> data);
		$this -> template -> write_view('content', 'home/homepage', $this -> data);
		//$this -> template -> write('page_title', 'Welcome');

		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}
	
	public function taproom()
	{
		$this -> template -> set_template('cms');

		$this -> data = '';
		
		$cmsM = new Cm();
		$this->data['cms'] = $cmsM->getCmsByType('taproom');
		
		$this -> template -> write('slider', 'The Tap Room');
		$this -> template -> write_view('content', 'home/taproom', $this -> data);
		//$this -> template -> write_view('content', 'designmenu/designmenu', $this -> data);

		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}
	
	public function resources()
	{
		$this -> template -> set_template('cms');

		$this -> data = '';
        
        $cmsM = new Cm();
        $this->data['cms_productsandprices'] = $cmsM->getCmsByType('reasource-products-and-prices');
        $this->data['cms_reasource_compliance'] = $cmsM->getCmsByType('reasource-compliance');
        $this->data['cms_reasource_partners_support'] = $cmsM->getCmsByType('reasource-partners-and-support');      
        
		$this -> template -> write_view('content', 'home/resources', $this -> data);
		$this -> template -> write('slider', 'Resources');
		//$this -> template -> write_view('content', 'designmenu/designmenu', $this -> data);

		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}
	
	public function news()
	{
		$this -> template -> set_template('cms');

		$this -> data = '';
        
        $cmsM = new Cm();
        
        $this->data['cms_news'] = $cmsM->getCmsByType('news');
        $this->data['cms_offers'] = $cmsM->getCmsByType('offers');
        $this->data['cms_incentives'] = $cmsM->getCmsByType('incentives');
        
        $this -> template -> write('slider', 'News, Offers &amp; Incentives');
		$this -> template -> write_view('content', 'home/news', $this -> data);
		//$this -> template -> write_view('content', 'designmenu/designmenu', $this -> data);

		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

	public function article($cmsID)
	{
		$this -> template -> set_template('cms');

		$this -> data = '';
        
        $cmsM = new Cm($cmsID);
        $this->data['post'] = $cmsM;
        
        $this -> template -> write('slider', $cmsM->title);
		$this -> template -> write_view('content', 'home/article', $this -> data);
		//$this -> template -> write_view('content', 'designmenu/designmenu', $this -> data);

		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

	public function offers()
	{
		$this -> template -> set_template('cms');

		$this -> data = '';
        
        $cmsM = new Cm();
        $this->data['cms'] = $cmsM->getCmsByType('offers');
        
        $this -> template -> write('slider', 'Offers');
		$this -> template -> write_view('content', 'home/offers', $this -> data);
		//$this -> template -> write_view('content', 'designmenu/designmenu', $this -> data);

		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}


	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
