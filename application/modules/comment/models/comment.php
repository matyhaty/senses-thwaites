<?php
	/**
	 * Class *
	 */
	class Comment extends Base_Model
	{

		/**
		 * table name
		 *
		 * @var String
		 */
		public $table = 'comments';
		public $has_one = array(
				'wall',
				'user'
		);
		public $has_many = array('like');
		public $default_order_by = array( 'created' => 'asc');
		/**
		 * Constructor
		 *
		 * @param unknown_type $id
		 */
		function __construct($id = NULL)
		{
			parent::__construct($id);
		}

		
		public function more($id,$model)
		{

		return $model->comments
					 ->where( 'id >' .$id )
					 ->order_by("created", 'asc' )
					 ->limit(3)
					 ->get();
					 
		
		}		
		
		
		
	}
?>