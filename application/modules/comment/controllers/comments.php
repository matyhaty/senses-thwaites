<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class comments extends Base_Controller {

	/**
	 * Constructor
	 * creates a model, cache created and asigned to model
	 *
	 */
	function __construct()
	{
		parent::__construct();
		$this->model = new Comment('comment');
		$this->model->cache = $this->cache;
	}


	function more()
	{
		

		if($this->is_ajax){
			if( $this->input->post() )
			{
				$id = $this->uri->segment(4); // username
				// the wall id
				$data =$this->input->post();
				$comment_id = $data['comment_id'];
				$wall = new wall($id);
				// get the wall
				
				$comments		= $this->model->more( $comment_id ,$wall )->all_to_array();
				$commentsRet	= '';
				
				
				foreach( $comments as $comment )
				{
					
					$commentsRet .= $this->load->view( 'common/comment_body', array( 'comment' => $comment , 'userDM'=>$this->user  ) ,true );
				
				}
				
				echo($commentsRet);
				die;
			}
		}else{
			
			$this->_show_message( l('_D_COMMENT').' '. l('_D_USAVED') , '/' );	
		}		
		
		
		
	}
	
	

	/**
	 * Creates an event
	 *
	 */
	public function create()
	{

		if( $this->input->post() )
		{
			$data						= $this->input->post();
			

			$return						= base64_decode( $data['return'] );
			// If posted and form validation passes
			if($this->form_validation->run() == TRUE || 1)
			{
				$data		= $this->input->post();
				$id			= $data['id'];
				$comment	= $data['comment'];
				$wall = new wall($id);
				$this->model->description = $comment;
				$this->model->save($wall);
			}

		}
		
		
		if($this->is_ajax){
			
			echo $this->load->view('common/comment_body', array( 'comment' => $this->model->to_array() , 'userDM'=>$this->user  ) );
			
		}else{
			
			$this->_show_message( l('_D_COMMENT').' '. l('_D_USAVED') , $return );	
		}		
		
		
		
		
		
		
	}
}
/* End of file events.php */
/* Location: ./application/controllers/events.php */
