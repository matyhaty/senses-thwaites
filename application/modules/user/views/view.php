<div class='sheet'>
    <div class='innersheet'>
        <!-- Start of Content of Sheet -->
        <div class="grid_8 page-header">
            <div class='title'>
                User Register
            </div>
            <div class='subtitle'>
                Current live users on the system.
            </div>
        </div>
        <div class="grid_16 page-options">
            <div class='option'>
                <a class="" id='createatask' href='<?php echo site_url('user/profile/create');?>'>
                <div class='title'>
                    Create a New User
                </div>
                <div class='subtitle'>
                    Quick add a new user.
                </div> </a>
            </div>
           
    
        </div>
        <div class="clear"></div>
        <div class="grid_24 datatable">
            
            <div class='somethingelse'></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display bordered-table" id="user_table" width="100%">
                <thead>
                    <tr >
                        <th>No:</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>DOB</th>
                        <th>Phone</th>
                        <th>Order</th>
                        <th>Status</th>
                        <th>Created</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $rowStyle = 1;
                    foreach ($users as $user)
                    {
                   
                    ?>

                    <tr id='<?php echo $user -> id;?>'>
                        
                        <td><?php echo $user->id; ?></td>
                        <td><?php echo $user->firstname;?> <?php echo $user->surname; ?></td>
                        <td><?php echo $user->username;?></td>
                        <td><?php echo $user -> email; ?></td>
                        <td><?php echo $user -> occupation; ?></td>
                        <td><?php echo $user -> dob;?></td>
                        <td><?php echo $user -> phone_number;?></td>
                        <td><?php echo $user -> sortorder;?></td>
                        <td><?php echo $user -> status;?></td>
                        <td><?php echo $user -> created;?></td>
                        
                    </tr>
                    <?php
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>No:</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>DOB</th>
                        <th>Phone</th>
                        <th>Order</th>
                        <th>Status</th>
                        <th>Created</th>
                    </tr>
                </tfoot>
            </table>
            
            
            <script type="text/javascript" charset="utf-8">
                $(document).ready(function() {
                    $('#user_table').dataTable({
                        'bJQueryUI' : true,
                        "sDom" : '<"top"  <"info"i>   <"show"l>  <"pagination"p> T <"search"f> >rt<"bottom"><"clear">',
                        "oTableTools": {
							"sSwfPath": "<?php echo base_url();?>assets/admin/swf/copy_csv_xls_pdf.swf"
						},
                        "oLanguage" : {
                            "sInfo" : "_TOTAL_ entries (_START_ to _END_)",
                            "sSearch" : "",
                            
                        },
                        "aaSorting": [[ 1, "asc" ]],
                        'iDisplayLength' : 50,
                    });

                    $('#user_table tr').live('click', function() {
                        if(this.id) {
                            //alert(this.id);
                            window.location = '<?php echo base_url(); ?>admin/user/profile/display/'+this.id;
                        }
                    });
                });

            </script>
            
            
        </div>
        <div class="clear"></div>
        <!-- End of Content of Sheet -->
        <!-- Start of Content of Form area -->
        
    </div>
</div>