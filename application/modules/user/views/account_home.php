<div class='sheet'>
    <div class='innersheet'>
        
        <div class="grid_8 page-header">
            <div class='title'>Your Account</div>
            <div class='subtitle'>
                All in control
            </div>
        </div>
        <div class='clear'></div>
        
        <div class="grid_24">
          <?php echo permAnchor( $this->user , 'user/account/index' , 'Edit Account'  );?>
        </div>
        <div class='clear'></div>
          
         
    </div>
</div>