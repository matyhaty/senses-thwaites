	<div id="lh1" class="col col2-1  rounded">
				
		<?php echo form_open(  '/login'  ); ?>
		<form method="post" action="<?php echo base_url("users/login")?>">
		
			<h2>Log in</h2>
			<input class="input-field rounded" type="text" name="username" id="username" value="Username" />
			
			<input class="input-field rounded" type="password" name="password" id="password" value="Password" />
			
			<input class="orange-btn-lrg rounded" type="submit" value="Login" />
			
			<?php /* echo anchor('user/auth/forgot_password/', 'Forgot password'); */?>
						
		</form>
		
		<div class="clear"></div>
		
		<p>Not got an account yet? <?php if ($this->config->item('allow_registration', 'tank_auth')) echo anchor('/user/register/', 'Register'); ?></p> 
		<a href="<?php echo $fb_data['loginUrl']; ?>" onClick="facebookLogin();return false;"><img src="/assets/images/fblogin.gif" alt="Facebook login icon" /></a>
	</div>

    <script src="http://connect.facebook.net/en_US/all.js"></script>
    <div id="fb-root"></div>
    <script>
    var facebookLoginWindow		= '';
    var loginWindowTimer		= '';

	function onTimerCallbackToCheckLoginWindowClosure()
	{
		//window.location = '<?php echo base_url("users/login")?>';
	}
    /**
    * @todo need some sort of polling to check they have succesfully logged in
    * Redirect will be used for now
    */
    function facebookLogin()
    {
        var popupWidth=500;
        var popupHeight=300;
        //var xPosition=($(window).width()-popupWidth)/2;
        //var yPosition=($(window).height()-popupHeight)/2;
         var xPosition= 0;
         var yPosition = 0;
        var loginUrl="<?php echo $fb_data['loginUrl']; ?>&"+
           // "response_type=token&"+
            "display=popup";
     
        facebookLoginWindow=window.open(loginUrl, "LoginWindow",
            "location=1,scrollbars=1,"+
            "width="+popupWidth+",height="+popupHeight+","+
            "left="+xPosition+",top="+yPosition);
     
        loginWindowTimer=setInterval(onTimerCallbackToCheckLoginWindowClosure, 1000);
    }
     </script>
