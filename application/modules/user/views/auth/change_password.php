<div class='sheet'>
	<div class='innersheet'>
	    
	    <div class="grid_24 page-header">
            <div class='title'>
                Change Password
            </div>
            <div class='subtitle'>
                Change the password for <?php echo $user->firstname; ?> <?php echo $user->lastname; ?>
            </div>
        </div>
	    
		<div class="grid_24">
			<?php echo validation_errors(); ?>
			<?php echo @$errors; ?>
			<br/>
			<?php echo $form; ?>
			
		</div>
	</div>
</div>
<!--
<div id="body">
<?php if(!$fb_data['me']): ?>
<p>Please login with your FB account: <a href="<?php echo $fb_data['loginUrl']; ?>" onClick="facebookLogin();return false;">login</a></p>
<?php else: ?>
<img src="https://graph.facebook.com/<?php echo $fb_data['uid']; ?>/picture" alt="" class="pic" />
<p>Hi <?php echo $fb_data['me']['name']; ?>,<br />
<a href="<?php echo site_url('topsecret'); ?>">You can access the top secret page</a> or <a href="<?php echo $fb_data['logoutUrl']; ?>">logout</a> </p>
<?php endif; ?>

<p>Enter your email address and password below.</p>
<?php echo validation_errors(); ?>
<hr/>
<?php echo $form; ?>
</div>
</div>

<script src="http://connect.facebook.net/en_US/all.js"></script>
<div id="fb-root"></div>
<script>
	var facebookLoginWindow		= '';
var loginWindowTimer		= '';

/**
*
*/
function onTimerCallbackToCheckLoginWindowClosure()
{
//window.location = '<?php echo base_url("users/login")?>';
}
/**
* @todo need some sort of polling to check they have succesfully logged in
* Redirect will be used for now
*/
function facebookLogin()
{
var popupWidth		= 500;
var popupHeight		= 300;
var xPosition		= ($(window).width()-popupWidth)/2;
var yPosition		= ($(window).height()-popupHeight)/2;
var loginUrl		="<?php echo $fb_data['loginUrl'];?>&"+

"display=popup";

facebookLoginWindow=window.open(loginUrl, "LoginWindow",
"location=1,scrollbars=1,"+
"width="+popupWidth+",height="+popupHeight+","+
"left="+xPosition+",top="+yPosition);

loginWindowTimer	=setInterval(onTimerCallbackToCheckLoginWindowClosure, 1000);
}
</script>
-->