<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Pubs Net - Thwaites</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content=""><!-- Le styles -->
    <link href="<?php echo base_url(); ?>assets/frontend/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/frontend/css/docs.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/frontend/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/frontend/css/layout.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/frontend/css/ui-lightness/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css"><!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
    <!-- Le fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/frontend/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/frontend/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/frontend/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/frontend/ico/apple-touch-frontend/icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/fronend/ico/favicon.png">
    <meta http-equiv="content-type" content="text/html; charset=utf-8"><?php //echo $page_meta; ?><?php //echo $_styles; ?><!-- Le javascript
    ================================================== --><!-- Placed at the end of the document so the pages load faster -->

    <script type="text/javascript" src="http://platform.twitter.com/widgets.js">
</script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/jQuery.js">
</script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/holder/holder.js">
</script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/google-code-prettify/prettify.js">
</script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/bootstrap-transition.js">
</script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/bootstrap-carousel.js">
</script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/bootstrap-dropdown.js">
</script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/jquery-1.7.2.min.js">
</script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/jquery-ui-1.8.20.custom.min.js">
</script><?php //echo $_scripts; ?>

<style type="text/css">
      /* Override some defaults */
      html, body {
        background-color: #eee;
      }
      body {
        padding-top: 40px; 
      }
      .container {
        width: 385px;
      }

      /* The white background content wrapper */
      .container > .content {
        background-color: #fff;
        padding: 20px;
        margin: 0 -20px; 
        -webkit-border-radius: 10px 10px 10px 10px;
           -moz-border-radius: 10px 10px 10px 10px;
                border-radius: 10px 10px 10px 10px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.15);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.15);
                box-shadow: 0 1px 2px rgba(0,0,0,.15);
      }

	  .login-form {
		margin-left: 65px;
	  }
	
	  legend {
		margin-right: -50px;
		font-weight: bold;
	  	color: #404040;
	  }

    </style>
    
    
</head>

<body>
    

      <div class="container">
    <div class="content">
      <div class="row">
        <div class="login-form">
          <h2>Login</h2>
          <form class="form-horizontal" action="<?php echo base_url(); ?>login" method="post" accept-charset="utf-8" id="1366911348" class="">
            <fieldset>
              <div class="control-group">
                                    <!-- Username -->
                                  

                                    <div class="controlss">
                                        <input type="text" id="username" name="username" placeholder="Username" class="input-xlarge">
                                    </div>
                                </div>

                                <div class="control-group">
                                    <!-- Password-->
                                    

                                    <div class="controlss">
                                        <input type="password" id="password" name="password" placeholder="Password" class="input-xlarge">
                                    </div>
                                </div>
              <button class="btn btn-success" type="submit">Sign in</button>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div> <!-- /container -->
  
  
  	
</body>
</html>
