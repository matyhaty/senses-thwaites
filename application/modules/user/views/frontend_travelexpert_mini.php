<div class="travelexperthello">
	<div class='title'>Our <?php echo $object->title; ?> travel expert</div>
    
    <div class="block">
        <div class="thumb">
            <div class="thumb">
                <?php echo anchor('persons/viewDetail/' . $object->expertID, slir('/users/profileimages/', $object_expert->profileimage, 75,75,array('width' => 75, 'height' => 75))); ?>
            </div>
        </div>
        
        <h2><?php echo $this->teamleaf->personNameFullFrontend($object_expert); ?></h2>

        <div class="details">
            Tel: <?php echo $object_expert->mobile; ?><br/>
            <?php echo $object_expert->email; ?>
                        
        </div>
        <div class="expert-copy">
            <?php 
            	if($object_expert->aboutmeshort !='')
            	{
            		echo $this->teamleaf->words($object_expert->aboutmeshort, 65); ?> <?php echo anchor('persons/viewDetail/'.$object_expert->id, 'Read More...'); 
            	}
            	else if($object_expert->aboutmemedium !='')
            	{
            		echo $this->teamleaf->words($object_expert->aboutmemedium, 65); ?>... <?php echo anchor('persons/viewDetail/'.$object_expert->id, 'Read More...'); 
            	}
            	else if($object_expert->aboutmelong !='')
            	{
            		echo $this->teamleaf->words($object_expert->aboutmelong, 65); ?>... <?php echo anchor('persons/viewDetail/'.$object_expert->id, 'Read More...'); 
            	}
            ?>
        </div>
        
       
    </div>
</div>