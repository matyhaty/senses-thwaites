<div class='sheet'>
    <div class="page-header-grey">
        <div class='image'>
            <?php echo slir('/users/profileimages/', $user -> profileimage, $width = 60, $height = 60, $crop = array(
				'width' => '55',
				'height' => '55'
			), 'title="Profile image" class="profile-image"');
            ?>
        </div>
        <div class='titles'>
            <div class='title'>
                <?php echo $user -> firstname; ?> <?php echo $user -> lastname; ?>'
                s Profile
            </div>
            <div class='subtitle'>
                Email: <?php echo $user -> email; ?>
                <br/>
                State: <?php echo $user -> state; ?>
            </div>
        </div>
        <div class='clear'></div>
    </div>
    <div class='innersheet'>
        <!--
        	<div class="grid_24 page-tab-bar">
            <ul>
                <li>Latest Activity</li>
                <li>Basic Information</li>
                <li>Qualifications</li>
                <li>Attendance</li>
                <li>Depots</li>
            </ul>
        </div>
        <div class='clear'></div>  
        -->  
            
        <div class="grid_24 profile-segment">
            <div class='segment-header'>
                <div class='title'>Basic Information</div>
                <div class='option'><a href='<?php echo base_url(); ?>admin/user/profile/create/<?php echo $user -> id; ?>'>Edit</a></div>
                 <div class='option'><a href='<?php echo base_url(); ?>admin/user/auth/change_user_password/<?php echo $user -> id; ?>''>Reset Password</a></div>
            </div>
            
            <div class='segment-content'>
            
                <table class='basic_information tt'>
                	<tr>
                        <td>Name</td>
                        <td><?php echo $user -> firstname; ?> <?php echo $user -> lastname; ?></td>
                    </tr>
                    <tr>
                        <td>Username</td>
                        <td><?php echo $user -> username; ?> </td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td><?php echo $user -> address; ?></td>
                    </tr>
                    <tr>
                        <td>Postcode</td>
                        <td><?php echo $user -> postcode; ?></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td><?php echo $user -> email; ?></td>
                    </tr>
                    <tr>
                        <td>Date of Birth</td>
                        <td><?php echo $user -> dob; ?></td>
                    </tr>
                    <tr>
                        <td>Phone Number</td>
                        <td><?php echo $user -> phone_number; ?></td>
                    </tr>
                    <tr>
                        <td>Occupation</td>
                        <td><?php echo $user -> occupation; ?></td>
                    </tr>
                    <tr>
                        <td>User Type</td>
                        <td><?php echo $user -> user_type; ?></td>
                    </tr>
                    <tr>
                        <td>Administrator</td>
                        <td><?php echo $user -> user_admin; ?></td>
                    </tr>
                    <tr>
                        <td>Per Hour Wage</td>
                        <td>&pound;<?php echo $user -> perhourcost; ?></td>
                    </tr>
                </table>
            
            </div>
            
            
        </div>
        <div class='clear'></div>        
    </div>
</div>