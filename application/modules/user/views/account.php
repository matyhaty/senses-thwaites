<div class='sheet'>
    <div class='innersheet'>
        
        <div class="grid_8 page-header">
            <div class='title'>About You</div>
            <div class='subtitle'>
                Tell us everything
            </div>
        </div>
        <div class='clear'></div>
        
        <div class="grid_24">
            <?php echo validation_errors();?>
        </div>
        <div class='clear'></div>
          
        <div class="grid_24"> 
            <?php echo $form;?>
        </div>
        <div class='clear'></div>
         
         
    </div>
</div>