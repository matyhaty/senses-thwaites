<div class='sheet'>
    <div class='innersheet'>
        <div class="grid_8 page-header">
            <div class='title'>
                Your Account
            </div>
            <div class='subtitle'>
                All in control
            </div>
        </div>
        <div class='clear'></div>
        <div class="grid_24">
            <?php echo permAnchor($this -> user, 'user/account/edit', 'Edit Account');?>
            <br />
            <?php echo permAnchor($this -> user, 'event/events/index', 'My Events');?>
            <br />
            <?php echo permAnchor($this -> user, 'lecture/lectures/index', 'My Classes');?>
            <br />
            <?php echo permAnchor($this -> user, 'job/jobs/index', 'My Jobs');?>
            <br />
            <?php echo permAnchor($this -> user, 'question/questions/index', 'My Questions');?>
            <br />
            <?php echo permAnchor($this -> user, 'group/groups/index', 'My Groups');?>
            <br />
            <?php echo anchor(array(
                    'user',
                    'auth',
                    'logout'
                ), 'Logout');
            ?>
        </div>
        <div class='clear'></div>
    </div>
</div>