<!-- Profile Head -->
<section class="section">
	
	<div class="row">
		
		<div class="col col2-1">
			<div class="profile-image-container">
			<img class="overlay" src="/assets/images/profile-image-overlay.png" alt="" />
			<img class="profile-image" src="/assets/images/test-profile.png" alt="" />
			</div>
		</div>
	
		<div class="col col2-1">
			<hgroup>
				<h2><?php echo $profile['fullname']; ?></h2>
				<h4>is currently out enjoying life walking and photographing <span>Edit</span></h4>
			</hgroup>
			
			<div class="profile-desc">
			  <p><?php echo $profile['bio']; ?></p>
			<a href="#" class="orange-btn-sml rounded">Edit profile</a>
			<a href="#" class="orange-btn-sml rounded">Edit/add resume</a>
			
			</div>
			
		</div>
	
	</div>


</section>

	<section class="section" style="margin-top: -102px;">

	<div class="row">

		<?php echo $this->load->view("user/pms/left"); ?>
	
		<div class="col col4-3" style="margin-top:18px;">
			<div class="row">
			
				<div class="col col1">
				<h3>Drafts</h3>
				</div>
			
				<div class="col col1">
				
					<div class="row">
						<div class="col col8-3"><span class="inbox-new"></span><span class="inbox-field">Name (From)</span></div>
						<div class="col col8-3"><span class="inbox-field">Subject</span></div>
						<div class="col col4-1"><span class="inbox-field">Date</span></div>
					</div>
					
					<?php foreach($messages as $message) : ?>
					
					<div class="row">
						<div class="col col8-3"><span class="inbox-new"></span><span class="inbox-name-new"><a href="/compose/<?php echo $message['id']; ?>"><?php echo get_user_fullname($message['created_by']); ?></a></span></div>
						<div class="col col8-3"><span class="inbox-subject-new"><a href="/compose/<?php echo $message['id']; ?>"><?php echo $message['subject']; ?></a></span></div>
						<div class="col col4-1"><span class="inbox-date" title="Message saved at <?php echo date("H:i:s", $message['date']); ?> on <?php echo date("d-m-Y", $message['date']); ?>"><?php echo date("d-m-Y", $message['date']); ?></span><a href="/user/pms/delete/<?php echo $message['id']; ?>"><span class="inbox-delete"></span></a></div>
					</div>					
					
					<?php endforeach; ?>					

				</div>
				

			</div>
		</div>
		
	</div>
</section>