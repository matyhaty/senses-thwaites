<!-- Profile Head -->
<section class="section">
	
	<div class="row">
		
		<div class="col col2-1">
			<div class="profile-image-container">
			<img class="overlay" src="/assets/images/profile-image-overlay.png" alt="" />
			<img class="profile-image" src="/assets/images/test-profile.png" alt="" />
			</div>
		</div>
	
		<div class="col col2-1">
			<hgroup>
				<h2><?php echo $profile['fullname']; ?></h2>
				<h4>is currently out enjoying life walking and photographing <span>Edit</span></h4>
			</hgroup>
			
			<div class="profile-desc">
			  <p><?php echo $profile['bio']; ?></p>
			<a href="#" class="orange-btn-sml rounded">Edit profile</a>
			<a href="#" class="orange-btn-sml rounded">Edit/add resume</a>
			
			</div>
			
		</div>
	
	</div>


</section>

<section class="section" style="margin-top: -102px;">
	
	<div class="row">
	
		<?php echo $this->load->view("user/pms/left", array()); ?>
	
	
		<div class="col col4-3" style="margin-top:18px;">
		
			<div class="row">
				<div class="col col1">
				<h3>Message</h3>
				</div>
			</div>
			
			<?php foreach($messages as $message) : ?>
				<section class="<?php echo $message['class']; ?>">
				
					<div class="row" id="<?php echo $message['id']; ?>">
						<div class="col col4-1"><h3>Sender:</h3></div>
						<div class="col col4-3 rounded"><strong><?php echo get_user_fullname($message['created_by']); ?></strong></div>
					</div>
					
					<div class="row">
						<div class="col col4-1"><h3>Date:</h3></div>
						<div class="col col4-3 rounded"><strong><?php echo date("M d, Y", $message['created']); ?></strong></div>
					</div>
					
					<div class="row">
						<div class="col col4-1"><h3>Subject:</h3></div>
						<div class="col col4-3 rounded"><strong><?php echo $message['subject']; ?></strong></div>
					</div>
					
					<div class="row">
						<div class="col col4-1"><h3>Message:</h3></div>
						<div class="col col4-3 rounded">
						<p><?php echo $message['message']; ?></p>
						</div>
					</div>
				
				</section>
				
				<hr />
			<?php endforeach; ?>
			
			<!--
			<section class="received-message">
			
				<div class="row">
					<div class="col col4-1"><h3>Sender:</h3></div>
					<div class="col col4-3 rounded"><strong></strong></div>
				</div>
				
				<div class="row">
					<div class="col col4-1"><h3>Date:</h3></div>
					<div class="col col4-3 rounded"><strong>Apr 23, 2012</strong></div>
				</div>
				
				<div class="row">
					<div class="col col4-1"><h3>Subject:</h3></div>
					<div class="col col4-3 rounded"><strong>.</strong></div>
				</div>
				
				<div class="row">
					<div class="col col4-1"><h3>Message:</h3></div>
					<div class="col col4-3 rounded">
					<p></p>
					</div>
				</div>
			
			</section>
			-->
			
			<?php if($to_user != null) : echo form_open('user/pms/send'); ?>	
				<div class="row">
					<div class="col col1">
					<h3>Send a reply</h3>
					</div>
				</div>
									  
				<!--<div class="row">
					<div class="col col4-1"><h3>Recipient (s):</h3></div>
					<div class="col col4-3"><input class="pm-field rounded" type="text" name/></div>
				</div>-->
				
				<div class="row">
					<div class="col col4-1"><h3>Subject:</h3></div>
					<div class="col col4-3"><input class="pm-field rounded" type="text" name="subject" value="RE: <?php echo $messages[0]['subject']; ?>" /></div>
				</div>
				
				<div class="row">
					<div class="col col4-1"><h3>Message:</h3></div>
					<div class="col col4-3"><textarea class="pm-field rounded" name="msg"></textarea></div>
				</div>
				
				<div class="row">
					<div class="col col4-1">&nbsp;</div>
					<div class="col col4-3">
					<input type="submit" class="orange-btn-med rounded" name="send" value="Send" />
					<input type="submit" class="orange-btn-med rounded" name="save" value="Save" />
					<input type="submit" class="orange-btn-med rounded" name="cancel" value="Cancel" />
					<input type="hidden" name="parent" value="<?php echo $msg; ?>" />
					<input type="hidden" name="to_user" value="<?php echo $to_user; ?>" />
					</div>
				</div>				
			<?php form_close(); endif; ?>
			
		</div>
		
	</div>
</section>