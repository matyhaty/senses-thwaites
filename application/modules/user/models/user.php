<?php
/**
 * User Class
 *
 */
class User extends Base_Model
{

	public $table = 'users';
	public $has_one = array( 'company');
	public $has_many = array(

			'permission',
			'message',
			'comment',
			'wall',
			'file',
			'communication',
			'task',
			'blog'
	);
	/**
	 * Cons
	 *
	 * @param Int $id
	 */
	function __construct($id = null)
	{
		parent::__construct($id);
	}

	/**
	 * Save the user
	 *
	 * @param unknown_type $data
	 * @param unknown_type $user
	 * @return unknown
	 */
	function saveUser($data, $user)
	{
		$user = $this -> getUserByID($user -> id);
		$user -> where(array("id" => $user -> id));
		// Return data
		return $user -> update($data);
	}

	/**
	 * Get a user, must be an asso array
	 *
	 * @param unknown_type $data
	 */
	protected function getUser(array $data, array $ignore = array())
	{
		foreach ($data as $field => $value)
		{
			if (in_array($field, $ignore))
			{
				continue;
			}

			$this -> where($field, $value);
		}
		$this -> get();

		return $this -> to_array();
	}

	/**
	 * Get all users, must be an asso array
	 *
	 * @param unknown_type $data
	 */
	
	public function getUsers()
	{
		return $this -> getObject('user', null, null, false, array('state' => 'Active'), '', '') -> dm_object;
	}

	public function getUsersBySortOrder()
	{
		return $this -> getObject('user', null, null, false, array('state' => 'Active'), array('sortorder' => 'asc'), '') -> dm_object;
	}
	
	
	public function getUsersAlpha()
	{
		return $this -> getObject('user', null, null, false, array('state' => 'Active'), '', '') -> dm_object;
	}

	function getUserOptions($currentFlag = NULL)
    {

        $types = $this->getUsersAlpha();
        $options = array();
        foreach ($types as $obj)
        {
            $options[$obj->id] = $obj->firstname.' '.$obj->surname;
        }
        if ($currentFlag)
        {
            $options['selected'] = $currentFlag;
        }

        return $options;
    }
	
	 function getStateOptions($currentFlag = NULL)
    {

        //$types = $this->getAll();
        $options = array(
            'Active' => 'Active',
            'Disabled' => 'Disabled',
            'Deleted' => 'Delete',
        );
        if ($currentFlag)
        {
            $options['selected'] = $currentFlag;
        }

        return $options;

    }
	

	/**
	 * getUser
	 *
	 * @param unknown_type $id
	 * @return unknown
	 */
	public function getUserByID($id)
	{
		return $this -> getObject('user', $id, null, false, array('state' => 'Active'), '', '') -> dm_object;
	}
	
	/**
	 * getUser
	 *
	 * @param unknown_type $id
	 * @return unknown
	 */
	public function getUserByUsername($username)
	{
		return $this -> getObject('user', null, null, false, array('state' => 'Active', 'username', $username), '', '') -> dm_object;
	}
	

		/**
	 * getUserDepots
	 *
	 * @param unknown_type $id
	 * @return unknown
	 */
	public function getUserDepots($user)
	{
		return $this -> getObject('depot', null, $user, false, array('state' => 'Active'), '', '') -> dm_object;
	}
	
		/**
	 * getUserCompany
	 *
	 * @param unknown_type $id
	 * @return unknown
	 */
	public function getUser_Company($user)
	{
		return $this -> getObject('company', null, $user, false, array('state' => 'Active'), '', '') -> dm_object;
	}
	
		/**
	 * getUserDepartments
	 *
	 * @param unknown_type $id
	 * @return unknown
	 */
	public function getUser_Departments($user)
	{
		return $this -> getObject('department', null, $user, false, array('state' => 'Active'), '', '') -> dm_object;
	}
	
	public function getUserInitials($user)
	{
		return strtoupper(substr($user->firstname, 0, 1).substr($user->lastname, 0, 1));
	}
	
	public function getUser_Taskassigns($taskID, $startdate = null, $enddate = null)
	{
		if ($startdate && $enddate)
		{
			$start = new DateTime($startdate);
			$end = new DateTime($enddate);

			return $this -> getObject('', null, $asset, null, array(
				'state' => 'Active',
				'start >=' => $start -> format('Y-m-d') . ' 00:00:00',
				'end <' => $end -> format('Y-m-d') . ' 00:00:00'
			)) -> dm_object;
		}
		else
		if ($startdate)
		{
			$start = new DateTime($startdate);
			$end = new DateTime($startdate);
			$end -> modify('+1 day');

			return $this -> getObject('taskassign', null, $asset, null, array(
				'state' => 'Active',
				'start >=' => $start -> format('Y-m-d') . ' 00:00:00',
				'end <' => $end -> format('Y-m-d') . ' 00:00:00'
			)) -> dm_object;
		}
		else
		{
			return $this -> getObject('taskassign', null, $asset, null, array('state' => 'Active'), '', '') -> dm_object;
		}

	}
	

	/**
	 * Remove the user cache, if they update the profile for example
	 *
	 */
	public function removeUserCache($user)
	{
		return parent::removeCachedObject('user', $user -> id);
	}

	/**
	 * get username by email
	 *
	 * @param String $email
	 * @return Object
	 */
	public function getUserByEmail($email)
	{
		return $this -> getObject('user', null, null, true, array('email' => $email)) -> dm_object;
	}


	/**
	 * display user
	 *
	 * @param datamapper user object
	 * @return string
	 */
	public function displayUser($user)
	{
		return anchor('user/profile/display/'.$user->id, $user->firstname.' '.$user->lastname, array('class' => 'displayUser')); 
	}

	/**
	 * Check accounts
	 *
	 * @param String $data
	 * @param String $data
	 * @return Object
	 */
	public function checkAcounts($email, $username)
	{
		$this -> or_where('email', $email);
		$this -> or_where('username', $username);
		$user = $this -> get() -> to_array();

		if ($user['id'])
		{
			return $user;
		}
		else
		{
			return false;
		}
	}

	/**
	 * remove session
	 *
	 * @param Int $old_session_id
	 */
	protected function removeSession($old_session_id)
	{
		$this -> clear();
		$this -> where('session_id', $old_session_id);
		$this -> delete();
	}

	/**
	 * Login the user
	 *
	 * @param array $data
	 * @param unknown_type $session
	 * @return unknown
	 */
	public function login(array $data, &$session)
	{

		$data = $data['data'];
		$user = $this -> getUser($data, array('password'));

		if (!$user['id'])
		{
			// no user found set flash message
			//$this->addFlashMessage('No user found');
			return false;
		}

		$parts = explode(':', $user['password']);
		$crypt = @$parts[0];
		$salt = @$parts[1];
		$testcrypt = getCryptedPassword($data['password'], $salt, true);

		if ($crypt == $testcrypt)
		{

			$old_session_id = $session -> userdata('session_id');
			if ($this -> createSession($user, $session, $old_session_id))
			{
				return $user;
			}
			else
			{
				$this -> addFlashMessage('Session could not be created.');
				return false;
			}
			// Password match!
		}
		else
		{
			//$this->addFlashMessage('Password or username in-correct');
			return false;
			// Password salt does not match
		}

		return false;
	}

	function change_user_password($data, $user, $session)
	{
		$salt = genRandomPassword(32);
		$plainpassword = $data['new_password'];
		$newPass = getCryptedPassword($plainpassword, $salt);
		
		$user->password = $newPass;
		$user->save();
		
		return TRUE;
		
	}

	/**
	 * Create a user session
	 *
	 * @param unknown_type $user
	 * @return unknown
	 */
	function createSession($user, &$session, $old_session_id)
	{

		$newdata = array(
				//'user_type' => array($user['user_type']),
				'userid' => $user['id'],
				'username' => $user['username'],
				'email' => $user['email'],
				'hash' => md5($user['username'] . $user['email']),
				'expires' => (time() + 7200),
			// hash to check against in case session data has been tampered with
		);

		$session -> set_userdata('user_data', $newdata);
		$session -> set_userdata($newdata);

		$this -> removeSession($old_session_id);

		return true;
		// sets its in the database

	}

	/**
	 * Register User
	 *
	 * @param array $data
	 * @return Boolean
	 */
	function register(array $data)
	{
		try
		{
			if (DMZ_Array::from_array($this, $data, array(), $save = true))
			{
				return $this;
			}
			return false;
			// call it via this results in some weird results
		}
		catch(Exception $e)
		{
			$this -> errors[] = $e;
			return false;
		}
	}

	private $table_name = 'users';
	// user accounts

	/* tank auth start */

	/**
	 * Check if username available for registering
	 *
	 * @param	string
	 * @return	bool
	 */
	function is_username_available($username)
	{
		$this -> db -> select('1', FALSE);
		$this -> db -> where('LOWER(username)=', strtolower($username));

		$query = $this -> db -> get($this -> table_name);
		return $query -> num_rows() == 0;
	}

	/**
	 * Check if email available for registering
	 *
	 * @param	string
	 * @return	bool
	 */
	function is_email_available($email)
	{

		$this -> db -> select('1', FALSE);
		$this -> db -> where('LOWER(email)=', strtolower($email));

		$query = $this -> db -> get($this -> table_name);
		return $query -> num_rows() == 0;
	}

	/**
	 * Set new email for user (may be activated or not).
	 * The new email cannot be used for login or notification before it is activated.
	 *
	 * @param	int
	 * @param	string
	 * @param	string
	 * @param	bool
	 * @return	bool
	 */
	function set_new_email($user_id, $new_email, $new_email_key, $activated)
	{
		$this -> db -> set($activated ? 'new_email' : 'email', $new_email);
		$this -> db -> set('new_email_key', $new_email_key);
		$this -> db -> where('id', $user_id);
		$this -> db -> where('state', $activated ? 'active' : 'pending');

		$this -> db -> update($this -> table_name);
		return $this -> db -> affected_rows() > 0;
	}

	/**
	 * Activate user if activation key is valid.
	 * Can be called for not activated users only.
	 *
	 * @param	int
	 * @param	string
	 * @param	bool
	 * @return	bool
	 */
	function activate_user($user_id, $activation_key, $activate_by_email)
	{
		$this -> db -> select('1', FALSE);
		$this -> db -> where('id', $user_id);
		if ($activate_by_email)
		{
			$this -> db -> where('new_email_key', $activation_key);
		}
		else
		{
			$this -> db -> where('new_password_key', $activation_key);
		}
		$this -> db -> where('state', 'pending');
		$query = $this -> db -> get($this -> table_name);

		if ($query -> num_rows() == 1)
		{

			$this -> db -> set('state', 'active');
			$this -> db -> set('new_email_key', NULL);
			$this -> db -> where('id', $user_id);
			$this -> db -> update($this -> table_name);
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Check if given password key is valid and user is authenticated.
	 *
	 * @param	int
	 * @param	string
	 * @param	int
	 * @return	void
	 */
	function can_reset_password($user_id, $new_pass_key, $expire_period = 900)
	{
		$this -> db -> select('1', FALSE);
		$this -> db -> where('id', $user_id);
		$this -> db -> where('new_password_key', $new_pass_key);
		$query = $this -> db -> get($this -> table_name);
		return $query -> num_rows() == 1;
	}

	/**
	 * Change user password if password key is valid and user is authenticated.
	 *
	 * @param	int
	 * @param	string
	 * @param	string
	 * @param	int
	 * @return	bool
	 */
	function reset_password($user_id, $new_pass, $new_pass_key)
	{
		$this -> db -> set('password', $new_pass);
		$this -> db -> set('new_password_key', NULL);
		$this -> db -> where('id', $user_id);
		$this -> db -> where('new_password_key', $new_pass_key);

		$this -> db -> update($this -> table_name);
		return $this -> db -> affected_rows() > 0;
	}

	/**
	 * Build the actitivy for a single user
	 * Created by Luke Steadman 04/04/2012
	 */
	public function build_activity($user)
	{
		// Now we need to produce an activity feed for the user profile
		// Uncomment below to test if the foreach is not working as expected

		//$user->lecture->get();
		//$user->event->get();
		//$user->job->get();
		//$user->image->get(); // not working as of 04/04/2012
		//$user->friend->get(); // not working as of 04/04/2012
		//$user->like->get();
		//$user->recommendation->get(); // not working as of 04/04/2012
		//$user->group->get();
		//$user->question->get();

		// List all activity

		$this -> userIn = $user;

		$cachedActivities = $this -> getCachedActivity();

		$activity = array();
		// return data
		$this -> userPage = $user;

		// Return activity
		if (is_array($cachedActivities))
			return array_reduce($cachedActivities, array(
					$this,
					'reduce'
			));

		return false;
		// should be better for memory, since its is is reducing a large array
	}

	function reduce($a, $act)
	{

		static $activity;
		// awesome, helps remember the variable
		$user = $this -> userIn;

		$acvivities = array(
				"lecture" => 1,
				"event" => 1,
				"video" => 1,
				"image" => 1,
				"job" => 1,
				"friend" => 1,
				"like" => 1,
				"recommendation" => 1,
				"group" => 1,
				"question" => 1
		);
		// 	search against index for speed boost rather then in_array

		// check item against the user id
		$type = key($act[$user -> id]);
		$theActv = $act[$user -> id][$type];
		$id = key($theActv);

		sort($theActv[$id]);
		$theActv = $theActv[$id][0];
		// set the index to 0 instead of the time stamp

		// the activity belongs to the user
		if (isset($act[$user -> id]))
			if (isset($acvivities[$type]))
			{
				$activity[] = array(
						"name" => $theActv['title'],
						"type" => ucfirst($type),
						"desc" => $theActv['description'],
						"created" => $theActv['created']
				);
			}

		return $activity;
	}

	/**
	 * Sort array by created date
	 * TODO: Move sort_created function to helper script.
	 */
	function sort_created($a, $b)
	{
		return strnatcmp($b['created'], $a['created']);
	}

}

/* End of file user.php */
/* Location: ./application/models/user.php */
