<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends Base_Controller {

	
	function __construct()
	{
		
		parent::__construct();
		$this->model = new User();	
		$this->model->cache = $this->cache;
		
	}	
	
	/**
	 * Register the user action
	 *
	 */
	public function index()
	{	
		$this->template->write( 'page_header', 'Dreamitize - Please login' );
		
		// Load the email helper
		$this->load->helper('email');
		$this->load->helper('password_helper');
		$this->load->helper('email_helper');
		$this->load->library('form_validation');
		
		$formdata	= $this->xml_forms->loadXML( APPPATH . 'modules/user/forms', 'register');
		$form		= $this->xml_forms->createform($formdata['xml'], null , $formdropdowns = null ,array() );
		$data		= array('form'=>$form);		
		if ($this->form_validation->run() == false){			
			// Load the view
			$this->template->write_view('sheet', 'user/register', $data );	
		}else{
			
			$email_activation		= $this->config->item('email_activation', 'tank_auth');
			$data					= $this->input->post();
			$salt					= genRandomPassword(32);
			//  get random salt			//$this->user_register is auto loaded see autoload file
			$plainpassword			= $data['password'];
			// in case it should be sent to the user
			$data['password'] 		= getCryptedPassword($plainpassword , $salt);
			$data['new_email_key']	= md5(rand().microtime());
			$data['state']			= 'pending';
			$data['user_type']		= 1;
			// safed up pass!
			if($userFound = $this->model->checkAcounts($data['email'] ,$data['username'])){
				if(in_array($data['email'],$userFound)){
					$this->template->write_view('flash_message', 'common/flash_message', array( 'message'=> 'Email is already in use' ) );
					
				}else{
					$this->template->write_view('flash_message', 'common/flash_message', array( 'message'=> 'Username is already in use' ) );			
				}
				$this->template->write_view('content', 'user/register', $data );
			}elseif($user = $this->model->register($data)){
				$data['user_id']	= $user->id;
				$data['site_name']	= $this->config->item('website_name', 'tank_auth');
				$this->_send_email('activate', $data['email'], $data);
				$this->_show_message($this->lang->line('auth_message_registration_completed_1'));					
			}else{
				$this->template->write_view('flash_message', 'common/flash_message', array( 'message'=> 'Something went wrong, please try again' ) );
				$this->template->write_view('sheet', 'user/register', $data );				
			}
		}
		$this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);		
		
	}
}  // end class