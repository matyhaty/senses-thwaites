<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Profile extends Base_Controller
{

	/**
	 * Constructor for profile page
	 *
	 */
	function __construct()
	{

		$this -> load -> library('form_validation');
		$this -> load -> helper(array(
			'form',
			'url'
		));
		parent::__construct();
		$this -> model = new User('user');
		$this -> model -> cache = $this -> cache;

	}

	public function frontend_view($slug, $id, $posted = false)
	{
		$this -> setTemplate('travelexpert');
		$this -> teamleaf -> expertFlashSet($id);
		$u = new User($id);
		
		
		
		if ($posted && $this -> input -> post('name') && $this -> input -> post('email') && $this -> input -> post('telephone'))
		{
			$e = new Communication();
			$e -> enquiry_name = $this -> input -> post('name');
			$e -> enquiry_email = $this -> input -> post('email');
			$e -> enquiry_telephone = $this -> input -> post('telephone');
			$e -> holiday_notes = $this -> input -> post('comments');

			//$e -> subscribed = $this -> input -> post('subscribed');
			$e -> enquiry_date = date('Y-m-d H:i:s');
			$e -> info_lead_source = '1';
			$e -> enquirytype = 'Enquiry';
			$e -> enquiry_address = $this -> input -> post('address');
			
			$e->status = 1;
			//$e -> enquiry = $this -> input -> get('enquiry');

			$e -> info_travelexpert = 259;
			if ($this -> input -> post('travelexpertID'))
			{
				$e -> info_travelexpert = $this -> input -> post('travelexpertID');
			}

			if (isset($data['travelexpertID']))
			{
				$e -> info_travelexpert = $data['travelexpertID'];
			}

			if (isset($data['leader']))
			{
				$r = new Referrer();
				$r->where('title', $data['leader']);
				$r->get();

				$e -> info_lead_source = $r->id;
				
				if (isset($data['leader_memberID']))
				{
					$e -> holiday_notes .= '   QUIDCO MEMBER ID: '.$data['leader_memberID'];
				}
			}

			$e -> save();
			$e -> enquiryalert($e -> id);
			
			$this -> maintaincache -> communication($e -> id);
					$this->maintaincache->communication_status($e -> id);
			
			
			$this->data['posted'] = true;
		}

		

		//$data = $this -> teamleaf -> getvars();
		//$this -> teamleaf -> travelExpertFlashSet($id);

		$person = new User($id);
		if ($person -> state != 'Active')
		{
			$person = new User(259);
			$id = 259;
			// force Amanda for deleted users.
		}

		$te = new Blog();
		$te -> where('creator', $id) -> order_by('blogdate', 'desc') -> limit(15) -> get();

		$this -> data['person'] = $person;
		$this -> data['blogs'] = $te;
		$this -> data['myRecommendedHotels'] ='';
		$count = 0;
		
			$h = new Hotel();
			$h -> where('state', 'Active');
			$h -> limit(6);
			$h -> get();
			foreach ($h as $recommendedhotel)
			{
				$count++;
				$data['h'] = new Hotel($recommendedhotel -> id);
				$this -> data['myRecommendedHotels'] .= $this -> load -> view('hotel/hotel_mini_3', $data, true);
			}
		
		
		if ($count < 6)
		{
			$this -> fb -> info('else');
			foreach ($recommendedHotels as $recommendedhotel)
			{
				$ht = new Hotel($recommendedhotel -> hotelID);
				$d = new Destination($ht -> subdestination);
				$d -> hotel -> limit(12) -> get();
				foreach ($d->hotel as $h)
				{
					$count++;

					$data['h'] = new Hotel($h -> id);
					$this -> data['myRecommendedHotels'] .= $this -> load -> view('hotel/hotel_mini_3', $data, true);
					if ($count == 6)
					{
						break;
					}
				}

				if ($count == 6)
				{
					break;
				}

			}
		}
		//$this -> fb -> info('render');
		$this -> data['latestreviews'] = $this -> load -> view('blog/latestreviews', '', true);
		$this -> data['latestblogs'] = $this -> load -> view('blog/latestarticles', '', true);

		$this -> data['travelexpertID'] = $person -> id;
		$this -> data['callmeback'] = $this -> load -> view('user/callmeback_viewdetail', $this -> data, true);

		$this -> template -> write_view('content', 'user/frontend_viewDetail', $this -> data);

		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);

	}

	public function display($username = '')
	{
		$this -> load -> helper('url');
		// Get the user profile
		// if its a post, we should search the DB
		$this -> template -> write('page_header', '');

		// Get the user profile
		if ($username != "" && is_numeric($username))
		{
			$user = $this -> model -> getUserByID($username);
		}
		else
		if ($username != "")
		{
			$user = $this -> model -> getUserByUsername($username);
			// Or just set the user?
		}
		else
		{
			$user = $this -> user;
		}
		//

		// Set profile data and render profile head
		$profile = array(
			"fullname" => $user -> firstname . " " . $user -> lastname,
			"firstname" => $user -> firstname,
			"lastname" => $user -> lastname,
			"bio" => $user -> bio
		);
		//$user_company = $this->model->getUser_Company($user);

		$this -> template -> write_view('sheet', 'user/profile', array(
			"profile" => $profile,
			"userid" => $user -> id,
			"user" => $user,
		));

		// Render template
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

	public function index()
	{
		$this -> display($username = '');
	}

	public function view()
	{
		$this -> template -> write('page_header', l('_D_MY') . ' ' . l('_D_QUESTIONS'));
		$this -> load -> library('session');
		$this -> load -> helper('url');
		$userid = $this -> session -> userdata('userid');
		$data['users'] = $this -> model -> getUsers();
		// super chain!
		$this -> template -> write_view('sheet', 'user/view', $data);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

	/**
	 * Creates a User
	 *
	 */
	public function create($id = null)
	{
		$this -> template -> write('page_header', l('_D_CREATE') . ' ' . l('_D_QUESTIONS'));
		$user = $this -> model;
		$formdropdowns = '';

		if ($id)
		{
			$user = new User($id);
		}
		else
		{

		}
		// Load XML
		$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/user/forms', 'account');

		if ($this -> input -> post())
		{
			// If posted and form validation passes
			if ($this -> form_validation -> run() == TRUE)
			{
				$data = $this -> input -> post();

				$path = 'users/profileimages';
				$this -> teamleaf -> checkpath($path);
				$fileresult = $this -> xml_forms -> uploadfiles(1, $path);
				if ($fileresult['result'])
				{
					$data['profileimage'] = @$fileresult[1]['file_name'];
				}
				$data['user_type'] = 1;

				//$userid = $this -> session -> userdata('userid');
				$saved = DMZ_Array::from_array($user, $data, null, true);
				// this saves this automatically to the Task Object (e.g. same as $user->save());

				// Save the task
				if ($saved)
				{
					//$company = new Company($data['company']);
					//$user->save($company);
					$this -> maintaincache -> user($user -> id);
					redirect(base_url() . 'admin/user/profile/display/' . $user -> id);
				}
				else
				{
					$this -> template -> write('validation_errors', l('_D_ERROR'));
					// with some errors!
				}
			}
			else
			{
				// render errors
				$this -> template -> write('validation_errors', 'Error:' . validation_errors());
				// with some errors!
			}
		}

		if ($id)
		{
			$u = $this -> model -> getUserByID($id);
			//$user_company = $this->model->getUser_Company($u);
			//$companyID = $user_company->id;
		}
		else
		{
			$companyID = '';
		}

		//$companyM = new Company();
		//$formdropdowns['company'] = $companyM->getOptions($companyID);
		$h = new Hotel();
		$formdropdowns['hotelexpert1'] = $h -> getHotelOptions($user -> hotelexpert1);
			$formdropdowns['hotelexpert2'] = $h -> getHotelOptions($user -> hotelexpert2);
			$formdropdowns['hotelexpert3'] = $h -> getHotelOptions($user -> hotelexpert3);
			$formdropdowns['hotelexpert4'] = $h -> getHotelOptions($user -> hotelexpert4);
			$formdropdowns['hotelexpert5'] = $h -> getHotelOptions($user -> hotelexpert5);
			$formdropdowns['hotelexpert6'] = $h -> getHotelOptions($user -> hotelexpert6);
		$formdropdowns['state'] = $user -> getStateOptions($user -> state);
		$form = $this -> xml_forms -> createform($formdata['xml'], $user, $formdropdowns, array());
		$this -> template -> write('sheet', $form);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

}

//end class
