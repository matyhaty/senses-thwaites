<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MiniAdmin extends Base_Controller {

	
	function __construct()
	{
		
		parent::__construct();
		$this->model = new User();	
		$this->model->cache = $this->cache;
		
	}	
	
	/**
	 * Register the user action
	 *
	 */
	public function index()
	{	
		$this->template->write_view('content', 'user/miniadmin', $data =array(  ) );
		$this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);		
	}
	
	
	public function createCategory()
	{
		$category_name			= $_POST['name'];
		$category				= new category();
		try{
			//$the_category		= new $_POST['for'](19);
			$category->title= $category_name;
			$category->save();
			//$the_category->save($category);
		}catch(Exception $e){
			echo $e;
			die;
		}
		redirect('admin/user/miniadmin/index');
	}
	
}  // end class