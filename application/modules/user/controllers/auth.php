<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Auth extends Base_Controller
{

	function __construct()
	{
		$this -> load -> library('form_validation');
		$this -> load -> helper(array(
				'form',
				'url'
		));

		parent::__construct('User');

	}

	/**
	 * Login will be show
	 *
	 * @return unknown
	 */
	public function index()
	{

		$this -> template -> write('page_header', 'Teamleaf - Please login');
		// add a page title

		$this -> load -> helper('password_helper');

		$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/user/forms', 'login');
		$form = $this -> xml_forms -> createform($formdata['xml'], null, $formdropdowns = null, array('nosheet' => 1));
		$fb_data = $this -> session -> userdata('fb_data');
		$data = array(
				'data' => null,
				'fb_data' => $fb_data,
				'form' => $form
		);

		if ($this -> loggedIn)
		{
			//user is already logged in redirect them to profile page
			$this -> addFlashMessage('you are already logged in');
			return redirect('admin/home');
		}

		if ($this -> form_validation -> run() == false)
		{
			// Load the view
			$this -> template -> write_view('sheet', 'user/auth/login', $data);
		}
		else
		{

			$postdata = $this -> input -> post();
			$data = array(
					'data' => $postdata,
					'fb_data' => $fb_data,
					'form' => $form
			);
			if ($this -> model -> login($data, $this -> session))
			{
				$this -> addFlashMessage('You have been logged in');
				return redirect('admin/home');
			}
			else
			{
				// login failed add flash message
				$this -> template -> write_view('sheet', 'user/auth/login', $data);
			}

		}

		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}
	
	public function frontend_login()
	{
		$userM = new User();
		$this -> template -> write('page_header', 'Teamleaf - Please login');
		// add a page title

		$this -> load -> helper('password_helper');

		$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/user/forms', 'login');
		$form = $this -> xml_forms -> createform($formdata['xml'], null, $formdropdowns = null, array('nosheet' => 1));
		$fb_data = $this -> session -> userdata('fb_data');
		$data = array(
				'data' => null,
				'fb_data' => $fb_data,
				'form' => $form
		);

		if ($this -> loggedIn)
		{
			//user is already logged in redirect them to profile page
			$this -> addFlashMessage('you are already logged in');
			return redirect('admin/home');
		}

		if ($this -> form_validation -> run() == false)
		{
			// Load the view
			$this -> template -> write_view('sheet', 'user/auth/login', $data);
		}
		else
		{

			$postdata = $this -> input -> post();
			$data = array(
					'data' => $postdata,
					'fb_data' => $fb_data,
					'form' => $form
			);
			if ($userM -> login($data, $this -> session))
			{
				$this -> addFlashMessage('You have been logged in');
				return redirect('');
			}
			else
			{
				// login failed add flash message
				$this -> template -> write_view('sheet', 'user/auth/login', $data);
			}

		}

		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}
	
	

	function change_user_password($userID)
	{
		$user = $this -> model -> getObject('user', $userID, null, false, array('state' => 'Active'), '', '') -> dm_object;

		$this -> template -> write('page_header', 'Dreamitize - Please login');
		$this -> load -> helper('password_helper');

		$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/user/forms', 'change_password');
		$form = $this -> xml_forms -> createform($formdata['xml'], null, $formdropdowns = null, array('nosheet' => 1));

		$postdata = $this -> input -> post();
		$data = array(
				'data' => $postdata,
				'user' => $user,
				'form' => $form
		);

		if ($this -> form_validation -> run() == TRUE)
		{
			
			if ($postdata['new_password'] != $postdata['new_password2'])
			{
				$data['errors'] = 'The two new passwords did not match.';
				$this -> template -> write_view('sheet', 'user/auth/change_password', $data);
			}
			else
			if ($this -> model -> change_user_password($postdata, $user, $this -> session))
			{
				$this -> addFlashMessage('You have changed the user\'s password.');
				$this -> model -> removeCachedObject('user', $userID, null, false, array('state' => 'Active'), '', '');
				$this -> model -> removeCachedObject('user', $userID, null, false, array(), '', '');
				return redirect('admin/user/profile/display/');
			}
			else
			{
				// login failed add flash message
				$this -> template -> write_view('sheet', 'user/auth/change_password', $data);
			}
		}
		else
		{
			$this -> template -> write_view('sheet', 'user/auth/change_password', $data);
		}

		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);

	}

	public function facebook_return()
	{

		$this -> load -> library('session');
		$this -> load -> model('facebook_model');
		$this -> template -> write_view('content', 'user/facebook_return', $data);
		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}

	/**
	 * Logout user
	 *
	 * @return void
	 */
	function logout()
	{

		$this -> tank_auth -> logout();
		//$this -> _show_message($this -> lang -> line('auth_message_logged_out'));
		redirect();
	}

	/**
	 * Send activation email again, to the same or new email address
	 *
	 * @return void
	 */
	public function send_again()
	{
		if (!$this -> loggedIn)
		{
			$this -> _show_message('You must be logged in');
		}
		else
		{

			$formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/user/forms', 'send_again');
			$form = $this -> xml_forms -> createform($formdata['xml'], null, $formdropdowns = null, array());

			$data['errors'] = array();

			if ($this -> form_validation -> run())
			{
				// validation ok
				if (!is_null($data = $this -> tank_auth -> change_email($this -> form_validation -> set_value('email'), $this -> session_user)))
				{
					// success

					$data['site_name'] = $this -> config -> item('website_name', 'tank_auth');
					$data['activation_period'] = $this -> config -> item('email_activation_expire', 'tank_auth') / 3600;

					$this -> _send_email('activate', $data['email'], $data);

					$this -> _show_message(sprintf($this -> lang -> line('auth_message_activation_email_sent'), $data['email']));

				}
				else
				{
					$errors = $this -> tank_auth -> get_error_message();
					foreach ($errors as $k => $v)
						$data['errors'][$k] = $this -> lang -> line($v);
				}
			}
			$this -> template -> write_view('content', 'user/auth/send_again_form', array('form' => $form));
			$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
		}
	}

	/**
	 * Activate user account.
	 * User is verified by user_id and authentication code in the URL.
	 * Can be called by clicking on link in mail.
	 *
	 * @return void
	 */
	function activate()
	{
		$user_id = $this -> uri -> segment(4);
		$new_email_key = $this -> uri -> segment(5);
		// Activate user
		if ($this -> tank_auth -> activate_user($user_id, $new_email_key))
		{
			// success

			$this -> tank_auth -> logout();
			$this -> _show_message($this -> lang -> line('auth_message_activation_completed') . ' ' . anchor('user/auth/index', 'Login'));

		}
		else
		{
			// fail

			$this -> _show_message($this -> lang -> line('auth_message_activation_failed'));
		}
	}

	/**
	 * Generate reset code (to change password) and send it to user
	 *
	 * @return void
	 */
	function forgot_password()
	{

		if ($this -> loggedIn)
		{
			// logged in
			redirect('admin/user/profile/index');

		}
		elseif ($this -> tank_auth -> is_logged_in($this -> user, null, $this -> cache))
		{
			// logged in, not activated
			redirect('admin/user/auth/send_again/');

		}
		else
		{
			$this -> form_validation -> set_rules('login', 'Email or login', 'trim|required|xss_clean');

			$data['errors'] = array();

			if ($this -> form_validation -> run())
			{
				// validation ok
				if (!is_null($data = $this -> tank_auth -> forgot_password($this -> form_validation -> set_value('login'))))
				{

					$data['site_name'] = $this -> config -> item('website_name', 'tank_auth');
					// Send email with password activation link
					$this -> _send_email('forgot_password', $data['email'], $data);
					$this -> _show_message($this -> lang -> line('auth_message_new_password_sent'));
				}
				else
				{
					$errors = $this -> tank_auth -> get_error_message();
					foreach ($errors as $k => $v)
						$data['errors'][$k] = $this -> lang -> line($v);
				}
			}

			$this -> template -> write_view('content', 'user/auth/forgot_password_form', $data);
			$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);

		}
	}

	/**
	 * Replace user password (forgotten) with a new one (set by user).
	 * User is verified by user_id and authentication code in the URL.
	 * Can be called by clicking on link in mail.
	 *
	 * @return void
	 */
	function reset_password()
	{

		$user_id = $this -> uri -> segment(4);
		$new_pass_key = $this -> uri -> segment(5);

		$this -> form_validation -> set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length[' . $this -> config -> item('password_min_length', 'tank_auth') . ']|max_length[' . $this -> config -> item('password_max_length', 'tank_auth') . ']|alpha_dash');
		$this -> form_validation -> set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');

		$data['errors'] = array();

		if ($this -> form_validation -> run())
		{
			// validation ok
			if (!is_null($data = $this -> tank_auth -> reset_password($user_id, $new_pass_key, $this -> form_validation -> set_value('new_password'))))
			{
				// success

				$data['site_name'] = $this -> config -> item('website_name', 'tank_auth');

				// Send email with new password
				$this -> _send_email('reset_password', $data['email'], $data);

				$this -> _show_message($this -> lang -> line('auth_message_new_password_activated') . ' ' . anchor('/auth/login/', 'Login'));

			}
			else
			{
				// fail
				$this -> _show_message($this -> lang -> line('auth_message_new_password_failed'));
			}
		}
		else
		{

			// Try to activate user by password key (if not activated yet)
			if ($this -> config -> item('email_activation', 'tank_auth'))
			{
				$this -> tank_auth -> activate_user($user_id, $new_pass_key, FALSE);
			}

			if (!$this -> tank_auth -> can_reset_password($user_id, $new_pass_key))
			{
				$this -> _show_message($this -> lang -> line('auth_message_new_password_failed'));
			}
		}
		$this -> load -> view('/auth/reset_password_form', $data);
	}

	/**
	 * Change user password
	 *
	 * @return void
	 */
	function change_password()
	{
		if (!$this -> tank_auth -> is_logged_in())
		{
			// not logged in or not activated
			redirect('/admin/auth/login/');

		}
		else
		{
			$this -> form_validation -> set_rules('old_password', 'Old Password', 'trim|required|xss_clean');
			$this -> form_validation -> set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length[' . $this -> config -> item('password_min_length', 'tank_auth') . ']|max_length[' . $this -> config -> item('password_max_length', 'tank_auth') . ']|alpha_dash');
			$this -> form_validation -> set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');

			$data['errors'] = array();

			if ($this -> form_validation -> run())
			{
				// validation ok
				if ($this -> tank_auth -> change_password($this -> form_validation -> set_value('old_password'), $this -> form_validation -> set_value('new_password')))
				{
					// success
					$this -> _show_message($this -> lang -> line('auth_message_password_changed'));

				}
				else
				{
					// fail
					$errors = $this -> tank_auth -> get_error_message();
					foreach ($errors as $k => $v)
						$data['errors'][$k] = $this -> lang -> line($v);
				}
			}
			$this -> load -> view('auth/change_password_form', $data);
		}
	}

	/**
	 * Change user email
	 *
	 * @return void
	 */
	function change_email()
	{
		if (!$this -> tank_auth -> is_logged_in())
		{
			// not logged in or not activated
			redirect('/auth/login/');

		}
		else
		{
			$this -> form_validation -> set_rules('password', 'Password', 'trim|required|xss_clean');
			$this -> form_validation -> set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');

			$data['errors'] = array();

			if ($this -> form_validation -> run())
			{
				// validation ok
				if (!is_null($data = $this -> tank_auth -> set_new_email($this -> form_validation -> set_value('email'), $this -> form_validation -> set_value('password'))))
				{
					// success

					$data['site_name'] = $this -> config -> item('website_name', 'tank_auth');

					// Send email with new email address and its activation link
					$this -> _send_email('change_email', $data['new_email'], $data);

					$this -> _show_message(sprintf($this -> lang -> line('auth_message_new_email_sent'), $data['new_email']));

				}
				else
				{
					$errors = $this -> tank_auth -> get_error_message();
					foreach ($errors as $k => $v)
						$data['errors'][$k] = $this -> lang -> line($v);
				}
			}
			$this -> load -> view('auth/login/change_email_form', $data);
		}
	}

	/**
	 * Replace user email with a new one.
	 * User is verified by user_id and authentication code in the URL.
	 * Can be called by clicking on link in mail.
	 *
	 * @return void
	 */
	function reset_email()
	{
		$user_id = $this -> uri -> segment(3);
		$new_email_key = $this -> uri -> segment(4);

		// Reset email
		if ($this -> tank_auth -> activate_new_email($user_id, $new_email_key))
		{
			// success
			$this -> tank_auth -> logout();
			$this -> _show_message($this -> lang -> line('auth_message_new_email_activated') . ' ' . anchor('/auth/login/', 'Login'));

		}
		else
		{
			// fail
			$this -> _show_message($this -> lang -> line('auth_message_new_email_failed'));
		}
	}

	/**
	 * Delete user from the site (only when user is logged in)
	 *
	 * @return void
	 */
	function unregister()
	{
		if (!$this -> tank_auth -> is_logged_in())
		{
			// not logged in or not activated
			redirect('/admin/auth/login/');

		}
		else
		{
			$this -> form_validation -> set_rules('password', 'Password', 'trim|required|xss_clean');

			$data['errors'] = array();

			if ($this -> form_validation -> run())
			{
				// validation ok
				if ($this -> tank_auth -> delete_user($this -> form_validation -> set_value('password')))
				{
					// success
					$this -> _show_message($this -> lang -> line('auth_message_unregistered'));

				}
				else
				{
					// fail
					$errors = $this -> tank_auth -> get_error_message();
					foreach ($errors as $k => $v)
						$data['errors'][$k] = $this -> lang -> line($v);
				}
			}
			$this -> load -> view('auth/login/unregister_form', $data);
		}
	}

	/**
	 * Create CAPTCHA image to verify user as a human
	 *
	 * @return	string
	 */
	private function _create_captcha()
	{
		$this -> load -> helper('captcha');

		$cap = create_captcha(array(
				'img_path' => './' . $this -> config -> item('captcha_path', 'tank_auth'),
				'img_url' => base_url() . $this -> config -> item('captcha_path', 'tank_auth'),
				'font_path' => './' . $this -> config -> item('captcha_fonts_path', 'tank_auth'),
				'font_size' => $this -> config -> item('captcha_font_size', 'tank_auth'),
				'img_width' => $this -> config -> item('captcha_width', 'tank_auth'),
				'img_height' => $this -> config -> item('captcha_height', 'tank_auth'),
				'show_grid' => $this -> config -> item('captcha_grid', 'tank_auth'),
				'expiration' => $this -> config -> item('captcha_expire', 'tank_auth'),
		));

		// Save captcha params in session
		$this -> session -> set_flashdata(array(
				'captcha_word' => $cap['word'],
				'captcha_time' => $cap['time'],
		));

		return $cap['image'];
	}

	/**
	 * Callback function. Check if CAPTCHA test is passed.
	 *
	 * @param	string
	 * @return	bool
	 */
	private function _check_captcha($code)
	{
		$time = $this -> session -> flashdata('captcha_time');
		$word = $this -> session -> flashdata('captcha_word');

		list($usec, $sec) = explode(" ", microtime());
		$now = ((float)$usec + (float)$sec);

		if ($now - $time > $this -> config -> item('captcha_expire', 'tank_auth'))
		{
			$this -> form_validation -> set_message('_check_captcha', $this -> lang -> line('auth_captcha_expired'));
			return FALSE;

		}
		elseif (($this -> config -> item('captcha_case_sensitive', 'tank_auth') AND $code != $word) OR strtolower($code) != strtolower($word))
		{
			$this -> form_validation -> set_message('_check_captcha', $this -> lang -> line('auth_incorrect_captcha'));
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * Create reCAPTCHA JS and non-JS HTML to verify user as a human
	 *
	 * @return	string
	 */
	private function _create_recaptcha()
	{
		$this -> load -> helper('recaptcha');

		// Add custom theme so we can get only image
		$options = "<script>var RecaptchaOptions = {theme: 'custom', custom_theme_widget: 'recaptcha_widget'};</script>\n";

		// Get reCAPTCHA JS and non-JS HTML
		$html = recaptcha_get_html($this -> config -> item('recaptcha_public_key', 'tank_auth'));

		return $options . $html;
	}

	/**
	 * Callback function. Check if reCAPTCHA test is passed.
	 *
	 * @return	bool
	 */
	private function _check_recaptcha()
	{
		$this -> load -> helper('recaptcha');

		$resp = recaptcha_check_answer($this -> config -> item('recaptcha_private_key', 'tank_auth'), $_SERVER['REMOTE_ADDR'], $_POST['recaptcha_challenge_field'], $_POST['recaptcha_response_field']);

		if (!$resp -> is_valid)
		{
			$this -> form_validation -> set_message('_check_recaptcha', $this -> lang -> line('auth_incorrect_captcha'));
			return FALSE;
		}
		return TRUE;
	}

} //end class
