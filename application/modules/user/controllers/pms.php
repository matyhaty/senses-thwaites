<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PMS extends Base_Controller 
{	
	// Default, AJAX isn't used.
	var $is_ajax = false;
	
	/**
	 * Constructor
	 *
	*/
	function __construct()
	{			
		if(!isLoggedIn($this->session)) redirect("/login"); // redirect the user to the login page if not already logged in.
	
		// Load libraries, models, helpers etc.
		$this->load->library('form_validation');
		
		// Return all results as json format.
		if(isset($_POST['is_ajax']) && $this->input->post("is_ajax") == 1) $this->is_ajax = true;
		
		parent::__construct('User');			
	}
	
	/**
	 * Grab messages (thread view)
	 *
	*/
	public function message()
	{
		$data = array();
		$id = $this->uri->segment(2);
		$to_user = 0;		
		
		$messages = new Message();
		$messages->where("id", $id);
		$messages->where_in("state", array("active", "sent"))->get();
		
		// Set profile data and render profile head
		$profile = array(
			"id"			=> $this->user->id,
			"fullname"  	=> $this->user->firstname . " " . $this->user->lastname,
			"firstname" 	=> $this->user->firstname,
			"lastname"		=> $this->user->lastname,
			"bio"			=> $this->user->bio			
		);		
		
		
		
		// Loop through all messages
		foreach($messages as $message)
		{
			$class = ($message->created_by == $this->user->id ? "sent-message" : "received-message");
			
			$data[] = array(
				'id' => $message->id,
				'created_by' => $message->created_by,
				'created' => $message->created,
				'subject' => $message->subject,
				'message' => $message->message,
				'class'	  => $class
			);
			
			// Set the to_user var
			if($to_user == 0 && $this->user->id != $message->created_by) $to_user = $message->created_by;
		}
		
		// Now get each reply
		$threads = new Message();		
		$threads->where("parent", $id);
		$threads->where("state", "active")->get();
		
		foreach($threads as $thread)
		{			
			$class = ($thread->created_by == $this->user->id ? "sent-message" : "received-message");
			
			$data[] = array(
				'id' => $thread->id,
				'created_by' => $thread->created_by,
				'created' => $thread->created,
				'subject' => $thread->subject,
				'message' => $thread->message,
				'class'   => $class
			);	

			// Set the to_user var
			if($to_user == 0 && $this->user->id != $thread->created_by) $to_user = $thread->created_by;			
		}

		// Last resort...
		if($to_user == 0) $to_user = $message->userid_to;

		if($this->is_ajax == false) 
		{
			// if its a post, we should search the DB
			$this->template->write('page_header', 'Message(s)');		
			
			// New method to construct profile page
			$this->template->write_view('content', 'user/pms/thread', array("profile" => $profile, "messages" => $data, "msg" => $id, "to_user" => $to_user));
			
			// Render template
			$this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);			
		}		
	}
	
	/**
	 * Displays the users inbox
	 *
	*/	
	public function inbox()
	{
		$user = new User();
		
		// Get the users messages
		$user->where("id", $this->user->id);
		$user->get();
		
		// Get the messages
		$_messages = new Message();
		$_messages->where("state", "active");
		$_messages->where('userid_to', $this->user->id)->get();
	
		// Array to hold messages
		$messages = array();
		
		foreach($_messages as $message)
		{
			$messages[] = array(
				"id" => $message->id,
				"subject" => $message->subject, 
				"content" => $message->message, 
				"created_by" => $message->created_by, 
				"date" => $message->created,
				"parent" => $message->parent
			);
		}
		
		// Set profile data and render profile head
		$profile = array(
			"id"			=> $this->user->id,
			"fullname"  	=> $this->user->firstname . " " . $this->user->lastname,
			"firstname" 	=> $this->user->firstname,
			"lastname"		=> $this->user->lastname,
			"bio"			=> $this->user->bio			
		);		
		
		if($this->is_ajax == false) 
		{
			// if its a post, we should search the DB
			$this->template->write('page_header', 'Inbox');		
			
			// New method to construct profile page
			$this->template->write_view('content', 'user/pms/inbox', array("inboxcnt" => count($messages), "messages" => $messages, "profile" => $profile));
			
			// Render template
			$this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);			
		}
		
		else if($this->is_ajax == true)	return json_encode($messages);
	}
	
	/**
	 * Displays the users sent box
	 *
	*/
	public function sent()
	{
		$user = new User();
		
		// Get the users messages
		$user->where("id", $this->user->id);
		$user->where("state", "active");			
		$user->get();
		
		// Get the messages		
		$user->message->where("state", "sent");
		$user->message->where("created_by", $this->user->id);
		$user->message->get();
		
		$messages = array();
		
		foreach($user->message as $message)
		{
			$messages[] = array(
				"id" => $message->id,
				"subject" => $message->subject, 
				"content" => $message->message, 
				"created_by" => $message->created_by, 
				"date" => $message->created,
				"parent" => $message->parent
			);
		}
		
		// Set profile data and render profile head
		$profile = array(
			"id"			=> $this->user->id,
			"fullname"  	=> $this->user->firstname . " " . $this->user->lastname,
			"firstname" 	=> $this->user->firstname,
			"lastname"		=> $this->user->lastname,
			"bio"			=> $this->user->bio			
		);		
		
		if($this->is_ajax == false) 
		{
			// if its a post, we should search the DB
			$this->template->write('page_header', 'Sent');		
			
			// New method to construct profile page
			$this->template->write_view('content', 'user/pms/sent', array("messages" => $messages, "profile" => $profile));
			
			// Render template
			$this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);			
		}
		
		else if($this->is_ajax == true)	return json_encode($messages);	
	}
	
	/**
	 * Displays the users draft messages
	 *
	*/
	public function draft()
	{
		$user = new User();
		
		// Get the users messages
		$user->where("id", $this->user->id);
		$user->where("state", "active");			
		$user->get();
		
		// Get the messages		
		$user->message->where("state", "draft");
		$user->message->where("created_by", $this->user->id);
		$user->message->get();
		
		$messages = array();
		
		foreach($user->message as $message)
		{
			$messages[] = array(
				"id" => $message->id,
				"subject" => $message->subject, 
				"content" => $message->message, 
				"created_by" => $message->created_by, 
				"date" => $message->created
			);
		}
		
		// Set profile data and render profile head
		$profile = array(
			"id"			=> $this->user->id,
			"fullname"  	=> $this->user->firstname . " " . $this->user->lastname,
			"firstname" 	=> $this->user->firstname,
			"lastname"		=> $this->user->lastname,
			"bio"			=> $this->user->bio			
		);		
		
		if($this->is_ajax == false) 
		{
			// if its a post, we should search the DB
			$this->template->write('page_header', 'Draft Messages');		
			
			// New method to construct profile page
			$this->template->write_view('content', 'user/pms/draft', array("messages" => $messages, "profile" => $profile));
			
			// Render template
			$this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);			
		}
		
		else if($this->is_ajax == true)	return json_encode($messages);	
	}	
	
	/**
	 * Send the message
	 *
	*/
	public function send()
	{
		// Post data
		$post = $this->input->post();
				
		// Set some rules for sending the message, basic validation should be message and to_user is set
		// Next part of the validation should be to check whether the user is allowed to send the message
		$rules = array (
			array(
				"field" => "msg",
				"label" => "Message",
				"rules" => "required"
			),
			array(
				"field" => "to_user",
				"label" => "To",
				"rules" => "required|integer"
			)			
		);
		
		// Set the rules 
		$this->form_validation->set_rules($rules);		
		
		// If validation passes...
		if($this->form_validation->run() == TRUE)
		{
			// Send the message
			$sent_user = new User($this->user->id);
			
			$message = new Message($this->input->post("msg_id"));
			
			$message->userid_to = $this->input->post("to_user");
			$message->message = $this->input->post("msg");
			$message->subject = $this->input->post("subject");
			$message->created = time(); // current timestamp
			$message->updated = time(); // current timestamp
			
			// Set the parent
			if(isset($_POST['parent'])) $message->parent = $this->input->post("parent");
			
			// Handle sent/draft messages
			if(isset($_POST['send']))
			{	
				$message->state = "sent";		// set to active
				$message->userid_to = null;				
			}
			
			else $message->state = "draft";		// set to draft
			
			// Save the message(s) - inbox
			$message->save($sent_user);
			
			// Create the sent message if state is active
			if($message->state == "sent")
			{
				// Save the message details
				$message2 = new Message();
				$user = new User($this->input->post("to_user"));
				
				$message2->message = $this->input->post("msg");
				$message2->subject = $this->input->post("subject");
				$message2->updated = time(); // current timestamp
				$message2->state = "active"; // set to active			
				$message2->userid_to = $this->input->post("to_user");
				$message2->parent = $message->parent;				
				
				// Save the message(s) - sent
				$message2->save($user);
				
				// Redirect back to the sent box
				redirect("/sent");				
			}
			
			redirect("/draft");	
		}
	
		else
		{
			// couldn't send message
			if($this->is_ajax == false)	die("Unable to send message");
			if($this->is_ajax == true)	echo json_encode(array("success" => "0", "msg" => "Unable to send message"));
		}
		
	}
	
	/**
	 * Delete a message (only if the user 'owns' the message
	 *
	*/
	public function delete($msg_id = null)
	{
		$message = new Message($msg_id);
				
		// Validation
		if($msg_id == null) redirect("/inbox"); // redirect back to inbox if the msg_id is blank				
		//if($message->created_by == null || $message->created_by == "") redirect("/inbox");
		//if($message->created_by != $this->user->id && ($message->userid_to != null && $message->userid_to != $message->userid_to)) redirect("/inbox");
		
		// Delete inbox / sent messages
		if($message->created_by == $this->user->id || $message->userid_to == $this->user->id)
		{
			$message->state = "deleted";
			$message->save();
		}
		
		// Redirect anyway ...
		redirect("/inbox");
		
		
	}
	
	/**
	 * Compose a message view
	 *
	*/
	public function compose()
	{	
		$_message = array("to_user" => "", "subject" => "", "message" => "");
		
		$msg = $this->uri->segment(2);
		$uid = $this->uri->segment(3);
		
		// Handle editing draft messages
		if($msg != null)
		{
			$message = new Message($msg);
			
			// Redirect the user to the message stream if the message is not set as draft
			if($message->state != "draft") redirect("/inbox");
			
			$_message['to_user'] = $message->userid_to;
			$_message['subject'] = $message->subject;
			$_message['message'] = $message->message;
		}
	
		// Set profile data and render profile head
		$profile = array(
			"id"			=> $this->user->id,
			"fullname"  	=> $this->user->firstname . " " . $this->user->lastname,
			"firstname" 	=> $this->user->firstname,
			"lastname"		=> $this->user->lastname,
			"bio"			=> $this->user->bio			
		);
		
		if($this->is_ajax == false) 
		{
			// if its a post, we should search the DB
			$this->template->write('page_header', 'Compose Message');		
			
			// New method to construct profile page
			$this->template->write_view('content', 'user/pms/compose', array("profile" => $profile, "message" => $_message, "msg" => $msg));
			
			// Render template
			$this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);			
		}
		
		else if($this->is_ajax == true)	return json_encode($messages);	
	}
}

