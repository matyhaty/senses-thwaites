<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Friendships extends Base_Controller {
	
	public $friendship =null;
	
	function __construct()
	{
		$this->friendship = new friendship();
		parent::__construct();
	}	
	
	/**
	 * Can probably show a list of friends
	 *
	 */
	public function index()
	{	
		$this->load->library('session');
		$this->load->helper('url');	
		/**
		 * @todo put into a helper 
		 */
		if(   !$this->session->userdata('username') ||  !$this->session->userdata('email')  ){			
			return redirect('admin/users/account/index');
		}
		
		
		
		$this->template->write_view('content', 'users/friendships', array(  ) );
		$this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);				
		
		
	}
	
	/**
	 * method for adding friend
	 *
	 */
	public function add()
	{
		
		$this->load->helper('url');	
		$this->load->library('session');		
		$segments					= $this->uri->uri_to_assoc();
		$friend_userid				= $segments['add'];
		$userid						= $this->session->userdata('userid');		
		// segments issues, need fix
		
		$friendship = $this->friendship;
		
		$friendship->userid 		= $userid;
		$friendship->friend_userid	= $friend_userid;
		$friendship->save();
				
		
	}
	
	public function remove()
	{
		$this->load->helper('url');	
		$this->load->library('session');		
		$segments					= $this->uri->uri_to_assoc();
		$friend_userid				= $segments['remove'];
		$userid						= $this->session->userdata('userid');		
		// segments issues, need fix
		
		$friendship = $this->friendship ;
		$friendship->where('userid',$userid);
		$friendship->where('friend_userid',$friend_userid);
		// set sql up
		
		if ($friendship->get()->delete()){
			// get then delete the friendship
			redirect('admin/users/friendships/index');		
		}else{
			/**
			 * @todo set flash message
			 */
			redirect('admin/users/friendships/index');
		}		
		
		
	}
} //end class