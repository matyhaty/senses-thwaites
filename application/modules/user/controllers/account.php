<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Account extends Base_Controller
{

    /**
     * Constructer
     *
     */
    function __construct()
    {
        parent::__construct('User');
    }

    public function index()
    {
        // Load the view
        $this -> template -> write_view('sheet', 'user/account_index', array());
        $this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
    }

    public function edit()
    {
        // Load the email helper
        $this -> load -> helper('email');
        $this -> load -> helper('url');
        $this -> load -> helper('password_helper');
        $this -> load -> helper(array(
            'form',
            'url'
        ));
        $this -> load -> library('form_validation');
        $this -> load -> library('session');

        $formdata = $this -> xml_forms -> loadXML(APPPATH . 'modules/user/forms', 'account');

        if (!$this -> user -> id)
        {

            //user is already logged in redirect them to profile page
            // add Flash Message?!
            return redirect('/admin/login');
        }

        // Get the username and password

        if ($this -> input -> post())
        {

            if (1)
            {

                $data = $this -> input -> post();

                if (!empty($data['password']))
                {

                    $salt = genRandomPassword(32);
                    $plainpassword = $data['password'];
                    $data['password'] = getCryptedPassword($plainpassword, $salt);
                    // safed up pass!
                }
                else
                {
                    unset($data['password']);
                }
                $cansave = true;
                $form = $this -> xml_forms -> createform($formdata['xml'], null, $formdropdowns = null, array());
                if ($userFound = $this -> model -> checkAcounts($data['email'], $data['username']))
                {
                    if (in_array($data['email'], $userFound) && $data['email'] != $this -> user -> email)
                    {
                        $this -> template -> write_view('flash_message', 'common/flash_message', array('message' => 'Email is already in use'));
                        $cansave = false;
                    }
                    elseif (in_array($data['username'], $userFound) && $data['username'] != $this -> user -> username)
                    {
                        $this -> template -> write_view('flash_message', 'common/flash_message', array('message' => 'Username is already in use'));
                        $cansave = false;
                    }
                    if ($cansave == false)
                    {
                        $this -> template -> write_view('content', 'user/account', array('form' => $form));
                        $this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
                        return;
                    }
                }

                if ($_FILES['photo']['error'] == 0)
                {
                    $file = time() . $_FILES['photo']['name'];
                    if (move_uploaded_file($_FILES['photo']['tmp_name'], l('_D_USERS_PROFILE_PHOTO') . $file) !== false)
                    {
                        $data['photo'] = $file;
                    }
                    else
                    {
                        die ;
                    }
                }
                else
                {

                }

                if ($this -> user -> saveUser($data, $this -> user))
                {
                    $this -> user -> removeUserCache($this -> user);
                    return redirect('admin/user/profile/index');
                    // profile save redirect back to profile
                }
                else
                {
                    die ;
                }

            }
        }

        // Load the view
        $this -> user -> password = '';
        $form = $this -> xml_forms -> createform($formdata['xml'], $this -> user, $formdropdowns = null, array('nosheet' => 1));
        $this -> template -> write_view('sheet', 'user/account', array('form' => $form));
        $this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);

    }

}//end class
