<?php

/**
 * Validation Set Up
 */

$config = array(
                 'auth/index' => array(
                                    array(
                                            'field' => 'username',
                                            'label' => 'Username',
                                            'rules' => 'required'
                                         ),                    
                                    array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'required'
                                         ),

                                    ),
                 'register/index' => array(
                                    array(
                                            'field' => 'username',
                                            'label' => 'Username',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'firstname',
                                            'label' => 'First name',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'lastname',
                                            'label' => 'Last name',
                                            'rules' => 'required'
                                         ),                                         
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email',
                                            'rules' => 'required'
                                         )
                                    ),                                    
               );