<?php
/**
 * User Class
 *
 */
class Librarysection extends Base_Model
{	
    public $table = 'librarysections';
    public $has_one = array(
    );
    public $has_many = array(
        'libraryitem'
    );

	
	
	/**
	 * Cons
	 *
	 * @param Int $id
	 */
	function __construct($id = null)
	{
		parent::__construct($id);
	}

	
}

/* End of file user.php */
/* Location: ./application/models/user.php */
