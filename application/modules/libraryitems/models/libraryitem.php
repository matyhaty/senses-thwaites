<?php
/**
 * User Class
 *
 */
class Libraryitem extends Base_Model
{	
    public $table = 'libraryitems';
    public $has_one = array(
    );
    public $has_many = array(
        'librarysection'
    );

	
	
	/**
	 * Cons
	 *
	 * @param Int $id
	 */
	function __construct($id = null)
	{
		parent::__construct($id);
	}

	
}

/* End of file user.php */
/* Location: ./application/models/user.php */
