<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Designmenu extends Base_Controller
{

	function __construct()
	{
		parent::__construct();
		//$theme				= 'home';
		//$this->template->set_master_template( $theme .'/template' );

	}

	public function index()
	{
		$this -> setTemplate('builder');

		$this -> data = '';
	
		$this -> template -> write_view('content', 'designmenu/designmenu', $this -> data);

		$this -> template -> render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}


	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
