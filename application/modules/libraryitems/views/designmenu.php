
<!-- FORMS
		================================================== -->

<div class="row">
	<div class="span24">
		<div class="foozone-logo"><img src="assets/frontend/img/my_menus.png" alt="The Food Zone"></div>
	</div>
</div>
<div class="row">
	<div class="span24">
		<div class="form-container">
			<div class="row">
				<div class="span10">
					<div class="left-content">
						<h1>Starters</h1>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
						teger elit odio, fermentum quis portti tor quis, rutrum nec
						erat. Aliq uam ut nulla in libero rhoncus mattis.</div>
				</div>
				<div class="span14">
					<div class="right-content">
						<div class="controls controls-row">
							<input class="span9 starter_autocomplete" type="text" placeholder="Enter title here..." name='starter_1_title' />
							<input class="span3" type="text" placeholder="Add Price..." name='starter_1_price' />
							<div class="span1"><a href="#"><img src="assets/frontend/img/menu.gif" alt="Menu"></a></div>
							
						</div>
						<div class="controls controls-row">
							<input class="span9 starter_autocomplete" type="text" placeholder="Enter title here..."  name='starter_2_title' />
							<input class="span3" type="text" placeholder="Add Price..."  name='starter_2_price' />
							<div class="span1"><a href="#"><img src="assets/frontend/img/menu.gif" alt="Menu"></a></div>
							
						</div>
						<div class="controls controls-row">
							<input class="span9 starter_autocomplete" type="text" placeholder="Enter title here..." name='starter_3_title' />
							<input class="span3" type="text" placeholder="Add Price..." name='starter_3_price' />
							<div class="span1"><a href="#"><img src="assets/frontend/img/menu.gif" alt="Menu"></a></div>
						
						</div>
						<div class="controls controls-row">
							<input class="span9 starter_autocomplete" type="text" placeholder="Enter title here..." name='starter_4_title' />
							<input class="span3" type="text" placeholder="Add Price..." name='starter_4_price' />
							<div class="span1"><a href="#"><img src="assets/frontend/img/menu.gif" alt="Menu"></a></div>
						</div>
						
						<div class='moreRowsStarter'>
							<div class='starterBlankSpace'></div>
						</div>
						
						<div class="controls controls-row">
							<button type="button" class="btn addRowStarter">Add Another Starter</button>
						</div>
						
						
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="span24">
		<div class="form-chain"></div>
	</div>
</div>

<div class="row">
	<div class="span24">
		<div class="form-container">
			<div class="row">
				<div class="span10">
					<div class="left-content">
						<h1>Mains</h1>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
						teger elit odio, fermentum quis portti tor quis, rutrum nec
						erat. Aliq uam ut nulla in libero rhoncus mattis.</div>
				</div>
				<div class="span14">
					<div class="right-content">
						<div class="controls controls-row">
							<input class="span9 main_autocomplete" type="text" placeholder="Enter title here..." name='main_1_title' />
							<input class="span3" type="text" placeholder="Add Price..." name='main_1_price' />
							<div class="span1"><a href="#"><img src="assets/frontend/img/menu.gif" alt="Menu"></a></div>
							
						</div>
						<div class="controls controls-row">
							<input class="span9 main_autocomplete" type="text" placeholder="Enter title here..."  name='main_2_title' />
							<input class="span3" type="text" placeholder="Add Price..."  name='main_2_price' />
							<div class="span1"><a href="#"><img src="assets/frontend/img/menu.gif" alt="Menu"></a></div>
							
						</div>
						<div class="controls controls-row">
							<input class="span9 main_autocomplete" type="text" placeholder="Enter title here..." name='main_3_title' />
							<input class="span3" type="text" placeholder="Add Price..." name='main_3_price' />
							<div class="span1"><a href="#"><img src="assets/frontend/img/menu.gif" alt="Menu"></a></div>
						
						</div>
						<div class="controls controls-row">
							<input class="span9 main_autocomplete" type="text" placeholder="Enter title here..." name='main_4_title' />
							<input class="span3" type="text" placeholder="Add Price..." name='main_4_price' />
							<div class="span1"><a href="#"><img src="assets/frontend/img/menu.gif" alt="Menu"></a></div>
						</div>
						
						<div class='moreRowsMain'>
							<div class='mainBlankSpace'></div>
						</div>
						
						
						<div class="controls controls-row">
							<button type="button" class="btn addRowMain">Add Another Main</button>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="span24">
		<div class="form-chain"></div>
	</div>
</div>

<div class="row">
	<div class="span24">
		<div class="form-container">
			<div class="row">
				<div class="span10">
					<div class="left-content">
						<h1>Desserts</h1>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
						teger elit odio, fermentum quis portti tor quis, rutrum nec
						erat. Aliq uam ut nulla in libero rhoncus mattis.</div>
				</div>
				<div class="span14">
					<div class="right-content">
						<div class="controls controls-row">
							<input class="span9 dessert_autocomplete" type="text" placeholder="Enter title here..." name='dessert_1_title' />
							<input class="span3" type="text" placeholder="Add Price..." name='dessert_1_price' />
							<div class="span1"><a href="#"><img src="assets/frontend/img/menu.gif" alt="Menu"></a></div>
							
						</div>
						<div class="controls controls-row">
							<input class="span9 dessert_autocomplete" type="text" placeholder="Enter title here..."  name='dessert_2_title' />
							<input class="span3" type="text" placeholder="Add Price..."  name='dessert_2_price' />
							<div class="span1"><a href="#"><img src="assets/frontend/img/menu.gif" alt="Menu"></a></div>
							
						</div>
						<div class="controls controls-row">
							<input class="span9 dessert_autocomplete" type="text" placeholder="Enter title here..." name='dessert_3_title' />
							<input class="span3" type="text" placeholder="Add Price..." name='dessert_3_price' />
							<div class="span1"><a href="#"><img src="assets/frontend/img/menu.gif" alt="Menu"></a></div>
						
						</div>
						<div class="controls controls-row">
							<input class="span9 dessert_autocomplete" type="text" placeholder="Enter title here..." name='dessert_4_title' />
							<input class="span3" type="text" placeholder="Add Price..." name='dessert_4_price' />
							<div class="span1"><a href="#"><img src="assets/frontend/img/menu.gif" alt="Menu"></a></div>
						</div>
						
						<div class='moreRowsDessert'>
							<div class='dessertBlankSpace'></div>
						</div>
						
						
						<div class="controls controls-row">
							<button type="button" class="btn addRowDessert">Add Another Dessert</button>
						</div>
						
						<div id='log'></div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="span24">
		<div class="form-chain"></div>
	</div>
</div>



<script type="text/javascript">
    $(document).ready(function() {
    
    	var num = 4;
    	
    	var starterHTML = "\
    				<div class='controls controls-row'>\
    					<input class='span9 starter_autocomplete' type='text' placeholder='Enter title here...' name='starter_"+num+"_title' />\
    					<input class='span3' type='text' placeholder='Add Price...' name='starter_"+num+"_price' />\
    					<div class='span1'><a href='#'><img src='assets/frontend/img/menu.gif' alt='Menu'></a></div>\
    				</div>";
    	
    	
        $(".addRowStarter").click(function() 
        { 
          $('.starterBlankSpace').append(starterHTML);
          num++;
          return false;
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
    
    	var num = 4;
    	
    	var mainHTML = "\
    				<div class='controls controls-row'>\
    					<input class='span9 main_autocomplete' type='text' placeholder='Enter title here...' name='main_"+num+"_title' />\
    					<input class='span3' type='text' placeholder='Add Price...' name='main_"+num+"_price' />\
    					<div class='span1'><a href='#'><img src='assets/frontend/img/menu.gif' alt='Menu'></a></div>\
    				</div>";
    				
    				
        $(".addRowMain").click(function() 
        { 
          $('.mainBlankSpace').append(mainHTML);
          num++;
          return false;
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
    
    	var num = 4;
    	
    	var dessertHTML = "\
    				<div class='controls controls-row'>\
    					<input class='span9 dessert_autocomplete' type='text' placeholder='Enter title here...' name='dessert_"+num+"_title' />\
    					<input class='span3' type='text' placeholder='Add Price...' name='dessert_"+num+"_price' />\
    					<div class='span1'><a href='#'><img src='assets/frontend/img/menu.gif' alt='Menu'></a></div>\
    				</div>";
    				
        $(".addRowDessert").click(function() 
        { 
          $('.dessertBlankSpace').append(dessertHTML);
          num++;
          return false;
        });
    });
</script>

<script>
	$(function() {
		//function log( message ) {
		//	$( "<div>" ).text( message ).prependTo( "#log" );
		//	$( "#log" ).scrollTop( 0 );
		//}
		
		$('.starter_autocomplete').live('keyup.autocomplete', function(){
			$(this).autocomplete({
				source: "<?php echo base_url(); ?>designmenu/autocomplete/starters",
				minLength: 2,
				select: function( event, ui ) {
					//log( ui.item ?
					//"Selected: " + ui.item.value + " aka " + ui.item.id :
					//"Nothing selected, input was " + this.value );
				}
			});
		});
		
		
		$('.main_autocomplete').live('keyup.autocomplete', function(){
			$(this).autocomplete({
				source: "<?php echo base_url(); ?>designmenu/autocomplete/mains",
				minLength: 2,
				select: function( event, ui ) {
					//log( ui.item ?
					//"Selected: " + ui.item.value + " aka " + ui.item.id :
					//"Nothing selected, input was " + this.value );
				}
			});
		});
		
		
		$('.dessert_autocomplete').live('keyup.autocomplete', function(){
			$(this).autocomplete({
				source: "<?php echo base_url(); ?>designmenu/autocomplete/desserts",
				minLength: 2,
				select: function( event, ui ) {
					//log( ui.item ?
					//"Selected: " + ui.item.value + " aka " + ui.item.id :
					//"Nothing selected, input was " + this.value );
				}
			});
		});
		
		
	});
</script>
