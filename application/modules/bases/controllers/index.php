<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends Base_Controller {

	public $model		= null;
	
	/**
	 * All sets in the constructor are required
	 * Also in the same order
	 *
	 */
	function __construct()
	{
		parent::__construct();
		// parent construct sets  up cache, it loads the driver since the model cant!
		$this->model = new Base();	
		// a new DM 
		$this->model->cache = $this->cache;
		// Asign the cache to the DM so it has access to the cache
	}
	
	/**
	 * Example cache use
	 *
	 * @return index
	 */
	public function index()
	{
		$dm = $this->model->getObject(  'user' , 1 )->dm_object;
		
		$messages = $this->model->getObject(  'message' , 1 , $dm )->dm_object; 

		$this->template->write_view('content',  'bases/index' , array( 'user'=>$dm->to_array() , 'messages'=>$messages->all_to_array()  ) );
		$this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);		
		
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
