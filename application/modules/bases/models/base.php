<?php
/**
 * Class *
 */
class Base extends Base_Model {

	/**
	 * Ignore table name here, as its just to make it compatiable 
	 * with data mapper without having to create another new table
	 *
	 * @var String
	 */
	public $table = 'base_models';
	
	
	/**
	 * Constructor
	 *
	 * @param unknown_type $id
	 */
	function __construct($id = NULL)
	{
	parent::__construct($id);
	}	
	
}
