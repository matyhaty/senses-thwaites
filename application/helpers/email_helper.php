<?php
if(!function_exists('email')){
	
	function email( array $email )
	{
		$this->load->library('email');
		
		$this->email->from('your@example.com', 'Your Name');
		$this->email->to($email['email']); 
		//$this->email->cc('another@another-example.com'); 
		//$this->email->bcc('them@their-example.com'); 
		
		$this->email->subject($email['subject']);
		$this->email->message($email['message']);	
		
		$this->email->send();
		
		echo $this->email->print_debugger();		
	}
	
	
}
?>