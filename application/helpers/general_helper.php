<?php

/**
 * Function to count the number of class subscriptions
 *
*/
function count_available_class_subscriptions($id)
{
	if($id == 0) return 0;
	
	$lecture = new Lecture();
	$lecture->where("id", $id)->get();
	$lecture->invoice->get();
	
	return ($lecture->invoice->count() != 0 ? ($lecture->places - $lecture->invoice->count()) : $lecture->places);	
}

/**
 * Function to count the number of available class subscriptions
 *
*/
function count_attending_class_subscriptions($id)
{
	if($id == 0) return 0;
	
	$lecture = new Lecture();
	$lecture->where("id", $id)->get();
	$lecture->invoice->get();
	
	return $lecture->invoice->count();	
}

/**
 *
 *
*/
function appear_in_categories($id = null, $type = null)
{
	if($id == null || $type == null) return "No categories.";
	
	$$type = new $type();
	$$type->where("id", $id)->get();
	$$type->categorys->get();
	
	$sep = "";
	
	foreach($$type->categorys as $obj)
	{
		echo $sep . "<a href='#'>".$obj->title."</a>";
		$sep = ", ";
	}
}