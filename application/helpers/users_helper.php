<?php

/**
 * User helper function for seeing if the user is logged in
 */
if (!function_exists('isLoggedIn'))
{

	function isLoggedIn($session)
	{
		$user = $session -> all_userdata();

		if (!isset($user['user_data']['username']) || !isset($user['user_data']['email']))
			return false;
		return $user['user_data'];

	}

}

/* Function checks whether the user has permission to access a function
 */

function check_permissions(DataMapper $user = NULL, $uri = null)
{
	$CI = &get_instance();

	static $allowedURIS;

	if (isset($allowedURIS[$uri]))
	{
		// this uri has allowed been allowed! cool trick aye
		return true;
	}

	// 0 non registered
	// 1 registered
	// 2 special guest
	// 3 some other account
	// 9 admin account

	$CI -> firephp -> error("No Permission settings for URL: " . $uri);
	
	$perms['admin/login'] = array(
		'user_type' => array(0 => 0),
		'fail_hook' => $failHook = array(),
		'success' => $successHook = array(),
		'logged_in' => 2
	);
	
	$perms['admin/home'] = array(
		'user_type' => array(1 => 1),
		'fail_hook' => $failHook = array(),
		'success' => $successHook = array(),
		'logged_in' => 1
	);

	$perms['admin/user/view'] = array(
		'user_type' => array(1 => 1),
		'fail_hook' => $failHook = array(),
		'success' => $successHook = array(),
		'logged_in' => 1
	);

	$perms['admin/communications/list'] = array(
		'user_type' => array(1 => 1),
		'fail_hook' => $failHook = array(),
		'success' => $successHook = array(),
		'logged_in' => 1
	);

	$perms['designmenu'] = array(
		'user_type' => array(1 => 1),
		'fail_hook' => $failHook = array(),
		'success' => $successHook = array(),
		'logged_in' => 1
	);
    
    $perms['designposter'] = array(
        'user_type' => array(1 => 1),
        'fail_hook' => $failHook = array(),
        'success' => $successHook = array(),
        'logged_in' => 1
    );
    
    $perms['news'] = array(
        'user_type' => array(1 => 1),
        'fail_hook' => $failHook = array(),
        'success' => $successHook = array(),
        'logged_in' => 1
    );
    
    $perms['resources'] = array(
        'user_type' => array(1 => 1),
        'fail_hook' => $failHook = array(),
        'success' => $successHook = array(),
        'logged_in' => 1
    );
    
    $perms['taproom'] = array(
        'user_type' => array(1 => 1),
        'fail_hook' => $failHook = array(),
        'success' => $successHook = array(),
        'logged_in' => 1
    );

	// could probably be moved to its own seperate config file!

	// perm usertype, key and val should always be the the same
	$uri = $uri ? $uri : uri_string(current_url());
	$uriArr = explode('/', $uri, 4);
	unset($uriArr[3]);
	// we are only interested in the first 3 segments
	$uri = implode('/', $uriArr);

	if (isset($perms[$uri]))
	{

		// they must be logged in first
		if ($perms[$uri]['logged_in'] == 1 && !$user -> id)
		{

			return 'must_be_logged_in';

		}

		// they cant be logged in to view this page, useful for the login page for example
		if ($perms[$uri]['logged_in'] == 2 && $user -> id)
		{

			return 'must_be_logged_in_out';
		}

		// check if the current uri has a permission, use isset for speed
		// admin overrules all
		if (isset($perms[$uri]['user_type'][(int)$user -> user_type]) || $user -> user_type == ADMIN_USER_TYPE)
		{

			// looks complicated but quite simple
			// check the $perms array against user_type array against the key of the actual users user_type
			// if it return true the have permission
			// run success hook
			$allowedURIS[$uri] = 1;
			return true;
		}
		else
		{
			//print_r($perms[$uri]);
			//echo '<hr><pre>';
			//print_r( $user -> all_to_array());
			return false;
			//run fail hook
		}// no acces
	}
	else
	{
		// we are only interested in the first 2 segments and wildcards
		unset($uriArr[2]);
		$uriWildcard = implode('/', $uriArr);
		$uriWildcard .= '/any';

		if (isset($perms[$uriWildcard]))
		{
			//$CI = &get_instance();
			//$CI -> firephp -> error("Wildcard match: " . $uriWildcard);
			// they must be logged in first
			if ($perms[$uriWildcard]['logged_in'] == 1 && !$user -> id)
			{

				return 'must_be_logged_in';

			}

			// they cant be logged in to view this page, useful for the login page for example
			if ($perms[$uriWildcard]['logged_in'] == 2 && $user -> id)
			{

				return 'must_be_logged_in_out';
			}

			// check if the current uri has a permission, use isset for speed
			// admin overrules all
			if (isset($perms[$uriWildcard]['user_type'][(int)$user -> user_type]) || $user -> user_type == ADMIN_USER_TYPE)
			{

				// looks complicated but quite simple
				// check the $perms array against user_type array against the key of the actual users user_type
				// if it return true the have permission
				// run success hook
				$allowedURIS[$uriWildcard] = 1;
				return true;
			}
			else
			{
				return false;
				//run fail hook
			}// no acces
		}
		else
		{
			$CI = &get_instance();
			$CI -> firephp -> error("No Permission settings for URL: " . $uri . " or " . $uriWildcard);
			$allowedURIS[$uri] = 1;
			// if not set has no perm association so return true
			//return true;
			
				return true;
			
		}
	}

}

/**
 * All links should use this function to utilise the basic ACL
 *
 * @param DataMapper $user
 * @paramString $uri
 * @param Boolean $returnURI
 * @param String $permURI
 * @return varies
 */
function checkURIPerm(DataMapper $user, $uri, $returnURI = true, $permURI = '')
{

	//if(!$uri) throw new Exception('URI must be sent to this checkURIPerm function');

	$uri = $permURI != '' ? $permURI : $uri;
	// add flexibilty for uri which have ids in them
	//$CI = &get_instance();
	//		$CI -> firephp -> error("not allowed: ".$uri);

	if (check_permissions($user, $uri))
	{
		if ($returnURI)
		{
			return site_url($uri);
		}
		else
		{
			return true;
		}
	}

	if ($returnURI)
	{
		//	return site_url('noaccess');
	}
	else
	{
		return false;
	}

}

function slir($path, $image, $width, $height, array $crop = array(), $attri = '', $uriOnly = false)
{
	$slircode = '';
	$slircode .= 'w' . $width . '-';
	$slircode .= 'h' . $height;

	if (isset($crop['width']) && isset($crop['height']))
		$slircode .= '-c' . $crop['width'] . ':' . $crop['height'];

	$imageRel = $path . $image;
	$imageAbs = FRONT_END_PATH . '/uploads/' . $path . $image;
	$imageAbsBackup = FRONT_END_PATH . '/uploads/' . $path . 'profile-image.png';
	$imageAbsFullback = FRONT_END_PATH . '/uploads/fallback-image.png';
	// the absolute path
	if (file_exists($imageAbs) && $image != "")
	{
		if ($uriOnly)
			return SLIR_URL . $slircode . $imageRel;
		$base = '<img src="' . SLIR_URL . $slircode . '/uploads' . $imageRel . '" ' . $attri . ' />';
	}
	
else
	if (file_exists($imageAbsBackup) && $image != "")
	{
		//echo $imageAbs;
		if ($uriOnly)
			return SLIR_URL . $slircode . $path . 'profile-image.png';
		$base = '<img src="' . SLIR_URL . $slircode . '/uploads' . $path . 'profile-image.png" alt="File not found: ' . $path . $image . '" />';
	}
	else
	{
		if ($uriOnly)
			return SLIR_URL . $slircode . 'fallback-image.png';
		$base = '<img src="' . SLIR_URL . $slircode . '/uploads/fallback-image.png" alt="File not found: ' . $path . $image . '" />';
	}

	return $base;
}

function slirPath($image, $slircode, $data = NULL)
{
	$imageAbs = FRONT_END_PATH . '/uploads/' . $image;
	//echo SLIR_URL . '?' . $slircode . '&i=' . UPLOAD_URL_ . $image;
	if (file_exists($imageAbs) && !is_dir($imageAbs))
	{
		$base = SLIR_URL . '?' . $slircode . '&i=' . UPLOAD_URL . $image;
	}
	else
	{
		$base = SLIR_URL . '?' . $slircode . '&i=' . UPLOAD_URL . 'placeholder.jpg';
	}

	return $base;
}

/**
 * All links should pass through this function
 *
 * @param DataMapper $user
 * @param unknown_type $uri
 * @param unknown_type $text
 * @param unknown_type $attribs
 * @return unknown
 */
function permAnchor(DataMapper $user, $uri, $text, $attribs = array())
{
	return anchor(checkURIPerm($user, $uri), $text, $attribs);
}

/**
 * Truncate string
 *
 * @param String $string
 * @param Int $your_desired_width
 * @return String
 */
function tokenTruncate($string, $your_desired_width)
{
	$parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
	$parts_count = count($parts);

	$length = 0;
	$last_part = 0;
	for (; $last_part < $parts_count; ++$last_part)
	{
		$length += strlen($parts[$last_part]);
		if ($length > $your_desired_width)
		{
			break;
		}
	}

	return implode(array_slice($parts, 0, $last_part));
}

/**
 * Get a language string
 *
 * @param String $langStr
 * @return String
 */
function l($langStr)
{
	static $CI;
	if ($CI)
		return $CI -> lang -> line($langStr);
	$CI = get_instance();
	return $CI -> lang -> line($langStr);
}

/**
 * Returns the number of images for the supplied user
 *
 */
function get_user_image_count($userid = 0)
{
	if ($userid == 0)
		return 0;
}

/**
 * Returns the number of classes for the supplied user
 *
 */
function get_user_class_count($userid = 0)
{
	if ($userid == 0)
		return 0;

	$user = new User();
	$user -> where("id", $userid) -> get();
	$user -> lecture -> get();
	return ($user -> lecture -> count() != 0 ? $user -> lecture -> count() : 0);
}

/**
 * Returns the number of events for the supplied user
 *
 */
function get_user_event_count($userid = 0)
{
	if ($userid == 0)
		return 0;

	$user = new User();
	$user -> where("id", $userid) -> get();
	$user -> event -> get();
	return ($user -> event -> count() != 0 ? $user -> event -> count() : 0);
}

/**
 * Returns the number of jobs for the supplied user
 *
 */
function get_user_job_count($userid = 0)
{
	if ($userid == 0)
		return 0;

	$user = new User();
	$user -> where("id", $userid) -> get();
	$user -> job -> get();
	return ($user -> job -> count() != 0 ? $user -> job -> count() : 0);
}

/**
 * Returns the number of groups for the supplied user
 *
 */
function get_user_group_count($userid = 0)
{
	if ($userid == 0)
		return 0;

	$user = new User();
	$user -> where("id", $userid) -> get();
	$user -> group -> get();
	return ($user -> group -> count() != 0 ? $user -> group -> count() : 0);
}

function isAdmin($id)
{
	$u = new User();
	$obj = $u -> getObject('user', $id, null, false, array('state' => 'Active'), '', '') -> dm_object;
	if ($obj -> user_admin)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/**
 * Returns the number of questions for the supplied user
 *
 */
function get_user_question_count($userid = 0)
{
	if ($userid == 0)
		return 0;

	$user = new User();
	$user -> where("id", $userid) -> get();
	$user -> group -> get();
	return ($user -> question -> count() != 0 ? $user -> question -> count() : 0);
}

/**
 * Get the user's fullname
 *
 */
function get_user_fullname($userid = 0)
{
	$user = new User();
	$user -> where("id", $userid) -> get();

	return $user -> firstname . " " . $user -> surname;
}

/**
 * The time since something was done by a user, or object
 *
 * @param UNIX time stamp
 * @return STRING
 */
function time_since($original)
{
	// array of time period chunks
	$chunks = array(
		array(
			60 * 60 * 24 * 365,
			'year'
		),
		array(
			60 * 60 * 24 * 30,
			'month'
		),
		array(
			60 * 60 * 24 * 7,
			'week'
		),
		array(
			60 * 60 * 24,
			'day'
		),
		array(
			60 * 60,
			'hour'
		),
		array(
			60,
			'minute'
		),
	);

	$today = time();
	/* Current unix time  */
	$since = $today - $original;

	if ($since > 604800)
	{
		$print = date("M jS", $original);

		if ($since > 31536000)
		{
			$print .= ", " . date("Y", $original);
		}

		return $print;

	}

	// $j saves performing the count function each time around the loop
	for ($i = 0, $j = count($chunks); $i < $j; $i++)
	{

		$seconds = $chunks[$i][0];
		$name = $chunks[$i][1];

		// finding the biggest chunk (if the chunk fits, break)
		if (($count = floor($since / $seconds)) != 0)
		{
			// DEBUG print "<!-- It's $name -->\n";
			break;
		}
	}

	$print = ($count == 1) ? '1 ' . $name : "$count {$name}s";

	return $print . " ago";

}

/**
 * Get the number of inbox messages for the user
 *
 */
function get_user_pms_count($uid, $state = "active", $created_by = null, $userid_to = null)
{
	$user = new User();

	// Get the users messages
	$user -> where("id", $uid);
	$user -> get();

	// Get the messages
	$messages = new Message();
	$messages -> where('state', $state);

	// handle user id to field
	if ($userid_to != 0)
		$messages -> where('userid_to', $userid_to);

	// handle created by field
	if ($created_by != 0)
		$messages -> where('created_by', $created_by);

	return $messages -> count();
}
