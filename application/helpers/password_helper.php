<?php


	/**
	 * Create a Random Password
	 */
	if(!function_exists('genRandomPassword')){
		
		function genRandomPassword($length = 8)
		{
			$salt = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			$len = strlen($salt);
			$makepass = '';
	
			for ($i = 0; $i < $length; $i++)
			{
				$makepass .= $salt[mt_rand(0, $len - 1)];
			}
	
			return $makepass;
		}
	}

	/**
	 * Get a secure salted password
	 */
	if(!function_exists('getCryptedPassword')){

		function getCryptedPassword($plaintext, $salt , $jusEncrypted =false)
		{
			// Get the salt to use.
			$encrypted	= md5($plaintext . $salt) ;
			return  ($jusEncrypted) ? $encrypted : $encrypted . ':' . $salt;
		}
	}