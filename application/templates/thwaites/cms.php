<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Pubs Net - Thwaites</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<!-- Le styles -->
	<link href="<?php echo base_url(); ?>assets/frontend/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/frontend/css/docs.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/frontend/js/google-code-prettify/prettify.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/frontend/css/layout.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/frontend/css/ui-lightness/jquery-ui-1.10.2.custom.css" rel="stylesheet">
	
	<?php echo $_styles; ?>
	
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->
	
	<!-- Le fav and touch icons -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/frontend/ico/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/frontend/ico/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/frontend/ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/frontend/ico/apple-touch-frontend/icon-57-precomposed.png">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/fronend/ico/favicon.png">	
	<link href='http://fonts.googleapis.com/css?family=Fauna+One' rel='stylesheet' type='text/css'>		  
	
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<?php echo $page_meta; ?>
	<?php //echo $_styles; ?>
		
	<!-- Le javascript
	================================================== --> 
	<!-- Placed at the end of the document so the pages load faster --> 
	<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script> 
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/jQuery.js"></script> 
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/holder/holder.js"></script> 
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/google-code-prettify/prettify.js"></script> 
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/bootstrap-transition.js"></script> 
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/bootstrap-carousel.js"></script> 
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/bootstrap-dropdown.js"></script> 
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/jquery-1.7.2.min.js"></script> 
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/jquery-ui-1.8.20.custom.min.js"></script> 
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/jquery.ba-hashchange.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/jquery.isotope.min.js"></script>
	
		<?php //echo $_scripts; ?>



</head>
	
<!-- HEADER
        ================================================== -->
<body class="thwaites">
    <div class="header-bck">
    
        
        <div class=" navbarbg">
            <div class="container">
                <div class="navbar">
                    <div class="navbar-inner">
                        <div class="nav-collapse collapse">
                            <ul class="nav">
                                <li class="home" style='margin-left:0px;'> <a href="<?php echo base_url(); ?>">Home</a> </li>
                                <li class="tap_room"> <a href="<?php echo base_url(); ?>taproom">Tap Room</a> </li>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Publicise It <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url(); ?>designmenu">My Menus</a></li>
                                        <li><a href="<?php echo base_url(); ?>designposter">My Posters</a></li>
                                        <!--
                                        <li><a href="<?php echo base_url(); ?>loyalty_cards">Loyalty Cards</a></li>
                                        <li><a href="<?php echo base_url(); ?>leaflets">My Leaflets</a></li>
                                        <li><a href="<?php echo base_url(); ?>banners">My Banners</a></li>
                                        -->
                                    </ul>
                                </li>
                                <li class="recources"> <a href="<?php echo base_url(); ?>resources">Resources</a> </li>
                                
                                <li class="menu-logo"> <img src='<?php echo base_url();?>assets/frontend/img/logo_main.png' /></li>
                                
                                <li class="news"> <a href="<?php echo base_url(); ?>news">News, Offers &amp; Incentives</a> </li>
                                
                                <li class="contact"> <a href="<?php echo base_url(); ?>contact">Contact</a> </li>
                                
                                <?php if($this->loggedIn) { ?>
                                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#"><?php echo $this->user->firstname.' '.$this->user->surname; ?></a></li>
                                        <li><a href="<?php echo base_url(); ?>">My Order History</a></li>
                                        <li><a href="<?php echo base_url(); ?>user/auth/logout">Logout</a></li>
                                        
                                    </ul>
                                </li>
                                <?php  } else { ?>
                                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        
                                        <li><a href="<?php echo base_url(); ?>login">Log in</a></li>
                                        
                                    </ul>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
		
		
		<div>
			<div class="container"> 
				<div class='pageTitleSlider'>
					<?php echo $slider; ?>
				</div>
			</div>
		</div>	
	
	</div>
	
	
	
	<?php if($this->loggedIn) { ?>
        <div>
            <div class="container mainContent"> 
                <?php echo $content; ?>
            </div>
        </div>  
    <?php } else { ?>
        
        <div>
            <div class="container mainContent"> 
                
                <div class="row">
                    <div class="span24">
                        <div class="form-chain"></div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="span24">
                        <div class="form-container">
                            <div class="row" style="padding-bottom:25px;">
                                <div class="span24">
                                    <div class="left-content">
                                        <div class="row" style="padding-bottom:25px;">
                                            <div class="span16">
                                                <h1>Welcome.</h1>
                                                <p>Please <a href="<?php echo base_url(); ?>login">log in</a> to see all the content of the site.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        
    <?php } ?>
		<!-- FOOTER
				================================================== -->
	       <!-- FOOTER
                ================================================== -->
        
        
        
<div class="base" style='clear:both'>
    <div class="container">
        <div class='row'>    
            <div class='span6'>
                <div class='footerTitle'>Links</div>
                <a href="#">Thwaites Website</a><br/>
                <a href="#">Run a Pub</a><br/>
                <a href="#">INNcognito</a><br/>
                <a href="#">Pubfinder</a><br/>
            </div>
            <div class='span6'>
                <div class='footerTitle'>Site Map</div>
                  <a href="<?php echo base_url(); ?>">Home</a><br/>
                  <a href="<?php echo base_url(); ?>taproom">Tap Room</a><br/>
                  <a href="<?php echo base_url(); ?>designmenu">My Menus</a><br/>
                  <a href="<?php echo base_url(); ?>designposter">My Posters</a><br/>
                  <a href="<?php echo base_url(); ?>resources">Resources</a><br/>
                  <a href="<?php echo base_url(); ?>news">News, Offers &amp; Incentives</a><br/>
                  <a href="<?php echo base_url(); ?>contact">Contact</a><br/>
                  <a href="<?php echo base_url(); ?>myaccount">My Account</a><br/>
            </div>
             <div class='span6'>
                  <div class='footerTitle'>Site Information</div>
                  <a href="<?php echo base_url(); ?>">Terms and Conditions</a><br/>
                  <a href="<?php echo base_url(); ?>">Privacy Policy</a><br/>
            </div>
            <div class='span6'>
                <div class='footerTitle'>Thwaites</div>
                Daniel Thwaites P.L.C - Co. No:51702<br/><br/>
                Daniel Thwaites PLC<br/>
Penny Street<br/>
Blackburn<br/>
Lancashire<br/>
BB1 6HL<br/>
<br/>
Telephone: 01254 686868<br/>
            </div>
      
        </div>   
        <div class='row' style='border-top: 1px #333 dashed; margin-top: 5px; padding-top: 5px;'> 
              <div class="span12">The Food Zone © Senses 2011-2012</div>
              <div class="span12">
                  <div class="base-right"><a href="#">Log-in</a></div>
              </div>
        
        </div>

    </div>
</div>
</body>
</html>