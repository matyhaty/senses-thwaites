<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html lang="en">
    <head>
        <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
        <title><?php echo SITE_NAME;?> &middot; <?php echo $page_header
            ?></title>
        <?php echo $_styles;
        ?>
        <?php echo  $_scripts;
        ?>
        
        
        <script type="text/javascript">
            $(document).ready(function() {
                $(".tooltiptrigger").tooltip({
                    effect : 'slide',
                    relative : true,
                    position : 'bottom center',
                    offset : [5, -80],
                });

                // Wysiwyg editor on all Textareas
                $('textarea').htmlarea({
                    toolbar : ["html", "|", "bold", "italic", "underline"],
                    css : '/assets/css/jHtmlAreaField.css'
                });
                
       			<?php echo $dom_ready; ?>
                                
            });
            // you can add tip: '.classname' to the above to ensure correct target.
            // for some reason doing this.id etc does not work :(
        </script>
    </head>
    <body id="home">
    	<div id='dialog'></div>
        <div id="page" class="outer">
            <!-- Start of Content Section -->
            <div class="container container_24">
                
                <?php echo $header;?>
                
                <?php echo $validation_errors;?>
                <?php echo $sheet;?>
                <div class='clear'></div>
            </div>
            <!-- End of Content Section -->
            <?php echo $footer;?>
        </div>
    </body>
</html>