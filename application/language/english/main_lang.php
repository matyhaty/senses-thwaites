<?php
/* Start events */
//start uris
$lang['_D_URI_FORM_CREATE']					= APPPATH . 'modules/event/forms/';
$lang['_D_URI_FORM_EDIT']					= APPPATH . 'modules/event/forms/';
$lang['_D_URI_EVENTS_INDEX']				= 'event/events/index';
$lang['_D_URI_EVENTS_DISPLAY']				= 'events';
$lang['_D_URI_EVENTS_CREATE']				= 'event/events/create/';
$lang['_D_URI_EVENTS_VIEW']					= 'event/events/view/id/';
$lang['_D_URI_EVENTS_EDIT']					= 'event/events/edit/id/';
$lang['_D_URI_EVENTS_DELETE']				= 'event/events/delete/id/';
$lang['_D_URI_EVENTS_IMAGES']				= 'event/events/images/id/';
$lang['_D_URI_EVENTS_WALL_IMAGES_DEL']		= 'file/files/delete/event/';
//end uris

// start file paths
$lang['_D_FILE_FORM_EVENT_WALL_IMAGES_REL']	= '/assets/images/wall/events/';
$lang['_D_FILE_FORM_EVENT_IMAGES']			= FRONT_END_PATH . '/assets/images/events/';
$lang['_D_FILE_FORM_EVENT_IMAGES_REL']		= '/assets/images/events/';
// end file paths

//start string
$lang['_D_EVENT']	= 'Event';
$lang['_D_EVENTS']	= 'Events';
//end strings

/*End event section*/


/* Start users */
    //start uris
    $lang['_D_URI_FORM_USERS_CREATE']   = APPPATH . 'modules/user/forms/';
    $lang['_D_USERS_URI_USERS_PROFILE']		= 'user/profile/index/';
    $lang['_D_USERS_PROFILE_PHOTO']			= FRONT_END_PATH . '/assets/images/profile/';
    //end uris
    
    //start string
    $lang['_D_PROFILES']	= "Profiles";
    $lang['_D_PROFILE']		= "Profile";
    //end strings
/*End users section*/

$lang['_D_URI_PROFILE_DISPLAY'] = "profile";
$lang['_D_URI_IMAGES_DISPLAY'] = "images";



//start string
$lang['_D_JOB']						= 'Job';
$lang['_D_JOBS']					= 'Jobs';
$lang['_D_JOBS_SELECT']				= 'Select job type';
$lang['_D_JOBS_FULL_TIME']			= 'Full time';
$lang['_D_JOBS_PART_TIME']			= 'Part time';
$lang['_D_JOBS_CONTRACT']			= 'Contract';
//end strings
/*End Jobs section*/

$lang['_D_URI_FILE_CREATE']			= 'file/create';
$lang['_D_URI_FILE']				= 'File';
$lang['_D_URI_FILE_FORM_CREATE']	= APPPATH . 'modules/file/forms/';
$lang['_D_FILE_USER_UPLOADS']		= APPPATH . '/assets/uploads/';
$lang['_D_FILE_USER_UPLOADS_REL']	= '/assets/uploads/';



/* Start Tasks */
$lang['_D_TASK']                    = 'Task';
$lang['_D_TASKS']                   = 'Tasks';
//start uris
$lang['_D_URI_FORM_TASKS_CREATE']   = APPPATH . 'modules/task/forms/';
$lang['_D_URI_TASKS_INDEX']         = 'tasks';
$lang['_D_URI_TASKS_DISPLAY']       = 'tasks/display/';




